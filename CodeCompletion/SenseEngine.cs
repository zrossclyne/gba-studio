﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
namespace SmartSense
{
    public class SenseEngine
    {
        public delegate List<Tuple<string, string>> ManagerDelegate(MatchCollection matches);

        SmartSense smartSense;

        Dictionary<string, Element> elements;
        Dictionary<string, string> codeBlocks;

        Dictionary<string, List<Tuple<string, string>>> nodes;

        public SenseEngine(SmartSense control)
        {
            smartSense = control;
            elements = new Dictionary<string, Element>();
            codeBlocks = new Dictionary<string, string>();
            nodes = new Dictionary<string, List<Tuple<string, string>>>();
        }

        public void AttachControl(SmartSense control)
        {
            smartSense = control;
        }

        public void AddElement(string identifier, Regex expression, ManagerDelegate managerDelegate)
        {
            if(elements.ContainsKey(identifier))
            {
                // If the identifier already exists, update the expression.
                elements[identifier] = new Element(identifier, expression, managerDelegate);
            }
            else
            {
                // If it does not exist, create a new entry.
                elements.Add(identifier, new Element(identifier, expression, managerDelegate));
            }
        }

        public void AddCodeBlock(string identifier, string contents)
        {
            if (codeBlocks.ContainsKey(identifier))
            {
                // If the identifier already exists, update the expression.
                codeBlocks[identifier] = contents;
            }
            else
            {
                // If it does not exist, create a new entry.
                codeBlocks.Add(identifier, contents);
            }
        }

        private void Clean()
        {
            nodes.Clear();
        }

        public void Build()
        {
            Clean();

            // Build the SmartSense Nodes ready for use by the control.
            foreach (KeyValuePair<string, string> codePair in codeBlocks)
            {
                string code = codePair.Value;

                List<Tuple<string, string>> members = new List<Tuple<string, string>>();

                foreach(KeyValuePair<string, Element> elementPair in elements)
                {
                    Element element = elementPair.Value;
                    MatchCollection matches = element.RegularExpression().Matches(code);

                    List<Tuple<string, string>> data = element.RunDelegate(matches);
                    if (data != null)
                    {
                        members.AddRange(data);
                    }
                }

                nodes.Add(codePair.Key, members);
            }
        }

        public void PopulateControl()
        {
            smartSense.ClearEntries();
            if(smartSense != null)
            {
                // Update the control. Needs to take parameters.
                foreach (KeyValuePair<string, List<Tuple<string, string>>> o in nodes)
                {
                    foreach (Tuple<string, string> node in o.Value)
                    {
                        // Will always display Item2, so this is where in the tuple the identifier should
                        // be located.
                        smartSense.AddEntry(node.Item2);
                    }
                }
                smartSense.Invalidate();
            }
            else
            {
                // Uninitialized control. Throw error.
            }
        }

        public void SetFilter(string filter)
        {
            if(smartSense != null)
            {
                smartSense.Filter = filter;
            }
        }
    }
}
