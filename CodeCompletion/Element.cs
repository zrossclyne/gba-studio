﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
namespace SmartSense
{
    class Element
    {

        private string identifier;
        private Regex regex;
        private SenseEngine.ManagerDelegate managerDelegate;

        public Element(string identifier, Regex regex, SenseEngine.ManagerDelegate managerDelegate)
        {
            this.identifier = identifier;
            this.regex = regex;
            this.managerDelegate = managerDelegate;
        }

        public string Identifier()
        {
            return identifier;
        }

        public Regex RegularExpression()
        {
            return regex;
        }

        public List<Tuple<string, string>> RunDelegate(MatchCollection matches)
        {
            if (managerDelegate != null)
            {
                return managerDelegate(matches);
            }
            else
            {
                // Delegate unassigned. Error here please.
                // Return an empty string list indicative of no members.
                return new List<Tuple<string, string>>();
            }
        }
    }
}
