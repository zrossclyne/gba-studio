﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using GBA_Studio.Forms;
using GBA_Studio.Tools;

namespace GBA_Studio
{
    static class Program
    {
        static MainForm mainForm;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            SplashScreen s = new SplashScreen();
            s.FormClosed += s_FormClosed;
            System.Windows.Forms.Application.EnableVisualStyles();
            //System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);
            //System.Windows.Forms.Application.Run(new MainForm(Args));
            s.Show();
            mainForm = new MainForm(args);
            mainForm.FormClosed += mainForm_FormClosed;
            s.Close();
            System.Windows.Forms.Application.Run();
        }

        static void mainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (mainForm.keyMonitor1 != null)
            {
                mainForm.keyMonitor1.Terminate();
                while (!mainForm.keyMonitor1.CanExitSafely)
                {

                }
            }
            Application.Exit();
        }

        static void s_FormClosed(object sender, FormClosedEventArgs e)
        {
            mainForm.Show();
        }
    }
}
