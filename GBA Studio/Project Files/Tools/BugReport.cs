﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBA_Studio.Tools
{
    public class BugReport
    {
        int id = -1;
        int classification = 0;
        string dateReported = "";
        string dateFixed = "";
        string description = "";
        bool archived = false;
        int developer = 0;

        public BugReport(int id, int classification, string dateReported, string dateFixed, string description, bool archived, int developer)
        {
            this.id = id;
            this.classification = classification;
            this.dateReported = dateReported;
            this.dateFixed = dateFixed;
            this.description = description;
            this.archived = archived;
            this.developer = developer;
        }

        public int ID
        {
            get
            {
                return id;
            }
        }

        public int Classification
        {
            get
            {
                return classification;
            }
            set
            {
                classification = value;
            }
        }

        public string DateReported
        {
            get
            {
                return dateReported;
            }
        }

        public string DateFixed
        {
            get
            {
                return dateFixed;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }
        }

        public int Developer
        {
            get
            {
                return developer;
            }
        }

        public bool Archived
        {
            get
            {
                return archived;
            }
            set
            {
                archived = value;
            }
        }
    }
}
