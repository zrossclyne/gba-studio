﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GBA_Studio.Tools
{
    enum ErrorType
    {
        FileNotFound,
        FileTypeInvalid,
        VersionMismatch,
        DirectoryFailed,
        FileFailed,
        FileFailedDelete,
        FileNameExists,
        SolutionExists,
        ProjectExists,
        EvaluationModeFailed,
        EvaluationModeExitWarning,
        DirectoryDeleteFailed,
        CorruptProject,
        CorruptSolution,
        InvalidSolutionPath,
        StateTransitionFailed,
        PasteFileFailed,
        SettingsWriteFailed,
        SettingsReadFailed,
        SettingsOutdated, 
        SettingsPredated,
        NoGBAFile,
        StackUnderflow,
        DatabaseConnectionFailed,
        DatabaseDisconnectionFailed,
        BugReportFailed,
        BugDeleteFailed,
        DatabaseUpdateFailed,
        DatabaseRetrievalFailed,
        LoginUserAuthentication,
        LoginUsernameUnknown,
        LoginPasswordIncorrect
    }
    
    class Errors
    {
        public static void Throw(ErrorType type)
        {
            string title = "";
            string caption = "";
            MessageBoxButtons buttons = MessageBoxButtons.OK;
            MessageBoxIcon icon = MessageBoxIcon.Error;

            switch(type)
            {
                case ErrorType.FileNotFound:
                    title = "File Not Found";
                    caption = "The file you requested could not be located.";
                    break;
                case ErrorType.FileTypeInvalid:
                    title = "Invalid File Type";
                    caption = "You have requested an unsupported file type.";
                    break;
                case ErrorType.FileNameExists:
                    title = "File Already Exists";
                    caption = "You already have a file in your project with this name, please choose another.";
                    break;
                case ErrorType.VersionMismatch:
                    title = "Project Version Mismatch";
                    caption = "The project was created with a newer version of GBA Studio.";
                    break;
                case ErrorType.DirectoryFailed:
                    title = "Directory Creation Failed";
                    caption = "GBA Studio failed to create a directory.";
                    break;
                case ErrorType.FileFailed:
                    title = "File Creation Failed";
                    caption = "GBA Studio failed to create/write the specified file.";
                    break;
                case ErrorType.FileFailedDelete:
                    title = "File Deletion Failed";
                    caption = "GBA Studio failed to delete the specified file.";
                    break;
                case ErrorType.ProjectExists:
                    title = "Project Already Exists";
                    caption = "A GBA Studio project under that name already exists.";
                    break;
                case ErrorType.SolutionExists:
                    title = "Solution Already Exists";
                    caption = "A GBA Studio solution under that name already exists.";
                    break;
                case ErrorType.EvaluationModeFailed:
                    title = "Evaluation Mode Failed";
                    caption = "GBA Studio failed to toggle Evaluation Mode due to a restore directory or file failing to create/delete.";
                    break;
                case ErrorType.EvaluationModeExitWarning:
                    title = "Evaluation Mode Exited Abnormally";
                    caption = "GBA Studio attempted to exit Evaluation Mode, however this was an abnormal exit. You may have lost the backup that was created.";
                    icon = MessageBoxIcon.Warning;
                    break;
                case ErrorType.DirectoryDeleteFailed:
                    title = "Failed to Delete Directory";
                    caption = "GBA Studio failed to delete a directory.";
                    break;
                case ErrorType.CorruptProject:
                    title = "Corrupted Project";
                    caption = "GBA Studio failed to open a project due to the project being corrupt.";
                    break;
                case ErrorType.CorruptSolution:
                    title = "Corrupted Solution";
                    caption = "GBA Studio failed to open a solution due to the solution being corrupt.";
                    break;
                case ErrorType.InvalidSolutionPath:
                    title = "Invalid Solution Path";
                    caption = "GBA Studio has detected that the path to the solution contains a whitespace character.\nPlease set the solution directory to a location that does not.";
                    break;
                case ErrorType.StateTransitionFailed:
                    title = "State Transition Failed";
                    caption = "GBA Studio attempted to transition to a state that was unreachable.";
                    break;
                case ErrorType.PasteFileFailed:
                    title = "Failed to Paste file.";
                    caption = "GBA Studio could not paste file because the file already exists.";
                    break;
                case ErrorType.SettingsWriteFailed:
                    title = "Failed to Write Settings File.";
                    caption = "GBA Studio failed to save the environment settings.";
                    break;
                case ErrorType.SettingsReadFailed:
                    title = "Failed to Read Settings File.";
                    caption = "GBA Studio failed to load the environment settings. It may be corrupt.";
                    break;
                case ErrorType.SettingsOutdated:
                    icon = MessageBoxIcon.Warning;
                    title = "Settings Outdated.";
                    caption = "The GBA Studio environmental settings may be outdated. This could result in the environment appearing differently.";
                    break;
                case ErrorType.SettingsPredated:
                    title = "Settings File Incompatible.";
                    caption = "The GBA Studio settings file is configured for a newer version of GBA Studio. No settings will be applied.";
                    break;
                case ErrorType.NoGBAFile:
                    title = "No GBA File Found.";
                    caption = "GBA Studio attempted to run a .gba but couldn't find one. Please ensure you perform an initial build prior to attempting to run.";
                    break;
                case ErrorType.StackUnderflow:
                    title = "Stack Underflow.";
                    caption = "GBA Studio's State Manager encountered a stack underflow.";
                    break;
                case ErrorType.DatabaseConnectionFailed:
                    title = "Bug Report Failed.";
                    caption = "GBA Studio failed to connect to the Bug Management database.";
                    break;
                case ErrorType.DatabaseDisconnectionFailed:
                    title = "Bug Report Failed.";
                    caption = "GBA Studio failed to disconnect from the Bug Management database.";
                    break;
                case ErrorType.BugReportFailed:
                    title = "Bug Report Failed.";
                    caption = "GBA Studio failed to submit a bug report. Please try again in a few minutes.";
                    break;
                case ErrorType.BugDeleteFailed:
                    title = "Bug Delete Failed.";
                    caption = "GBA Studio failed to delete a bug report. Please try again in a few minutes.";
                    break;
                case ErrorType.DatabaseUpdateFailed:
                    title = "Bug Report Failed.";
                    caption = "GBA Studio failed to update the Bug Management database. Any changes to the database may be lost.";
                    break;
                case ErrorType.DatabaseRetrievalFailed  :
                    title = "Bug Report Failed.";
                    caption = "GBA Studio failed to retrieve the bugs from the Bug Management database.";
                    break;
                case ErrorType.LoginUserAuthentication:
                    title = "User Authentication Failed.";
                    caption = "GBA Studio failed to authenticate you as it could not connect to the database.";
                    break;
                case ErrorType.LoginUsernameUnknown:
                    title = "User Authentication Failed.";
                    caption = "GBA Studio failed to authenticate you as the username and password combination does not exist.";
                    break;
                case ErrorType.LoginPasswordIncorrect:
                    title = "User Authentication Failed.";
                    caption = "GBA Studio failed to authenticate you as the username and password combination does not exist.";
                    break;
            }

            MessageBox.Show(caption, title, buttons, icon);
        }
    }
}

