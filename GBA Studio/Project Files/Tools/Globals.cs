﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Microsoft.Win32;
using System.Windows.Forms;

using GBA_Studio.IO;
using GBA_Studio.Tools;

namespace GBA_Studio.Tools
{
    class Globals
    {
        public enum RunState
        {
            Running,
            Stopped,
            Idle
        }

        public enum Authentication
        {
            Normal = 0,
            Privileged = 1,
            Developer = 2
        }

        public static string EncryptionKey = "enc#Key!PRESS_s0m£+5h|t";
        public static User CurrentUser;
        public static bool StudioUserStateChanged = false;

        public static Authentication AuthenticationMode = Authentication.Normal;

        public static Version CurrentVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;//new Version(1, 0, 0, 0);

        public static int MaxClipboardCount = 10;

        public static event EventHandler SolutionOpenChanged;

        public static Management.Solution CurrentSolution;

        public static string RootDirectory = Application.StartupPath + "\\GBA_Studio";
        public static string ProjectDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\GBA_Studio\\Projects";
        public static string ConfigDirectory = Application.StartupPath + "\\GBA_Studio\\Configuration";
        
        public static string DefaultCompileDirectory = "compile";

        private static bool isSolutionOpen = false;

        public static List<KeyValuePair<String, string>> StartPageRecentFiles = new List<KeyValuePair<String, string>>();
        public static string StartPageText = "";
        public static string StartPageNews = "";

        public static List<BugReport> BugReports = new List<BugReport>();
        public static BugDatabase BugDatabaseConnection;

        public static string[] IgnoreFileNames = {
            "Makefile"
        };
        public static string[] IgnoreFolders = {
            "Evaluation",
            "compile",
            "build"
        };
        public static string[] IgnoreExtensions = {
            "gbaproj",
            "gbasln",
            "gba",
            "elf",
            "o",
            "d"
        };

        // This is for loading files only not for checking which to allow in project (that is handled seperately)
        public static string[] AllowedExtensions = {
            ".cs",
            ".cpp",
            ".c",
            ".h",
            ".asm",
            ".s",
            ".d",
            ".a",
            ".ga",
            ".gc",
            ".gh",
            ".gs",
            ".vb"
        };

        public static bool IsSolutionOpen
        {
            get
            {
                return isSolutionOpen;
            }
            set
            {
                SolutionOpenChanged(value, EventArgs.Empty);
                isSolutionOpen = value;
            }
        }

        public static void UpdateRecentFiles(string name, string path)
        {
            KeyValuePair<string, string> pair = new KeyValuePair<string, string>(name, path);

            if(StartPageRecentFiles.Contains(pair))
            {
                StartPageRecentFiles.Remove(pair);
            }

            StartPageRecentFiles.Insert(0, pair);
            
            if(StartPageRecentFiles.Count > 5)
            {
                StartPageRecentFiles.RemoveRange(5, StartPageRecentFiles.Count - 5);
            }
            
            InputOutput.WriteRecentFiles();
        }

        public static string GetRecentFileHTML()
        {
            string text = "";
            string html = "";

            for (int i = 0; i < StartPageRecentFiles.Count; i++)
            {
                html += "<li><a href='#recent" + i.ToString() + "'>" + StartPageRecentFiles[i].Key + "</a></li>";
            }

            text += "<ul>";
            text += html;
            text += "</ul>";

            return html.Length > 0 ? text : "No recent files!";
        }

        public static UserDatabase.RESPONSE VerifyUser(string username, string password)
        {
            UserDatabase udb = new UserDatabase();
            udb.Connect();

            User user = new User();
            UserDatabase.RESPONSE response = udb.VerifyUser(username.ToLower(), password, ref user);

            switch (response)
            {
                case UserDatabase.RESPONSE.RESPONSE_USERNAME:
                    //Invalid username
                    Errors.Throw(ErrorType.LoginUsernameUnknown);
                    break;
                case UserDatabase.RESPONSE.RESPONSE_MISMATCH:
                    //Incorrect Password
                    Errors.Throw(ErrorType.LoginPasswordIncorrect);
                    break;
                case UserDatabase.RESPONSE.RESPONSE_AUTHENTICATED:
                    //Login successful
                    Globals.AuthenticationMode = (Authentication)user.Authentication;

                    //Update so this saves.
                    StudioSettings.Username = username;
                    StudioSettings.Password = CryptographyIO.Encrypt(password, EncryptionKey);
                    break;
            }
            Globals.CurrentUser = user;
            StudioUserStateChanged = true;
            InputOutput.WriteSettings();
            return response;
        }
        
        public static string AssemblyStub = "";
        public static string HeaderStub = "";
    }
}
