﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using MySql.Data;
using MySql.Data.MySqlClient;

using GBA_Studio.IO;

namespace GBA_Studio.Tools
{
    public class BugHandling
    {
        public static void UpdateBugListExternal()
        {
            Globals.BugDatabaseConnection.SubmitUpdate();
        }

        public static void InitializeBugDatabaseConnection()
        {
            Globals.BugDatabaseConnection = new BugDatabase();
            Globals.BugDatabaseConnection.Connect();
            Globals.BugDatabaseConnection.RetrieveBugList();
        }
    }
}

