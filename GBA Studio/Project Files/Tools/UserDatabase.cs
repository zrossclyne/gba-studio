﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;

namespace GBA_Studio.Tools
{
    public class User
    {
        bool _NULL = true;
        string username = "";
        string password = "";
        int authentication = 0;
        string forename = "";
        string surname = "";

        public User()
        {
            _NULL = true;
        }

        public User(string username, string password, int authentication, string forename, string surname)
        {
            this.username = username;
            this.password = password;
            this.authentication = authentication;
            this.forename = forename;
            this.surname = surname;
            _NULL = false;
        }

        public string Username
        {
            get
            {
                return username;
            }
        }
        public string Password
        {
            get
            {
                return password;
            }
        }
        public int Authentication
        {
            get
            {
                return authentication;
            }
        }
        public string Forename
        {
            get
            {
                return forename;
            }
        }
        public string Surname
        {
            get
            {
                return surname;
            }
        }
        public bool NULL
        {
            get
            {
                return _NULL;
            }
        }
    }
    class UserDatabase
    {
        private SqlConnection connection;
        private string server = "184.168.47.10";
        private string databaseName = "zachhs712_";
        private string table = "GBA_Studio_Users";
        private string username = "GBA";
        private string password = "gbaAdmin2016";


        public enum RESPONSE
        {
            RESPONSE_AUTHENTICATED,
            RESPONSE_USERNAME,
            RESPONSE_MISMATCH
        }

        #region Constructors
        public UserDatabase()
        {
        }
        #endregion

        #region Functions
        public void Connect()
        {
            string conString = "Data Source=" + server + ";Initial Catalog=" + databaseName + ";User ID=" + username + ";Password=" + password;
            connection = new SqlConnection(conString);
            try
            {
                connection.Open();
            }
            catch (Exception e)
            {
                Errors.Throw(ErrorType.DatabaseConnectionFailed);
            }
        }

        public void Disconnect()
        {
            try
            {
                connection.Close();
            }
            catch
            {
                Errors.Throw(ErrorType.DatabaseDisconnectionFailed);
            }
        }

        public RESPONSE VerifyUser(string username, string password, ref User userRef)
        {
            User profile = new User();
            try
            {
                SqlCommand cmd = new SqlCommand("SELECT * from " + table + " WHERE Username = '" + username +"'", connection);
                SqlDataReader reader = cmd.ExecuteReader();


                while (reader.Read())
                {
                    Int32 id = (Int32)reader.GetValue(0);
                    string user = (string)reader.GetValue(1);
                    string pass = (string)reader.GetValue(2);
                    Int32 authentication = (Int32)reader.GetValue(3);
                    string fore = (string)reader.GetValue(4);
                    string sur = (string)reader.GetValue(5);
                    profile = new User(user, pass, (int)authentication, fore, sur);
                    userRef = profile;
                }
                reader.Close();
                reader.Dispose();
            }
            catch (Exception e)
            {
                Errors.Throw(ErrorType.LoginUserAuthentication);
            }

            if (!profile.NULL)
            {
                if (profile.Password == password)
                {
                    //All ok
                    return RESPONSE.RESPONSE_AUTHENTICATED;
                }
                else
                {
                    //Incorrect password
                    return RESPONSE.RESPONSE_MISMATCH;
                }
            }
            else
            {
                //Unknown username
                return RESPONSE.RESPONSE_USERNAME;
            }
        }
        #endregion



        #region Properties
        public string Server
        {
            get
            {
                return this.server;
            }
            set
            {
                this.server = value;
            }
        }
        public string Database
        {
            get
            {
                return this.databaseName;
            }
            set
            {
                this.databaseName = value;
            }
        }
        public string Table
        {
            get
            {
                return this.table;
            }
            set
            {
                this.table = value;
            }
        }
        public string Username
        {
            get
            {
                return this.username;
            }
            set
            {
                this.username = value;
            }
        }
        public string Password
        {
            get
            {
                return this.password;
            }
            set
            {
                this.password = value;
            }
        }
        #endregion
    }
}