﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBA_Studio.Tools
{
    public class ClipboardManager
    {
        private List<string> clipboard;

        public ClipboardManager()
        {
            clipboard = new List<string>();
        }

        public int ClipboardCount
        {
            get
            {
                return clipboard.Count;
            }
        }

        public void AddToClipboard(string copied)
        {
            clipboard.Insert(0, copied);

            if (clipboard.Count > Globals.MaxClipboardCount)
            {
                clipboard.RemoveRange(Globals.MaxClipboardCount, clipboard.Count - Globals.MaxClipboardCount);
            }
        }

        public string GetFromClipboard(int index)
        {
            if (index < clipboard.Count)
            {
                return clipboard[index];
            }
            else
            {
                return "";
            }
        }
    }
}
