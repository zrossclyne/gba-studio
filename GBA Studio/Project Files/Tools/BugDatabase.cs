﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;

namespace GBA_Studio.Tools
{
    class BugDatabase
    {
        private SqlConnection connection;
        private string server = "184.168.47.10";
        private string databaseName = "zachhs712_";
        private string table = "GBA_Studio_Bugs";
        private string username = "GBA";
        private string password = "gbaAdmin2016";

        #region Constructors
        public BugDatabase()
        {
        }
        #endregion

        #region Functions
        public void Connect()
        {
            string conString = "Data Source=" + server + ";Initial Catalog=" + databaseName + ";User ID=" + username + ";Password=" + password;
            connection = new SqlConnection(conString);
            try
            {
                connection.Open();
            }
            catch (Exception e)
            {
                Errors.Throw(ErrorType.DatabaseConnectionFailed);
            }
        }

        public void Disconnect()
        {
            try
            {
                connection.Close();
            }
            catch
            {
                Errors.Throw(ErrorType.DatabaseDisconnectionFailed);
            }
        }

        public bool SubmitBugReport(string description)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("insert into " + table + " (archived, classification, date_reported, date_fixed, description, assigned) values (@classification, @archived, GetDate(), GetDate(), @description, 0)", connection); 
                cmd.Parameters.AddWithValue("@archived", 0);
                cmd.Parameters.AddWithValue("@classification", 0);
                //cmd.Parameters.AddWithValue("@date_reported", "GetDate()");
                //cmd.Parameters.AddWithValue("@date_fixed", "GetDate()");
                cmd.Parameters.AddWithValue("@description", description);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch(Exception e)
            {
                Errors.Throw(ErrorType.BugReportFailed);
                return false;
            }
        }

        public bool RemoveBugReport(int id)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("delete from " + table + " where id = " + id.ToString(), connection);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception e)
            {
                Errors.Throw(ErrorType.BugDeleteFailed);
                return false;
            }
        }

        public bool SubmitUpdate()
        {
            try
            {
                foreach (BugReport bug in Globals.BugReports)
                {
                    SqlCommand cmdCheckExists = new SqlCommand("SELECT count(*) FROM GBA_Studio_Bugs WHERE id=" + bug.ID, connection);
                    int count = (int)cmdCheckExists.ExecuteScalar();

                    if (count > 0)
                    {
                        SqlCommand cmdCheck = new SqlCommand("UPDATE GBA_Studio_Bugs SET archived = @archived, classification = @classification, date_reported = @date_reported, date_fixed = @date_fixed, description = @description, assigned = @assigned WHERE id = @id", connection);
                        cmdCheck.Parameters.AddWithValue("@id", bug.ID);
                        cmdCheck.Parameters.AddWithValue("@classification", bug.Classification);
                        cmdCheck.Parameters.AddWithValue("@archived", bug.Archived ? 1 : 0);
                        cmdCheck.Parameters.AddWithValue("@date_reported", DateTime.Parse(bug.DateReported));
                        cmdCheck.Parameters.AddWithValue("@date_fixed", DateTime.Parse(bug.DateFixed));
                        cmdCheck.Parameters.AddWithValue("@description", bug.Description);
                        cmdCheck.Parameters.AddWithValue("@assigned", bug.Developer);
                        cmdCheck.ExecuteNonQuery();
                    }
                    else
                    {
                        SubmitBugReport(bug.Description);
                    }
                }
                return true;
            }
            catch
            {
                Errors.Throw(ErrorType.DatabaseUpdateFailed);
                return false;
            }
        }

        public void RetrieveBugList()
        {
            List<BugReport> bugs = new List<BugReport>();
            try
            {
                SqlCommand cmd = new SqlCommand("Select * from " + table, connection);
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Int32 id = (Int32)reader.GetValue(0);
                    Boolean archived = ((bool)reader.GetValue(1));
                    Int32 classification = (Int32)reader.GetValue(2);
                    string dateReported = ((DateTime)reader.GetValue(3)).ToShortDateString();
                    string dateFixed = dateReported;
                    if (archived)
                    {
                        dateFixed = ((DateTime)reader.GetValue(4)).ToShortDateString();
                    }
                    string desc = (string)reader.GetValue(5);
                    int developer = (int)reader.GetValue(6);
                    bugs.Add(new BugReport(id, classification, dateReported, dateFixed, desc, archived, developer));
                }
                reader.Close();
                reader.Dispose();
                Globals.BugReports = bugs;
            }
            catch (Exception e)
            {
                Errors.Throw(ErrorType.DatabaseRetrievalFailed);
            }
        }
        #endregion



        #region Properties
        public string Server
        {
            get
            {
                return this.server;
            }
            set
            {
                this.server = value;
            }
        }
        public string Database
        {
            get
            {
                return this.databaseName;
            }
            set
            {
                this.databaseName = value;
            }
        }
        public string Table
        {
            get
            {
                return this.table;
            }
            set
            {
                this.table = value;
            }
        }
        public string Username
        {
            get
            {
                return this.username;
            }
            set
            {
                this.username = value;
            }
        }
        public string Password
        {
            get
            {
                return this.password;
            }
            set
            {
                this.password = value;
            }
        }
        #endregion
    }
}