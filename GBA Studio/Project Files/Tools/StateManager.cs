﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBA_Studio.Tools
{
    /// <summary>
    /// Represents the possible states that the studio can be in.
    /// </summary>
    public enum StudioState
    {
        Uninitialized,
        NoSolution,
        NoProject,
        ProjectSolutionOpen,
        Building,
        Running
    }

    public class StateManager
    {
        #region variables
        private int previousState;
        private int currentState = 0;
        private List<State> states = new List<State>();
        private Stack<StudioState> stateStack = new Stack<StudioState>();
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a StateManager instance.
        /// </summary>
        /// <param name="initialState">The initial state of the manager.</param>
        public StateManager()
        {
            InitializeManager();
        }
        #endregion

        #region Functions
        /// <summary>
        /// Initializes the manager to contain all possible states.
        /// </summary>
        private void InitializeManager()
        {
            AddState(StudioState.Uninitialized);
            AddState(StudioState.Building);
            AddState(StudioState.NoProject);
            AddState(StudioState.NoSolution);
            AddState(StudioState.ProjectSolutionOpen);
            AddState(StudioState.Running);
        }

        /// <summary>
        /// Creates a State object representative of the StudioState provided.
        /// </summary>
        /// <param name="state">The StudioState that should be used to create the State object.</param>
        private void AddState(StudioState state)
        {
            bool foundState = false;
            for(int i = 0; i < states.Count; i++)
            {
                foundState |= (states[i].StateState == state);
            }
            if(!foundState)
            {
                states.Add(new State(state));
            }
        }

        /// <summary>
        /// Gets the index of the State object that represents the specified state.
        /// </summary>
        /// <param name="state">The StudioState with which to find an index of.</param>
        /// <returns>The index of the State object.</returns>
        private int GetStateIndexFromStudioState(StudioState state)
        {
            for(int i = 0; i < states.Count; i++)
            {
                if(states[i].StateState == state)
                {
                    return i;
                }
            }
            return -1;
        }

        /// <summary>
        /// Attempts to transition from the current state to the specified state.
        /// </summary>
        /// <param name="state"The state to attempt to transition to.></param>
        /// <returns>Returns a boolean representing the success of the transition.</returns>
        public bool Transition(StudioState state)
        {
            int stateIndex = GetStateIndexFromStudioState(state);
            if(!(stateIndex == currentState))
            {
                if (states[currentState].CanTransitionTo(state))
                {
                    previousState = currentState;
                    currentState = stateIndex;
                    stateStack.Push(state);

                    RaiseTransitionEvent(true, state);
                    return true;
                }
                else
                {
                    Errors.Throw(ErrorType.StateTransitionFailed);
                    RaiseTransitionEvent(false, state);
                    return false;
                }
            }
            RaiseTransitionEvent(true, state);
            return true;
        }

        /// <summary>
        /// Raises a Transitioned/TransitionFailed event.
        /// </summary>
        /// <param name="succeeded">Whether the manager succeeded in transitioning.</param>
        /// <param name="state">The state that the manager attempted to transition to.</param>
        private void RaiseTransitionEvent(bool succeeded, StudioState state)
        {
            StateManagerEventArgs stateArgs = new StateManagerEventArgs(states[currentState].StateState, state);
            if (succeeded)
            {
                if (StateTransitioned != null)
                {
                    StateTransitioned(this, stateArgs);
                }
            }
            else
            {
                if (StateTransitionFailed != null)
                {
                    StateTransitionFailed(this, stateArgs);
                }
            }
        }

        /// <summary>
        /// Adds a transition to a specific State object.
        /// </summary>
        /// <param name="fromState">The state from which to transition from.</param>
        /// <param name="toState">The state to transition to.</param>
        public void AddTransitionToState(StudioState fromState, StudioState toState)
        {
            states[GetStateIndexFromStudioState(fromState)].AddTransition(toState);
        }

        /// <summary>
        /// Rolls bthe StateManager back to it's previous state.
        /// </summary>
        public void Rollback()
        {
            if (stateStack.Count > 0)
            {
                stateStack.Pop();
                if (stateStack.Count > 0)
                {
                    currentState = GetStateIndexFromStudioState(stateStack.Peek());
                }
                else
                {
                    currentState = 0;
                    Errors.Throw(ErrorType.StackUnderflow);
                }
                StateManagerEventArgs stateArgs = new StateManagerEventArgs(states[currentState].StateState, states[previousState].StateState);

                if (StateRolledBack != null)
                {
                    StateRolledBack(this, stateArgs);
                }
                if (StateTransitioned != null)
                {
                    StateTransitioned(this, stateArgs);
                }
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// The event handler used to raise StateManager events
        /// </summary>
        /// <param name="sender">The object sending the event.</param>
        /// <param name="e">The arguments to pass through to the event.</param>
        public delegate void StateManagerEventHandler(object sender, StateManagerEventArgs e);

        /// <summary>
        /// An event called upon a successful state transition.
        /// </summary>
        public event StateManagerEventHandler StateTransitioned;

        /// <summary>
        /// An event called upon a failed state transition.
        /// </summary>
        /// 
        public event StateManagerEventHandler StateTransitionFailed;

        /// <summary>
        /// An event called upon a state roll-back transition.
        /// </summary>
        public event StateManagerEventHandler StateRolledBack;

        /// <summary>
        /// The event arguments used to raise StateManager events.
        /// </summary>
        public class StateManagerEventArgs : EventArgs
        {
            StudioState fromState;
            StudioState toState;

            /// <summary>
            /// Creates an instance of StateManagerEventArgs
            /// </summary>
            /// <param name="fromState">The state that was transition from.</param>
            /// <param name="toState">The state that was transition to.</param>
            public StateManagerEventArgs(StudioState fromState, StudioState toState)
            {
                this.fromState = fromState;
                this.toState = toState;
            }

            /// <summary>
            /// Gets the state that was transition from.
            /// </summary>
            public StudioState FromState
            {
                get
                {
                    return fromState;
                }
            }

            /// <summary>
            /// Gets the state that was transition to.
            /// </summary>
            public StudioState ToState
            {
                get
                {
                    return toState;
                }
            }
        }
        #endregion

        public StudioState CurrentState
        {
            get
            {
                if (stateStack.Count > 0)
                {
                    return stateStack.Peek();
                }
                else
                {
                    return StudioState.Uninitialized;
                }
            }
        }

        private class State
        {
            private StudioState stateState;
            private List<StudioState> transitions = new List<StudioState>();

            /// <summary>
            /// Constructs a State object.
            /// </summary>
            /// <param name="state">The StudioState that this State object represents.</param>
            public State(StudioState state)
            {
                stateState = state;
            }

            #region Functions
            /// <summary>
            /// Allows the state to transition to another.
            /// </summary>
            /// <param name="state">The state that should be allowed to transition to.</param>
            public void AddTransition(StudioState state)
            {
                if (!transitions.Contains(state))
                {
                    transitions.Add(state);
                }
            }

            /// <summary>
            /// Prevents the state from transitioning to another.
            /// </summary>
            /// <param name="state">The state that should not be allowed to transition to.</param>
            public void RemoveTransition(StudioState state)
            {
                if (transitions.Contains(state))
                {
                    transitions.Remove(state);
                }
            }

            /// <summary>
            /// Gets a boolean indicating if a state can be transition to.
            /// </summary>
            /// <param name="state">The state to test whether transitioning to is possible.</param>
            /// <returns>A boolean representing the transition possibility.</returns>
            public bool CanTransitionTo(StudioState state)
            {
                if(transitions.Contains(state))
                {
                    return true;
                }
                return false;
            }
            #endregion

            #region Properties
            /// <summary>
            /// Gets the StudioState element of the State object.
            /// </summary>
            public StudioState StateState
            {
                get
                {
                    return stateState;
                }
            }
            #endregion

        }
    }
}
