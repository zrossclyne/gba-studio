﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GBA_Studio.Tools;

namespace GBA_Studio.Management
{
    public enum FileType
    {
        Header,
        C,
        Assembly
    }

    class Solution : GBAType
    {
        public class ProjectEventArgs : EventArgs
        {
            private Project p;

            public Project Project
            {
                get
                {
                    return p;
                }
            }

            public ProjectEventArgs(Project p)
            {
                this.p = p;
            }
        }

        public delegate void ProjectChangedEventHandler(object sender, ProjectEventArgs e);
        public event ProjectChangedEventHandler DefaultProjectChanged;

        public Solution(string path, Project p)
        {
            projects = new List<Project>();

            this.path = path;
            projects.Add(p);
        }
        public Solution(string path)
        {
            projects = new List<Project>();

            this.path = path;
        }

        public Project GetProjectFromName(string name)
        {
            foreach(Project p in projects)
            {
                if (System.IO.Path.GetFileNameWithoutExtension(p.Path) == name)
                {
                    return p;
                }
            }

            return null;
        }

        public void OpenFile(string localPath, string projectName)
        {
            foreach(Project p in projects)
            {
                if (System.IO.Path.GetFileNameWithoutExtension(p.Path) == projectName)
                {
                    p.OpenFiles.Add(localPath);
                }
            }
        }

        public void SetDefaultProject(Project proj)
        {
            foreach (Project p in projects)
            {
                if (p.Path == proj.Path)
                {
                    ProjectEventArgs eventArgs = new ProjectEventArgs(p);

                    if (DefaultProjectChanged != null)
                    {
                        DefaultProjectChanged(this, eventArgs);
                    }

                    p.Default = true;
                }
                else
                {
                    p.Default = false;
                }
            }
        }

        public void DeleteProject(Project p, bool shouldDelete)
        {
            try
            {
                if (shouldDelete)
                {
                    string path = System.IO.Path.GetDirectoryName(this.path + @"\" + p.Path);

                    System.IO.Directory.Delete(path);
                }

                projects.Remove(p);
            }
            catch
            {
                Errors.Throw(ErrorType.DirectoryDeleteFailed);
            }
        }

        public Project GetDefaultProject()
        {
            for (int i = 0; i < projects.Count; i++)
            {
                if (projects[i].Default)
                {
                    return projects[i];
                }
            }
            return null;
        }

        private Version version = Globals.CurrentVersion;
        private List<Project> projects;
        private string path;
        private bool evaluationMode;

        public bool EvaluationMode
        {
            get
            {
                return evaluationMode;
            }
            set
            {
                evaluationMode = value;
            }
        }

        public Version Version
        {
            get
            {
                return version;
            }
        }

        public string Path
        {
            get
            {
                return path;
            }
        }

        public List<Project> Projects
        {
            get
            {
                return projects;
            }
        }

        public void NewProject(string projectName)
        {
        }
    }

    public class Project : GBAType
    {
        private string path;
        private bool isDefault;
        private List<string> openFiles;
        private List<string> referencedFiles;
        private List<KeyValuePair<string, KeyValuePair<string, string>>> filters;
        private List<KeyValuePair<string, string>> filesFilters;

        private string sourceFolder;
        private string compileFolder;


        public string Path
        {
            get
            {
                return path;
            }
        }

        public string CompileFolder
        {
            get
            {
                return compileFolder;
            }
            set
            {
                compileFolder = value;
            }
        }

        public string SourceFolder
        {
            get
            {
                return sourceFolder;
            }
            set
            {
                sourceFolder = value;
            }
        }

        public List<string> OpenFiles
        {
            get
            {
                return openFiles;
            }
        }

        public List<string> ReferencedFiles
        {
            get
            {
                return referencedFiles;
            }
        }

        public List<KeyValuePair<string, KeyValuePair<string, string>>> Filters
        {
            get
            {
                return filters;
            }
        }

        public List<KeyValuePair<string, string>> FileFilters
        {
            get
            {
                return filesFilters;
            }
        }

        public bool Default
        {
            get
            {
                return isDefault;
            }
            set
            {
                isDefault = value;
            }
        }

        public Project(string path)
        {
            openFiles = new List<string>();
            referencedFiles = new List<string>();
            filters = new List<KeyValuePair<string, KeyValuePair<string, string>>>();
            filesFilters = new List<KeyValuePair<string, string>>();

            sourceFolder = "source";
            compileFolder = Globals.DefaultCompileDirectory;
            this.path = path;
        }

        public bool ContainsFile(string fileName)
        {
            foreach(string file in referencedFiles)
            {
                if (System.IO.Path.GetFileName(file) == fileName)
                {
                    return true;
                }
            }

            return false;
        }

        public void ApplyToFilter(int fileIndex, string guid)
        {
            filesFilters.Add(new KeyValuePair<string, string>(guid, referencedFiles[fileIndex]));
        }

        public void NewFile(string fileName, string folderPath = "")
        {
            if (folderPath == "")
            {
                folderPath = sourceFolder;
            }

            folderPath = folderPath.Replace(System.IO.Path.GetDirectoryName(IO.InputOutput.GetDrivePath(Globals.CurrentSolution.Path)) + @"\", "").Replace(System.IO.Path.GetDirectoryName(this.Path) + @"\", "");

            referencedFiles.Add(folderPath + @"\" + fileName);
        }

        public void AddFile(string localPath)
        {
            referencedFiles.Add(localPath);
        }

        public void RemoveFile(string localPath)
        {
            referencedFiles.Remove(localPath);
            openFiles.Remove(localPath);
        }

        public void DeleteFile(string filePath, bool shouldDelete)
        {
            try
            {
                if (shouldDelete)
                {
                    System.IO.File.Delete(filePath);
                }

                string solPath = Globals.CurrentSolution.Path;
                string projPath = this.Path;

                string relativeFilePath = filePath.Replace(System.IO.Path.GetDirectoryName(solPath) + @"\", "").Replace(System.IO.Path.GetDirectoryName(projPath) + @"\", "");

                this.referencedFiles.Remove(relativeFilePath);
                this.openFiles.Remove(relativeFilePath);
            }
            catch
            {
                Errors.Throw(ErrorType.FileFailed);
            }
        }
    }

    public class GBAType
    {
        public string Location;

        public GBAType()
        {

        }

        public GBAType(string location)
        {
            Location = location;
        }
    }
}
