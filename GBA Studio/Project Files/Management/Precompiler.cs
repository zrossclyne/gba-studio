﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

using GBA_Studio.IO;
using GBA_Studio.Tools;

namespace GBA_Studio.Management
{
    class Precompiler
    {
        public static bool Compile()
        {
            foreach (Project p in Globals.CurrentSolution.Projects)
            {
                string path = Path.GetDirectoryName(InputOutput.GetDrivePath(Globals.CurrentSolution.Path)) + @"\" + Path.GetDirectoryName(p.Path);

                if (Directory.Exists(path + @"\" + Globals.DefaultCompileDirectory))
                {
                    Directory.Delete(path + @"\" + Globals.DefaultCompileDirectory, true);
                }

                Directory.CreateDirectory(path + @"\" + Globals.DefaultCompileDirectory);
                
                foreach (string file in p.ReferencedFiles)
                {
                    string filePath = path + @"\" + file;
                    string extension = Path.GetExtension(filePath).Replace(".", "");
                    if (File.Exists(filePath))
                    {
                        string text = File.ReadAllText(filePath);
                        if (Globals.AuthenticationMode >= Globals.Authentication.Privileged)
                        {
                            if (!IsAssembly(file))
                            {
                                text = ChangeImports(text);
                                text = CorrectEnums(text);
                                text = CorrectStructures(text);
                            }
                            else
                            {
                                text = ChangeRegistryNames(text);
                            }
                        }

                        string newExtension = extension;

                        if (newExtension.StartsWith("g"))
                        {
                            newExtension = newExtension.Substring(1);
                        }

                        string destination = filePath.Replace(Path.GetDirectoryName(file), p.CompileFolder);
                        destination = Path.ChangeExtension(destination, newExtension);

                        if (!InputOutput.WriteToFile(destination, text))
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        private static bool IsAssembly(string fileName)
        {
            string extension = Path.GetExtension(fileName);

            if (extension == ".gs")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static string CorrectStructures(string curText)
        {
            string rtn = curText;

            Regex r = new Regex(@"(struct ([\w]+)\s*{)([^§]+?)(?=}\s*;)}\s*;");

            rtn = r.Replace(curText, "typedef $1$3} $2;");

            rtn = rtn.Replace("typedef typedef", "typedef");

            return rtn;
        }

        private static string CorrectEnums(string curText)
        {
            string rtn = curText;

            Regex r = new Regex(@"(enum ([\w_0-9]+)[^{]+{)([^}]+)};");

            rtn = r.Replace(curText, "typedef $1$3} $2;");

            rtn = rtn.Replace("typedef typedef", "typedef");

            return rtn;
        }

        private static string ChangeImports(string curText)
        {
            string rtn = "";

            Regex r = new Regex("(#include [<\"][^\\.]+)\\.(g)([\\w]+[>\"])");

            rtn = r.Replace(curText, "$1.$3");

            return rtn;
        }

        private static string FixForLoops(string curText)
        {
            string rtn = curText;



            return rtn;
        }

        private static string ChangeRegistryNames(string curText)
        {
            string rtn = curText;

            Regex r = new Regex(@"#reg (r\d) ([^\s]+)");
            MatchCollection mc = r.Matches(curText);

            foreach(Match m in mc)
            {
                string regName = m.Groups[1].Value;
                string regLocalised = m.Groups[2].Value;

                string fullMatch = m.Groups[0].Value;
                rtn = rtn.Replace(fullMatch, "");

                Regex repl = new Regex(@"([^A-Za-z0-9]|^)" + regLocalised + @"([^A-Za-z0-9]|$)");
                rtn = repl.Replace(rtn, @"$1" + regName + @"$2");
            }

            return rtn;
        }
    }
}
