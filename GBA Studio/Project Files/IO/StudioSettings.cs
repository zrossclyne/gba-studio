﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBA_Studio.IO
{
    class StudioSettings
    {
        public static bool ConsoleVisible = true;
        public static bool MemberExplorerVisible = true;
        public static bool SolutionExplorerVisible = true;
        public static bool StartPagevisible = true;
        public static bool ErrorListVisible = true;

        public static bool FileToolbarVisible = true;
        public static bool DebugToolbarVisible = true;
        public static bool DefaultToolbarVisible = true;

        public static string Username = "";
        public static string Password = "";

        public static string PreviousPath = "";
    }
}
