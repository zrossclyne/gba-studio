﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;
using System.Net;

using GBA_Studio.Tools;

namespace GBA_Studio.IO
{
    class InputOutput
    {
        public static string ReadFromResources(string resource)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();

            using (Stream stream = assembly.GetManifestResourceStream(resource))
            using (StreamReader reader = new StreamReader(stream))
            {
                return (reader.ReadToEnd());
            }
        }

        public static string GetDrivePath(string path)
        {
            Regex r = new Regex(@"\\\\S[Uu]2\\[Uu][Tt]\\UP\d+");
            path = r.Replace(path, "N:");

            return path;
        }

        public static string ReadFromFile(string path)
        {
            string data = "";
            if (File.Exists(path))
            {
                try
                {
                    FileStream stream = new FileStream(path, FileMode.Open);
                    StreamReader reader = new StreamReader(stream);
                    data = reader.ReadToEnd();
                    reader.Close();
                    reader.Dispose();
                }
                catch
                {
                    Errors.Throw(ErrorType.FileFailed);
                }
            }
            else
            {
                Errors.Throw(ErrorType.FileNotFound);
            }
            return data;
        }

        public static bool WriteToFile(string path, string data)
        {
            if (Path.GetExtension(path) == ".gbass")
            {
                Forms.Sprite_Designer s = new Forms.Sprite_Designer();
                s.SaveSprite(path);
            }
            else
            {
                try
                {
                    //FileStream stream = new FileStream(path, FileMode.OpenOrCreate);
                    StreamWriter writer = new StreamWriter(path, false);
                    writer.Write(data);
                    writer.Flush();
                    writer.Close();
                    writer.Dispose();
                }
                catch (Exception e)
                {
                    Errors.Throw(ErrorType.FileFailed);
                    return false;
                }
            }
            
            return true;
        }

        public static void WriteXmlToFile(string path, XmlDocument xml)
        {
            StringWriter sr = new StringWriter();

            XmlTextWriter write = new XmlTextWriter(sr);

            xml.WriteContentTo(write);

            string s = sr.ToString();

            WriteToFile(path, s);
        }

        public static void ReadRecentFiles()
        {
            try
            {
                string path = Globals.ConfigDirectory + @"\recent.xml";
                XmlDocument document = new XmlDocument();
                document.Load(path);

                XmlNodeList solutions = document.SelectNodes("/Recents/Solution");

                foreach (XmlNode node in solutions)
                {
                    string solutionName = node.SelectSingleNode("Name").InnerText;
                    string solutionPath = node.SelectSingleNode("Path").InnerText;

                    Globals.StartPageRecentFiles.Add(new KeyValuePair<string, string>(solutionName, solutionPath));
                }
            }
            catch
            {

            }
        }

        public static void WriteRecentFiles()
        {
            XmlDocument document = new XmlDocument();
            document.LoadXml("<Recents></Recents>");
            XmlNode recents = document.SelectSingleNode("/Recents");


            foreach (var v in Globals.StartPageRecentFiles)
            {
                KeyValuePair<string, string> pair = v;

                XmlNode n = document.CreateNode(XmlNodeType.Element, "Solution", null);
                XmlNode name = document.CreateNode(XmlNodeType.Element, "Name", null);
                XmlNode path = document.CreateNode(XmlNodeType.Element, "Path", null);

                name.InnerText = pair.Key;
                path.InnerText = pair.Value;

                n.AppendChild(name);
                n.AppendChild(path);

                recents.AppendChild(n);
            }


            string writePath = Globals.ConfigDirectory + @"\recent.xml";
            InputOutput.WriteToFile(writePath, document.InnerXml);
        }

        /// <summary>
        /// Loads the previous news from the configuration folder.
        /// </summary>
        /// <returns></returns>
        public static void ReadNewsCache()
        {
            string newsString = "GBA Studio news is currently unavailable. Please ensure you are connected to the internet.";
            string newsPath = Globals.ConfigDirectory + "/news.cfg";
            if (File.Exists(newsPath))
            {
                string str = InputOutput.ReadFromFile(newsPath);
                if (str.Length > 0)
                {
                    newsString = str;
                }
            }
            else
            {
                DownloadNews();
            }
            Globals.StartPageNews = newsString;
        }

        /// <summary>
        /// Saves the provided string to a file in the configuration folder.
        /// </summary>
        /// <param name="newsString">The string containing the news.</param>
        private static void WriteNewsCache(string newsString)
        {
            string newsPath = Globals.ConfigDirectory + "/news.cfg";
            InputOutput.WriteToFile(newsPath, newsString);
        }

        public static void DownloadNews()
        {
            if (Globals.AuthenticationMode == Globals.Authentication.Developer)
            {
                DownloadNewsAdmin();
            }
            else
            {
                DownloadNewsNormal();
            }
        }

        /// <summary>
        /// Downloads the latest news packet from the external server.
        /// </summary>
        /// <returns>Returns a string containing the news.</returns>
        private static void DownloadNewsNormal()
        {
            string newsString = "GBA Studio news is currently unavailable. Please ensure you are connected to the internet.";
            try
            {
                HttpWebRequest newsGet = (HttpWebRequest)WebRequest.Create("http://www.zachhoward-smith.com/projectdata/gbastudio/news.txt");
                newsGet.Method = "GET";
                WebResponse newsResponse = newsGet.GetResponse();
                StreamReader newsStream = new StreamReader(newsResponse.GetResponseStream(), System.Text.Encoding.UTF8);
                newsString = newsStream.ReadToEnd();
                newsStream.Close();
                newsResponse.Close();
                WriteNewsCache(newsString);
            }
            catch
            {
                //Error averted.
            }
            Globals.StartPageNews = newsString;
        }

        private static void DownloadNewsAdmin()
        {
            string newsString = "GBA Studio bug report is currently unavailable. Please ensure you are connected to the internet.";
            try
            {
                newsString = PopulateAdminStartPage();
            }
            catch
            {

            }
            Globals.StartPageNews = newsString;
        }

        private static string PopulateAdminStartPage()
        {
            if (Globals.BugReports.Count > 0)
            {
                string tempString = "";
                int bugCount = 0;
                foreach (BugReport bug in Globals.BugReports)
                {
                    if (!bug.Archived)
                    {
                        tempString += "<li>" + bug.Description + "</li>";
                        bugCount++;
                    }
                }
                if(bugCount == 0)
                {
                    return "There are currently no known bugs. GO TEAM!";
                }
                return "<h2>There " + (bugCount == 1 ? "is" : "are") + " currently " + bugCount + " bug" + (bugCount == 1 ? "" : "s") + " listed.</h2><ul>" + tempString + "</ul>";
            }
            return "There are currently no known bugs. GO TEAM!";
        }

        public static void ReadStartPage()
        {
            if (Globals.AuthenticationMode <= Globals.Authentication.Privileged)
            {
                Globals.StartPageText = InputOutput.ReadFromResources("GBA_Studio.Resources.Text_Templates.Start Page.txt");
            }
            else
            {
                Globals.StartPageText = InputOutput.ReadFromResources("GBA_Studio.Resources.Text_Templates.Start Page Admin.txt");
            }
        }

        public static void WriteSettings()
        {
            try
            {
                string path = Globals.ConfigDirectory + @"\settings.cfg";
                FileStream fs = new FileStream(path, FileMode.OpenOrCreate);
                BinaryWriter bw = new BinaryWriter(fs);

                bw.Write(Globals.CurrentVersion.ToString());
                bw.Write(StudioSettings.ConsoleVisible);
                bw.Write(StudioSettings.MemberExplorerVisible);
                bw.Write(StudioSettings.SolutionExplorerVisible);
                bw.Write(StudioSettings.ErrorListVisible);
                bw.Write(StudioSettings.StartPagevisible);
                bw.Write(StudioSettings.FileToolbarVisible);
                bw.Write(StudioSettings.DebugToolbarVisible);
                bw.Write(StudioSettings.DefaultToolbarVisible);
                bw.Write(StudioSettings.Username);
                bw.Write(StudioSettings.Password);
                bw.Write(StudioSettings.PreviousPath);

                bw.Flush();
                bw.Close();
                bw.Dispose();
            }
            catch
            {
                Errors.Throw(ErrorType.SettingsWriteFailed);
            }
        }

        public static void ReadSettings()
        {
            string path = Globals.ConfigDirectory + @"\settings.cfg";

            if (File.Exists(path))
            {
                try
                {
                    FileStream fs = new FileStream(path, FileMode.Open);
                    BinaryReader br = new BinaryReader(fs);

                    Version settingsVersion = Version.Parse(br.ReadString());

                    int comparison = settingsVersion.CompareTo(Globals.CurrentVersion);

                    switch (comparison)
                    {
                        case -1: //newer settings than program
                            Errors.Throw(ErrorType.SettingsPredated);
                            break;
                        case 1: //older settings than program;
                            Errors.Throw(ErrorType.SettingsOutdated);
                            goto case 0;
                        case 0:
                            StudioSettings.ConsoleVisible = br.ReadBoolean();
                            StudioSettings.MemberExplorerVisible = br.ReadBoolean();
                            StudioSettings.SolutionExplorerVisible = br.ReadBoolean();
                            StudioSettings.ErrorListVisible = br.ReadBoolean();
                            StudioSettings.StartPagevisible = br.ReadBoolean();
                            StudioSettings.FileToolbarVisible = br.ReadBoolean();
                            StudioSettings.DebugToolbarVisible = br.ReadBoolean();
                            StudioSettings.DefaultToolbarVisible = br.ReadBoolean();
                            StudioSettings.Username = br.ReadString();
                            StudioSettings.Password = br.ReadString();
                            StudioSettings.PreviousPath = br.ReadString();

                            br.Close();
                            br.Dispose();
                            break;
                    }
                }
                catch
                {
                    Errors.Throw(ErrorType.SettingsReadFailed);
                }
            }
        }
    }
}
