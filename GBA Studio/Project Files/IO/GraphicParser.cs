﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using GBA_Studio.Properties;

namespace GBA_Studio.IO
{
    public enum ParseDirection
    {
        Horizontal,
        Vertical
    }
    struct ParseDescription
    {
        public Bitmap sourceImg;
        public int tileStridePixelsH;
        public int tileStridePixelsV;
        public ParseDirection direction;
        public Color transparentColor;

    }
    class GraphicParser
    {
        public static void Parse(ParseDescription desc, out Bitmap[] images)
        {
            Bitmap source = desc.sourceImg;
            int tileCount = desc.direction == ParseDirection.Horizontal ? (source.Width / desc.tileStridePixelsH) : (source.Height / desc.tileStridePixelsV);
            Bitmap[] elementImages = new Bitmap[tileCount];

            for (int i = 0; i < tileCount; i++)
            {
                elementImages[i] = new Bitmap(desc.tileStridePixelsH, desc.tileStridePixelsV);
                Graphics g = Graphics.FromImage(elementImages[i]);
                g.DrawImage(source,
                    new Rectangle(0, 0, desc.tileStridePixelsH, desc.tileStridePixelsV),
                    desc.direction == ParseDirection.Horizontal ?
                    new Rectangle(i * desc.tileStridePixelsH, 0, desc.tileStridePixelsH, desc.tileStridePixelsV) :
                    new Rectangle(0, i * desc.tileStridePixelsV, desc.tileStridePixelsH, desc.tileStridePixelsV),
                    GraphicsUnit.Pixel);
                source.MakeTransparent(desc.transparentColor);
            }
            images = elementImages;
        }
    }
}
