﻿#define DEBUG

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ICSharpCode.AvalonEdit;
using GBA_Studio.Properties;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Reflection; //Used for the start page
using TabControl;
using System.Net;
using System.Runtime.InteropServices;

using GBA_Studio.IO;
using GBA_Studio.Controls;

namespace GBA_Studio.Forms
{
    public partial class VSForm : Form
    {
        #region Variables
        public System.Windows.Media.Brush background = (System.Windows.Media.Brush)(new System.Windows.Media.BrushConverter().ConvertFrom("#1e1e1e"));
        public System.Windows.Media.Brush foreground = (System.Windows.Media.Brush)(new System.Windows.Media.BrushConverter().ConvertFrom("#ffffff"));

        private bool resizable = true;
        private Point origOffset;
        private bool isMoving = false;
        private bool exitBox = true;

        Bitmap[] navIcons;

        int hoverIndex = -1;
        int clickIndex = -1;

        private Rectangle[] drawLocations = new Rectangle[4];

        private string statusText = "";
        private bool statusVisible = false;
        private bool statusRightToLeft = false;
        //Form Moving and Docking
        #endregion

        #region Properties
        public string StatusText
        {
            get
            {
                return statusText;
            }
            set
            {
                statusText = value;
                this.Invalidate();
            }
        }

        public bool StatusStripVisible
        {
            get
            {
                return statusVisible;
            }
            set
            {
                statusVisible = value;
                this.Padding = new Padding(0, 25, 0, statusVisible ? 25 : 0);
                this.Invalidate();
            }
        }

        public bool StatusRightToLeft
        {
            get
            {
                return statusRightToLeft;
            }
            set
            {
                statusRightToLeft = value;
                this.Invalidate();
            }
        }

        public bool ExitBox
        {
            get
            {
                return exitBox;
            }
            set
            {
                exitBox = value;
                this.Invalidate();
            }
        }
        private Color navigationIdleBackground = Color.FromArgb(45, 45, 48);
        private Color navigationHoverBackground = Color.FromArgb(62, 62, 64);
        private Color navigationClickedBackground = Color.FromArgb(0, 122, 204);
        private Color statusForecolor = Color.FromArgb(255, 255, 255);
        private Color statusBackcolor = Color.FromArgb(0, 122, 204);


        public Color NavigationIdleBackground
        {
            get
            {
                return navigationIdleBackground;
            }
            set
            {
                navigationIdleBackground = value;
                this.Invalidate();
            }
        }
        public Color NavigationHoverBackground
        {
            get
            {
                return navigationHoverBackground;
            }
            set
            {
                navigationHoverBackground = value;
                this.Invalidate();
            }
        }
        public Color NavigationClickedBackground
        {
            get
            {
                return navigationClickedBackground;
            }
            set
            {
                navigationClickedBackground = value;
                this.Invalidate();
            }
        }
        public Color StatusForecolor
        {
            get
            {
                return statusForecolor;
            }
            set
            {
                statusForecolor = value;
                this.Invalidate();
            }
        }
        public Color StatusBackcolor
        {
            get
            {
                return statusBackcolor;
            }
            set
            {
                statusBackcolor = value;
                this.Invalidate();
            }
        }
        public bool Resizable
        {
            get
            {
                return resizable;
            }
            set
            {
                resizable = value;
                this.Invalidate();
            }
        }

        #endregion

        protected override CreateParams CreateParams
        {
            get
            {
                const int CS_DROPSHADOW = 0x20000;
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }

        #region Event Handling
        #endregion

        #region Misc

        public VSForm()
        {
            InitializeComponent();

            ParseDescription desc;
            desc.direction = ParseDirection.Horizontal;
            desc.sourceImg = Resources.NavIcons;
            desc.tileStridePixelsH = 16;
            desc.tileStridePixelsV = 16;
            desc.transparentColor = Color.White;

            GraphicParser.Parse(desc, out navIcons);

            //menuBtnMin.BackgroundImage = navIcons[1];
            //menuBtnMax.BackgroundImage = navIcons[2];
            //menuBtnClose.BackgroundImage = navIcons[3];
        }

        #endregion

        private Point resizeClickOrigin;
        private Size resizeFormOrigin;

        private void ResizeForm()
        {
            if (resizeFormOrigin != Size.Empty && resizeClickOrigin != Point.Empty)
            {
                Size t = new Size(resizeClickOrigin.X - MousePosition.X, resizeClickOrigin.Y - MousePosition.Y);
                Size newSize = new Size(resizeFormOrigin.Width - t.Width, resizeFormOrigin.Height - t.Height);


                if (newSize.Width < MinimumSize.Width)
                {
                    newSize.Width = MinimumSize.Width;
                }
                else if (newSize.Width > MaximumSize.Width && MaximumSize != new Size(0, 0))
                {
                    newSize.Width = MaximumSize.Width;
                }
                

                if (newSize.Height < MinimumSize.Height)
                {
                    newSize.Height = MinimumSize.Height;
                }
                else if (newSize.Height > MaximumSize.Height && MaximumSize != new Size(0, 0))
                {
                    newSize.Height = MaximumSize.Height;
                }
                
                this.Size = newSize;
            }
        }

        private void updateConsole_Tick(object sender, EventArgs e)
        {
            ControlMovement();

            Point mousePos = new Point(MousePosition.X - this.Left, MousePosition.Y - this.Top);

            int tempIndex = GetButtonIndex(mousePos);
            if(tempIndex != hoverIndex)
            {
                hoverIndex = tempIndex;
                this.Invalidate();
            }

        }

        private void ControlMovement()
        {
            if (isMoving)
            {
                this.Left = MousePosition.X - origOffset.X;
                this.Top = MousePosition.Y - origOffset.Y;

                if (MousePosition.Y == 0)
                {
                    this.WindowState = FormWindowState.Maximized;
                }
                else
                {
                    Size prevSize = this.Size;
                    this.WindowState = FormWindowState.Normal;

                    float widthPercentage = (float)this.Size.Width / (float)prevSize.Width;
                    float heightPercentage = (float)this.Size.Height / (float)prevSize.Height;
                    origOffset.X = (int)((float)origOffset.X * widthPercentage);
                    origOffset.Y = (int)((float)origOffset.Y * heightPercentage);
                }
            }

            ResizeForm();
        }

        private void formTitle_TextChanged(object sender, EventArgs e)
        {

        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.FillRectangle(new SolidBrush(BackColor), new Rectangle(0, 0, this.Width, 25));

            int navigationPaddingX = 10;
            int navigationPaddingY = 2;
            int navigationHeight = 25;

            //Draw Icon
            {
                Point iconPosition = new Point(navigationPaddingX, navigationPaddingY);
                int iconSize = (navigationHeight - (2 * navigationPaddingY));
                e.Graphics.DrawImage(Resources.GBAStudioLogoPNG, iconPosition.X, iconPosition.Y, iconSize, iconSize);
            }

            //Draw Text Label
            {
                SizeF stringSize = e.Graphics.MeasureString(this.Text, this.Font);
                int drawLocationx = navigationHeight + (navigationPaddingX) + 5;
                int drawLocationY = (navigationHeight - (int)stringSize.Height) / 2;
                //Do check to see if buttons overlap.
                e.Graphics.DrawString(this.Text, this.Font, new SolidBrush(this.ForeColor), new PointF(drawLocationx, drawLocationY));
            }

            //Draw Navigations Buttons
            {
                if (MinimizeBox)
                {
                    Color currentColor = clickIndex == 0 ? navigationClickedBackground : (hoverIndex == 0 ? navigationHoverBackground : navigationIdleBackground);
                    int drawX = this.Width - (ExitBox ? 34 : 0) - (MaximizeBox ? 34 : 0) - 34;
                    e.Graphics.FillRectangle(new SolidBrush(currentColor), drawX, 0, 34, 25);
                    e.Graphics.DrawImage(navIcons[1], drawX + ((34 - navIcons[1].Width) / 2), (25 - navIcons[1].Height) / 2);

                    drawLocations[0] = new Rectangle(drawX, 0, 34, 25);
                }
                if (MaximizeBox)
                {
                    Color currentColor = clickIndex == 1 ? navigationClickedBackground : (hoverIndex == 1 ? navigationHoverBackground : navigationIdleBackground);
                    int drawX = this.Width - (ExitBox ? 34 : 0) - 34;
                    e.Graphics.FillRectangle(new SolidBrush(currentColor), drawX, 0, 34, 25);
                    e.Graphics.DrawImage(navIcons[2], drawX + ((34 - navIcons[2].Width) / 2), (25 - navIcons[2].Height) / 2);

                    drawLocations[1] = new Rectangle(drawX, 0, 34, 25);
                }
                if (ExitBox)
                {
                    Color currentColor = clickIndex == 2 ? navigationClickedBackground : (hoverIndex == 2 ? navigationHoverBackground : navigationIdleBackground);
                    int drawX = this.Width - 34;
                    e.Graphics.FillRectangle(new SolidBrush(currentColor), drawX, 0, 34, 25);
                    e.Graphics.DrawImage(navIcons[3], drawX + ((34 - navIcons[3].Width) / 2), (25 - navIcons[3].Height) / 2);

                    drawLocations[2] = new Rectangle(drawX, 0, 34, 25);
                }
            }

            //Draw Status Bar
            {
                e.Graphics.FillRectangle(new SolidBrush(statusBackcolor), new Rectangle(0, this.Height - 25, this.Width, 25));

                int drawX = this.Width - 14;
                int drawY = this.Height - 14;

                //Grip
                if (resizable)
                {
                    e.Graphics.DrawImage(Resources.Grip, drawX, drawY, 12, 12);
                    drawLocations[3] = new Rectangle(drawX, drawY, 12, 12);
                }

                //Label
                SizeF stringSize = e.Graphics.MeasureString(this.StatusText, this.Font);
                int drawLocationx = (this.statusRightToLeft ? drawX - 10 - (int)stringSize.Width : 2);
                int drawLocationY = this.Height - 25 + ((navigationHeight - (int)stringSize.Height) / 2);
                
                e.Graphics.DrawString(this.StatusText, this.Font, new SolidBrush(this.statusForecolor), new PointF(drawLocationx, drawLocationY));
            }
            base.OnPaint(e);
        }

        private void VSForm_Load(object sender, EventArgs e)
        {
            
        }

        private void VSForm_MouseMove(object sender, MouseEventArgs e)
        {
            hoverIndex = GetButtonIndex(e.Location);
            if(hoverIndex == 3)
            {
                this.Cursor = Cursors.SizeNWSE;
            }
            else
            {
                this.Cursor = Cursors.Default;
            }
            this.Invalidate();
        }

        private void VSForm_MouseDown(object sender, MouseEventArgs e)
        {
            int drawX = this.Width - (ExitBox ? 34 : 0) - (MaximizeBox ? 34 : 0) - 34;
            clickIndex = GetButtonIndex(e.Location);
            this.Invalidate();

            if (clickIndex < 0)
            {
                origOffset = new Point(MousePosition.X - this.Location.X, MousePosition.Y - this.Location.Y);
                isMoving = true;
            }
            else if (clickIndex == 3 && this.WindowState != FormWindowState.Maximized && resizable)
            {
                resizeFormOrigin = this.Size;
                resizeClickOrigin = MousePosition;
            }
        }

        private void VSForm_MouseUp(object sender, MouseEventArgs e)
        {
            int tempIndex = GetButtonIndex(e.Location);

            if (tempIndex == clickIndex)
            {
                switch (clickIndex)
                {
                    case 0:
                        this.WindowState = FormWindowState.Minimized;
                        break;
                    case 1:
                        this.WindowState = this.WindowState == FormWindowState.Maximized ? FormWindowState.Normal : FormWindowState.Maximized;
                        break;
                    case 2:
                        this.Close();
                        break;
                }
            }
            resizeFormOrigin = Size.Empty;
            resizeClickOrigin = Point.Empty;

            clickIndex = -1;
            this.Invalidate();

            isMoving = false;
        }

        private int GetButtonIndex(Point position)
        {
            for(int i = 0; i < drawLocations.Length; i++)
            {
                if(CheckIntersection(position, drawLocations[i]))
                {
                    return i;
                }
            }
            return -1;
        }

        private bool CheckIntersection(Point position, Rectangle rect)
        {
            if(position.X >= rect.X && position.X <= rect.Right)
            {
                if (position.Y >= rect.Y && position.Y <= rect.Bottom)
                {
                    return true;
                }
            }
            return false;
        }

        private void VSForm_Resize(object sender, EventArgs e)
        {
            this.Invalidate();
        }
    }
}
