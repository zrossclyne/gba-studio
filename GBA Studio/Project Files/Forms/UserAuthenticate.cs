﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GBA_Studio.Tools;
using GBA_Studio.IO;

namespace GBA_Studio.Forms
{
    public partial class UserAuthenticate : VSForm
    {
        public UserAuthenticate()
        {
            InitializeComponent();
        }

        private void btnLogIn_Click(object sender, EventArgs e)
        {
            Submit();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Submit()
        {
            UserDatabase.RESPONSE response = Globals.VerifyUser(tbUsername.Text, tbPassword.Text);
            switch (response)
            {

                case UserDatabase.RESPONSE.RESPONSE_USERNAME:
                    //Invalid username
                    tbUsername.Text = "";
                    tbPassword.Text = "";
                    tbUsername.Focus();

                    StatusText = "Invalid Username.";
                    break;
                case UserDatabase.RESPONSE.RESPONSE_MISMATCH:
                    //Incorrect Password
                    tbPassword.Text = "";
                    tbPassword.Focus();

                    StatusText = "Invalid Password.";
                    break;
                case UserDatabase.RESPONSE.RESPONSE_AUTHENTICATED:
                    this.Close();
                    break;
            }
        }

        private void btnSignOut_Click(object sender, EventArgs e)
        {
            Globals.CurrentUser = new User();
            Globals.AuthenticationMode = Globals.Authentication.Normal;
            Globals.StudioUserStateChanged = true;
            StudioSettings.Username = "";
            StudioSettings.Password = "";
            this.Close();
        }

        private void btnCancel2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tbPassword_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                Submit();
            }
            else if(e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void tbUsername_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                tbPassword.Focus();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void UserAuthenticate_Load(object sender, EventArgs e)
        {
            if (Globals.CurrentUser.NULL)
            {
                splitContainer1.Panel2Collapsed = true;
            }
            else
            {
                splitContainer1.Panel1Collapsed = true;
            }
        }
    }
}
