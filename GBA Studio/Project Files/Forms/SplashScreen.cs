﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using GBA_Studio.Properties;

namespace GBA_Studio.Forms
{
    public partial class SplashScreen : Form
    {
        private bool clicked = false;
        private int i = 0;
        private Point originalLocation;
        private string[] processes = {
            "Preparing Satan",
            "The Phantom is a Menace",
            "Pretending I'm doing something",
            "Help I'm stuck in 1942 and I can't get back",
            "If anyone gets the reference then you get a IDE for GBA",
            "It's okay you'll get that anyway",
            "The Clones are Attacking",
            "Loading explitives",
            "Implanting Trojan",
            "The Sith's never ending Revenge",
            "Zachrificing Goats",
            "Loading insults about your mother",
            "Loading insults about your mother",
            "Loading your mother",
            "Loading your mother",
            "Loading your mother",
            "Loading your mother",
            "Your New Hope is loading",
            "Plagiarising your coursework",
            "The Empire is striking back",
            "Rewatching Friends",
            "This is not the IDE you're looking for",
            "The Jedi have returned",
            "Copying browser history",
            "Awakening the Force",
            "Starting GBA Studio 2016"
        };

        private const int SHORT_TIME = 200;
        private const int LONG_TIME = 1000;
        
        public SplashScreen()
        {
            InitializeComponent();
            //System.Windows.Forms.Application.Run(new MainForm(Args));
        }

        private void LoadProcesses()
        {
            Random r = new Random();

            loadingStatusCurrent.Text = processes[i] + "...";

            i++;

            timer1.Interval = r.Next(SHORT_TIME, LONG_TIME);
        }

        private void SplashScreen_MouseDown(object sender, MouseEventArgs e)
        {
            Point mousePos = MousePosition;
            originalLocation = new Point(mousePos.X - this.Location.X, mousePos.Y - this.Location.Y);
            clicked = true;
        }

        private void SplashScreen_MouseMove(object sender, MouseEventArgs e)
        {
            if (clicked)
            {
                this.Location = new Point(MousePosition.X - originalLocation.X, MousePosition.Y - originalLocation.Y);
                //originalLocation = MousePosition;
            }
        }

        private void SplashScreen_MouseUp(object sender, MouseEventArgs e)
        {
            clicked = false;
        }

        private void SplashScreen_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(Resources.SplashScreenBackground, new Rectangle(0, 0, this.Width, this.Height));
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (i < processes.Length)
            {
                LoadProcesses();
            }
            else
            {
                this.Close();
            }
        }

        private void SplashScreen_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
