﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace GBA_Studio.Forms
{
    public partial class Sprite_Designer : VSForm
    {
        private Stack<List<int>> designerUndoStack = new Stack<List<int>>();
        private Stack<List<int>> designerRedoStack = new Stack<List<int>>();

        private Stack<List<Color>> paletteUndoStack = new Stack<List<Color>>();
        private Stack<List<Color>> paletteRedoStack = new Stack<List<Color>>();

        private Stack<int> selectedUndoStack = new Stack<int>();
        private Stack<int> selectedRedoStack = new Stack<int>();

        private const int PALETTE_ELEMENT_WIDTH = 25;
        private const int PALETTE_WIDTH = (PALETTE_ELEMENT_WIDTH * 8) + 9;
        private const int PALETTE_HEIGHT = (PALETTE_ELEMENT_WIDTH * 2) + 3;
        private List<Rectangle> paletteRects = new List<Rectangle>();
        private List<Color> paletteColours = new List<Color>();
        private Bitmap paletteOverlay;

        private int selectedColour = -1;
        private int paletteHoverIndex = -1;
        private int designerHoverIndex = -1;

        private const int DESIGNER_ELEMENT_WIDTH = 40;
        private const int DESIGNER_WIDTH = (DESIGNER_ELEMENT_WIDTH * 8) + 9;
        private const int DESIGNER_HEIGHT = (DESIGNER_ELEMENT_WIDTH * 8) + 9;
        private List<Rectangle> designerRects = new List<Rectangle>();
        private List<int> designerColours = new List<int>();
        private Bitmap designerOverlay;

        public string FileName
        {
            get;
            set;
        }

        public Sprite_Designer()
        {
            InitializeComponent();
            GeneratePalette();
            GenerateDesigner();

            keyMonitor1.AddWatchedKey(Keys.D1, Keys.D2, Keys.D3, Keys.D4, Keys.D5, Keys.D6, Keys.D7, Keys.D8);
            keyMonitor1.Begin();
        }

        private void GeneratePalette()
        {
            for(int i = 0; i < 16; i++)
            {
                paletteColours.Add(new Color());
            }

            Bitmap b = new Bitmap(PALETTE_WIDTH, PALETTE_HEIGHT);
            Graphics g = Graphics.FromImage(b);
            for(int y = 0; y < 2; y++)
            {
                for (int x = 0; x < 8; x++)
                {
                    int posX = (x * PALETTE_ELEMENT_WIDTH) + 1;
                    int posY = (y * PALETTE_ELEMENT_WIDTH) + 1;

                    g.DrawRectangle(Pens.Black, posX, posY, PALETTE_ELEMENT_WIDTH, PALETTE_ELEMENT_WIDTH);
                    paletteRects.Add(new Rectangle(posX + 1, posY + 1, PALETTE_ELEMENT_WIDTH - 1, PALETTE_ELEMENT_WIDTH - 1));
                }
            }
            paletteOverlay = b;
        }

        private void GenerateDesigner()
        {
            for (int i = 0; i < 64; i++)
            {
                designerColours.Add(0);
            }

            Bitmap b = new Bitmap(DESIGNER_WIDTH, DESIGNER_HEIGHT);
            Graphics g = Graphics.FromImage(b);
            for (int y = 0; y < 8; y++)
            {
                for (int x = 0; x < 8; x++)
                {
                    int posX = (x * DESIGNER_ELEMENT_WIDTH) + 1;
                    int posY = (y * DESIGNER_ELEMENT_WIDTH) + 1;

                    g.DrawRectangle(Pens.Black, posX, posY, DESIGNER_ELEMENT_WIDTH, DESIGNER_ELEMENT_WIDTH);
                    designerRects.Add(new Rectangle(posX + 1, posY + 1, DESIGNER_ELEMENT_WIDTH - 1, DESIGNER_ELEMENT_WIDTH - 1));
                }
            }
            designerOverlay = b;
        }

        private void canvasTimer_Tick(object sender, EventArgs e)
        {
            DrawPalette();
            DrawDesigner();
            SortStacks();
        }

        private void DrawPalette()
        {

            int selectBuffer = 3;

            Bitmap b = new Bitmap(PALETTE_WIDTH, PALETTE_HEIGHT);
            Graphics g = Graphics.FromImage(b);

            g.DrawImage(paletteOverlay, 0, 0);

            for (int i = 0; i < 16; i++)
            {
                if (i < paletteColours.Count && paletteColours[i].A > 0)
                {
                    SolidBrush brush = new SolidBrush(paletteColours[i]);
                    g.FillRectangle(brush, paletteRects[i]);
                }
            }

            if (selectedColour >= 0)
            {
                Brush brush;
                if (selectedColour == 0)
                {
                    brush = new SolidBrush(paletteColours[0].A == 0 ? this.BackColor : paletteColours[0]);
                }
                else
                {
                    brush = new SolidBrush(paletteColours[selectedColour]);
                }
               
                int posX = paletteRects[selectedColour].X - 1 - selectBuffer;
                int posY = paletteRects[selectedColour].Y - 1 - selectBuffer;
                posX = posX < 0 ? 0 : posX > PALETTE_WIDTH - PALETTE_ELEMENT_WIDTH ? PALETTE_WIDTH - PALETTE_ELEMENT_WIDTH : posX;
                posY = posY < 0 ? 0 : posY > PALETTE_HEIGHT - (PALETTE_ELEMENT_WIDTH + (2 * selectBuffer) + 1) ? PALETTE_HEIGHT - (PALETTE_ELEMENT_WIDTH + (2 * selectBuffer) + 1) : posY;

                g.FillRectangle(brush, new Rectangle(posX, posY, PALETTE_ELEMENT_WIDTH + (2 * selectBuffer), PALETTE_ELEMENT_WIDTH + (2 * selectBuffer)));
                g.DrawRectangle(Pens.White, new Rectangle(posX, posY, PALETTE_ELEMENT_WIDTH + (2 * selectBuffer), PALETTE_ELEMENT_WIDTH + (2 * selectBuffer)));
                
            }

            if (paletteHoverIndex >= 0)
            {
                Brush brush;
                if (paletteColours[paletteHoverIndex].A == 0)
                {
                    brush = new SolidBrush(this.BackColor);
                }
                else
                {
                    brush = new SolidBrush(paletteColours[paletteHoverIndex]);
                }
               
                int posX = paletteRects[paletteHoverIndex].X - 1 - selectBuffer;
                int posY = paletteRects[paletteHoverIndex].Y - 1 - selectBuffer;
                posX = posX < 0 ? 0 : posX > PALETTE_WIDTH - PALETTE_ELEMENT_WIDTH ? PALETTE_WIDTH - PALETTE_ELEMENT_WIDTH : posX;
                posY = posY < 0 ? 0 : posY > PALETTE_HEIGHT - (PALETTE_ELEMENT_WIDTH + (2 * selectBuffer) + 1) ? PALETTE_HEIGHT - (PALETTE_ELEMENT_WIDTH + (2 * selectBuffer) + 1) : posY;

                g.FillRectangle(brush, new Rectangle(posX, posY, PALETTE_ELEMENT_WIDTH + (2 * selectBuffer), PALETTE_ELEMENT_WIDTH + (2 * selectBuffer)));
                g.DrawRectangle(Pens.White, new Rectangle(posX, posY, PALETTE_ELEMENT_WIDTH + (2 * selectBuffer), PALETTE_ELEMENT_WIDTH + (2 * selectBuffer)));
                
            }

            pictureBoxPalette.Image = b;
        }

        private void DrawDesigner()
        {
            int selectBuffer = 12;
            Bitmap b = new Bitmap(DESIGNER_WIDTH, DESIGNER_HEIGHT);
            Graphics g = Graphics.FromImage(b);

            g.DrawImage(designerOverlay, 0, 0);

            for (int i = 0; i < 64; i++)
            {
                if (i < designerColours.Count && designerColours[i] >= 0)
                {
                    SolidBrush brush = new SolidBrush(paletteColours[designerColours[i]]);
                    g.FillRectangle(brush, designerRects[i]);
                }
            }

            if (selectedColour >= 0 && designerHoverIndex >= 0)
            {
                Brush brush;
                if (paletteColours[selectedColour].A == 0)
                {
                    brush = new SolidBrush(this.BackColor);
                }
                else
                {
                    brush = new SolidBrush(paletteColours[selectedColour]);
                }

                int posX = designerRects[designerHoverIndex].X - 1 - selectBuffer;
                int posY = designerRects[designerHoverIndex].Y - 1 - selectBuffer;
                posX = posX < 0 ? 0 : posX > DESIGNER_WIDTH - DESIGNER_ELEMENT_WIDTH ? DESIGNER_WIDTH - DESIGNER_ELEMENT_WIDTH : posX;
                posY = posY < 0 ? 0 : posY > DESIGNER_HEIGHT - (DESIGNER_ELEMENT_WIDTH + (2 * selectBuffer) + 1) ? DESIGNER_HEIGHT - (DESIGNER_ELEMENT_WIDTH + (2 * selectBuffer) + 1) : posY;

                g.FillRectangle(brush, new Rectangle(posX, posY, DESIGNER_ELEMENT_WIDTH + (2 * selectBuffer), DESIGNER_ELEMENT_WIDTH + (2 * selectBuffer)));
                g.DrawRectangle(Pens.White, new Rectangle(posX, posY, DESIGNER_ELEMENT_WIDTH + (2 * selectBuffer), DESIGNER_ELEMENT_WIDTH + (2 * selectBuffer)));
                
            }

            pictureBoxDesigner.Image = b;
        }

        private void pictureBoxPalette_MouseUp(object sender, MouseEventArgs e)
        {
            PalettePress(e);
        }

        private void PalettePress(Rectangle r)
        {
            int offsetX = (int)((pictureBoxPalette.Width - PALETTE_WIDTH) / 2.0f);
            int offsetY = (int)((pictureBoxPalette.Height - PALETTE_HEIGHT) / 2.0f);
            MouseEventArgs e = new MouseEventArgs(MouseButtons.Left, 1, offsetX + r.Left + (int)(PALETTE_ELEMENT_WIDTH / 2), offsetY + r.Top + (int)(PALETTE_ELEMENT_WIDTH / 2), 0);
            PalettePress(e);
        }

        private void PalettePress(MouseEventArgs e)
        {
            int selectedIndex = -1;
            for (int i = 0; i < 16; i++)
            {
                int clickLocX = e.X - (int)((pictureBoxPalette.Width - PALETTE_WIDTH) / 2.0f);
                int clickLocY = e.Y - (int)((pictureBoxPalette.Height - PALETTE_HEIGHT) / 2.0f);
                if (paletteRects[i].Contains(new Point(clickLocX, clickLocY)))
                {
                    selectedIndex = i;
                    break;
                }
            }
            if (selectedIndex >= 0)
            {
                if (e.Button == MouseButtons.Left)
                {

                    if (selectedIndex > 0)
                    {
                        if (paletteColours[selectedIndex].A > 0)
                        {
                            SetUndoRedo();
                            selectedColour = selectedIndex;
                        }
                        else
                        {
                            ChooseColour(selectedIndex, true);
                        }
                    }
                    else
                    {
                        SetUndoRedo();
                        selectedColour = 0;
                    }
                }
                else if (e.Button == MouseButtons.Right)
                {
                    if (selectedColour == 0)
                    {
                        if (paletteColours[0].A == 0)
                        {
                            DialogResult d = MessageBox.Show("This colour is generally used for image transparency. Are you sure you want to change this?", "Change from transparent?", MessageBoxButtons.YesNo);
                            if (d == DialogResult.Yes)
                            {
                                ChooseColour(selectedIndex);
                            }
                        }
                        else
                        {
                            DialogResult d = MessageBox.Show("This colour is generally used for image transparency but you've currently got it set as a solid colour.\nWould you like to reset this to transparent?", "Reset to transparent?", MessageBoxButtons.YesNo);
                            if (d == DialogResult.Yes)
                            {
                                SetUndoRedo();
                                paletteColours[0] = Color.FromArgb(0, 0, 0, 0);
                            }
                            else
                            {
                                ChooseColour(selectedIndex);
                            }
                        }
                    }
                    else
                    {
                        ChooseColour(selectedIndex);
                    }
                }
            }
        }

        private void ChooseColour(int selectedIndex, bool autoSet = false)
        {

            ColorDialog c = new ColorDialog();
            if (c.ShowDialog() == DialogResult.OK)
            {
                SetUndoRedo();

                paletteColours[selectedIndex] = c.Color;
                if(autoSet)
                {
                    selectedColour = selectedIndex;
                }
            }
        }

        private void pictureBoxDesigner_MouseUp(object sender, MouseEventArgs e)
        {
            int selectedIndex = -1;
            for (int i = 0; i < 64; i++)
            {
                int clickLocX = e.X - (int)((pictureBoxDesigner.Width - DESIGNER_WIDTH) / 2.0f);
                int clickLocY = e.Y - (int)((pictureBoxDesigner.Height - DESIGNER_HEIGHT) / 2.0f);
                if (designerRects[i].Contains(new Point(clickLocX, clickLocY)))
                {
                    selectedIndex = i;
                    break;
                }
            }

            if (selectedIndex >= 0 && selectedColour >= 0)
            {
                SetUndoRedo();
                designerColours[selectedIndex] = selectedColour;
            }

        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog s = new SaveFileDialog();
            s.Filter = "GBA Studio Sprite | *.gbass";
            s.ShowDialog();
            if(s.FileName != "")
            {
                SaveSprite(s.FileName);
            }
        }

        private void importToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog o = new OpenFileDialog();
            o.Filter = "GBA Studio Sprite | *.gbass";
            o.ShowDialog();
            if (o.FileName != "")
            {
                OpenSprite(o.FileName);
            }
        }

        public void SaveSprite(string fileName)
        {
            try
            {
                FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate);
                BinaryWriter bw = new BinaryWriter(fs);

                bw.Write(selectedColour);
                for (int i = 0; i < 16; i++)
                {
                    bw.Write(paletteColours[i].A);
                    bw.Write(paletteColours[i].R);
                    bw.Write(paletteColours[i].G);
                    bw.Write(paletteColours[i].B);
                }
                for (int i = 0; i < 64; i++)
                {
                    bw.Write(designerColours[i]);
                }
                bw.Flush();
                bw.Close();
                bw.Dispose();

                FileName = fileName;

                saveToolStripMenuItem.Enabled = true;
            }
            catch
            {
                //throw
            }
        }

        public void OpenSprite(string fileName)
        {
            try
            {
                FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate);
                BinaryReader br = new BinaryReader(fs);

                selectedColour = br.ReadInt32();
                for (int i = 0; i < 16; i++)
                {
                    byte a, r, g, b;
                    a = br.ReadByte();
                    r = br.ReadByte();
                    g = br.ReadByte();
                    b = br.ReadByte();
                    paletteColours[i] = Color.FromArgb(a, r, g, b);
                }
                for (int i = 0; i < 64; i++)
                {
                    designerColours[i] = br.ReadInt32();
                }
                br.Close();
                br.Dispose();

                FileName = fileName;

                saveToolStripMenuItem.Enabled = true;
            }
            catch
            {
                //throw
            }
        }

        private void keyMonitor1_KeyDown(object sender, KeyEventArgs e)
        {
            if (this.ContainsFocus)
            {
                int selClr = -1;
                switch (e.KeyCode)
                {
                    case Keys.D1:
                        selClr = 0;
                        break;
                    case Keys.D2:
                        selClr = 1;
                        break;
                    case Keys.D3:
                        selClr = 2;
                        break;
                    case Keys.D4:
                        selClr = 3;
                        break;
                    case Keys.D5:
                        selClr = 4;
                        break;
                    case Keys.D6:
                        selClr = 5;
                        break;
                    case Keys.D7:
                        selClr = 6;
                        break;
                    case Keys.D8:
                        selClr = 7;
                        break;
                }

                if (selClr >= 0)
                {
                    if (selectedColour >= 0)
                    {
                        if ((selectedColour + 8) % 8 != selClr)
                        {
                            PalettePress(paletteRects[selClr]);
                        }
                        else
                        {

                            int destIndex = selectedColour >= 8 ? selectedColour - 8 : selectedColour + 8;
                            PalettePress(paletteRects[destIndex]);
                        }
                    }
                    else
                    {
                        PalettePress(paletteRects[selClr]);
                    }
                }
            }
        }

        private void pictureBoxDesigner_MouseMove(object sender, MouseEventArgs e)
        {
            int selectedIndex = -1;
            for (int i = 0; i < 64; i++)
            {
                int clickLocX = e.X - (int)((pictureBoxDesigner.Width - DESIGNER_WIDTH) / 2.0f);
                int clickLocY = e.Y - (int)((pictureBoxDesigner.Height - DESIGNER_HEIGHT) / 2.0f);
                if (designerRects[i].Contains(new Point(clickLocX, clickLocY)))
                {
                    selectedIndex = i;
                    break;
                }
            }

            designerHoverIndex = selectedIndex >= 0 ? selectedIndex : -1;
        }

        private void pictureBoxPalette_MouseMove(object sender, MouseEventArgs e)
        {
            int selectedIndex = -1;
            for (int i = 0; i < 16; i++)
            {
                int clickLocX = e.X - (int)((pictureBoxPalette.Width - PALETTE_WIDTH) / 2.0f);
                int clickLocY = e.Y - (int)((pictureBoxPalette.Height - PALETTE_HEIGHT) / 2.0f);
                if (paletteRects[i].Contains(new Point(clickLocX, clickLocY)))
                {
                    selectedIndex = i;
                    break;
                }
            }

            paletteHoverIndex = selectedIndex >= 0 ? selectedIndex : -1;
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<int> popItemDesigner = designerUndoStack.Peek();
            designerRedoStack.Push(designerColours);
            designerColours = popItemDesigner;
            designerUndoStack.Pop();

            List<Color> popItemPalette = paletteUndoStack.Peek();
            paletteRedoStack.Push(paletteColours);
            paletteColours = popItemPalette;
            paletteUndoStack.Pop();

            int popItemSelected = selectedUndoStack.Peek();
            selectedRedoStack.Push(selectedColour);
            selectedColour = popItemSelected;
            selectedUndoStack.Pop();
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            List<int> popItemDesigner = designerRedoStack.Peek();
            designerUndoStack.Push(designerColours);
            designerColours = popItemDesigner;
            designerRedoStack.Pop();

            List<Color> popItemPalette = paletteRedoStack.Peek();
            paletteUndoStack.Push(paletteColours);
            paletteColours = popItemPalette;
            paletteRedoStack.Pop();

            int popItemSelected = selectedRedoStack.Peek();
            selectedUndoStack.Push(selectedColour);
            selectedColour = popItemSelected;
            selectedRedoStack.Pop();
        }

        private void SortStacks()
        {
            if (designerUndoStack.Count > 0)
            {
                undoToolStripMenuItem.Enabled = true;
            }
            else
            {
                undoToolStripMenuItem.Enabled = false;
            }
            if (designerRedoStack.Count > 0)
            {
                redoToolStripMenuItem.Enabled = true;
            }
            else
            {
                redoToolStripMenuItem.Enabled = false;
            }
        }

        void SetUndoRedo()
        {
            //By default, .Net was referencing. This is my hacky work-around to get undo working.
            List<int> newDesignerList = new List<int>();
            for (int i = 0; i < 64; i++)
            {
                int clr = designerColours[i];

                newDesignerList.Add(clr);
            }
            List<Color> newPaletteList = new List<Color>();
            for (int i = 0; i < 16; i++)
            {
                Color clr = paletteColours[i];

                newPaletteList.Add(clr);
            }

            designerUndoStack.Push(newDesignerList);
            designerRedoStack.Clear();
            paletteUndoStack.Push(newPaletteList);
            paletteRedoStack.Clear();
            selectedUndoStack.Push(selectedColour);
            selectedRedoStack.Clear();
        }

        private void swapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(splitWindow.Panel1Collapsed)
            {
                splitWindow.Panel2Collapsed = true;
            }
            else
            {
                splitWindow.Panel1Collapsed = true;
            }

            Recalc();
        }

        private void objRadioChanged_CheckedChanged(object sender, EventArgs e)
        {
            Recalc();
        }

        private void Recalc()
        {
            CalcPalette();
            CalcObject();
        }

        private void CalcPalette()
        {
            string strPalette = "short palette[] = { ";
            if (rbIntegerColour.Checked)
            {

                foreach (Color c in paletteColours)
                {
                    ushort clr = GetGBAClrShort(c);
                    strPalette += clr + ", ";
                }
                strPalette += "};";
            }
            else
            {

                foreach (Color c in paletteColours)
                {
                    Color clr = GetGBAClrElements(c);
                    strPalette += "\r\n\t" + clr.R + ", " + clr.G + ", " + clr.B + ",";
                }
                strPalette += "\r\n};";
            }
            textBox1.Text = strPalette;
        }

        private void CalcObject()
        {
            string strObj = "int object[] = { ";

            if (rbHexadecimalObject.Checked)
            {
                for (int i = 0; i < 8; i++)
                {
                    string temp = "\r\n\t0x0";
                    for (int j = 7; j >= 0; j--)
                    {
                        temp += GetHexVal(designerColours[(i * 8) + j]);
                    }

                    strObj += temp + ",";
                }
            }
            else
            {
                for (int i = 0; i < 8; i++)
                {
                    string temp = "";
                    for (int j = 0; j < 8; j++)
                    {
                        temp += GetHexVal(designerColours[(i * 8) + j]);
                    }
                    uint decValue = uint.Parse(temp, System.Globalization.NumberStyles.HexNumber);
                    strObj += "\r\n\t" + decValue + ", ";
                }

            }


            textBox2.Text = strObj + "\r\n};";

        }

        private char GetHexVal(int i)
        {
            if(i < 10)
            {
                return (char)(48 + i);
            }
            else
            {
                return (char)(i - 10 + 65);
            }
        }

        private ushort GetGBAClrShort(Color inputClr)
        {
            Color clr = GetGBAClrElements(inputClr);

            ushort s = 0;

            s |= (ushort)(clr.R << 0);
            s |= (ushort)(clr.G << 5);
            s |= (ushort)(clr.B << 10);
            return s;
        }

        private Color GetGBAClrElements(Color clr)
        {
            const float multiplier = 31.0f / 255.0f;
            Color newClr = Color.FromArgb(31, (int)(clr.R * multiplier), (int)(clr.G * multiplier), (int)(clr.B * multiplier));
            return newClr;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (FileName != null && FileName != "")
            {
                SaveSprite(FileName);
            }
        }

    }
}
