﻿#define DEBUG
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ICSharpCode.AvalonEdit;
using GBA_Studio.Properties;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Reflection; //Used for the start page
using TabControl;
using System.Net;
using System.Runtime.InteropServices;
using GBA_Studio.IO;
using GBA_Studio.Controls;
using GBA_Studio.Management;
using GBA_Studio.Tools;

using SmartSense;

namespace GBA_Studio.Forms
{
    public partial class MainForm : VSForm
    {

        private event EventHandler FinishedCommand;
        private event EventHandler BuildError;
        private event System.Windows.Input.KeyEventHandler KeyDownWPF;

        private FindReplace findReplaceForm;

        #region Variables
        private ClipboardInterface clipboardInterface;

        private Process proc;
        private Process emuProc;
        private string consoleText;
        private TreeView memberExplorer;

        private List<ConsoleError> currentErrors;

        private TreeNode newFileToNode;

        private bool shouldCopy = true;

        private StateManager stateManager;

        private bool rolledbackFromError = false;
        private bool waitingForCommandFinish = true;

        Globals.RunState runState = Globals.RunState.Idle;

        private bool waitingForBuildComplete = false;
        bool waitingForNewBuild = false;

        private string previousOutput = "";

        private bool copiedFile = false;
        private bool cutFile = false;
        private string clipboardFilePath = "";

        private string statusTextAppend = "";

        private BitwiseSandbox bitwiseSandbox;
        //Form Moving and Docking

        //Smart Sense
        SenseEngine senseEngine;
        
        #endregion
        #region Enums

        public enum NodeType
        {
            Project,
            Folder,
            Filter,
            File
        }

        #endregion
        #region Console
        private void PrepareConsole()
        {
            string exeLocation = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "");

            proc = new Process();

            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.RedirectStandardInput = true;
            proc.StartInfo.RedirectStandardOutput = true;
            proc.StartInfo.RedirectStandardError = true;
            proc.StartInfo.CreateNoWindow = true;
            //proc.StartInfo.FileName = "cmd.exe cd /d " + exeLocation;
            proc.StartInfo.FileName = "cmd.exe";
            proc.StartInfo.Arguments = "--login -i";

            proc.Start();

            proc.OutputDataReceived += new DataReceivedEventHandler(OutputHandler);
            proc.ErrorDataReceived += new DataReceivedEventHandler(OutputHandler);

            proc.BeginOutputReadLine();
            proc.BeginErrorReadLine();

            ChangeDirectory("console/bin");
            ExecuteCommand("bash --login");

            WaitForCommand();
            ClearConsole();
        }


        private void ConfigureStateManager(string[] args)
        {
            stateManager = new StateManager();

            stateManager.StateTransitioned += StateManagerTransitioned;
            stateManager.StateTransitionFailed += StateManagerTransitionFailed;
            stateManager.StateRolledBack += StateManagerRollBack;

            stateManager.AddTransitionToState(StudioState.Uninitialized, StudioState.NoSolution);

            stateManager.AddTransitionToState(StudioState.NoSolution, StudioState.NoProject);
            stateManager.AddTransitionToState(StudioState.NoSolution, StudioState.ProjectSolutionOpen);

            stateManager.AddTransitionToState(StudioState.NoProject, StudioState.ProjectSolutionOpen);

            stateManager.AddTransitionToState(StudioState.ProjectSolutionOpen, StudioState.Building);
            stateManager.AddTransitionToState(StudioState.ProjectSolutionOpen, StudioState.Running);
            stateManager.AddTransitionToState(StudioState.ProjectSolutionOpen, StudioState.NoProject);
            stateManager.AddTransitionToState(StudioState.ProjectSolutionOpen, StudioState.NoSolution);

            stateManager.AddTransitionToState(StudioState.Building, StudioState.Running);
            stateManager.AddTransitionToState(StudioState.Building, StudioState.ProjectSolutionOpen);

            stateManager.AddTransitionToState(StudioState.Running, StudioState.ProjectSolutionOpen);

            stateManager.Transition(StudioState.NoSolution); // this needs to happen all the time, state is changed for args at a later stage
        }

        private void ClearConsole()
        {
            ExecuteCommand("cls");
            consoleText = "";
            ExecuteCommand("echo Welcome to GBA Studio 2016");
        }

        public void OutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            //* Do your stuff with the output (write to console/log/StringBuilder)
            string output = outLine.Data;
            if (output != null)
            {
                if (!output.StartsWith("Microsoft") && !output.StartsWith("(c) 2015") && output != "")
                {
                    output = (output.Replace(Environment.CurrentDirectory, "").Replace("[2J", ""));

                    consoleText += output + "\r\n";
                    previousOutput = output;
                    //ThreadSafeScrollToEndAccessor(); //Doesn't work at current
                }
            }

            if (output.Contains("*** No rule to make target"))
            {
                Clean(false);
            }

            if (output.ToLower().Contains("error") || output.ToLower().Contains("warning"))
            {
                HandleBuildError(output);
            }

            if (output.EndsWith("login") || output.EndsWith("$"))
            {
                if (FinishedCommand != null)
                {
                    FinishedCommand(this, EventArgs.Empty);
                }
            }
        }

        private void HandleBuildError(string errorLine)
        {
            if (!rolledbackFromError)
            {
                rolledbackFromError = true;
                ThreadSafeRollBackStateAccessor();
            }
            if (errorLine.Contains("***")) // could cause state lock if error doesn't contain ***
            {
                if (BuildError != null)
                {
                    BuildError(errorLine, EventArgs.Empty);
                }
            }
            else
            {
                Regex r = new Regex(@"^([^`]+):(\d+):(\d+):\s*([Ff][Aa][Tt][Aa][Ll]\s+)?[Ee][Rr][Rr][Oo][Rr]:\s?([^`]+)");
                Match m = r.Match(errorLine);

                if (m.Success)
                {
                    string fileName = m.Groups[1].Value;
                    string lineNumber = m.Groups[2].Value;
                    string columnNumber = m.Groups[3].Value;
                    string fatal = m.Groups[4].Value;
                    string errorMessage = m.Groups[5].Value;
                    string fullError = m.Groups[0].Value;

                    ConsoleError c = new ConsoleError();
                    c.FileName = fileName.Replace(".h", ".gh").Replace(".c", ".gc").Replace(".s", ".gs");

                    TreeNode tr;

                    if (solutionExplorer.InvokeRequired)
                    {
                        tr = (TreeNode)this.Invoke(new Func<TreeNode>(() => GetProjectNodeFromNode(solutionExplorer.SelectedNode == null ? solutionExplorer.Nodes[0] : solutionExplorer.SelectedNode)));

                        tr = (TreeNode)this.Invoke(new Func<TreeNode>(() => GetTreeNodeFromText(tr, Path.GetFileName(c.FileName))));
                    }
                    else
                    {
                        tr = GetTreeNodeFromText(GetProjectNodeFromNode(solutionExplorer.SelectedNode), Path.GetFileName(c.FileName));
                    }

                    c.FileName = tr.Tag.ToString().ToLower();

                    c.Message = errorMessage;
                    c.LineNumber = int.Parse(lineNumber);
                    c.ColumnNumber = int.Parse(columnNumber);
                    c.Type = ConsoleErrorType.ERROR;
                    c.IsFatal = fatal != "";
                    currentErrors.Add(c);
                }
                else
                {
                    r = new Regex(@"^([^`]+):(\d+):(\d+):\s*[Ww][Aa][Rr][Nn][Ii][Nn][Gg]:\s?([^`]+)");
                    m = r.Match(errorLine);

                    if (m.Success)
                    {
                        string fileName = m.Groups[1].Value;
                        string lineNumber = m.Groups[2].Value;
                        string columnNumber = m.Groups[3].Value;
                        string errorMessage = m.Groups[4].Value;
                        string fullError = m.Groups[0].Value;

                        ConsoleError c = new ConsoleError();
                        c.FileName = fileName.Replace(".h", ".gh").Replace(".c", ".gc").Replace(".s", ".gs");

                        TreeNode tr;

                        if (solutionExplorer.InvokeRequired)
                        {
                            tr = (TreeNode)this.Invoke(new Func<TreeNode>(() => GetProjectNodeFromNode(solutionExplorer.SelectedNode == null ? solutionExplorer.Nodes[0] : solutionExplorer.SelectedNode)));

                            tr = (TreeNode)this.Invoke(new Func<TreeNode>(() => GetTreeNodeFromText(tr, Path.GetFileName(c.FileName))));
                        }
                        else
                        {
                            tr = GetTreeNodeFromText(GetProjectNodeFromNode(solutionExplorer.SelectedNode), Path.GetFileName(c.FileName));
                        }

                        c.FileName = tr.Tag.ToString().ToLower();

                        c.Message = errorMessage;
                        c.LineNumber = int.Parse(lineNumber);
                        c.ColumnNumber = int.Parse(columnNumber);
                        c.Type = ConsoleErrorType.WARNING;
                        c.IsFatal = false;

                        currentErrors.Add(c);
                    }
                }
            }

        }

        public void ExecuteCommand(string cmd)
        {
            StreamWriter sw = proc.StandardInput;

            sw.WriteLine(cmd);
        }

        public void Print(string message)
        {
            ExecuteCommand("echo " + message);
        }

        private void updateConsole_Tick(object sender, EventArgs e)
        {
            if (MouseButtons == MouseButtons.Left)
            {
                Point mousePos = MousePosition;

                if (!(smartSense1.Visible && (mousePos.X - this.Left > smartSense1.Left && mousePos.X - this.Left < smartSense1.Right) && (mousePos.Y - this.Top > smartSense1.Top && mousePos.Y - this.Top < smartSense1.Bottom)))
                {
                    smartSenseFilter = "";
                    senseEngine.SetFilter(smartSenseFilter);

                    smartSense1.Visible = false;
                }
            }
            
            //User login status has changed. Update the studio.
            if (Globals.StudioUserStateChanged)
            {
                UpdateStudioPrivileges();
                Globals.StudioUserStateChanged = false;
                InputOutput.WriteSettings();
            }

            this.StatusText = (Globals.CurrentSolution != null ? (Globals.CurrentSolution.EvaluationMode ? "Evaluation Mode" : "Design Mode") : "") + statusTextAppend;

            if (tabControlAttributes.Tabs.Count > 0 && tabControlAttributes.Tabs[tabControlAttributes.SelectedIndex].Title == "New Object" && (tabControlAttributes.Focused || tabControlAttributes.ContainsFocus) == false)
            {
                NewObjectUnmountTab();
                tabControlAttributes.SelectTab(0);
            }

            try
            {
                errorList.Clear();

                if (currentErrors.Count > 0)
                {
                    foreach (ConsoleError error in currentErrors)
                    {
                        string prefix = error.Type == ConsoleErrorType.ERROR ? "Error: " : error.Type == ConsoleErrorType.WARNING ? "Warning: " : "";

                        string errorMessage = (error.IsFatal ? "Fatal " : "") + prefix + error.Message + " in " + Path.GetFileName(error.FileName).Replace(".c", ".gc").Replace(".h", ".gh") + " at line " + error.LineNumber + ", column " + error.ColumnNumber;

                        bool found = false;
                        foreach (var item in errorList.Items)
                        {
                            if (item.Item2 == errorMessage)
                            {
                                found = true;
                            }
                        }

                        if (!found)
                        {
                            errorList.AddItem(error, errorMessage);
                        }
                    }
                }
            }
            catch
            {

            }

            if (consoleBox != null && consoleText != null && consoleText != consoleBox.Text)
            {
                consoleBox.Text = consoleText;

                ThreadSafeScrollToEndAccessor();
            }
        }

        private void NewObjectMountTab(int index = -1)
        {
            newTab = tabControlAttributes.AddTab("New Object");
            tabControlAttributes.AttachControl(itemListInterface1, newTab);
            itemListInterface1.Visible = true;
            itemListInterface1.Focus();

            if (index >= 0)
            {
                itemListInterface1.SelectItem(index);
            }
        }

        private void NewObjectUnmountTab()
        {
            tabControlAttributes.DetachControl(newTab);
            tabControlAttributes.DeleteTab(newTab);
            itemListInterface1.Reset();
            newTab = -1;
            itemListInterface1.Visible = false;
            this.Invalidate();
        }

        void MainForm_FinishedCommand(object sender, EventArgs e)
        {
            waitingForCommandFinish = false;
        }

        private void WaitForCommand()
        {
            waitingForCommandFinish = true;

            while (waitingForCommandFinish)
            {

            }
        }
        #endregion
        #region Project/Solution Functionality

        #region Loading

        private void GenerateMakeFile(Project p)
        {
            if (Globals.CurrentSolution != null)
            {
                string sampleMakefile = InputOutput.ReadFromResources("GBA_Studio.Resources.Text_Templates.Makefile");

                string makeFile = sampleMakefile.Replace("#COMPILE_FROM_DIR#", Globals.DefaultCompileDirectory);

                string drivePath = InputOutput.GetDrivePath(Globals.CurrentSolution.Path);
                string directoryName = Path.GetDirectoryName(drivePath);

                string projectName = Path.GetFileNameWithoutExtension(p.Path);

                File.WriteAllText(directoryName + "\\" + projectName + "\\Makefile", makeFile);
            }
        }

        public void UpdateRecentFileMenu()
        {
            foreach (ToolStripMenuItem t in toolStripMenuItemRecentFiles.DropDownItems)
            {
                t.Click -= RecentFileClick;
            }

            toolStripMenuItemRecentFiles.DropDownItems.Clear();

            foreach (KeyValuePair<string, string> v in Globals.StartPageRecentFiles)
            {
                toolStripMenuItemRecentFiles.DropDownItems.Add(v.Key, null, RecentFileClick);

                int num = toolStripMenuItemRecentFiles.DropDownItems.Count;

                toolStripMenuItemRecentFiles.DropDownItems[num - 1].Tag = num;
                toolStripMenuItemRecentFiles.DropDownItems[num - 1].ForeColor = Color.White;

                // f3 - b = 1 :)

                Keys key = Keys.Control;

                switch (num)
                {
                    case 1:
                        key |= Keys.D1;
                        break;
                    case 2:
                        key |= Keys.D2;
                        break;
                    case 3:
                        key |= Keys.D3;
                        break;
                    case 4:
                        key |= Keys.D4;
                        break;
                    case 5:
                        key |= Keys.D5;
                        break;
                }

                ((ToolStripMenuItem)toolStripMenuItemRecentFiles.DropDownItems[num - 1]).ShortcutKeys = ((System.Windows.Forms.Keys)(key));
            }

        }

        void RecentFileClick(object sender, EventArgs e)
        {
            OpenRecentFile(int.Parse(((ToolStripMenuItem)sender).Tag.ToString()) - 1);
        }

        private bool VerifyLoad(string solutionPath)
        {
            bool openAuthorised = true;

            if (Globals.CurrentSolution != null)
            {
                if (solutionPath != Globals.CurrentSolution.Path)
                {
                    DialogResult result = MessageBox.Show("You are about to open a new solution. Any unsaved changes to the current solution will be lost. Would you like to continue?", "Open A Solution", MessageBoxButtons.YesNo);
                    openAuthorised = result == DialogResult.Yes;

                    if (openAuthorised)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        void OpenRecentFile(int index)
        {
            string filePath = Globals.StartPageRecentFiles[index].Value;
            if (VerifyLoad(filePath))
            {
                if (File.Exists(filePath))
                {
                    LoadProjectFile(filePath);
                }
                else
                {
                    //throw error
                    KeyValuePair<string, string> key = new KeyValuePair<string, string>(Path.GetFileNameWithoutExtension(filePath), filePath);
                    Globals.StartPageRecentFiles.Remove(key);
                    InputOutput.WriteRecentFiles();
                    UpdateRecentFileMenu();
                    LoadStartPage();
                }
            }
        }

        private void LoadProjectDialog(bool projSol = true)
        {
            string navPath = "";

            if (StudioSettings.PreviousPath != "" && Directory.Exists(StudioSettings.PreviousPath))
            {
                navPath = StudioSettings.PreviousPath;
            }
            else
            {
                navPath = Globals.ProjectDirectory;
            }

            OpenFileDialog dialog = new OpenFileDialog();
            dialog.InitialDirectory = navPath;
            dialog.Filter = projSol ? "GBA Studio File (*.gbasln,*.gbaproj) | *.gbasln;*.gbaproj" : "GBA Code File (*.gc,*.gh,*.gs) | *.gc;*.gh;*.gs,*.c,*.h,*.s,*.asm";

            string solutionPath = ""; //TODO: this needs to be updated to the solution we find or create upon loading a .gbaproj

            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string fileName = dialog.FileName;
                string extension = Path.GetExtension(fileName);
                StudioSettings.PreviousPath = Path.GetDirectoryName(fileName);
                InputOutput.WriteSettings();
                if (extension == ".gbasln" || extension == ".gbaproj")
                {
                    if (VerifyLoad(fileName))
                    {
                        if (Path.GetExtension(fileName) == ".gbasln")
                        {
                            solutionPath = fileName;
                        }

                        LoadProjectFile(fileName);
                    }
                }
                else
                {
                    MessageBox.Show("This is not the error you are looking for.\n\nDear Zach. I hereby decree that thou must pause'th here else reap the consequences of ones actions.\nWe must make changes here to allow for open confirmation. Just thought you ought to know.");
                    LoadProjectFile(fileName);
                }
            }
        }

        private TreeNode LoadTreeView(string location, Project p)
        {
            TreeNode item = new TreeNode();

            string[] parts = location.Split('\\');
            string displayName = parts[parts.Length - 1];

            item.Text = displayName;
            item.ForeColor = Color.White;
            item.Tag = "IsFolder";

            string[] dirs = Directory.GetDirectories(location);
            string[] files = Directory.GetFiles(location);

            foreach (string dir in dirs)
            {
                if (ShouldShowInTreeView(dir, true) && ContainsReferencedFile(dir, p))
                {
                    item.Nodes.Add(LoadTreeView(dir, p));
                }
            }

            Dictionary<string, TreeNode> filterNodes = new Dictionary<string, TreeNode>();

            foreach (KeyValuePair<string, KeyValuePair<string, string>> filter in p.Filters)
            {
                TreeNode tmpFilter = new TreeNode();
                tmpFilter.Tag = filter.Key;
                tmpFilter.Text = filter.Value.Key;

                filterNodes.Add(filter.Key, tmpFilter);

                item.Nodes.Add(tmpFilter);
            }

            foreach (string file in files)
            {
                if (ShouldShowInTreeView(file, false) && IsReferencedFile(file, p))
                {
                    TreeNode item2 = new TreeNode();
                    item2.Text = System.IO.Path.GetFileName(file);
                    item2.Tag = file;
                    item2.ForeColor = Color.White;

                    string guid = FileFilterGuid(p, file);

                    if (guid != null)
                    {
                        // in a filter
                        filterNodes[guid].Nodes.Add(item2);
                    }
                    else
                    {
                        item.Nodes.Add(item2);
                    }
                }
            }

            return item;
        }

        private TreeNode GetFolderNodeFromFolderNode(TreeNode node)
        {
            if (node == null)
            {
                return null;
            }

            if (node.Tag != null && node.Tag.ToString() == "IsFolder")
            {
                return node;
            }
            else
            {
                if (node.Parent != null)
                {
                    return GetFolderNodeFromFolderNode(node.Parent);
                }
                else
                {
                    return null;
                }
            }
        }

        private string GetProjectNameFromNode(TreeNode node)
        {
            if (node == null && solutionExplorer.Nodes.Count == 1)
            {
                node = solutionExplorer.Nodes[0];
            }

            if (node.Parent != null)
            {
                if (node.Parent.Tag != null)
                {
                    if (node.Parent.Tag.ToString() == "IsProject")
                    {
                        return node.Parent.Text;
                    }
                    else
                    {
                        if (node.Parent != null)
                        {
                            return GetProjectNameFromNode(node.Parent);
                        }
                        else
                        {
                            return "NotFound";
                        }
                    }
                }
                else
                {
                    if (node.Parent != null)
                    {
                        return GetProjectNameFromNode(node.Parent);
                    }
                    else
                    {
                        return "NotFound";
                    }
                }
            }
            else
            {
                if (node.Tag != null)
                {
                    if (node.Tag.ToString() == "IsProject")
                    {
                        return node.Text;
                    }
                    else
                    {
                        return "NotFound";
                    }
                }
                else
                {
                    return "NotFound";
                }
            }
        }

        private ContextMenu PrepareContextMenu(NodeType contextType)
        {
            ContextMenu rtn = new ContextMenu();

            switch (contextType)
            {
                case NodeType.Project:
                    rtn.MenuItems.Add(new MenuItem("Set as Default Project", SetProjectAsDefault_EventHandler));
                    rtn.MenuItems.Add(new MenuItem("-"));
                    rtn.MenuItems.Add(new MenuItem("Add File", AddFileToProject_EventHandler));
                    rtn.MenuItems.Add(new MenuItem("-"));
                    rtn.MenuItems.Add(new MenuItem("Delete", DeleteProject_EventHandler));
                    rtn.MenuItems.Add(new MenuItem("Remove Reference", RemoveFromSolution_EventHandler));
                    rtn.MenuItems.Add(new MenuItem("Show in Explorer", ShowInExplorer_EventHandler));
                    break;
                case NodeType.Folder:
                    rtn.MenuItems.Add(new MenuItem("New Folder", NewFolder_EventHandler));
                    rtn.MenuItems.Add(new MenuItem("Add Filter", AddFilterToFolder_EventHandler));
                    rtn.MenuItems.Add(new MenuItem("Add File", AddFileToFolder_EventHandler));
                    rtn.MenuItems.Add(new MenuItem("-"));
                    rtn.MenuItems.Add(new MenuItem("Delete", DeleteFolder_EventHandler));
                    break;
                case NodeType.Filter:
                    rtn.MenuItems.Add(new MenuItem("Add File", AddFileToFilter_EventHandler));
                    rtn.MenuItems.Add(new MenuItem("-"));
                    rtn.MenuItems.Add(new MenuItem("Rename", RenameFilter_EventHandler));
                    rtn.MenuItems.Add(new MenuItem("Remove", RemoveFilter_EventHandler));
                    break;
                case NodeType.File:
                    rtn.MenuItems.Add(new MenuItem("Open", OpenFile_EventHandler));
                    rtn.MenuItems.Add(new MenuItem("-"));
                    rtn.MenuItems.Add(new MenuItem("Remove Reference", RemoveFromProject_EventHandler));
                    rtn.MenuItems.Add(new MenuItem("Delete", DeleteFile_EventHandler));
                    rtn.MenuItems.Add(new MenuItem("-"));
                    rtn.MenuItems.Add(new MenuItem("Copy", CopyFile_EventHandler));
                    rtn.MenuItems.Add(new MenuItem("Cut", CutFile_EventHandler));
                    rtn.MenuItems.Add(new MenuItem("Paste", PasteFile_EventHandler));
                    rtn.MenuItems.Add(new MenuItem("Rename", RenameFile_EventHandler));
                    rtn.MenuItems.Add(new MenuItem("-"));
                    rtn.MenuItems.Add(new MenuItem("Add to New Filter", AddFilterWithFile_EventHandler));
                    break;
            }

            return rtn;
        }

        private TreeNode GetSelectedTreeNode(TreeNode node)
        {
            if (node.IsSelected)
            {
                return node;
            }
            else
            {
                if (node.Parent != null)
                {
                    return GetSelectedTreeNode(node.Parent);
                }
                else
                {
                    return null;
                }
            }
        }

        private void AddFileToProject_EventHandler(object sender, EventArgs e)
        {
            NewObjectMountTab(3);
        }

        private void DeleteFolder_EventHandler(object sender, EventArgs e)
        {
            if (solutionExplorer.SelectedNode.Text != Globals.CurrentSolution.GetProjectFromName(GetProjectNameFromNode(solutionExplorer.SelectedNode)).SourceFolder)
            {
                string folderPath = Path.GetDirectoryName(Path.GetDirectoryName(InputOutput.GetDrivePath(Globals.CurrentSolution.Path)) + @"\" + Path.GetDirectoryName(Globals.CurrentSolution.GetProjectFromName(GetProjectNameFromNode(solutionExplorer.SelectedNode)).Path) + @"\" + GetFolderPath(solutionExplorer.SelectedNode, ""));

                if (Directory.Exists(folderPath))
                {
                    Directory.Delete(folderPath, true);
                }

                Project p = Globals.CurrentSolution.GetProjectFromName(GetProjectNameFromNode(solutionExplorer.SelectedNode));

                solutionExplorer.SelectedNode.Remove();

                UpdateProject(p);
            }
        }

        private void NewFolder_EventHandler(object sender, EventArgs e)
        {
            TreeNode newNode = new TreeNode();

            TreeNode fromNode = solutionExplorer.SelectedNode;

            TextDialog t = new TextDialog("Folder Title");
            DialogResult res = t.ShowDialog();

            if (t.UserText.Length > 0 && res == System.Windows.Forms.DialogResult.OK)
            {
                newNode.Text = t.UserText;
                newNode.Tag = "IsFolder";

                if (fromNode.Tag.ToString() == "IsFolder")
                {
                    Project p = Globals.CurrentSolution.GetProjectFromName(GetProjectNameFromNode(fromNode));

                    string path = Path.GetDirectoryName(InputOutput.GetDrivePath(Globals.CurrentSolution.Path)) + @"\" + Path.GetDirectoryName(p.Path) + @"\" + GetFolderPath(fromNode, t.UserText);

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                }
                else if (fromNode.Tag.ToString() == "IsProject")
                {
                    Project p = Globals.CurrentSolution.GetProjectFromName(fromNode.Text);

                    string path = Path.GetDirectoryName(InputOutput.GetDrivePath(Globals.CurrentSolution.Path)) + @"\" + Path.GetDirectoryName(p.Path) + @"\" + t.UserText;

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                }

                fromNode.Nodes.Add(newNode);
            }
        }

        private string GetFolderPath(TreeNode node, string path)
        {
            string rtn = path;

            if (node.Tag.ToString() == "IsFolder")
            {
                rtn = node.Text + "\\" + path;
            }

            if (node.Parent != null && node.Parent.Tag != null && (node.Parent.Tag.ToString() == "IsFolder" || node.Parent.Tag.ToString() == "IsProject"))
            {
                rtn = GetFolderPath(node.Parent, rtn);
            }

            return rtn;
        }

        private void AddFileToFilter_EventHandler(object sender, EventArgs e)
        {
            newFileToNode = solutionExplorer.SelectedNode;

            NewObjectMountTab(3);
        }

        private void AddFileToFolder_EventHandler(object sender, EventArgs e)
        {
            newFileToNode = solutionExplorer.SelectedNode;
            NewObjectMountTab(3);
        }

        private void AddFilterToFolder_EventHandler(object sender, EventArgs e)
        {
            Forms.TextDialog nameDialog = new Forms.TextDialog("Please enter a name for your filter");

            nameDialog.TextBoxFocus();
            DialogResult result = nameDialog.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.Cancel && nameDialog.UserText != "")
            {
                string guid = Guid.NewGuid().ToString();

                TreeNode filter = new TreeNode();
                filter.Tag = guid;
                filter.Text = nameDialog.UserText;

                solutionExplorer.SelectedNode.Nodes.Add(filter);

                Project p = Globals.CurrentSolution.GetProjectFromName(GetProjectNodeFromNode(solutionExplorer.SelectedNode).Text);

                p.Filters.Add(new KeyValuePair<string, KeyValuePair<string, string>>(guid, new KeyValuePair<string, string>(nameDialog.UserText, solutionExplorer.SelectedNode.Text)));

                UpdateProject(p);
            }
        }

        private void RenameFilter_EventHandler(object sender, EventArgs e)
        {
            Forms.TextDialog t = new Forms.TextDialog("Rename filter '" + solutionExplorer.SelectedNode.Text + "'");
            t.UserText = solutionExplorer.SelectedNode.Text;

            if (t.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
            {
                if (t.UserText != solutionExplorer.SelectedNode.Text)
                {
                    Project p = Globals.CurrentSolution.GetProjectFromName(GetProjectNameFromNode(solutionExplorer.SelectedNode));

                    int i = 0;
                    int index = -1;
                    foreach (var filter in p.Filters)
                    {
                        if (filter.Value.Key == solutionExplorer.SelectedNode.Text)
                        {
                            index = i;
                        }

                        i++;
                    }

                    if (index >= 0)
                    {
                        KeyValuePair<string, KeyValuePair<string, string>> filter2 = p.Filters.ElementAt(index);

                        filter2 = new KeyValuePair<string, KeyValuePair<string, string>>(filter2.Key, new KeyValuePair<string, string>(t.UserText, filter2.Value.Value));

                        p.Filters[index] = filter2;

                        solutionExplorer.SelectedNode.Text = t.UserText;
                    }

                    UpdateProject(p);
                }
            }
        }

        private void RemoveFilter_EventHandler(object sender, EventArgs e)
        {
            TreeNode folderNode = solutionExplorer.SelectedNode.Parent;

            Project p = Globals.CurrentSolution.GetProjectFromName(GetProjectNameFromNode(folderNode));

            string filterGuid = solutionExplorer.SelectedNode.Tag.ToString();

            KeyValuePair<string, KeyValuePair<string, string>> filterToRemove = new KeyValuePair<string, KeyValuePair<string, string>>();

            foreach (var filter in p.Filters)
            {
                if (filter.Key == filterGuid)
                {
                    filterToRemove = filter;
                }
            }

            p.Filters.Remove(filterToRemove);

            List<KeyValuePair<string, string>> filesToRemoveFromFilter = new List<KeyValuePair<string, string>>();

            foreach (var fileFilter in p.FileFilters)
            {
                if (fileFilter.Key == filterGuid)
                {
                    filesToRemoveFromFilter.Add(fileFilter);
                }
            }

            foreach (var file in filesToRemoveFromFilter)
            {
                p.FileFilters.Remove(file);
            }

            List<TreeNode> fileNodes = new List<TreeNode>();

            foreach (TreeNode n in solutionExplorer.SelectedNode.Nodes)
            {
                n.Remove();
                folderNode.Nodes.Add(n);
            }

            solutionExplorer.SelectedNode.Remove();

            UpdateProject(p);
        }

        private void SetProjectAsDefault_EventHandler(object sender, EventArgs e)
        {
            Globals.CurrentSolution.SetDefaultProject(Globals.CurrentSolution.GetProjectFromName(solutionExplorer.SelectedNode.Text));
            UpdateSolution(Globals.CurrentSolution);
        }

        private void RemoveFromProject_EventHandler(object sender, EventArgs e)
        {
            DeleteFile(GetProjectNameFromNode(solutionExplorer.SelectedNode), solutionExplorer.SelectedNode.Tag.ToString(), false);

            solutionExplorer.SelectedNode.Remove();
        }

        private void DeleteFile_EventHandler(object sender, EventArgs e)
        {
            DeleteFile(GetProjectNameFromNode(solutionExplorer.SelectedNode), solutionExplorer.SelectedNode.Tag.ToString(), true);

            solutionExplorer.SelectedNode.Remove();
        }

        private void DeleteFile(string projectName, string filePath, bool shouldDelete)
        {
            Project p = Globals.CurrentSolution.GetProjectFromName(projectName);

            int tabIndex = tabControlMain.GetTabByTitle(Path.GetFileName(filePath));

            if (tabIndex < 0)
            {
                tabIndex = tabControlMain.GetTabByTitle(Path.GetFileName(filePath) + " *");
            }

            if (tabIndex >= 0)
            {
                if (InputOutput.GetDrivePath(((EditPad)tabControlMain.Tabs[tabIndex].Child).FilePath) == InputOutput.GetDrivePath(filePath))
                {
                    tabControlMain.DeleteTab(tabIndex);
                }
            }
            p.DeleteFile(filePath, shouldDelete);

            UpdateProject(p);
        }

        private void RemoveFromSolution_EventHandler(object sender, EventArgs e)
        {
            DeleteProject(solutionExplorer.SelectedNode.Text, false);
        }

        private void ShowInExplorer_EventHandler(object sender, EventArgs e)
        {
            string dir = Path.GetDirectoryName(Globals.CurrentSolution.Path);
            Process.Start("explorer.exe", dir);
        }

        private void DeleteProject_EventHandler(object sender, EventArgs e)
        {
            DeleteProject(solutionExplorer.SelectedNode.Text, true);
        }

        private void DeleteProject(string projectName, bool shouldDeleteDirectory)
        {
            Project p = Globals.CurrentSolution.GetProjectFromName(projectName);

            Globals.CurrentSolution.DeleteProject(p, shouldDeleteDirectory);

            if (Globals.CurrentSolution.Projects.Count == 0)
            {
                stateManager.Transition(StudioState.NoProject);
            }
            else
            {
                if (Globals.CurrentSolution.GetDefaultProject() == null)
                {
                    Globals.CurrentSolution.SetDefaultProject(Globals.CurrentSolution.Projects[0]);
                }
            }

            UpdateSolution(Globals.CurrentSolution);
        }

        private void OpenFile_EventHandler(object sender, EventArgs e)
        {
            OpenOrSelectTab(Globals.CurrentSolution.GetProjectFromName(GetProjectNameFromNode(solutionExplorer.SelectedNode)), solutionExplorer.SelectedNode.Tag.ToString());
        }

        private void CopyFile_EventHandler(object sender, EventArgs e)
        {
            copiedFile = true;
            cutFile = false;
            clipboardFilePath = solutionExplorer.SelectedNode.Tag.ToString();
        }

        private void CutFile_EventHandler(object sender, EventArgs e)
        {
            copiedFile = false;
            cutFile = true;
            clipboardFilePath = solutionExplorer.SelectedNode.Tag.ToString();
        }

        private void PasteFile_EventHandler(object sender, EventArgs e)
        {
            TreeNode folderNode = GetFolderNodeFromFolderNode(solutionExplorer.SelectedNode);
            Project p = Globals.CurrentSolution.GetProjectFromName(GetProjectNameFromNode(folderNode));

            string fileName = Path.GetFileName(clipboardFilePath);

            string destPath = Path.GetDirectoryName(Globals.CurrentSolution.Path) + @"\" + Path.GetDirectoryName(p.Path) + @"\" + folderNode.Text + @"\" + fileName;

            string[] parts = Path.GetDirectoryName(destPath).Split('\\');

            string clipboardFolder = parts.Last();

            if (File.Exists(destPath))
            {
                Errors.Throw(ErrorType.PasteFileFailed);
                return;
            }

            if (copiedFile)
            {
                p.AddFile(folderNode.Text + @"\" + fileName);

                File.Copy(clipboardFilePath, destPath);
            }
            else if (cutFile)
            {
                p.RemoveFile(clipboardFolder + @"\" + fileName);
                p.AddFile(folderNode.Text + @"\" + fileName);

                File.Move(clipboardFilePath, destPath);

                copiedFile = false;
                cutFile = false;
                clipboardFilePath = "";
            }
        }

        private void ChangeIncludesInProject(TreeNode node, string prevPath, string newPath)
        {
            if (node.Tag != null)
            {
                if (File.Exists(node.Tag.ToString()))
                {
                    SaveAll();

                    string text = File.ReadAllText(node.Tag.ToString());

                    Regex r = new Regex("(#include [\"'])([^\"']+)([\"'])");
                    MatchCollection matches = r.Matches(text);

                    foreach (Match match in matches)
                    {
                        string fileIncluded = match.Groups[2].Value;

                        if (prevPath.EndsWith(fileIncluded.Replace(".h", ".gh").Replace(".c", ".gc")))
                        {
                            text = text.Replace(match.Value, match.Result("$1" + Path.GetFileName(newPath) + "$3"));

                            File.WriteAllText(node.Tag.ToString(), text);

                            for (int i = 0; i < tabControlMain.Tabs.Count; i++)
                            {
                                try
                                {
                                    EditPad ep = (EditPad)tabControlMain.Tabs[i].Child;

                                    if (ep.FilePath.ToLower() == node.Tag.ToString().ToLower())
                                    {
                                        ep.Text = text;
                                        ep.Save();
                                    }
                                }
                                catch
                                {

                                }
                            }

                        }
                    }
                }
            }

            foreach (TreeNode n in node.Nodes)
            {
                ChangeIncludesInProject(n, prevPath, newPath);
            }
        }

        private void RenameFile_EventHandler(object sender, EventArgs e)
        {
            Forms.TextDialog t = new Forms.TextDialog("Rename file '" + solutionExplorer.SelectedNode.Text + "'");

            string extension = Path.GetExtension(solutionExplorer.SelectedNode.Text);

            t.UserText = Path.GetFileNameWithoutExtension(solutionExplorer.SelectedNode.Text);

            DialogResult result = t.ShowDialog();

            if (result == DialogResult.OK)
            {
                if (t.UserText != Path.GetFileNameWithoutExtension(solutionExplorer.SelectedNode.Text))
                {
                    Project p = Globals.CurrentSolution.GetProjectFromName(GetProjectNameFromNode(solutionExplorer.SelectedNode));
                    string prevPath = solutionExplorer.SelectedNode.Tag.ToString();
                    string newPath = solutionExplorer.SelectedNode.Tag.ToString().Replace(Path.GetFileNameWithoutExtension(solutionExplorer.SelectedNode.Text) + ".", t.UserText + ".");

                    if (!p.ContainsFile(Path.GetFileName(newPath)))
                    {
                        if (!File.Exists(newPath))
                        {
                            ChangeIncludesInProject(GetProjectNodeFromNode(solutionExplorer.SelectedNode), prevPath, newPath);

                            solutionExplorer.SelectedNode.Tag = newPath;

                            for (int i = 0; i < p.ReferencedFiles.Count; i++)
                            {
                                if (prevPath.EndsWith(p.ReferencedFiles[i]))
                                {
                                    p.ReferencedFiles[i] = p.ReferencedFiles[i].Replace(Path.GetFileNameWithoutExtension(solutionExplorer.SelectedNode.Text) + ".", t.UserText + ".");
                                }
                            }

                            solutionExplorer.SelectedNode.Text = t.UserText + extension;

                            File.Move(prevPath, newPath);
                        }

                        foreach (TabControl.TabControlPro.TabData td in tabControlMain.Tabs)
                        {
                            if (td.Tag != null)
                            {
                                if (td.Tag.ToString() == prevPath)
                                {
                                    td.Title = Path.GetFileName(newPath);
                                    td.Tag = newPath;
                                    tabControlMain.Invalidate();
                                    break;
                                }
                            }
                        }
                        UpdateProject(p);
                    }
                    else
                    {
                        Errors.Throw(ErrorType.FileNameExists);
                    }
                }
            }
        }

        private void AddFilterWithFile_EventHandler(object sender, EventArgs e)
        {
            Forms.TextDialog nameDialog = new Forms.TextDialog("Please enter a name for your filter");

            DialogResult result = nameDialog.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.Cancel)
            {
                string guid = Guid.NewGuid().ToString();

                TreeNode filter = new TreeNode();
                filter.Tag = guid;
                filter.Text = nameDialog.UserText;

                TreeNode folderNode = GetFolderNodeFromFolderNode(solutionExplorer.SelectedNode);
                Project p = Globals.CurrentSolution.GetProjectFromName(GetProjectNodeFromNode(folderNode).Text);

                TreeNode fileNode = solutionExplorer.SelectedNode;

                solutionExplorer.SelectedNode.Remove();
                filter.Nodes.Add(fileNode);

                folderNode.Nodes.Add(filter);

                p.Filters.Add(new KeyValuePair<string, KeyValuePair<string, string>>(guid, new KeyValuePair<string, string>(nameDialog.UserText, folderNode.Text)));
                p.FileFilters.Add(new KeyValuePair<string, string>(guid, fileNode.Tag.ToString()));

                UpdateProject(p);
            }
        }

        private void LoadProjectTreeView(string projectName, string projectLocation, bool shouldClear)
        {
            if (shouldClear)
            {
                solutionExplorer.Nodes.Clear();
            }

            Project p = Globals.CurrentSolution.GetProjectFromName(projectName);

            TreeNode projectNode = new TreeNode();
            projectNode.Text = projectName;
            projectNode.Tag = "IsProject";

            string[] dirs = Directory.GetDirectories(Path.GetDirectoryName(projectLocation));
            string[] files = Directory.GetFiles(Path.GetDirectoryName(projectLocation));

            foreach (string dir in dirs)
            {
                if (ShouldShowInTreeView(dir, true) && ContainsReferencedFile(dir, Globals.CurrentSolution.GetProjectFromName(projectName)))
                {
                    projectNode.Nodes.Add(LoadTreeView(dir, Globals.CurrentSolution.GetProjectFromName(projectName)));
                }
            }

            Dictionary<string, TreeNode> filterNodes = new Dictionary<string, TreeNode>();

            foreach (KeyValuePair<string, KeyValuePair<string, string>> filter in p.Filters)
            {
                if (!CheckForFilter(projectNode, filter))
                {
                    TreeNode tmpFilter = new TreeNode();
                    tmpFilter.Tag = filter.Key;
                    tmpFilter.Text = filter.Value.Key;

                    filterNodes.Add(filter.Key, tmpFilter);

                    GetFolderTreeNodeFromText(projectNode, filter.Value.Value).Nodes.Add(tmpFilter);
                }
            }

            foreach (string file in files)
            {
                if (ShouldShowInTreeView(file, false) && IsReferencedFile(file, Globals.CurrentSolution.GetProjectFromName(projectName)))
                {
                    TreeNode item2 = new TreeNode();

                    item2.Text = System.IO.Path.GetFileName(file);
                    item2.ForeColor = Color.White;
                    item2.Tag = file;

                    string guid = FileFilterGuid(p, file);

                    if (guid != null)
                    {
                        // in a filter
                        filterNodes[guid].Nodes.Add(item2);
                    }
                    else
                    {
                        projectNode.Nodes.Add(item2);
                    }
                }
            }

            solutionExplorer.Nodes.Add(projectNode);
        }

        private bool CheckForFilter(TreeNode node, KeyValuePair<string, KeyValuePair<string, string>> filter)
        {
            if (node != null)
            {
                if (node.Tag.ToString() == filter.Key)
                {
                    return true;
                }
                else
                {
                    foreach (TreeNode n in node.Nodes)
                    {
                        return CheckForFilter(n, filter);
                    }
                }
            }

            return false;
        }

        private TreeNode GetFolderTreeNodeFromText(TreeNode node, string text)
        {
            if (node != null)
            {
                if (node.Text == text)
                {
                    return node;
                }
                else
                {
                    foreach (TreeNode n in node.Nodes)
                    {
                        return GetFolderTreeNodeFromText(n, text);
                    }
                }
            }

            return null;
        }

        private string FileFilterGuid(Project p, string filePath)
        {
            foreach (KeyValuePair<string, string> fileFilter in p.FileFilters)
            {
                if (filePath.EndsWith(fileFilter.Value))
                {
                    return fileFilter.Key;
                }
            }

            return null;
        }

        private bool IsReferencedFile(string filePath, Project project)
        {
            foreach (string file in project.ReferencedFiles)
            {
                if (filePath.EndsWith(file))
                {
                    return true;
                }
            }

            return false;
        }

        private bool ContainsReferencedFile(string directoryPath, Project project)
        {
            foreach (string dir in Directory.GetDirectories(directoryPath))
            {
                return ContainsReferencedFile(dir, project);
            }

            foreach (string file in project.ReferencedFiles)
            {
                string fileFolder = Path.GetDirectoryName(file);

                if (directoryPath.EndsWith(fileFolder))
                {
                    return true;
                }
            }

            return false;
        }

        private bool ShouldShowInTreeView(string path, bool directory)
        {
            if (directory)
            {
                string[] parts = path.Split('\\');
                string useThis = parts[parts.Length - 1];

                if (Globals.IgnoreFolders.Contains(useThis))
                {
                    return false;
                }
            }
            else
            {
                if (Globals.IgnoreExtensions.Contains(Path.GetExtension(path).Replace(".", "")) || Globals.IgnoreFileNames.Contains(Path.GetFileNameWithoutExtension(path)))
                {
                    return false;
                }
            }

            return true;
        }

        private string FindSolutionPath(string currentDirectory)
        {
            string[] files = Directory.GetFiles(currentDirectory);

            foreach (string file in files)
            {
                if (Path.GetExtension(file) == ".gbasln")
                {
                    string fileContent = File.ReadAllText(file);

                    XmlDocument doc = new XmlDocument();

                    try
                    {
                        doc.LoadXml(fileContent);

                        XmlNodeList projects = doc.DocumentElement.SelectNodes("/Solution/Projects/Project");

                        foreach (XmlNode project in projects)
                        {
                            string projectTitle = project.SelectSingleNode("Name").InnerText;

                            if (Path.GetFileNameWithoutExtension(file) == projectTitle)
                            {
                                return file;
                            }
                        }
                    }
                    catch
                    {

                    }
                }
            }

            string val = FindSolutionPath(Path.GetDirectoryName(currentDirectory));

            if (val != null)
            {
                return val;
            }

            return null;
        }

        private void LoadProjectFile(string projectFile)
        {
            if (File.Exists(projectFile))
            {
                string extension = Path.GetExtension(projectFile);
                string rootDir = Path.GetDirectoryName(projectFile);

                if (extension == ".gbaproj" && Globals.CurrentSolution == null)
                {
                    // we need to find a solution or stop the function here.
                    string solutionDir = FindSolutionPath(rootDir);

                    if (solutionDir != null)
                    {
                        LoadProjectFile(solutionDir);
                        return;
                    }
                    else
                    {
                        return;
                    }
                }

                if (extension == ".gbasln")
                {
                    ClearConsole();
                    try
                    {
                        Globals.CurrentSolution = new Solution(projectFile);
                        Globals.CurrentSolution.DefaultProjectChanged += CurrentSolution_DefaultProjectChanged;
                        XmlDocument reader = new XmlDocument();
                        reader.LoadXml(File.ReadAllText(projectFile).Replace("\r\n", ""));

                        Print("Solution: " + Path.GetFileNameWithoutExtension(projectFile) + " successfully loaded.");

                        solutionExplorer.Nodes.Clear();
                        Globals.IsSolutionOpen = true;
                        string majorVersion = reader.DocumentElement.SelectSingleNode("/Solution/Version/Major").InnerText;
                        string minorVersion = reader.DocumentElement.SelectSingleNode("/Solution/Version/Minor").InnerText;
                        string buildVersion = reader.DocumentElement.SelectSingleNode("/Solution/Version/Build").InnerText;

                        bool evalMode = reader.DocumentElement.SelectSingleNode("/Solution/EvaluationMode").InnerText == "Yes" ? true : false;

                        Version v = new Version(int.Parse(majorVersion), int.Parse(minorVersion), int.Parse(buildVersion));

                        int comparison = Globals.CurrentVersion.CompareTo(v);

                        if (comparison < 0)
                        {
                            Errors.Throw(ErrorType.VersionMismatch);
                            return;
                        }
                        else if (comparison > 0)
                        {
                            //UpdateSolution(projectFile);
                        }

                        XmlNodeList projects = reader.DocumentElement.SelectNodes("/Solution/Projects/Project");
                        if (stateManager.Transition(StudioState.NoProject))
                        {
                            foreach (XmlNode project in projects)
                            {
                                string projectTitle = project.SelectSingleNode("Name").InnerText;
                                string projectPath = projectTitle + @"\" + projectTitle + ".gbaproj";

                                bool isDefault = project.SelectSingleNode("IsDefault").InnerText == "Yes" ? true : false;

                                if (isDefault)
                                {
                                    string dir = (rootDir + @"\" + Path.GetDirectoryName(projectPath));
                                    ChangeDirectory(dir);
                                    Print("Project: " + projectTitle + " successfully loaded as default.");
                                }
                                else
                                {
                                    Print("Project: " + projectTitle + " successfully loaded.");
                                }

                                Project tmpProj = new Project(projectPath);
                                tmpProj.Default = isDefault;
                                Globals.CurrentSolution.Projects.Add(tmpProj);

                                LoadProjectFile(rootDir + @"\" + projectPath);


                                UpdateAllProjects();
                                UpdateSolution(Globals.CurrentSolution);
                            }
                        }
                        Globals.UpdateRecentFiles(Path.GetFileNameWithoutExtension(projectFile), projectFile);
                        LoadStartPage();
                        UpdateRecentFileMenu();

                        if (evalMode)
                        {
                            ToggleEvaluationMode(false);
                        }
                        senseEngine.Build();
                    }
                    catch
                    {
                        stateManager.Rollback();
                        //TODO:REMOVE ANY DATA LOADED IN BY THE SOLUTION

                        Globals.CurrentSolution = null;


                        Errors.Throw(ErrorType.CorruptSolution);
                    }
                }
                else if (extension == ".gbaproj")
                {
                    try
                    {


                        //                    LoadTreeViewFirst(projectFile.Replace(@"\" + Path.GetFileName(projectFile), ""), false);
                        Project curProj = Globals.CurrentSolution.GetProjectFromName(Path.GetFileNameWithoutExtension(projectFile));

                        XmlDocument reader = new XmlDocument();
                        reader.LoadXml(File.ReadAllText(projectFile).Replace("\r\n", ""));

                        XmlNodeList filters = reader.DocumentElement.SelectNodes("/Project/Filters/Filter");

                        foreach (XmlNode filter in filters)
                        {
                            string guid = filter.SelectSingleNode("Guid").InnerText;
                            string name = filter.SelectSingleNode("Name").InnerText;
                            string folder = filter.SelectSingleNode("Folder").InnerText;

                            KeyValuePair<string, KeyValuePair<string, string>> filterPair = new KeyValuePair<string, KeyValuePair<string, string>>(guid, new KeyValuePair<string, string>(name, folder));

                            curProj.Filters.Add(filterPair);
                        }

                        XmlNodeList referencedFiles = reader.DocumentElement.SelectNodes("/Project/ReferencedFiles/File");

                        foreach (XmlNode referenceFile in referencedFiles)
                        {
                            string filePath = referenceFile.SelectSingleNode("FilePath").InnerText;

                            XmlNode filterGuid = referenceFile.SelectSingleNode("FilterGuid");

                            if (filterGuid != null)
                            {
                                curProj.FileFilters.Add(new KeyValuePair<string, string>(filterGuid.InnerText, filePath));
                            }

                            curProj.ReferencedFiles.Add(filePath);
                        }

                        XmlNodeList openFiles = reader.DocumentElement.SelectNodes("/Project/OpenFiles/File");

                        foreach (XmlNode openFile in openFiles)
                        {
                            string filePath = Path.GetDirectoryName(Globals.CurrentSolution.Path) + @"\" + Path.GetFileNameWithoutExtension(projectFile) + @"\" + openFile.InnerText;

                            if (File.Exists(filePath))
                            {
                                OpenOrSelectTab(Globals.CurrentSolution.GetProjectFromName(Path.GetFileNameWithoutExtension(projectFile)), filePath);
                            }
                        }

                        if (stateManager.Transition(StudioState.ProjectSolutionOpen))
                        {
                            LoadProjectTreeView(Path.GetFileNameWithoutExtension(projectFile), projectFile, false);
                        }
                    }
                    catch (Exception e)
                    {
                        stateManager.Rollback();
                        //TODO:REMOVE ANY DATA LOADED IN BY THE SOLUTION

                        Globals.CurrentSolution = null;

                        Errors.Throw(ErrorType.CorruptProject);
                    }
                }
                else if (Globals.AllowedExtensions.Contains(extension))
                {
                    OpenOrSelectTab(null, projectFile);
                }
                else
                {
                    // unsupported file
                    Errors.Throw(ErrorType.FileTypeInvalid);
                }
            }
            else
            {
                // not a real file :(
                Errors.Throw(ErrorType.FileNotFound);
            }
        }

        void CurrentSolution_DefaultProjectChanged(object sender, Solution.ProjectEventArgs e)
        {
            string projectName = Path.GetFileNameWithoutExtension(e.Project.Path);

            ChangeDirectory(Path.GetDirectoryName(InputOutput.GetDrivePath(((Solution)sender).Path)) + @"\" + Path.GetDirectoryName(e.Project.Path));

            Print(projectName + " set as default project.");
        }

        private void ChangeDirectory(string newDir)
        {
            ExecuteCommand("cd " + newDir.Replace("\\", "/").Replace(" ", @"\ "));
        }

        #endregion

        #region IN PROGRESS: Creating

        private void NewProject(string projectName)
        {
            if (stateManager.Transition(StudioState.ProjectSolutionOpen))
            {
                string projectDirectory = Path.GetDirectoryName(Globals.CurrentSolution.Path) + "\\" + projectName;
                if (!Directory.Exists(projectDirectory))
                {
                    if (CheckAndCreateDirectory(projectDirectory))
                    {
                        string projPath = projectName + "\\" + projectName + ".gbaproj";

                        Project p = new Project(projPath);

                        Globals.CurrentSolution.Projects.Add(p);

                        if (Globals.CurrentSolution.Projects.Count == 1)
                        {
                            ChangeDirectory(projectDirectory);
                        }

                        if (Globals.CurrentSolution.GetDefaultProject() == null)
                        {
                            Globals.CurrentSolution.SetDefaultProject(p);
                        }

                        InputOutput.WriteToFile(Path.GetDirectoryName(Globals.CurrentSolution.Path) + "\\" + projPath, InputOutput.ReadFromResources("GBA_Studio.Resources.Text_Templates.EmptyProject.txt"));
                        //TODO:Add XML Code to modify the solution

                        UpdateSolution(Globals.CurrentSolution);

                        GenerateMakeFile(p);
                    }
                    else
                    {
                        stateManager.Rollback();
                    }
                }
                else
                {
                    stateManager.Rollback();
                    Errors.Throw(ErrorType.ProjectExists);
                }
            }
        }

        private void NewSolution(string projectName = "")
        {
            if (stateManager.Transition(StudioState.ProjectSolutionOpen))
            {
                projectName = projectName.Replace(' ', '_');
                string solutionDirectory = Globals.ProjectDirectory + "\\" + projectName;
                string projectDirectory = solutionDirectory + "\\" + projectName;
                if (VerifyLoad(solutionDirectory))
                {
                    if (!Path.GetDirectoryName(projectDirectory).Contains(' '))
                    {
                        if (Directory.Exists(solutionDirectory))
                        {
                            Errors.Throw(ErrorType.SolutionExists);
                            stateManager.Rollback();
                        }
                        else
                        {
                            bool slnDirCreated = CheckAndCreateDirectory(solutionDirectory);
                            bool projDirCreated = CheckAndCreateDirectory(projectDirectory);
                            if (slnDirCreated && projDirCreated)
                            {

                                string projPath = projectName + "\\" + projectName + ".gbaproj";
                                Project newProj = new Project(projPath);

                                string slnPath = solutionDirectory + "\\" + projectName + ".gbasln";
                                Solution newSol = new Solution(slnPath, newProj);
                                newSol.DefaultProjectChanged += CurrentSolution_DefaultProjectChanged;

                                newSol.SetDefaultProject(newProj);

                                Globals.CurrentSolution = newSol;

                                InputOutput.WriteToFile(Path.GetDirectoryName(Globals.CurrentSolution.Path) + "\\" + projPath, InputOutput.ReadFromResources("GBA_Studio.Resources.Text_Templates.EmptyProject.txt"));

                                UpdateSolution(newSol);
                                Globals.UpdateRecentFiles(projectName, solutionDirectory + "\\" + projectName + ".gbasln");
                                UpdateRecentFileMenu();
                                LoadStartPage();
                                GenerateMakeFile(newProj);
                                ChangeDirectory(solutionDirectory + "\\" + projectName);
                            }
                            else
                            {
                                if (Directory.Exists(solutionDirectory))
                                {
                                    Directory.Delete(solutionDirectory, true);
                                }
                                if (Directory.Exists(projectDirectory))
                                {
                                    Directory.Delete(projectDirectory, true);
                                }
                            }
                        }

                    }
                    else
                    {
                        Errors.Throw(ErrorType.InvalidSolutionPath);
                        stateManager.Rollback();
                    }
                }
            }
        }

        private void NewFile(string fileName, bool autoOpen = false, TreeNode addToNode = null)
        {
            Project currentProject = Globals.CurrentSolution.GetProjectFromName(GetProjectNameFromNode(solutionExplorer.SelectedNode));

            if (!currentProject.ContainsFile(fileName))
            {
                string projectPath = Path.GetDirectoryName(InputOutput.GetDrivePath(Globals.CurrentSolution.Path)) + @"\" + Path.GetFileNameWithoutExtension(InputOutput.GetDrivePath(currentProject.Path)) + @"\";

                string folderPath = "";

                if (addToNode != null && addToNode.Tag != null && addToNode.Tag.ToString() == "IsFolder")
                {
                    folderPath = Path.GetDirectoryName(Path.GetDirectoryName(InputOutput.GetDrivePath(Globals.CurrentSolution.Path)) + @"\" + Path.GetDirectoryName(currentProject.Path) + @"\" + GetFolderPath(addToNode, ""));
                }

                currentProject.NewFile(fileName, folderPath);

                if (addToNode != null && addToNode.Tag != null && !File.Exists(addToNode.Tag.ToString()) && addToNode.Tag.ToString() != "IsProject" && addToNode.Tag.ToString() != "IsFolder")
                {
                    // its a filter
                    currentProject.ApplyToFilter(currentProject.ReferencedFiles.Count - 1, addToNode.Tag.ToString());
                }

                if (!Directory.Exists(projectPath + currentProject.SourceFolder))
                {
                    Directory.CreateDirectory(projectPath + currentProject.SourceFolder);
                }

                string data = "";

                if (Path.GetExtension(fileName) == ".gs")
                {
                    data = Globals.AssemblyStub;

                    data = data.Replace("{{NAME}}", Path.GetFileNameWithoutExtension(fileName.Replace(" ", "").Replace("_", "")));
                }
                else if (Path.GetExtension(fileName) == ".gh")
                {
                    data = Globals.HeaderStub;

                    Regex r = new Regex(@"([A-Z])");

                    data = data.Replace("{{NAME}}", Path.GetFileNameWithoutExtension(fileName.Substring(0, 1).ToUpper() + r.Replace(fileName.Substring(1), "_$1").ToUpper()));
                }

                //FileName
                //FILE_NAME

                InputOutput.WriteToFile(projectPath + (folderPath != "" ? GetFolderPath(addToNode, "") : currentProject.SourceFolder + @"\") + fileName, data);

                UpdateProject(currentProject);
                UpdateSolutionExplorer();

                foreach (TreeNode n in solutionExplorer.Nodes)
                {
                    if (n.Text == Path.GetFileNameWithoutExtension(currentProject.Path))
                    {
                        n.Expand();
                        if (currentProject.SourceFolder != "" && currentProject.SourceFolder != null && n.Nodes != null && n.Nodes.Count > 0)
                        {
                            n.Nodes[0].Expand();
                        }
                    }
                }

                if (addToNode != null)
                {
                    TreeNode toAdd = new TreeNode();
                    toAdd.Text = fileName;
                    toAdd.Tag = projectPath + (folderPath != "" ? GetFolderPath(addToNode, "") : currentProject.SourceFolder + @"\") + fileName;


                    addToNode.Nodes.Add(toAdd);

                    addToNode.Expand();

                    UpdateProject(currentProject);
                }

                if (autoOpen)
                {
                    OpenOrSelectTab(currentProject, projectPath + (folderPath != "" ? GetFolderPath(addToNode, "") : currentProject.SourceFolder + @"\") + fileName);
                }
            }
            else
            {
                Errors.Throw(ErrorType.FileNameExists);
            }
        }

        #endregion

        #region TODO: Updating

        private void UpdateProject(Project proj)
        {
            string projPath = Path.GetDirectoryName(Globals.CurrentSolution.Path) + @"\" + Path.GetDirectoryName(proj.Path);

            string xml = InputOutput.ReadFromResources("GBA_Studio.Resources.Text_Templates.EmptyProject.txt");

            XmlDocument projXml = new XmlDocument();
            projXml.LoadXml(xml);

            XmlNode isDefault = projXml.SelectSingleNode("/Project/IsDefault");
            isDefault.InnerText = proj.Default ? "Yes" : "No";

            XmlNode openFiles = projXml.SelectSingleNode("/Project/OpenFiles");

            // <Project><Name></Name><Path></Path></Project> 

            foreach (string file in proj.OpenFiles)
            {
                XmlNode fileName = projXml.CreateNode(XmlNodeType.Element, "File", null);
                fileName.InnerText = file;

                openFiles.AppendChild(fileName);
            }

            XmlNode referencedFiles = projXml.SelectSingleNode("/Project/ReferencedFiles");

            foreach (string file in proj.ReferencedFiles)
            {
                string fullPath = projPath + @"\" + file;

                if (File.Exists(fullPath))
                {
                    XmlNode fileName = projXml.CreateNode(XmlNodeType.Element, "File", null);

                    XmlNode filePath = projXml.CreateNode(XmlNodeType.Element, "FilePath", null);
                    filePath.InnerText = file;

                    fileName.AppendChild(filePath);

                    foreach (KeyValuePair<string, string> fileFilter in proj.FileFilters)
                    {
                        string guid = fileFilter.Key;
                        string filterFile = fileFilter.Value;

                        if (filterFile.EndsWith(file))
                        {
                            XmlNode filter = projXml.CreateNode(XmlNodeType.Element, "FilterGuid", null);
                            filter.InnerText = guid;

                            fileName.AppendChild(filter);
                        }
                    }

                    referencedFiles.AppendChild(fileName);
                }
            }

            XmlNode filters = projXml.SelectSingleNode("/Project/Filters");

            if (filters != null) // backward compatibility error check
            {
                foreach (KeyValuePair<string, KeyValuePair<string, string>> filter in proj.Filters)
                {
                    string guid = filter.Key;
                    string filterName = filter.Value.Key;
                    string folder = filter.Value.Value;

                    XmlNode filterNode = projXml.CreateNode(XmlNodeType.Element, "Filter", null);

                    XmlNode guidNode = projXml.CreateNode(XmlNodeType.Element, "Guid", null);
                    XmlNode filterNameNode = projXml.CreateNode(XmlNodeType.Element, "Name", null);
                    XmlNode filterFolderNode = projXml.CreateNode(XmlNodeType.Element, "Folder", null);

                    guidNode.InnerText = guid;
                    filterNameNode.InnerText = filterName;
                    filterFolderNode.InnerText = folder;

                    filterNode.AppendChild(guidNode);
                    filterNode.AppendChild(filterNameNode);
                    filterNode.AppendChild(filterFolderNode);

                    filters.AppendChild(filterNode);
                }
            }
            InputOutput.WriteXmlToFile(Path.GetDirectoryName(Globals.CurrentSolution.Path) + @"\" + proj.Path, projXml);
        }

        private void UpdateAllProjects()
        {
            foreach (Project p in Globals.CurrentSolution.Projects)
            {
                UpdateProject(p);
            }
        }

        private void UpdateSolution(Solution sol)
        {
            string xml = InputOutput.ReadFromResources("GBA_Studio.Resources.Text_Templates.EmptySolution.txt");

            XmlDocument slnXml = new XmlDocument();
            slnXml.LoadXml(xml.Replace("\r", "").Replace("\n", "").Replace("\t", ""));

            XmlNode projects = slnXml.SelectSingleNode("/Solution/Projects");

            // <Project><Name></Name><Path></Path></Project> 

            XmlNode version = slnXml.SelectSingleNode("/Solution/Version");

            version.SelectSingleNode("Major").InnerText = Globals.CurrentVersion.Major.ToString();
            version.SelectSingleNode("Minor").InnerText = Globals.CurrentVersion.Minor.ToString();
            version.SelectSingleNode("Build").InnerText = Globals.CurrentVersion.Build.ToString();
            version.SelectSingleNode("Revision").InnerText = Globals.CurrentVersion.Revision.ToString();

            XmlNode evaluationMode = slnXml.SelectSingleNode("/Solution/EvaluationMode");
            evaluationMode.InnerText = sol.EvaluationMode ? "Yes" : "No";

            solutionExplorer.Nodes.Clear();

            foreach (Project p in sol.Projects)
            {
                XmlNode project = slnXml.CreateNode(XmlNodeType.Element, "Project", null);

                XmlNode isDefault = slnXml.CreateNode(XmlNodeType.Element, "IsDefault", null);
                isDefault.InnerText = p.Default ? "Yes" : "No";

                XmlNode projName = slnXml.CreateNode(XmlNodeType.Element, "Name", null);
                projName.InnerText = Path.GetFileNameWithoutExtension(p.Path);

                XmlNode projPath = slnXml.CreateNode(XmlNodeType.Element, "Path", null);
                projPath.InnerText = p.Path;

                project.AppendChild(projName);
                project.AppendChild(projPath);
                project.AppendChild(isDefault);
                projects.AppendChild(project);

                LoadProjectTreeView(Path.GetFileNameWithoutExtension(p.Path), Path.GetDirectoryName(sol.Path) + @"\" + p.Path, false);
            }

            InputOutput.WriteToFile(sol.Path, slnXml.InnerXml);
        }

        #endregion

        #region Misc



        #endregion

        #endregion
        #region Event Handling

        void Globals_SolutionOpenChanged(object sender, EventArgs e)
        {
            // sender is a boolean
            //UpdateButtons((bool)sender);

            if ((bool)sender == true)
            {
                // solution has been loaded
            }
            else
            {
                // solution has been closed

                memberExplorer.Nodes.Clear();
                solutionExplorer.Nodes.Clear();

                Globals.CurrentSolution = null;

                // toolbars
            }
        }

        private void CodePad_TextChanged(object sender, EventArgs e)
        {
            UpdateMemberExplorer();

            EditPad ep = (EditPad)sender;

            string title = tabControlMain.Tabs[tabControlMain.SelectedIndex].Title;

            if (ep.IsInDate())
            {
                if (title.EndsWith(" *"))
                {
                    title = title.Substring(0, title.Length - 2);
                    tabControlMain.Invalidate();
                }
            }
            else
            {
                if (!title.EndsWith(" *"))
                {
                    title = title + " *";
                    tabControlMain.Invalidate();
                }
            }

            tabControlMain.Tabs[tabControlMain.SelectedIndex].Title = title;
        }

        private void toolStripContainer1_TopToolStripPanel_ClientSizeChanged(object sender, EventArgs e)
        {
            splitContainer1.SplitterDistance = toolStripContainer1.TopToolStripPanel.Height;
        }

        private void solutionExplorer_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Tag != null && File.Exists(e.Node.Tag.ToString()))
            {
                string file = e.Node.Tag.ToString();

                string projectName = GetProjectNameFromNode(e.Node);

                string localFile = file.Replace(Path.GetDirectoryName(Globals.CurrentSolution.Path) + "\\", "").Replace(projectName + "\\", "");

                OpenOrSelectTab(Globals.CurrentSolution.GetProjectFromName(projectName), file);

                UpdateAllProjects();
            }
        }

        private void memberExplorer_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.Tag != null)
            {
                int lineNumber = int.Parse(e.Node.Tag.ToString());

                EditPad ep = (EditPad)(tabControlMain.Tabs[tabControlMain.SelectedIndex].Child);
                ep.CaretColumn(0);
                ep.CaretLine(lineNumber + 1);

                ep.SelectText(ep.CurrentCaretPosition(), 1);
                ep.ScrollToSelection();
                ep.SelectText(ep.CurrentCaretPosition(), 0);

                ep.Focus();
            }
        }

        private void undoButton_Click(object sender, EventArgs e)
        {
            ((EditPad)(tabControlMain.Tabs[tabControlMain.SelectedIndex].Child)).Undo();
        }

        private void redoButton_Click(object sender, EventArgs e)
        {
            ((EditPad)(tabControlMain.Tabs[tabControlMain.SelectedIndex].Child)).Redo();
        }

        private void listDirectory_Click(object sender, EventArgs e)
        {
            ExecuteCommand("ls");
        }

        private void buildButton_Click(object sender, EventArgs e)
        {
            Build(false);
        }

        private void SaveAll()
        {
            for (int i = 0; i < tabControlMain.Tabs.Count; i++)
            {
                if (tabControlMain.Tabs[i].Tag != null)
                {
                    if (File.Exists(tabControlMain.Tabs[i].Tag.ToString()))
                    {
                        string title = tabControlMain.Tabs[i].Title;

                        if (title.EndsWith(" *"))
                        {
                            title = title.Substring(0, title.Length - 2);
                            tabControlMain.Tabs[i].Title = title;
                        }

                        InputOutput.WriteToFile(tabControlMain.Tabs[i].Tag.ToString(), ((EditPad)(tabControlMain.Tabs[i].Child)).Text);
                    }
                }
            }

            tabControlMain.Invalidate();
        }

        private void SaveFile(EditPad editPad)
        {
            try
            {
                editPad.Save();

                string title = tabControlMain.Tabs[tabControlMain.SelectedIndex].Title;

                if (title.EndsWith(" *"))
                {
                    title = title.Substring(0, title.Length - 2);
                    tabControlMain.Tabs[tabControlMain.SelectedIndex].Title = title;
                }

                tabControlMain.Invalidate();
            }
            catch
            {

            }
        }

        private void saveCurrentFileButton_Click(object sender, EventArgs e)
        {
            try
            {
                EditPad ep = ((EditPad)(tabControlMain.Tabs[tabControlMain.SelectedIndex].Child));

                SaveFile(ep);
            }
            catch
            {

            }
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            ValidateStartPageCommand();
        }

        private void tabControlMain_TabChanged(object sender, int index)
        {
            string title = tabControlMain.Tabs[tabControlMain.SelectedIndex].Title;

            if (title == "Start Page" || title == "Zach Ross-Clyne" || title == "Zac Howard-Smith")
            {
                toolStripMenuItemUndo.Enabled = false;
                toolStripMenuItemRedo.Enabled = false;
                toolStripMenuItemCopy.Enabled = false;
                toolStripMenuItemCut.Enabled = false;
                toolStripMenuItemPaste.Enabled = false;
                toolStripMenuItemSelectAll.Enabled = false;
                toolStripMenuItemFindAndReplace.Enabled = false;
            }
            else
            {
                toolStripMenuItemUndo.Enabled = true;
                toolStripMenuItemRedo.Enabled = true;
                toolStripMenuItemCopy.Enabled = true;
                toolStripMenuItemCut.Enabled = true;
                toolStripMenuItemPaste.Enabled = true;
                toolStripMenuItemSelectAll.Enabled = true;
                toolStripMenuItemFindAndReplace.Enabled = true;
            }

            UpdateMemberExplorer();
        }

        private void tabControlMain_TabClosing(object sender, int index)
        {

            if (tabControlMain.Tabs[index].Title == "Start Page")
            {
                ProcessMenuClick(toolStripMenuItemStartPage, TabWindows.StartPage, false);
                //                AdjustTab((TabControlPro)sender, webBrowser1, "Start Page", false, false);
            }
            else
            {
                if (tabControlMain.Tabs[index].Tag != null)
                {
                    string title = tabControlMain.Tabs[index].Title;
                    string toRemove = null;

                    if (title.EndsWith(" *"))
                    {
                        // file needs saving
                        DialogResult result = MessageBox.Show("This file has changes, would you like to save before closing?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                        if (result == DialogResult.Yes)
                        {
                            SaveFile((EditPad)tabControlMain.Tabs[index].Child);
                        }
                    }

                    if (((EditPad)tabControlMain.Tabs[index].Child).ParentProject != null)
                    {
                        List<string> openFiles = ((EditPad)tabControlMain.Tabs[index].Child).ParentProject.OpenFiles;

                        foreach (string s in openFiles)
                        {
                            if (Path.GetFileName(s) == title)
                            {
                                toRemove = s;
                            }
                        }

                        if (toRemove != null)
                        {
                            openFiles.Remove(toRemove);
                        }

                        UpdateAllProjects();
                    }
                }

                if (tabControlMain.Tabs.Count == 1)
                {
                    if (tabControlMain.Tabs[0].Title == "Start Page")
                    {
                        undoButton.Enabled = false;
                        redoButton.Enabled = false;
                    }
                }
                else if (tabControlMain.Tabs.Count == 0)
                {
                    undoButton.Enabled = false;
                    redoButton.Enabled = false;
                }
            }
        }
        #endregion
        #region Menu Controls
        private void SetTitle(string title)
        {
            this.formTitle.Text = title;
            this.Text = title;
        }

        private void menuBtnMin_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void menuBtnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Normal;
            }
            else
            {
                this.WindowState = FormWindowState.Maximized;
            }
        }

        private void menuBtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region Menustrip

        #region File

        int newTab = -1;

        private void toolStripMenuItemNewSolution_Click(object sender, EventArgs e)
        {
            //Enable once we remove the toolstriptextbox and add UI
            //NewSolution();
            if (newTab == -1)
            {
                NewObjectMountTab();
                if (Globals.CurrentSolution == null)
                {
                    itemListInterface1.SelectItem(0);
                }
            }
        }

        private void tabControlAttributes_TabChanged(object sender, int index)
        {
            for (int i = 0; i < tabControlAttributes.Tabs.Count; i++)
            {
                if (tabControlAttributes.Tabs[i].Title == "New Object" && tabControlAttributes.SelectedIndex != i)
                {
                    NewObjectUnmountTab();
                }
            }
        }

        private void elementToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        private void toolStripMenuItemOpenProjectSolution_Click(object sender, EventArgs e)
        {
            LoadProjectDialog();
        }

        private void toolStripMenuItemOpenElement_Click(object sender, EventArgs e)
        {
            LoadProjectDialog(false);
        }

        private void toolStripMenuItemClose_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItemCloseSolution_Click(object sender, EventArgs e)
        {
            if (stateManager.Transition(StudioState.NoSolution))
            {
                Globals.IsSolutionOpen = false;
            }
        }

        private void toolStripMenuItemSaveAs_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItemRecentFiles_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItemExit_Click(object sender, EventArgs e)
        {

        }
        #endregion

        #region Edit
        #endregion

        #region View
        #region Functionality
        enum TabWindows
        {
            StartPage,
            Console,
            Solution,
            Member,
            Error
        }

        private void ChangeViewTabStatus(TabWindows tab, bool visible, bool shouldClose = true)
        {
            switch (tab)
            {
                case TabWindows.StartPage:
                    if (tabControlMain.GetTabByTitle("Start Page") <= 0)
                    {
                        StudioSettings.StartPagevisible = visible;
                        AdjustTab(tabControlMain, startPage, "Start Page", visible, shouldClose); //HERE
                    }
                    break;
                case TabWindows.Console:
                    AdjustTab(tabControlSecondary, consoleBox, "Console", visible, shouldClose);
                    break;
                case TabWindows.Solution:
                    AdjustTab(tabControlAttributes, solutionExplorer, "Solution Explorer", visible, shouldClose);
                    break;
                case TabWindows.Member:
                    AdjustTab(tabControlAttributes, memberExplorer, "Member Explorer", visible, shouldClose);
                    break;
                case TabWindows.Error:
                    AdjustTab(tabControlSecondary, errorList, "Error List", visible, shouldClose);
                    break;
            }
        }

        private void AdjustTab(TabControlPro tabCtrl, Control ctrl, string tabName, bool visible, bool shouldClose = true)
        {

            int window = tabCtrl.GetTabByTitle(tabName);
            if (visible)
            {
                if (window < 0)
                {
                    tabCtrl.AttachControl(ctrl, tabCtrl.AddTab(tabName));
                    ctrl.Visible = true;
                }
            }
            else
            {
                if (window >= 0 && shouldClose)
                {
                    tabCtrl.DetachControl(window);
                    tabCtrl.DeleteTab(window);
                }
                ctrl.Visible = false;
            }
        }

        private void ProcessMenuClick(object toolStripMenuItem, TabWindows tab, bool shouldClose = true)
        {
            ((ToolStripMenuItem)toolStripMenuItem).Checked = !((ToolStripMenuItem)toolStripMenuItem).Checked;
            ChangeViewTabStatus(tab, ((ToolStripMenuItem)toolStripMenuItem).Checked, shouldClose);
        }

        private void UpdateFromSettings()
        {
            ChangeViewTabStatus(TabWindows.StartPage, StudioSettings.StartPagevisible, !StudioSettings.StartPagevisible);
            ChangeViewTabStatus(TabWindows.Console, StudioSettings.ConsoleVisible, !StudioSettings.ConsoleVisible);
            ChangeViewTabStatus(TabWindows.Solution, StudioSettings.SolutionExplorerVisible, !StudioSettings.SolutionExplorerVisible);
            ChangeViewTabStatus(TabWindows.Member, StudioSettings.MemberExplorerVisible, !StudioSettings.MemberExplorerVisible);
            ChangeViewTabStatus(TabWindows.Error, StudioSettings.ErrorListVisible, !StudioSettings.ErrorListVisible);

            toolStripFile.Visible = StudioSettings.FileToolbarVisible;
            toolStripDefault.Visible = StudioSettings.DefaultToolbarVisible;

            toolStripMenuItemConsole.Checked = StudioSettings.ConsoleVisible;
            toolStripMenuItemStartPage.Checked = StudioSettings.StartPagevisible;
            toolStripMenuItemSolutionExplorer.Checked = StudioSettings.SolutionExplorerVisible;
            toolStripMenuItemMemberExplorer.Checked = StudioSettings.MemberExplorerVisible;
            toolStripMenuItemErrorList.Checked = StudioSettings.ErrorListVisible;

            toolStripMenuItemFileToolbar.Checked = StudioSettings.FileToolbarVisible;
            toolStripMenuItemDefaultToolbar.Checked = StudioSettings.DefaultToolbarVisible;
            InputOutput.WriteSettings();
        }
        #endregion
        private void toolStripMenuItemConsole_Click(object sender, EventArgs e)
        {
            StudioSettings.ConsoleVisible = !StudioSettings.ConsoleVisible;
            UpdateFromSettings();
        }

        private void toolStripMenuItemStartPage_Click(object sender, EventArgs e)
        {
            LoadStartPage();
            StudioSettings.StartPagevisible = !StudioSettings.StartPagevisible;
            UpdateFromSettings();
        }

        private void toolStripMenuItemSolutionExplorer_Click(object sender, EventArgs e)
        {
            StudioSettings.SolutionExplorerVisible = !StudioSettings.SolutionExplorerVisible;
            UpdateFromSettings();
        }

        private void toolStripMenuItemMemberExplorer_Click(object sender, EventArgs e)
        {
            StudioSettings.MemberExplorerVisible = !StudioSettings.MemberExplorerVisible;
            UpdateFromSettings();
        }

        private void toolStripMenuItemErrorList_Click(object sender, EventArgs e)
        {
            StudioSettings.ErrorListVisible = !StudioSettings.ErrorListVisible;
            UpdateFromSettings();
        }

        #region Toolbars
        private void toolStripMenuItemFileToolbar_Click(object sender, EventArgs e)
        {
            StudioSettings.FileToolbarVisible = !StudioSettings.FileToolbarVisible;
            UpdateFromSettings();
        }

        private void toolStripMenuItemDebugToolbar_Click(object sender, EventArgs e)
        {
            StudioSettings.DefaultToolbarVisible = !StudioSettings.DefaultToolbarVisible;
            UpdateFromSettings();
        }
        #endregion

        #endregion

        #region Project
        #endregion

        #region Debug
        #endregion

        #region Tools
        private void toolStripMenuItemEnterEvaluationMode_Click(object sender, EventArgs e)
        {
            ToggleEvaluationMode();
        }
        #endregion

        #region Window
        #endregion

        #region Help
        #endregion

        #endregion
        #region Misc

        public MainForm(string[] args)
        {
            this.FinishedCommand += MainForm_FinishedCommand;
            this.BuildError += MainForm_BuildError;
            Globals.SolutionOpenChanged += Globals_SolutionOpenChanged;

            this.Text = "GBA Studio 2016";
            InitializeComponent();

            InitializeStudio(args);

            PrepareConsole();

            toolStripFile.Renderer = new CustomColorTables.VS2013DarkToolStripRenderer();
            toolStripDefault.Renderer = new CustomColorTables.VS2013DarkToolStripRenderer();
            menuStrip1.Renderer = new CustomColorTables.VS2013DarkToolStripRenderer();

            currentErrors = new List<ConsoleError>();

            FontColorMainMenu();

            if (args == null || args.Length == 0)
            {
                toolStripMenuItemStartPage.Checked = true;
                ChangeViewTabStatus(TabWindows.StartPage, StudioSettings.StartPagevisible, !StudioSettings.StartPagevisible);
                //tabControlMain.AttachControl(startPage, tabControlMain.AddTab("Start Page"));
                LoadStartPage();
                startPage.Visible = true;
            }
            else
            {
                if (File.Exists(args[0]))
                {
                    LoadProjectFile(args[0]);
                }
            }

            solutionExplorer.AllowDrop = true;
            solutionExplorer.ItemDrag += solutionExplorer_ItemDrag;
            solutionExplorer.DragOver += solutionExplorer_DragOver;
            solutionExplorer.DragDrop += solutionExplorer_DragDrop;
            solutionExplorer.NodeMouseClick += solutionExplorer_NodeMouseClick;
            solutionExplorer.Scrollable = false; // change this when we're working with huge solutions

            solutionExplorer.ContextMenuStrip = new System.Windows.Forms.ContextMenuStrip();
            solutionExplorer.ContextMenuStrip.Renderer = new CustomColorTables.VS2013DarkToolStripRenderer();

            memberExplorer.Scrollable = false;

            errorList.DoubleClick += ErrorList_MouseDoubleClick;

            //tabControlSecondary.AttachControl(consoleBox, tabControlSecondary.AddTab("Console"));
        }

        private void ErrorList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (errorList.SelectedIndex < errorList.Items.Count)
            {
                ConsoleError error = (ConsoleError)errorList.Items[errorList.SelectedIndex < 0 ? 0 : errorList.SelectedIndex].Item1;

                OpenOrSelectTab(Globals.CurrentSolution.GetDefaultProject(), error.FileName);

                EditPad ep = (EditPad)tabControlMain.Tabs[tabControlMain.SelectedIndex].Child;

                ep.GotoLine(error.LineNumber);
                ep.CaretLine(error.LineNumber);
                ep.CaretColumn(error.ColumnNumber);

                ep.Focus();
            }
        }

        void solutionExplorer_ItemDrag(object sender, ItemDragEventArgs e)
        {
            TreeNode node = (TreeNode)e.Item;

            if (node.Tag != null)
            {
                if (File.Exists(node.Tag.ToString()))
                {
                    DoDragDrop(e.Item, DragDropEffects.Move);
                }
            }
        }

        void MainForm_BuildError(object sender, EventArgs e)
        {

        }

        void solutionExplorer_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                solutionExplorer.SelectedNode = solutionExplorer.GetNodeAt(e.X, e.Y);

                if (solutionExplorer.SelectedNode != null)
                {
                    string tag = solutionExplorer.SelectedNode.Tag.ToString();

                    if (tag == "IsProject")
                    {
                        PrepareContextMenu(NodeType.Project).Show(solutionExplorer, e.Location);
                    }
                    else if (tag == "IsFolder")
                    {
                        PrepareContextMenu(NodeType.Folder).Show(solutionExplorer, e.Location);
                    }
                    else if (File.Exists(tag))
                    {
                        PrepareContextMenu(NodeType.File).Show(solutionExplorer, e.Location);
                    }
                    else
                    {
                        PrepareContextMenu(NodeType.Filter).Show(solutionExplorer, e.Location);
                    }
                }
            }
        }

        private void FontColorMainMenu()
        {
            for (int i = 0; i < menuStrip1.Items.Count; i++)
            {
                ((ToolStripMenuItem)menuStrip1.Items[i]).ForeColor = Color.White;
            }
        }

        private void UpdateMemberExplorer()
        {
            memberExplorer.Nodes.Clear();

            if (tabControlMain.Tabs[tabControlMain.SelectedIndex].Child != null && tabControlMain.Tabs[tabControlMain.SelectedIndex].Child.GetType() == typeof(EditPad))
            {
                string text = ((EditPad)(tabControlMain.Tabs[tabControlMain.SelectedIndex].Child)).Text;

                TreeNode functions = new TreeNode();
                functions.Text = "Functions";

                Regex funcReg = new Regex(@"(\w+) ([^\(\s]+)\(([^\)]*)\)[^{\n]*[\s]*{");
                MatchCollection foundFunctions = funcReg.Matches(text);

                foreach (Match m in foundFunctions)
                {
                    string lineText = m.Groups[0].ToString().Replace("\n", "").Replace("\r", "");
                    string type = m.Groups[1].ToString();
                    string functionName = m.Groups[2].ToString();

                    string[] lines = text.Split('\n');

                    int lineNumber = 0;

                    for (int i = 0; i < lines.Length; i++)
                    {
                        string useThis = lines[i].Replace("\r", "");

                        if (useThis.Contains(type) && useThis.Contains(functionName) && !useThis.Contains(";"))
                        {
                            lineNumber = i;
                            break;
                        }
                    }

                    TreeNode node = new TreeNode();
                    node.Tag = lineNumber;
                    node.Text = functionName;

                    functions.Nodes.Add(node);
                }

                memberExplorer.Nodes.Add(functions);
            }
        }

        private void SetupAttributesTabs()
        {
            solutionExplorer = new TreeView();
            memberExplorer = new TreeView();

            solutionExplorer.BackColor = System.Drawing.Color.FromArgb(20, 20, 20);
            solutionExplorer.FullRowSelect = true;
            solutionExplorer.Location = new System.Drawing.Point(12, 91);
            solutionExplorer.Name = "solutionExplorer";
            solutionExplorer.ShowLines = false;
            solutionExplorer.Size = new System.Drawing.Size(203, 246);
            solutionExplorer.TabIndex = 0;
            solutionExplorer.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.solutionExplorer_NodeMouseDoubleClick);
            solutionExplorer.ForeColor = Color.White;

            memberExplorer.BackColor = System.Drawing.Color.FromArgb(20, 20, 20);
            memberExplorer.FullRowSelect = true;
            memberExplorer.Location = new System.Drawing.Point(12, 91);
            memberExplorer.Name = "memberExplorer";
            memberExplorer.ShowLines = false;
            memberExplorer.Size = new System.Drawing.Size(203, 246);
            memberExplorer.TabIndex = 0;
            memberExplorer.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.memberExplorer_NodeMouseDoubleClick);
            memberExplorer.ForeColor = Color.White;


            toolStripMenuItemSolutionExplorer.Checked = true;
            ChangeViewTabStatus(TabWindows.Solution, true);

            toolStripMenuItemMemberExplorer.Checked = true;
            ChangeViewTabStatus(TabWindows.Member, true);

            tabControlAttributes.SelectTab(0);
        }

        private void OpenOrSelectTab(Project selectedProject, string path, bool inProject = true)
        {
            path = path.ToLower();

            if (Path.GetExtension(path) == ".gbass")
            {
                Sprite_Designer s = new Sprite_Designer();
                s.OpenSprite(path);

                s.Show();
            }
            else
            {
                bool found = false;
                for (int i = 0; i < tabControlMain.Tabs.Count; i++)
                {
                    if (tabControlMain.Tabs[i].Tag != null)
                    {
                        if (tabControlMain.Tabs[i].Tag.ToString() == path)
                        {
                            found = true;
                            tabControlMain.SelectTab(i);
                        }
                    }
                }

                if (!found)
                {
                    EditPad tmpPad = new EditPad(PadType.File, selectedProject);
                    tmpPad.TextChanged += CodePad_TextChanged;
                    tmpPad.CopiedText += tmpPad_CopiedText;
                    tmpPad.CtrlTab += tmpPad_CtrlTab;
                    tmpPad.FinishedCopy += tmpPad_FinishedCopy;
                    tmpPad.KeyCharDown += tmpPad_KeyCharDown;
                    tmpPad.Scroll += TmpPad_Scroll;

                    string tabTitle = Path.GetFileName(path);

                    if (!inProject)
                    {
                        tabTitle = "[" + tabTitle + "]";
                    }
                    else
                    {
                        string localFile = null;

                        foreach (string file in selectedProject.ReferencedFiles)
                        {
                            if (path.EndsWith(file.ToLower()))
                            {
                                localFile = file;
                            }
                        }

                        if (localFile != null)
                        {
                            Globals.CurrentSolution.OpenFile(localFile, Path.GetFileNameWithoutExtension(selectedProject.Path));
                        }
                    }

                    int index = tabControlMain.AddTab(tabTitle);

                    tabControlMain.AttachControl(tmpPad, index);
                    tabControlMain.Tabs[index].Tag = path;

                    tmpPad.Load(path);

                    UpdateMemberExplorer();


                    toolStripMenuItemUndo.Enabled = true;
                    toolStripMenuItemRedo.Enabled = true;
                    toolStripMenuItemCopy.Enabled = true;
                    toolStripMenuItemCut.Enabled = true;
                    toolStripMenuItemPaste.Enabled = true;
                    toolStripMenuItemSelectAll.Enabled = true;
                    toolStripMenuItemFindAndReplace.Enabled = true;

                    undoButton.Enabled = true;
                    redoButton.Enabled = true;

                    senseEngine.AddCodeBlock(Path.GetFileName(path), tmpPad.Text);
                }
            }
        }

        private void TmpPad_Scroll(object sender, ScrollEventArgs e)
        {
            MoveSmartSense(true);
        }

        private void tmpPad_KeyCharDown(object sender, EditPad.KeyCharEventArgs e, out bool cancel)
        {
            bool cancel2 = false;
            RunSmartSense(e.Character, e, out cancel2);

            cancel = cancel2;
        }

        void tmpPad_FinishedCopy(object sender, EventArgs e)
        {
            shouldCopy = true;
        }

        void tmpPad_CtrlTab(object sender, EventArgs e)
        {
            if (!clipboardInterface.Visible && clipboardInterface.Clipboard.ClipboardCount > 0)
            {
                clipboardInterface.ShowDialog();
            }
        }

        private void tmpPad_CopiedText(object sender, EventArgs e)
        {
            if (shouldCopy)
            {
                CopyToClipboardManager();
                shouldCopy = false;
            }
        }

        void consoleBox_CtrlTab(object sender, System.EventArgs e)
        {
            if (!clipboardInterface.Visible)
            {
                clipboardInterface.ShowDialog();
            }
        }

        private void ValidateStartPageCommand()
        {
            string str = startPage.Url.ToString().ToLower();
            if (str.Contains("#"))
            {
                str = str.Substring(str.IndexOf("#") + 1);

                if (str.StartsWith("new"))
                {
                    NewObjectMountTab(0);
                }
                else if (str.StartsWith("open"))
                {
                    LoadProjectDialog();
                }
                else if (str.StartsWith("recent"))
                {
                    string address = str.Remove(0, 6);
                    OpenRecentFile(int.Parse(address));
                }
            }
        }

        private void OpenGBA(string gbaLocation)
        {
            string exeLocation = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "");

            Process p = new Process();
            p.StartInfo.FileName = exeLocation + " " + gbaLocation;
            p.Start();
        }

        private string FindGBA(string location)
        {
            string[] files = Directory.GetFiles(location);

            string gbaPath = null;

            foreach (string file in files)
            {
                if (file.EndsWith(".gba"))
                {
                    gbaPath = file;
                }
            }

            if (gbaPath != null)
            {
                return gbaPath;
            }

            // not found yet

            string[] directories = Directory.GetDirectories(location);

            foreach (string dir in directories)
            {
                if (!dir.ToLower().Contains("evaluation"))
                {
                    gbaPath = FindGBA(dir);

                    if (gbaPath != "NOT FOUND")
                    {
                        return gbaPath;
                    }
                }

            }

            return "NOT FOUND";
        }

        private void ToggleEvaluationMode(bool overwrite = true)
        {
            bool updateTheme = !Globals.CurrentSolution.EvaluationMode;
            bool modeChangeSuccessful = !overwrite;

            SaveAll();

            if (overwrite)
            {
                if (Globals.CurrentSolution.EvaluationMode)
                {
                    DialogResult msgResult = MessageBox.Show("You are about to exit Evaluation Mode. Would you like to retain the changes you have made?", "Retain Evaluation Copy?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                    if (msgResult == DialogResult.Yes) //Apply
                    {
                        toolStripMenuItemEnterEvaluationMode.Checked = !toolStripMenuItemEnterEvaluationMode.Checked;
                        updateTheme = true;
                        modeChangeSuccessful = EvaluationModeApply();
                    }
                    else if (msgResult == DialogResult.No) //Revert
                    {
                        toolStripMenuItemEnterEvaluationMode.Checked = !toolStripMenuItemEnterEvaluationMode.Checked;
                        updateTheme = true;
                        modeChangeSuccessful = EvaluationModeRevert();
                    }
                }
                else
                {
                    modeChangeSuccessful = EvaluationModeVerify();
                }
            }

            if (updateTheme && modeChangeSuccessful) //Will only execute upon a mode change. Allows for cancellation.
            {
                toolStripMenuItemEnterEvaluationMode.Checked = Globals.CurrentSolution.EvaluationMode;
                this.StatusText = (Globals.CurrentSolution.EvaluationMode ? "Evaluation Mode" : "Design Mode") + statusTextAppend;
                toolStripMenuItemEnterEvaluationMode.Text = Globals.CurrentSolution.EvaluationMode ? "Exit Evaluation Mode" : "Enter Evaluation Mode";

                Color exitHighlight = !Globals.CurrentSolution.EvaluationMode ? Color.FromArgb(255, 224, 192) : Color.FromArgb(82, 176, 239);
                Color highlight = !Globals.CurrentSolution.EvaluationMode ? Color.FromArgb(255, 192, 128) : Color.FromArgb(26, 138, 213);
                Color selected = !Globals.CurrentSolution.EvaluationMode ? Color.FromArgb(202, 81, 0) : Color.FromArgb(0, 114, 191);
                Color statusBack = !Globals.CurrentSolution.EvaluationMode ? Color.FromArgb(202, 81, 0) : Color.FromArgb(0, 122, 204);
                Color foreColor = !Globals.CurrentSolution.EvaluationMode ? Color.Black : Color.White;
                TabControlPro[] controls = { tabControlMain, tabControlSecondary, tabControlAttributes };

                foreach (TabControlPro ctrl in controls)
                {
                    ctrl.ExitHighlightColor = exitHighlight;
                    ctrl.HighlightColor = highlight;
                    ctrl.SelectedColor = selected;
                    ctrl.ForeColor = foreColor;
                    this.StatusBackcolor = statusBack;
                }

                Globals.CurrentSolution.EvaluationMode = !Globals.CurrentSolution.EvaluationMode;

                UpdateSolution(Globals.CurrentSolution);
            }
        }

        //Ensures that the folder doesn't exist, and we're therefore not potentially losing information
        private bool EvaluationModeVerify()
        {
            string evalDirectory = Path.GetDirectoryName(Globals.CurrentSolution.Path) + @"\Evaluation";
            if (Directory.Exists(evalDirectory))
            {
                DialogResult d = MessageBox.Show("An evaluation version already exists, indicating that evaluation mode was not exited normally. Continuing will overwrite this copy. Would you like to continue?", "Exit Evaluation Mode", MessageBoxButtons.YesNo);
                if (d == DialogResult.Yes)
                {
                    try
                    {
                        Directory.Delete(evalDirectory, true);
                        return EvaluationModeCreate();
                    }
                    catch (Exception e)
                    {
                        Errors.Throw(ErrorType.EvaluationModeFailed);
                        return false;
                    }
                }
                else
                {
                    return false;
                    //do nothing
                }
            }
            else
            {
                return EvaluationModeCreate();
            }
        }

        private bool EvaluationModeCreate()
        {
            bool modeChangeSuccessful = false;
            string evalDirectory = Path.GetDirectoryName(Globals.CurrentSolution.Path) + @"\Evaluation";
            if (!Directory.Exists(evalDirectory))
            {
                Directory.CreateDirectory(evalDirectory);
                if (!Directory.Exists(evalDirectory))
                {
                    Errors.Throw(ErrorType.EvaluationModeFailed);
                }
                else
                {
                    modeChangeSuccessful = true;
                }
            }
            else
            {
                modeChangeSuccessful = true;
            }

            if (modeChangeSuccessful)
            {
                string[] files = Directory.GetFiles(Path.GetDirectoryName(Globals.CurrentSolution.Path), "*.*", SearchOption.AllDirectories);

                try
                {
                    foreach (string f in files)
                    {

                        string newPath = f.Replace(Path.GetDirectoryName(Globals.CurrentSolution.Path), evalDirectory);
                        string newDirectory = Path.GetDirectoryName(newPath);
                        if (!Directory.Exists(Path.GetDirectoryName(newPath)))
                        {
                            Directory.CreateDirectory(newDirectory);
                        }
                        File.Copy(f, newPath);
                    }
                    toolStripMenuItemEnterEvaluationMode.Checked = !toolStripMenuItemEnterEvaluationMode.Checked;
                }
                catch
                {
                    Errors.Throw(ErrorType.EvaluationModeFailed);
                }
            }

            return modeChangeSuccessful;
        }

        private bool EvaluationModeRevert()
        {
            bool exited = false;
            string evalDirectory = Path.GetDirectoryName(Globals.CurrentSolution.Path) + @"\Evaluation";
            if (Directory.Exists(evalDirectory))
            {
                string[] files = Directory.GetFiles(evalDirectory, "*.*", SearchOption.AllDirectories);

                try
                {
                    foreach (string f in files)
                    {
                        string newPath = f.Replace(evalDirectory, Path.GetDirectoryName(Globals.CurrentSolution.Path));
                        string newDirectory = Path.GetDirectoryName(newPath);
                        File.Copy(f, newPath, true);
                    }
                    toolStripMenuItemEnterEvaluationMode.Checked = !toolStripMenuItemEnterEvaluationMode.Checked;
                    exited = true;
                    Directory.Delete(evalDirectory, true);
                }
                catch
                {
                    Errors.Throw(ErrorType.EvaluationModeFailed);
                }
                return exited;
            }
            else
            {
                Errors.Throw(ErrorType.EvaluationModeExitWarning);
                return true;
            }
        }

        private bool EvaluationModeApply()
        {
            try
            {
                string evalDirectory = Path.GetDirectoryName(Globals.CurrentSolution.Path) + @"\Evaluation";
                Directory.Delete(evalDirectory, true);
            }
            catch
            {
                Errors.Throw(ErrorType.EvaluationModeFailed);
                return false;
            }
            return true;
        }

        #endregion
        #region Initialization
        private void InitializeStudio(string[] args)
        {
            Globals.AssemblyStub = InputOutput.ReadFromResources("GBA_Studio.Resources.Text_Templates.StubAssembly.txt");
            Globals.HeaderStub = InputOutput.ReadFromResources("GBA_Studio.Resources.Text_Templates.StubHeader.txt");

            this.StatusText += statusTextAppend;

            keyMonitor1.AddKeyCombination(Keys.Alt, Keys.F4);
            keyMonitor1.AddWatchedKey(Keys.Enter);
            keyMonitor1.AddWatchedKey(Keys.Escape);
            keyMonitor1.AddWatchedKey(Keys.LButton);
            keyMonitor1.AddWatchedKey(Keys.F4);
            keyMonitor1.Begin();

            InputOutput.ReadRecentFiles();

            itemListInterface1.Items.Add(new Controls.ItemList.ItemData("New Solution", "", Resources.New_Solution, true));
            itemListInterface1.Items.Add(new Controls.ItemList.ItemData("New Project In Solution", "", Resources.New_Project, true));
            itemListInterface1.Items.Add(new Controls.ItemList.ItemData("", "", null, true));
            itemListInterface1.Items.Last().Spacer = true;
            itemListInterface1.Items.Add(new Controls.ItemList.ItemData("New C File", "", Resources.new_C, true));
            itemListInterface1.Items.Add(new Controls.ItemList.ItemData("New Header File", "", Resources.new_H, true));
            itemListInterface1.Items.Add(new Controls.ItemList.ItemData("New Assembly File", "", Resources.new_S, true));
            itemListInterface1.Items.Add(new Controls.ItemList.ItemData("", "", null, true));
            itemListInterface1.Items.Last().Spacer = true;
            itemListInterface1.Items.Add(new Controls.ItemList.ItemData("New Sprite", "", Resources.new_SS, true));

            clipboardInterface = new ClipboardInterface();
            clipboardInterface.ClipboardItemSelected += clipboardInterface_ClipboardItemSelected;

            consoleBox = new Controls.EditPad(PadType.NotFile);
            consoleBox.SyntaxHighlighting = (ICSharpCode.AvalonEdit.Highlighting.IHighlightingDefinition)(new ICSharpCode.AvalonEdit.Highlighting.HighlightingDefinitionTypeConverter().ConvertFrom(""));

            CheckDirectoryConfiguration();

            UpdateRecentFileMenu();


            InputOutput.ReadSettings();

            //If a saved login exists
            if (StudioSettings.Username != "")
            {
                Globals.VerifyUser(StudioSettings.Username, CryptographyIO.Decrypt(StudioSettings.Password, Globals.EncryptionKey));
            }
            UpdateStudioPrivileges();
            InputOutput.DownloadNews();
            InputOutput.ReadStartPage();

            //This is fucking cool
            ConfigureStateManager(args);
            SetupAttributesTabs();

            UpdateFromSettings();

            InitializeSmartSense();


        }

        private void UpdateStudioPrivileges()
        {
            //Lol at this if statement
            if (Globals.CurrentUser != null && !Globals.CurrentUser.NULL)
            {
                goolStripMenuItemProfile.Text = Globals.CurrentUser.Forename + " " + Globals.CurrentUser.Surname;
            }
            else
            {
                goolStripMenuItemProfile.Text = "Guest";
                Globals.CurrentUser = new User();
            }

            switch (Globals.AuthenticationMode)
            {
                case Globals.Authentication.Normal:
                    statusTextAppend = " [Normal]";
                    toolStripMenuItemDeveloper.Visible = false;
                    break;
                case Globals.Authentication.Privileged:
                    statusTextAppend = " [Privileged]";
                    toolStripMenuItemDeveloper.Visible = false;
                    break;
                case Globals.Authentication.Developer:
                    statusTextAppend = " [Developer]";
                    toolStripMenuItemDeveloper.Visible = true;
                    BugHandling.InitializeBugDatabaseConnection();
                    break;
            }
        }

        private void clipboardInterface_ClipboardItemSelected(object sender, ClipboardInterface.ClipboardEventArgs e)
        {
            Clipboard.SetText(e.Clipboard);

            ((EditPad)(tabControlMain.Tabs[tabControlMain.SelectedIndex].Child)).Paste();
        }

        private void CheckDirectoryConfiguration()
        {
            bool root = CheckAndCreateDirectory(Globals.RootDirectory);

            if (root)
            {
                bool a = CheckAndCreateDirectory(Globals.ProjectDirectory);
                bool b = CheckAndCreateDirectory(Globals.ConfigDirectory);
            }
        }

        private bool CheckAndCreateDirectory(string directory)
        {
            if (!Directory.Exists(directory))
            {
                DirectoryInfo info = Directory.CreateDirectory(directory);
                if (!info.Exists)
                {
                    Errors.Throw(ErrorType.DirectoryFailed);
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        #region Start Page

        /// <summary>
        /// Loads the start page.
        /// </summary>
        private void LoadStartPage()
        {
            if (Globals.AuthenticationMode == Globals.Authentication.Developer)
            {
                if (Globals.BugDatabaseConnection != null)
                {
                    Globals.BugDatabaseConnection.RetrieveBugList();
                    InputOutput.DownloadNews();
                }
                else
                {
                    Globals.StartPageNews = "Could not connect to the bug database!";
                }
            }
            string newsString = Globals.StartPageNews;

            startPage.Navigate("about:blank");
            HtmlDocument doc = startPage.Document;
            doc.Write(string.Empty);

            startPage.DocumentText = Globals.StartPageText.Replace("#NEWS#", newsString).Replace("#BUGS#", newsString).Replace("#RECENTS#", Globals.GetRecentFileHTML());
        }
        #endregion

        private void newProjectButton_Click(object sender, EventArgs e)
        {
            // add messagebox with text box

            string projectName = ""; // change this with the result of the messagebox when its done

            NewProject(projectName);
        }

        #endregion
        #region Runtime Stuff

        [DllImport("user32.dll")]
        static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        [DllImport("USER32.DLL")]
        public static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        [DllImport("USER32.DLL")]
        public static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("USER32.DLL")]
        public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        private void Clean(bool shouldRun)
        {
            if (Globals.CurrentSolution != null && Globals.CurrentSolution.Projects.Count > 0)
            {
                foreach (Project p in Globals.CurrentSolution.Projects)
                {
                    string projectDir = Path.GetDirectoryName(Globals.CurrentSolution.Path) + @"\" + Path.GetFileNameWithoutExtension(p.Path);

                    if (Directory.Exists(projectDir + @"\build"))
                    {
                        Directory.Delete(projectDir + @"\build", true);
                    }
                    if (Globals.CurrentSolution.GetDefaultProject() == p)
                    {
                        ChangeDirectory(projectDir);
                    }
                }
                int cnt = Globals.CurrentSolution.Projects.Count;
                consoleText += cnt + " project" + (cnt != 1 ? "s were" : " was") + " succesfully cleaned.\r\n";
            }

            if (shouldRun)
            {
                Build(true);
            }
        }

        private void Build(bool shouldRun)
        {
            SaveAll();
            rolledbackFromError = false;

            currentErrors.Clear();
            errorList.Clear();

            if (StudioSettings.ErrorListVisible)
            {
                errorList.MinimumSize = tabControlSecondary.Tabs[tabControlSecondary.GetTabByTitle("Error List")].Container.Size;
            }

            if (stateManager.Transition(StudioState.Building) && Globals.CurrentSolution != null)
            {
                if (Globals.CurrentSolution.GetDefaultProject() != null)
                {
                    gbaMonitor.Path = Path.GetDirectoryName(Globals.CurrentSolution.Path) + @"\" + Path.GetDirectoryName(Globals.CurrentSolution.GetDefaultProject().Path);
                    waitingForBuildComplete = shouldRun;
                    waitingForNewBuild = true;

                    if (!Precompiler.Compile())
                    {
                        waitingForNewBuild = false;
                        waitingForBuildComplete = false;
                        stateManager.Rollback();
                    }
                    else
                    {
                        ClearConsole();
                        ExecuteCommand("make");
                    }
                }
                else
                {
                    stateManager.Rollback();
                }
            }
        }

        private void Run()
        {
            if (stateManager.Transition(StudioState.Running) && Globals.CurrentSolution != null)
            {
                string exeLocation = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace(@"file:\", "");
                string gbaLocation = FindGBA(Path.GetDirectoryName(InputOutput.GetDrivePath(Globals.CurrentSolution.Path)));

                if (gbaLocation != "NOT FOUND")
                {
                    emuProc = new Process();

                    emuProc.EnableRaisingEvents = true;
                    emuProc.StartInfo.FileName = exeLocation + "\\VisualBoyAdvance.exe";

                    emuProc.StartInfo.Arguments = gbaLocation;
                    emuProc.StartInfo.WindowStyle = ProcessWindowStyle.Minimized;

                    emuProc.Exited += emuProc_Exited;
                    emuProc.Disposed += emuProc_Exited;

                    emuProc.Start();

                    runState = Globals.RunState.Running;

                    System.Threading.Thread.Sleep(500);

                    SetParent(emuProc.MainWindowHandle, splitContainer6.Handle);

                    int lStyle = GetWindowLong(emuProc.MainWindowHandle, -16);
                    lStyle &= ~(0x00C00000 | 0x00040000 | 0x20000000 | 0x01000000 | 0x00080000);

                    SetWindowLong(emuProc.MainWindowHandle, -16, lStyle);

                    ShowWindow(emuProc.MainWindowHandle, 3);

                    splitContainer6.Panel1Collapsed = true;
                    splitContainer6.Panel2Collapsed = false;
                }
                else
                {
                    Errors.Throw(ErrorType.NoGBAFile);
                    stateManager.Transition(StudioState.ProjectSolutionOpen);
                }
            }
        }

        void emuProc_Exited(object sender, EventArgs e)
        {
            runState = Globals.RunState.Stopped;
            ThreadSafeStopExecutionAccessor();

            currentErrors.Clear();
        }

        #endregion
        private void UpdateSolutionExplorer()
        {//This is a comment
            solutionExplorer.Nodes.Clear();

            foreach (Project p in Globals.CurrentSolution.Projects)
            {
                //Modification
                LoadProjectTreeView(Path.GetFileNameWithoutExtension(p.Path), Path.GetDirectoryName(Globals.CurrentSolution.Path) + @"\" + p.Path, false);
            }
        }//hello world
        private void interfaceNewItem1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void interfaceNewItem1_HoveredIndexChanged(object sender, EventArgs e)
        {

        }
        private void buildAndRunButton_Click(object sender, EventArgs e)
        {
            Build(true);
        }
        private void cleanSolution_Click(object sender, EventArgs e)
        {
            Clean(false);
        }
        private void buttonPro1_ButtonClicked(object sender, EventArgs e)
        {

        }
        private void splitContainer5_Resize(object sender, EventArgs e)
        {
            splitContainer5.SplitterDistance = this.Width - 106;
        }
        private void itemListInterface1_ButtonClicked(object sender, ItemListInterfaceEventArgs e)
        {
            int index = e.SelectedIndex;
            string fileName = e.SelectedText;
            bool validSubmit = false;
            switch (index)
            {
                case 0: //New Solution
                    validSubmit = true;
                    NewSolution(fileName);
                    break;
                case 1: //New Project
                    validSubmit = true;
                    NewProject(fileName);
                    break;
                case 3: //New C
                    validSubmit = true;
                    NewFile(fileName + ".gc", true, newFileToNode);
                    break;
                case 4: //New H
                    validSubmit = true;
                    NewFile(fileName + ".gh", true, newFileToNode);
                    break;
                case 5: //New S
                    validSubmit = true;
                    NewFile(fileName + ".gs", true, newFileToNode);
                    break;
                case 7: //New SS
                    validSubmit = true;
                    NewFile(fileName + ".gbass", true, newFileToNode);
                    break;
            }
            if (validSubmit)
            {
                //Hide the 'New Object' interface.
                NewObjectUnmountTab();
                tabControlAttributes.SelectTab(0);
                newFileToNode = null;
            }
        }
        private void startButton_Click(object sender, EventArgs e)
        {
            waitingForBuildComplete = false;
            Run();
        }
        private void splitContainer3_Panel1_Resize(object sender, EventArgs e)
        {
            if (emuProc != null)
            {
                ShowWindow(emuProc.MainWindowHandle, 9);
                System.Threading.Thread.Sleep(100);
                ShowWindow(emuProc.MainWindowHandle, 3);
            }
        }
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            TerminateExecution();
        }

        void TerminateExecution()
        {
            try
            {
                emuProc.Kill();
                emuProc.Dispose();

                emuProc = null;
            }
            catch
            {
            }
        }

        private string GetSolutionFromProject(string projectFilePath)
        {
            string dir = Path.GetDirectoryName(projectFilePath);
            dir = Path.GetDirectoryName(dir);

            string[] files = Directory.GetFiles(dir);

            foreach (string file in files)
            {
                if (Path.GetExtension(file) == ".gbasln")
                {
                    return file;
                }
            }

            return null;
        }

        private void tabControlMain_DragDrop(object sender, DragEventArgs e)
        {
            //string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

            bool loadedSolution = false;

            if (files.Length == 1)
            {
                if (Path.GetExtension(files[0]) == ".gbasln" && Globals.CurrentSolution == null)
                {
                    loadedSolution = true;
                    LoadProjectFile(files[0]);
                }
                else if (Path.GetExtension(files[0]) == ".gbaproj" && Globals.CurrentSolution == null)
                {
                    loadedSolution = true;
                    LoadProjectFile(GetSolutionFromProject(files[0]));
                }
            }

            if (!loadedSolution)
            {
                foreach (string file in files)
                {
                    bool inProject = false;

                    foreach (Project p in Globals.CurrentSolution.Projects)
                    {
                        if (p.ContainsFile(Path.GetFileName(file)))
                        {
                            inProject = true;
                            break;
                        }
                    }
                    OpenOrSelectTab(null, file, inProject);
                }
            }
        }
        private void tabControlMain_DragEnter(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
        }
        private void gbaMonitor_Changed(object sender, FileSystemEventArgs e)
        {
            if (Globals.CurrentSolution != null)
            {
                string gbaFile = Path.GetFileName(e.FullPath);
                string projName = Path.GetFileNameWithoutExtension(Globals.CurrentSolution.GetDefaultProject().Path);
                string fileName = Path.GetFileNameWithoutExtension(projName);
                if (gbaFile == fileName + ".gba")
                {
                    if (waitingForBuildComplete && waitingForNewBuild)
                    {
                        Run();
                        waitingForBuildComplete = false;
                        waitingForNewBuild = false;
                        Print("--------------------------------");
                        Print("Build Complete. Executing build.");
                        Print("--------------------------------");
                    }
                    else if (waitingForNewBuild)
                    {
                        waitingForNewBuild = false;
                        Print("---------------");
                        Print("Build Complete.");
                        Print("---------------");
                        stateManager.Transition(StudioState.ProjectSolutionOpen);
                    }

                    errorList.Clear();
                }
            }
        }
        private void startPage_VisibleChanged(object sender, EventArgs e)
        {
            LoadStartPage();
        }
        private void tabControlMain_DragOver(object sender, DragEventArgs e)
        {
            // Determine whether string data exists in the drop data. If not, then 
            // the drop effect reflects that the drop cannot occur. 
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

            // Set the effect based upon the KeyState. 
            if ((e.KeyState & (8 + 32)) == (8 + 32) && (e.AllowedEffect & DragDropEffects.Link) == DragDropEffects.Link)
            {
                // KeyState 8 + 32 = CTL + ALT 
                // Link drag-and-drop effect.
                e.Effect = DragDropEffects.Link;
            }
            else if ((e.KeyState & 32) == 32 && (e.AllowedEffect & DragDropEffects.Link) == DragDropEffects.Link)
            {
                // ALT KeyState for link.
                e.Effect = DragDropEffects.Link;
            }
            else if ((e.KeyState & 4) == 4 && (e.AllowedEffect & DragDropEffects.Move) == DragDropEffects.Move)
            {
                // SHIFT KeyState for move.
                e.Effect = DragDropEffects.Move;
            }
            else if ((e.KeyState & 8) == 8 && (e.AllowedEffect & DragDropEffects.Copy) == DragDropEffects.Copy)
            {
                // CTL KeyState for copy.
                e.Effect = DragDropEffects.Copy;
            }
            else if ((e.AllowedEffect & DragDropEffects.Move) == DragDropEffects.Move)
            {
                // By default, the drop action should be move, if allowed.
                e.Effect = DragDropEffects.Move;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }
        void solutionExplorer_DragDrop(object sender, DragEventArgs e)
        {
            if (Globals.CurrentSolution != null)
            {
                Point p = ((TreeView)sender).PointToClient(new Point(e.X, e.Y));

                TreeNode destNode = ((TreeView)sender).GetNodeAt(p);

                object itemDragged = e.Data.GetData(typeof(TreeNode));

                if (itemDragged != null)
                {
                    TreeNode draggedNode = (TreeNode)itemDragged;

                    if (!draggedNode.Equals(destNode) && destNode != null && !destNode.Equals(draggedNode.Parent))
                    {
                        if (destNode.Tag != null)
                        {
                            if (destNode.Tag.ToString() == "IsProject")
                            {
                                destNode = null;
                            }
                            else if (File.Exists(destNode.Tag.ToString()))
                            {
                                destNode = destNode.Parent;
                            }
                        }

                        //destNode != null prevents the crash that occurred when a file was added.
                        if (destNode != null && GetProjectNameFromNode(destNode) == GetProjectNameFromNode(draggedNode))
                        {

                            string newLocalPath = GetFolderPath(destNode, draggedNode.Text);
                            string newFullPath = Path.GetDirectoryName(InputOutput.GetDrivePath(Globals.CurrentSolution.Path)) + @"\" + GetProjectNameFromNode(destNode) + @"\" + newLocalPath;

                            //0 = invalid, 1 = valid, 2 = replace
                            byte validMove = 1;

                            //Prompts for action
                            if (newFullPath != draggedNode.Tag.ToString())
                            {
                                if (File.Exists(newFullPath))
                                {
                                    validMove = 0;
                                    DialogResult res = MessageBox.Show("A file with the same name already exists in that location. Would you like to overwrite it?", "Overwrite file?", MessageBoxButtons.YesNo);
                                    if (res == System.Windows.Forms.DialogResult.Yes)
                                    {
                                        File.Delete(newFullPath);
                                        validMove = 2;
                                    }
                                }
                            }

                            if (validMove >= 1)
                            {
                                draggedNode.Remove(); // Ensures node doesn't get removed before a replace/cancel decision has been made.
                                if (validMove == 2)
                                {
                                    foreach (TreeNode node in destNode.Nodes)
                                    {
                                        if ((string)node.Tag == newFullPath)
                                        {
                                            node.Remove(); //Locates the old node and removes it in place of the new node.
                                        }
                                    }
                                }

                                Project project = Globals.CurrentSolution.GetProjectFromName(GetProjectNameFromNode(destNode));

                                int index = -1;
                                for (int i = 0; i < project.ReferencedFiles.Count; i++)
                                {
                                    if (draggedNode.Tag.ToString().EndsWith(project.ReferencedFiles[i]))
                                    {
                                        index = i;
                                    }
                                }

                                if (index >= 0)
                                {
                                    project.ReferencedFiles[index] = newLocalPath;
                                }

                                File.Move(draggedNode.Tag.ToString(), newFullPath);

                                draggedNode.Tag = newFullPath;

                                destNode.Nodes.Add(draggedNode);

                                destNode.Expand();

                                UpdateProject(project);
                            }
                        }
                    }
                }
                else
                {

                    //string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                    string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

                    if (destNode != null)
                    {
                        Project proj = Globals.CurrentSolution.GetProjectFromName(GetProjectNameFromNode(destNode));

                        if (proj != null)
                        {
                            foreach (string file in files)
                            {
                                if (File.Exists(file) && Globals.AllowedExtensions.Contains(Path.GetExtension(file)))
                                {
                                    string guid = null;
                                    if (File.Exists(destNode.Tag.ToString()))
                                    {
                                        // its a file add to parent
                                        destNode = destNode.Parent;
                                    }

                                    if (destNode.Tag.ToString() == "IsProject")
                                    {
                                        destNode = GetTreeNodeFromText(destNode, proj.SourceFolder);
                                    }
                                    else if (destNode.Tag.ToString() != "IsFolder")
                                    {
                                        // its a filter
                                        guid = destNode.Tag.ToString();
                                    }

                                    string folderPath = Path.GetDirectoryName(GetFolderPath(destNode, ""));

                                    proj.NewFile(Path.GetFileName(file), folderPath);
                                    string destPath = Path.GetDirectoryName(InputOutput.GetDrivePath(Globals.CurrentSolution.Path)) + @"\" + Path.GetDirectoryName(proj.Path) + @"\" + folderPath + @"\" + Path.GetFileName(file);

                                    if (guid != null)
                                    {
                                        proj.ApplyToFilter(proj.ReferencedFiles.Count - 1, guid);
                                    }

                                    if (!File.Exists(destPath) || destPath == InputOutput.GetDrivePath(file))
                                    {
                                        InputOutput.WriteToFile(destPath, InputOutput.ReadFromFile(file));

                                        TreeNode node = new TreeNode();
                                        node.Tag = destPath;
                                        node.Text = Path.GetFileName(destPath);

                                        if (destNode != null)
                                        {
                                            destNode.Nodes.Add(node);

                                            OpenOrSelectTab(proj, destPath);
                                        }
                                    }
                                }
                            }
                        }
                        UpdateProject(proj);
                    }
                    else
                    {
                        AddFileDroppedFile(files);
                    }
                }
            }
            else
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

                if (files.Length == 1)
                {
                    if (Path.GetExtension(files[0]) == ".gbasln" && Globals.CurrentSolution == null)
                    {
                        LoadProjectFile(files[0]);
                    }
                    else if (Path.GetExtension(files[0]) == ".gbaproj" && Globals.CurrentSolution == null)
                    {
                        LoadProjectFile(GetSolutionFromProject(files[0]));
                    }
                }
            }
        }
        private TreeNode GetProjectNodeFromNode(TreeNode node)
        {
            if (node.Tag != null)
            {
                if (node.Tag.ToString() == "IsProject")
                {
                    return node;
                }
                else
                {
                    if (node.Parent != null)
                    {
                        return GetProjectNodeFromNode(node.Parent);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            else
            {
                if (node.Parent != null)
                {
                    return GetProjectNodeFromNode(node.Parent);
                }
                else
                {
                    return null;
                }
            }
        }
        private void AddFileDroppedFile(string[] files)
        {
            // present user interface
        }
        private TreeNode GetTreeNodeFromText(TreeNode tr, string text)
        {
            foreach (TreeNode node in tr.Nodes)
            {
                if (node.Text.ToLower() == text.ToLower())
                {
                    return node;
                }
                else if (node.Nodes != null)
                {
                    TreeNode treeNode = GetTreeNodeFromText(node, text);

                    if (treeNode != null)
                    {
                        return treeNode;
                    }
                }
            }

            return null;
        }
        void solutionExplorer_DragOver(object sender, DragEventArgs e)
        {
            if (Globals.CurrentSolution != null)
            {
                // Determine whether string data exists in the drop data. If not, then 
                // the drop effect reflects that the drop cannot occur. 
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

                // Set the effect based upon the KeyState. 
                if ((e.KeyState & (8 + 32)) == (8 + 32) && (e.AllowedEffect & DragDropEffects.Link) == DragDropEffects.Link)
                {
                    // KeyState 8 + 32 = CTL + ALT 
                    // Link drag-and-drop effect.
                    e.Effect = DragDropEffects.Link;
                }
                else if ((e.KeyState & 32) == 32 && (e.AllowedEffect & DragDropEffects.Link) == DragDropEffects.Link)
                {
                    // ALT KeyState for link.
                    e.Effect = DragDropEffects.Link;
                }
                else if ((e.KeyState & 4) == 4 && (e.AllowedEffect & DragDropEffects.Move) == DragDropEffects.Move)
                {
                    // SHIFT KeyState for move.
                    e.Effect = DragDropEffects.Move;
                }
                else if ((e.KeyState & 8) == 8 && (e.AllowedEffect & DragDropEffects.Copy) == DragDropEffects.Copy)
                {
                    // CTL KeyState for copy.
                    e.Effect = DragDropEffects.Copy;
                }
                else if ((e.AllowedEffect & DragDropEffects.Move) == DragDropEffects.Move)
                {
                    // By default, the drop action should be move, if allowed.
                    e.Effect = DragDropEffects.Move;
                }
                else
                {
                    e.Effect = DragDropEffects.None;
                }
            }
            else
            {
                e.Effect = DragDropEffects.Move;
            }
        }
        void StateManagerRollBack(object sender, StateManager.StateManagerEventArgs e)
        {

        }
        void StateManagerTransitioned(object sender, StateManager.StateManagerEventArgs e)
        {
            switch (e.ToState)
            {
                case StudioState.Building:
                    #region Settings
                    //File Menu
                    addToolStripMenuItem.Enabled = false;
                    projectToolStripMenuItem.Enabled = false;
                    elementToolStripMenuItem.Enabled = false;
                    toolStripMenuItemClose.Enabled = false;
                    toolStripMenuItemCloseSolution.Enabled = false;
                    toolStripMenuItemSave.Enabled = false;
                    toolStripMenuItemSaveAll.Enabled = false;

                    //Edit Menu
                    toolStripMenuItemUndo.Enabled = false;
                    toolStripMenuItemRedo.Enabled = false;
                    toolStripMenuItemCopy.Enabled = false;
                    toolStripMenuItemCut.Enabled = false;
                    toolStripMenuItemPaste.Enabled = false;
                    toolStripMenuItemSelectAll.Enabled = false;
                    toolStripMenuItemFindAndReplace.Enabled = false;
                    toolStripMenuItemAddElement.Enabled = false;
                    toolStripMenuItemProperties.Enabled = false;

                    //Debug
                    toolStripMenuItemCleanSolution.Enabled = false;
                    toolStripMenuItemStart.Enabled = false;
                    toolStripMenuItemStartWithoutBuild.Enabled = false;
                    buildToolStripMenuItem.Enabled = false;

                    //Tools
                    toolStripMenuItemEnterEvaluationMode.Enabled = false;

                    //Toolstrips
                    saveCurrentFileButton.Enabled = false;
                    saveAllButton.Enabled = false;
                    buildButton.Enabled = false;
                    buildAndRunButton.Enabled = false;
                    startButton.Enabled = false;
                    stopButton.Enabled = false;
                    #endregion
                    break;
                case StudioState.NoProject:
                    #region Settings
                    //File Menu
                    addToolStripMenuItem.Enabled = true;
                    projectToolStripMenuItem.Enabled = true;
                    elementToolStripMenuItem.Enabled = false;
                    toolStripMenuItemClose.Enabled = false;
                    toolStripMenuItemCloseSolution.Enabled = true;
                    toolStripMenuItemSave.Enabled = false;
                    toolStripMenuItemSaveAll.Enabled = true;

                    //Edit Menu
                    toolStripMenuItemUndo.Enabled = false;
                    toolStripMenuItemRedo.Enabled = false;
                    toolStripMenuItemCopy.Enabled = false;
                    toolStripMenuItemCut.Enabled = false;
                    toolStripMenuItemPaste.Enabled = false;
                    toolStripMenuItemSelectAll.Enabled = false;
                    toolStripMenuItemFindAndReplace.Enabled = false;
                    toolStripMenuItemAddElement.Enabled = false;
                    toolStripMenuItemProperties.Enabled = true;

                    //Debug
                    toolStripMenuItemCleanSolution.Enabled = false;
                    toolStripMenuItemStart.Enabled = false;
                    toolStripMenuItemStartWithoutBuild.Enabled = false;
                    buildToolStripMenuItem.Enabled = false;

                    //Tools
                    toolStripMenuItemEnterEvaluationMode.Enabled = true;

                    //Toolstrips
                    saveCurrentFileButton.Enabled = false;
                    saveAllButton.Enabled = true;
                    buildButton.Enabled = false;
                    buildAndRunButton.Enabled = false;
                    startButton.Enabled = false;
                    stopButton.Enabled = false;
                    #endregion
                    CleanEditorTabs();
                    break;
                case StudioState.NoSolution:
                    #region Settings
                    //File Menu
                    addToolStripMenuItem.Enabled = false;
                    projectToolStripMenuItem.Enabled = false;
                    elementToolStripMenuItem.Enabled = false;
                    toolStripMenuItemClose.Enabled = false;
                    toolStripMenuItemCloseSolution.Enabled = false;
                    toolStripMenuItemSave.Enabled = false;
                    toolStripMenuItemSaveAll.Enabled = false;

                    //Edit Menu Later
                    toolStripMenuItemUndo.Enabled = false;
                    toolStripMenuItemRedo.Enabled = false;
                    toolStripMenuItemCopy.Enabled = false;
                    toolStripMenuItemCut.Enabled = false;
                    toolStripMenuItemPaste.Enabled = false;
                    toolStripMenuItemSelectAll.Enabled = false;
                    toolStripMenuItemFindAndReplace.Enabled = false;
                    toolStripMenuItemAddElement.Enabled = false;
                    toolStripMenuItemProperties.Enabled = false;

                    //Debug
                    toolStripMenuItemCleanSolution.Enabled = false;
                    toolStripMenuItemStart.Enabled = false;
                    toolStripMenuItemStartWithoutBuild.Enabled = false;
                    buildToolStripMenuItem.Enabled = false;

                    //Tools
                    toolStripMenuItemEnterEvaluationMode.Enabled = false;

                    //Toolstrips
                    saveCurrentFileButton.Enabled = false;
                    saveAllButton.Enabled = false;
                    buildButton.Enabled = false;
                    buildAndRunButton.Enabled = false;
                    startButton.Enabled = false;
                    stopButton.Enabled = false;
                    #endregion
                    CleanEditorTabs();
                    break;
                case StudioState.ProjectSolutionOpen:
                    #region Settings
                    //File Menu
                    addToolStripMenuItem.Enabled = true;
                    projectToolStripMenuItem.Enabled = true;
                    elementToolStripMenuItem.Enabled = true;
                    toolStripMenuItemClose.Enabled = true;
                    toolStripMenuItemCloseSolution.Enabled = true;
                    toolStripMenuItemSave.Enabled = true;
                    toolStripMenuItemSaveAll.Enabled = true;

                    //Edit Menu Later
                    toolStripMenuItemUndo.Enabled = false;
                    toolStripMenuItemRedo.Enabled = false;
                    toolStripMenuItemCopy.Enabled = false;
                    toolStripMenuItemCut.Enabled = false;
                    toolStripMenuItemPaste.Enabled = false;
                    toolStripMenuItemSelectAll.Enabled = false;
                    toolStripMenuItemFindAndReplace.Enabled = false;
                    toolStripMenuItemAddElement.Enabled = true;
                    toolStripMenuItemProperties.Enabled = true;

                    //Debug
                    toolStripMenuItemCleanSolution.Enabled = true;
                    toolStripMenuItemStart.Enabled = true;
                    toolStripMenuItemStartWithoutBuild.Enabled = true;
                    buildToolStripMenuItem.Enabled = true;

                    //Tools
                    toolStripMenuItemEnterEvaluationMode.Enabled = true;

                    //Toolstrips
                    saveCurrentFileButton.Enabled = true;
                    saveAllButton.Enabled = true;
                    buildButton.Enabled = true;
                    buildAndRunButton.Enabled = true;
                    startButton.Enabled = true;
                    stopButton.Enabled = false;
                    #endregion
                    break;
                case StudioState.Running:
                    #region Settings
                    //File Menu
                    addToolStripMenuItem.Enabled = false;
                    projectToolStripMenuItem.Enabled = false;
                    elementToolStripMenuItem.Enabled = false;
                    toolStripMenuItemClose.Enabled = false;
                    toolStripMenuItemCloseSolution.Enabled = false;
                    toolStripMenuItemSave.Enabled = false;
                    toolStripMenuItemSaveAll.Enabled = false;

                    //Edit Menu Later
                    toolStripMenuItemUndo.Enabled = false;
                    toolStripMenuItemRedo.Enabled = false;
                    toolStripMenuItemCopy.Enabled = false;
                    toolStripMenuItemCut.Enabled = false;
                    toolStripMenuItemPaste.Enabled = false;
                    toolStripMenuItemSelectAll.Enabled = false;
                    toolStripMenuItemFindAndReplace.Enabled = false;
                    toolStripMenuItemAddElement.Enabled = false;
                    toolStripMenuItemProperties.Enabled = false;

                    //Debug
                    toolStripMenuItemCleanSolution.Enabled = false;
                    toolStripMenuItemStart.Enabled = false;
                    toolStripMenuItemStartWithoutBuild.Enabled = false;
                    buildToolStripMenuItem.Enabled = false;

                    //Tools
                    toolStripMenuItemEnterEvaluationMode.Enabled = false;

                    //Toolstrips
                    saveCurrentFileButton.Enabled = false;
                    saveAllButton.Enabled = false;
                    buildButton.Enabled = false;
                    buildAndRunButton.Enabled = false;
                    startButton.Enabled = false;
                    stopButton.Enabled = true;
                    #endregion
                    break;
            }

            bool isFileOpen = false;

            for (int i = 0; i < tabControlMain.Tabs.Count; i++)
            {
                try
                {
                    EditPad ep = (EditPad)tabControlMain.Tabs[i].Child;

                    toolStripMenuItemUndo.Enabled = true;
                    toolStripMenuItemRedo.Enabled = true;
                    toolStripMenuItemCopy.Enabled = true;
                    toolStripMenuItemCut.Enabled = true;
                    toolStripMenuItemPaste.Enabled = true;
                    toolStripMenuItemSelectAll.Enabled = true;
                    toolStripMenuItemFindAndReplace.Enabled = true;
                }
                catch
                {

                }
            }
        }
        void StateManagerTransitionFailed(object sender, StateManager.StateManagerEventArgs e)
        {

        }
        void CleanEditorTabs()
        {
            for (int i = 0; i < tabControlMain.Tabs.Count; i++)
            {
                if (tabControlMain.Tabs[i].Tag != null)
                {
                    tabControlMain.DeleteTab(i);
                }
            }

            if (tabControlMain.Tabs.Count == 0)
            {
                LoadStartPage();
            }
        }
        private void toolStripDebug_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
        private void toolStripMenuItemAbout_Click(object sender, EventArgs e)
        {
            About aboutDialog = new About();
            aboutDialog.ShowDialog(this);
        }
        private void MainForm_KeyDownWPF(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (((e.Key & System.Windows.Input.Key.LeftCtrl) == System.Windows.Input.Key.LeftCtrl) || ((e.Key & System.Windows.Input.Key.RightCtrl) == System.Windows.Input.Key.Right))
            {
                if ((e.Key & System.Windows.Input.Key.Tab) == System.Windows.Input.Key.Tab)
                {
                    if (clipboardInterface.Clipboard.ClipboardCount > 0)
                    {
                        clipboardInterface.ShowDialog();
                    }
                }
                if ((e.Key & System.Windows.Input.Key.C) == System.Windows.Input.Key.C)
                {
                    CopyToClipboardManager();
                }
            }
        }
        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control)
            {
                if ((e.KeyCode & Keys.Tab) == Keys.Tab)
                {
                    if (clipboardInterface.Clipboard.ClipboardCount > 0)
                    {
                        clipboardInterface.ShowDialog();
                    }
                }

                if ((e.KeyCode & Keys.C) == Keys.C)
                {
                    CopyToClipboardManager();
                }
            }
        }
        private void CopyToClipboardManager()
        {
            if (tabControlMain.Tabs.Count > 0 && tabControlMain.Tabs[tabControlMain.SelectedIndex].Title != "Start Page" && ((EditPad)(tabControlMain.Tabs[tabControlMain.SelectedIndex].Child)).Editor.SelectionLength > 0)
            {
                string text = ((EditPad)(tabControlMain.Tabs[tabControlMain.SelectedIndex].Child)).SelectedText;
                Clipboard.SetText(text);
                clipboardInterface.AddClipboardItem(text);
            }
        }
        void toolStripMenuItemCopy_Click(object sender, System.EventArgs e)
        {
            CopyToClipboardManager();
        }
        private void keyMonitor1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.LButton)
            {
                ThreadSafeFocusFormAccessor();
            }
            else if (e.KeyCode == Keys.Enter)
            {
                ThreadSafeOpenTabAccessor();
            }
            else if (e.KeyCode == Keys.Escape)
            {
                ThreadSafeTerminateExecutionAccessor();
            }
        }
        private void keyMonitor1_KeyToggled(object sender, KeyEventArgs e)
        {

        }
        private void keyMonitor1_KeyUp(object sender, KeyEventArgs e)
        {

        }
        private void keyMonitor1_KeyCombinationDown(object sender, Controls.KeyMonitor.KeyCombinationEventArgs e)
        {
            ThreadSafeProcessCombinationAccessor(e.Combination);
        }
        private void toolStripMenuItemBitwiseSandbox_Click_1(object sender, EventArgs e)
        {
            BitwiseSandbox b = new BitwiseSandbox();
            b.MaximizeBox = false;

            b.ShowDialog();
        }
        public void LoadPortfolio(int index)
        {
            string url = "";
            int tabIndex = -1;
            switch (index)
            {
                case 0:
                    if (tabControlMain.GetTabByTitle("Zach Howard-Smith") == -1)
                    {
                        url = "http://www.zachhoward-smith.com";
                        tabIndex = tabControlMain.AddTab("Zach Howard-Smith");
                    }
                    break;
                case 1:
                    if (tabControlMain.GetTabByTitle("Zach Ross-Clyne") == -1)
                    {
                        url = "http://www.zrossclyne.co.uk";
                        tabIndex = tabControlMain.AddTab("Zach Ross-Clyne");
                    }
                    break;
            }

            WebBrowser browser = new WebBrowser();
            browser.Navigate(url);
            tabControlMain.AttachControl(browser, tabIndex);
        }
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (stateManager.CurrentState == StudioState.Running)
            {
                TerminateExecution();
                e.Cancel = true;
            }
            else
            {
                if (Globals.CurrentSolution != null)
                {
                    DialogResult dialogResult = MessageBox.Show("You are about to exit GBA Studio 2016 and close the solution you have open.\nDo you want to continue?", "Do you want to exit?", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.No)
                    {
                        e.Cancel = true;
                    }
                }
            }
        }
        private void toolStripMenuItemBugManagement_Click(object sender, EventArgs e)
        {
            BugManagement bugMgr = new BugManagement();
            bugMgr.ShowDialog();
        }
        private void reportABugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BugManagement bugMgr = new BugManagement(true);
            bugMgr.ShowDialog();
        }
        #region Thread-Safe Callbacks
        /// <summary>
        /// The delegate functions used to safely make cross-thread function calls.
        /// </summary>
        #region Delegates
        private delegate void ThreadSafeFocusFormDelegate();
        private delegate void ThreadSafeOpenTabDelegate();
        private delegate void ThreadSafeCloseErrorListDelegate();
        private delegate void ThreadSafeRollBackStateDelegate();
        private delegate void ThreadSafeStopExecutionDelegate();
        private delegate void ThreadSafeProcessCombinationDelegate(List<Keys> combination);
        private delegate void ThreadSafeTerminateExecutionDelegate();
        private delegate void ThreadSafeScrollToEndDelegate();

        #endregion

        /// <summary>
        /// Functions used to safely cross-thread call functions.
        /// </summary>
        #region Accessors
        private void ThreadSafeFocusFormAccessor()
        {
            if (this.InvokeRequired)
            {
                ThreadSafeFocusFormDelegate del = new ThreadSafeFocusFormDelegate(ThreadSafeFocusFormCallback);
                this.Invoke(del);
            }
            else
            {
                ThreadSafeFocusFormCallback();
            }
        }
        private void ThreadSafeOpenTabAccessor()
        {
            if (this.InvokeRequired)
            {
                ThreadSafeOpenTabDelegate del = new ThreadSafeOpenTabDelegate(ThreadSafeOpenTabCallback);
                this.Invoke(del);
            }
            else
            {
                ThreadSafeOpenTabCallback();
            }
        }
        private void ThreadSafeCloseErrorListAccessor()
        {
            if (this.InvokeRequired)
            {
                ThreadSafeCloseErrorListDelegate del = new ThreadSafeCloseErrorListDelegate(ThreadSafeCloseErrorListCallback);
                this.Invoke(del);
            }
            else
            {
                ThreadSafeCloseErrorListCallback();
            }
        }
        private void ThreadSafeRollBackStateAccessor()
        {
            if (this.InvokeRequired)
            {
                ThreadSafeRollBackStateDelegate del = new ThreadSafeRollBackStateDelegate(ThreadSafeRollBackStateCallback);
                this.Invoke(del);
            }
            else
            {
                ThreadSafeRollBackStateCallback();
            }
        }
        private void ThreadSafeStopExecutionAccessor()
        {
            if (this.InvokeRequired)
            {
                ThreadSafeStopExecutionDelegate del = new ThreadSafeStopExecutionDelegate(ThreadSafeStopExecutionCallback);
                this.Invoke(del);
            }
            else
            {
                ThreadSafeStopExecutionCallback();
            }
        }
        private void ThreadSafeProcessCombinationAccessor(List<Keys> combination)
        {
            if (this.InvokeRequired)
            {
                ThreadSafeProcessCombinationDelegate del = new ThreadSafeProcessCombinationDelegate(ThreadSafeProcessCombinationCallback);
                this.Invoke(del, new Object[] { combination });
            }
            else
            {
                ThreadSafeStopExecutionCallback();
            }
        }
        private void ThreadSafeTerminateExecutionAccessor()
        {
            if (this.InvokeRequired)
            {
                ThreadSafeTerminateExecutionDelegate del = new ThreadSafeTerminateExecutionDelegate(ThreadSafeTerminateExecutionCallback);
                this.Invoke(del);
            }
            else
            {
                ThreadSafeTerminateExecutionCallback();
            }
        }
        private void ThreadSafeScrollToEndAccessor()
        {
            if (this.InvokeRequired)
            {
                ThreadSafeScrollToEndDelegate del = new ThreadSafeScrollToEndDelegate(ThreadSafeScrollToEndCallback);
                this.Invoke(del, null);
            }
            else
            {
                ThreadSafeScrollToEndCallback();
            }
        }
        #endregion

        /// <summary>
        /// The functions that have potential to be called from multiple threads.
        /// </summary>
        #region Functions
        private void ThreadSafeFocusFormCallback()
        {
            bool isFocused = false;
            foreach (Form f in Application.OpenForms)
            {
                isFocused |= f.ContainsFocus;
            }
            if (!isFocused)
            {
                this.Focus();
            }
        }
        private void ThreadSafeOpenTabCallback()
        {
            if (solutionExplorer != null && solutionExplorer.Focused && this.Focused)
            {
                if (File.Exists(solutionExplorer.SelectedNode.Tag.ToString()))
                {
                    OpenOrSelectTab(Globals.CurrentSolution.GetProjectFromName(GetProjectNameFromNode(solutionExplorer.SelectedNode)), solutionExplorer.SelectedNode.Tag.ToString());
                }
            }
        }
        private void ThreadSafeCloseErrorListCallback()
        {
            if (tabControlSecondary.GetTabByTitle("Error List") >= 0)
            {
                tabControlSecondary.DeleteTab(tabControlSecondary.GetTabByTitle("Error List"));
            }
        }
        private void ThreadSafeRollBackStateCallback()
        {
            stateManager.Rollback();
        }
        private void ThreadSafeStopExecutionCallback()
        {
            if (runState == Globals.RunState.Stopped && stateManager.Transition(StudioState.ProjectSolutionOpen))
            {
                splitContainer6.Panel1Collapsed = false;
                splitContainer6.Panel2Collapsed = true;
                runState = Globals.RunState.Idle;
            }
        }
        private void ThreadSafeProcessCombinationCallback(List<Keys> keys)
        {
            Keys combination = Keys.None;
            foreach (Keys key in keys)
            {
                combination |= key;
            }

            switch (combination)
            {
                case (Keys.LControlKey | Keys.F4):
                    if (this.ContainsFocus)
                    {
                        Application.Exit();
                    }
                    break;
            }
        }
        private void ThreadSafeTerminateExecutionCallback()
        {
            TerminateExecution();
        }
        private void ThreadSafeScrollToEndCallback()
        {
            if (consoleBox != null && consoleBox.TextLength > 0)
            {
                consoleBox.ScrollToBottom();
            }
        }
        #endregion
        private void toolStripMenuItemBitwiseSandbox_Click(object sender, EventArgs e)
        {
            if (bitwiseSandbox == null || bitwiseSandbox.IsDisposed)
            {
                bitwiseSandbox = new BitwiseSandbox();
            }
            bitwiseSandbox.Show();
            bitwiseSandbox.BringToFront();
        }
        #endregion

        private void toolStripMenuItemSpriteCanvas_Click(object sender, EventArgs e)
        {
            Sprite_Designer s = new Sprite_Designer();
            s.Show();
        }

        private void goolStripMenuItemGuest_Click(object sender, EventArgs e)
        {
            UserAuthenticate ua = new UserAuthenticate();
            ua.ShowDialog();
        }

        private void toolStripMenuItemFindAndReplace_Click(object sender, EventArgs e)
        {
            if (tabControlMain.Tabs[tabControlMain.SelectedIndex].Tag != null)
            {
                //It has an EditPad, therefore we can search.
                findReplaceForm = new FindReplace((EditPad)tabControlMain.Tabs[tabControlMain.SelectedIndex].Child);
                findReplaceForm.ShowDialog();
            }
        }

        private void tabControlMain_TabClosed(object sender, int index)
        {
            if (tabControlMain.Tabs.Count == 1)
            {
                string title = tabControlMain.Tabs[tabControlMain.SelectedIndex].Title;

                if (title == "Start Page" || title == "Zach Ross-Clyne" || title == "Zac Howard-Smith")
                {
                    toolStripMenuItemUndo.Enabled = false;
                    toolStripMenuItemRedo.Enabled = false;
                    toolStripMenuItemCopy.Enabled = false;
                    toolStripMenuItemCut.Enabled = false;
                    toolStripMenuItemPaste.Enabled = false;
                    toolStripMenuItemSelectAll.Enabled = false;
                    toolStripMenuItemFindAndReplace.Enabled = false;
                }
            }
            else if (tabControlMain.Tabs.Count == 0)
            {
                toolStripMenuItemUndo.Enabled = false;
                toolStripMenuItemRedo.Enabled = false;
                toolStripMenuItemCopy.Enabled = false;
                toolStripMenuItemCut.Enabled = false;
                toolStripMenuItemPaste.Enabled = false;
                toolStripMenuItemSelectAll.Enabled = false;
                toolStripMenuItemFindAndReplace.Enabled = false;
            }
        }

        private void toolStripMenuItemUndo_Click(object sender, EventArgs e)
        {
            try
            {
                EditPad ep = (EditPad)tabControlMain.Tabs[tabControlMain.SelectedIndex].Child;
                ep.Undo();
            }
            catch
            {

            }
        }

        private void toolStripMenuItemRedo_Click(object sender, EventArgs e)
        {
            try
            {
                EditPad ep = (EditPad)tabControlMain.Tabs[tabControlMain.SelectedIndex].Child;
                ep.Redo();
            }
            catch
            {

            }
        }

        private void toolStripMenuItemCut_Click(object sender, EventArgs e)
        {
            try
            {
                EditPad ep = (EditPad)tabControlMain.Tabs[tabControlMain.SelectedIndex].Child;
                CopyToClipboardManager();

                ep.Replace("");
            }
            catch
            {

            }
        }

        private void toolStripMenuItemPaste_Click(object sender, EventArgs e)
        {
            try
            {
                EditPad ep = (EditPad)tabControlMain.Tabs[tabControlMain.SelectedIndex].Child;
                ep.Paste();
            }
            catch
            {

            }
        }

        private void toolStripMenuItemDelete_Click(object sender, EventArgs e)
        {
        }

        private void toolStripMenuItemSelectAll_Click(object sender, EventArgs e)
        {

            try
            {
                EditPad ep = (EditPad)tabControlMain.Tabs[tabControlMain.SelectedIndex].Child;
                ep.SelectText(0, ep.Text.Length);
            }
            catch
            {

            }
        }
        #region SmartSense
        void InitializeSmartSense()
        {
            // Initialize the smart sense engine
            senseEngine = new SenseEngine(smartSense1);
            SenseEngine.ManagerDelegate variableDelegate = new SenseEngine.ManagerDelegate(variableManager);
            SenseEngine.ManagerDelegate structureDelegate = new SenseEngine.ManagerDelegate(structureManager);
            SenseEngine.ManagerDelegate functionDelegate = new SenseEngine.ManagerDelegate(functionManager);
            SenseEngine.ManagerDelegate enumerationDelegate = new SenseEngine.ManagerDelegate(enumerationManager);
            SenseEngine.ManagerDelegate includeDelegate = new SenseEngine.ManagerDelegate(includeManager);

            senseEngine.AddElement("Variable", new Regex(@"^\s*(?!return |typedef )((\w+\s*\*?\s+)+)+(\w+)(\[\w*\])?(\s*=|;)", RegexOptions.Multiline), variableDelegate);
            senseEngine.AddElement("Structure", new Regex(@"struct\s+([^\s]+)\s*{([^}]+)}", RegexOptions.Multiline), structureDelegate);
            senseEngine.AddElement("Function", new Regex(@"(\w+) ([^\(\s]+)\(([^\)]*)\)[^{\n]*[\s]*{"), functionDelegate);
            senseEngine.AddElement("Enumeration", new Regex(@"enum ([^{]+){([^}]+)}"), enumerationDelegate);
            senseEngine.AddElement("Include", new Regex("#include \"([^\"]+)\"", RegexOptions.Multiline), includeDelegate);

            //smartSense1.AddEntry("test1");
            //smartSense1.AddEntry("test2");
        }

        List<Tuple<string, string>> variableManager(MatchCollection matches)
        {
            List<Tuple<string, string>> data = new List<Tuple<string, string>>();
            foreach (Match match in matches)
            {
                // Returns a tuple containing the type and the name of the variable.
                Tuple<string, string> returnPacket = new Tuple<string, string>(match.Groups[2].Value, match.Groups[3].Value);
                data.Add(returnPacket);
            }
            return data;
        }
        List<Tuple<string, string>> structureManager(MatchCollection matches)
        {
            List<Tuple<string, string>> data = new List<Tuple<string, string>>();
            foreach (Match match in matches)
            {
                // Returns a tuple containing the return type and the name of the function.
                Tuple<string, string> returnPacket = new Tuple<string, string>(match.Groups[0].Value, match.Groups[1].Value);
                data.Add(returnPacket);
            }
            return null;
            return data;
        }
        List<Tuple<string, string>> functionManager(MatchCollection matches)
        {
            List<Tuple<string, string>> data = new List<Tuple<string, string>>();
            foreach (Match match in matches)
            {
                // Returns a tuple containing the return type and the name of the function.
                Tuple<string, string> returnPacket = new Tuple<string, string>(match.Groups[1].Value, match.Groups[2].Value);
                data.Add(returnPacket);
            }
            //return null;
            return data;
        }
        List<Tuple<string, string>> enumerationManager(MatchCollection matches)
        {
            return null;
        }
        List<Tuple<string, string>> includeManager(MatchCollection matches)
        {
            return null;
        }


        string smartSenseFilter = "";
        void RunSmartSense(char character, EditPad.KeyCharEventArgs e, out bool cancel)
        {
            cancel = false;
            switch (character)
            {
                case (char)13: // Enter/Return
                    smartSense1.Visible = false;
                    // Complete the word
                    break;
                case '.':
                    smartSense1.Visible = true;
                    // Enable Control
                    senseEngine.PopulateControl();
                    MoveSmartSense();
                    break;
                case (char)8: // Backspace
                    if (smartSenseFilter.Length > 0)
                    {
                        smartSenseFilter = smartSenseFilter.Substring(0, smartSenseFilter.Length - 1);
                        senseEngine.SetFilter(smartSenseFilter);
                        senseEngine.PopulateControl();
                    }
                    break;
                case ' ':
                    if (Control.ModifierKeys != Keys.Shift && Control.ModifierKeys != Keys.Control)
                    {
                        smartSenseFilter = "";
                        senseEngine.SetFilter(smartSenseFilter);
                        smartSense1.Visible = false;
                    }
                    break;
                case '\t':
                    if (smartSense1.Visible)
                    {
                        string val = smartSense1.GetSelectedValue();

                        val = val.Substring(smartSenseFilter.Length);
                        smartSenseFilter = "";
                        smartSense1.Visible = false;
                        senseEngine.SetFilter(smartSenseFilter);

                        string curClipboard = Clipboard.GetText();
                        Clipboard.SetText(val);
                        ((EditPad)tabControlMain.Tabs[tabControlMain.SelectedIndex].Child).Paste();
                        Clipboard.SetText(curClipboard);

                        cancel = true;
                    }
                    break;
                default:
                    // Update Filter.
                    if ((character >= 48 && character <= 57) || (character >= 65 && character <= 90) || (character >= 97 && character <= 122))
                    {
                        smartSense1.Visible = true;

                        // Update Filter
                        smartSenseFilter += character;

                        senseEngine.SetFilter(smartSenseFilter);
                        senseEngine.PopulateControl();

                        if (smartSenseFilter.Length == 1)
                        {
                            MoveSmartSense();
                        }
                    }
                    break;
            }
            toolStripMenuItemHelp.Text = smartSenseFilter;
            // If it's a valid point.
        }

        private void MoveSmartSense(bool isScrolling = false)
        {
            Point drawPoint = new Point(-1, -1);
            try
            {
                drawPoint = ((EditPad)tabControlMain.Tabs[tabControlMain.SelectedIndex].Child).GetCaretPosition();
            }
            catch
            {

            }

            if (drawPoint.X >= 0)
            {
                if (!isScrolling)
                {
                    smartSense1.Left = drawPoint.X + splitContainer2.Panel1.Width;
                }
                smartSense1.Top = drawPoint.Y + splitContainer1.Panel1.Height + this.Padding.Top + menuStrip1.Height;
            }
        }
        #endregion

        private void smartSense1_Click(object sender, EventArgs e)
        {
            tabControlMain.Tabs[tabControlMain.SelectedIndex].Child.Focus();
        }
    }



    public enum ConsoleErrorType
    {
        ERROR,
        WARNING
    }

    public class ConsoleError
    {
        public bool IsFatal
        {
            get;
            set;
        }

        public int LineNumber
        {
            get;
            set;
        }

        public int ColumnNumber
        {
            get;
            set;
        }

        public string Message
        {
            get;
            set;
        }

        public string FileName
        {
            get;
            set;
        }

        public ConsoleErrorType Type
        {
            get;
            set;
        }
    }
}
