﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GBA_Studio.Controls;

namespace GBA_Studio.Forms
{
    public partial class FindReplace : VSForm
    {
        enum FIND_STYLE
        {
            FIND_STYLE_FIND,
            FIND_STYLE_REPLACE,
            FIND_STYLE_REPLACE_ALL
        }

        EditPad editPad;

        public FindReplace(EditPad ctrl)
        {
            editPad = ctrl;
            InitializeComponent();
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            ExecuteFindReplace((ButtonPro)sender);
        }

        private void ExecuteFindReplace(ButtonPro sender)
        {
            string strFind = tbFind.Text;
            string strReplace = tbReplace.Text;
            FIND_STYLE style = DetermineStyle(sender);

            switch (style)
            {
                case FIND_STYLE.FIND_STYLE_FIND:
                    Find(strFind);
                    break;
                case FIND_STYLE.FIND_STYLE_REPLACE:
                    Replace(strFind, strReplace);
                    break;
                case FIND_STYLE.FIND_STYLE_REPLACE_ALL:
                    editPad.Text = editPad.Text.Replace(strFind, strReplace);
                    break;
            }
        }

        private FIND_STYLE DetermineStyle(ButtonPro sender)
        {
            switch(sender.ButtonText.ToLower())
            {
                case "find":
                    return FIND_STYLE.FIND_STYLE_FIND;
                case "replace":
                    return FIND_STYLE.FIND_STYLE_REPLACE;
                default:
                    return FIND_STYLE.FIND_STYLE_REPLACE_ALL;
            }
        }

        private Tuple<int, int> Find(string find)
        {
            StatusText = "";
            int index = 0;
            int cpos = editPad.CurrentCaretPosition();
            bool complete = false;
            do
            {
                index = editPad.Text.IndexOf(find, cpos);
                if (index == -1)
                {
                    //None left past this point/Non-existant in document.
                    if(editPad.Text.Contains(find))
                    {
                        //Exists in document, but not after the caret. Move to top
                        index = 0;
                        cpos = 0;
                    }
                    else
                    {
                        //Doesn't exist.
                        StatusText = "No more occurrances.";
                        break;
                    }
                }
                else
                {
                    //Prevents selection of 0 - element-length bug introduced by ring searching
                    complete = true;
                }
            } while (complete == false || index < cpos);

            //Select the text
            if (index >= 0)
            {
                editPad.SelectText(index, find.Length);
                editPad.ScrollToSelection();
            }

            return new Tuple<int, int>(index, find.Length);
        }

        private bool Replace(string find, string replace)
        {
            bool replaced = false;
            if(editPad.SelectedText == find)
            {
                editPad.Replace(replace);
                replaced = true;
            }
            Find(find);
            return replaced;
        }

        private void tbFind_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                ExecuteFindReplace(btnFind);
            }
            else if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void tbReplace_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ExecuteFindReplace(btnReplace);
            }
            else if(e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
    }
}
