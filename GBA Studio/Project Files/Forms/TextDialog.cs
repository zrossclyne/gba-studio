﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GBA_Studio.Controls;

namespace GBA_Studio.Forms
{
    public partial class TextDialog : VSForm
    {
        public string UserText
        {
            get
            {
                return textBox.Text;
            }
            set
            {
                textBox.Text = value;
            }
        }

        public void TextBoxFocus()
        {
            this.textBox.Focus();
        }

        public TextDialog(string title)
        {
            InitializeComponent();
            this.Text = title;

            this.AcceptButton = okButton;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            Accept();
        }

        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                Accept();
            }
        }

        private void Accept()
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
