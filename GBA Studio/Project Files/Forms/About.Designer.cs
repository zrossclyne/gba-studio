﻿namespace GBA_Studio.Forms
{
    partial class About
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelVersion = new System.Windows.Forms.Label();
            this.labelZac = new System.Windows.Forms.LinkLabel();
            this.labelZach = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Segoe UI Light", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.ForeColor = System.Drawing.Color.White;
            this.labelTitle.Location = new System.Drawing.Point(93, 3);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(208, 37);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "GBA Studio 2016";
            // 
            // labelVersion
            // 
            this.labelVersion.AutoSize = true;
            this.labelVersion.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.labelVersion.ForeColor = System.Drawing.Color.White;
            this.labelVersion.Location = new System.Drawing.Point(181, 40);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(44, 19);
            this.labelVersion.TabIndex = 1;
            this.labelVersion.Text = "label2";
            // 
            // labelZac
            // 
            this.labelZac.AutoSize = true;
            this.labelZac.Font = new System.Drawing.Font("Segoe UI Light", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelZac.LinkColor = System.Drawing.Color.White;
            this.labelZac.Location = new System.Drawing.Point(105, 70);
            this.labelZac.Name = "labelZac";
            this.labelZac.Size = new System.Drawing.Size(171, 25);
            this.labelZac.TabIndex = 2;
            this.labelZac.TabStop = true;
            this.labelZac.Text = "Zach Howard-Smith";
            this.labelZac.VisitedLinkColor = System.Drawing.Color.White;
            this.labelZac.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.label_LinkClicked);
            // 
            // labelZach
            // 
            this.labelZach.AutoSize = true;
            this.labelZach.Font = new System.Drawing.Font("Segoe UI Light", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelZach.LinkColor = System.Drawing.Color.White;
            this.labelZach.Location = new System.Drawing.Point(105, 99);
            this.labelZach.Name = "labelZach";
            this.labelZach.Size = new System.Drawing.Size(144, 25);
            this.labelZach.TabIndex = 3;
            this.labelZach.TabStop = true;
            this.labelZach.Text = "Zach Ross-Clyne";
            this.labelZach.VisitedLinkColor = System.Drawing.Color.White;
            this.labelZach.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.label_LinkClicked);
            // 
            // About
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 205);
            this.Controls.Add(this.labelZach);
            this.Controls.Add(this.labelZac);
            this.Controls.Add(this.labelVersion);
            this.Controls.Add(this.labelTitle);
            this.Location = new System.Drawing.Point(0, 0);
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(415, 205);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(415, 205);
            this.Name = "About";
            this.Padding = new System.Windows.Forms.Padding(0, 29, 0, 29);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusText = "Click our names and visit our sites!";
            this.Text = "";
            this.WindowState = System.Windows.Forms.FormWindowState.Normal;
            this.Load += new System.EventHandler(this.About_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.LinkLabel labelZach;
        private System.Windows.Forms.LinkLabel labelZac;
        private System.Windows.Forms.Label labelVersion;
        private System.Windows.Forms.Label labelTitle;
    }
}