﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime;

using GBA_Studio.Tools;

namespace GBA_Studio.Forms
{
    public partial class About : VSForm
    {
        public About()
        {
            InitializeComponent();
        }

        private void About_Load(object sender, EventArgs e)
        {
            labelVersion.Text = Globals.CurrentVersion.ToString();

            labelTitle.Left = GetDrawPosition(labelTitle.Width);
            labelVersion.Left = GetDrawPosition(labelVersion.Width);
            labelZac.Left = GetDrawPosition(labelZac.Width);
            labelZach.Left = GetDrawPosition(labelZach.Width);

            int[] position = { labelZac.Top, labelZach.Top };
            Random rand = new Random();
            int ran = rand.Next(0, 2);
            labelZac.Top = position[ran];
            labelZach.Top = position[Math.Abs(ran - 1)];

        }

        private int GetDrawPosition(int width)
        {
            return (this.Width - width) / 2;
        }

        private void label_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            switch(((LinkLabel)sender).Text)
            {
                case "Zach Howard-Smith":
                    ((MainForm)Owner).LoadPortfolio(0);
                    break;
                case "Zach Ross-Clyne":
                    ((MainForm)Owner).LoadPortfolio(1);
                    break;
            }
        }

        
    }
}
