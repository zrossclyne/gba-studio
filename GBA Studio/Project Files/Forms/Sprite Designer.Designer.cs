﻿namespace GBA_Studio.Forms
{
    partial class Sprite_Designer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                keyMonitor1.Terminate();
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.canvasTimer = new System.Windows.Forms.Timer(this.components);
            this.splitDesigner = new System.Windows.Forms.SplitContainer();
            this.pictureBoxPalette = new System.Windows.Forms.PictureBox();
            this.pictureBoxDesigner = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fILEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eDITToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.swapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitWindow = new System.Windows.Forms.SplitContainer();
            this.gbPalette = new System.Windows.Forms.GroupBox();
            this.rbRGBColour = new System.Windows.Forms.RadioButton();
            this.rbIntegerColour = new System.Windows.Forms.RadioButton();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.gbObject = new System.Windows.Forms.GroupBox();
            this.rbHexadecimalObject = new System.Windows.Forms.RadioButton();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.rbIntegerObject = new System.Windows.Forms.RadioButton();
            this.keyMonitor1 = new GBA_Studio.Controls.KeyMonitor(this.components);
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitDesigner)).BeginInit();
            this.splitDesigner.Panel1.SuspendLayout();
            this.splitDesigner.Panel2.SuspendLayout();
            this.splitDesigner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPalette)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDesigner)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitWindow)).BeginInit();
            this.splitWindow.Panel1.SuspendLayout();
            this.splitWindow.Panel2.SuspendLayout();
            this.splitWindow.SuspendLayout();
            this.gbPalette.SuspendLayout();
            this.gbObject.SuspendLayout();
            this.SuspendLayout();
            // 
            // canvasTimer
            // 
            this.canvasTimer.Enabled = true;
            this.canvasTimer.Interval = 1;
            this.canvasTimer.Tick += new System.EventHandler(this.canvasTimer_Tick);
            // 
            // splitDesigner
            // 
            this.splitDesigner.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitDesigner.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitDesigner.IsSplitterFixed = true;
            this.splitDesigner.Location = new System.Drawing.Point(0, 0);
            this.splitDesigner.Name = "splitDesigner";
            this.splitDesigner.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitDesigner.Panel1
            // 
            this.splitDesigner.Panel1.Controls.Add(this.pictureBoxPalette);
            // 
            // splitDesigner.Panel2
            // 
            this.splitDesigner.Panel2.Controls.Add(this.pictureBoxDesigner);
            this.splitDesigner.Size = new System.Drawing.Size(135, 100);
            this.splitDesigner.SplitterDistance = 71;
            this.splitDesigner.SplitterWidth = 1;
            this.splitDesigner.TabIndex = 1;
            // 
            // pictureBoxPalette
            // 
            this.pictureBoxPalette.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxPalette.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxPalette.Name = "pictureBoxPalette";
            this.pictureBoxPalette.Size = new System.Drawing.Size(135, 71);
            this.pictureBoxPalette.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxPalette.TabIndex = 0;
            this.pictureBoxPalette.TabStop = false;
            this.pictureBoxPalette.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBoxPalette_MouseMove);
            this.pictureBoxPalette.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxPalette_MouseUp);
            // 
            // pictureBoxDesigner
            // 
            this.pictureBoxDesigner.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.pictureBoxDesigner.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxDesigner.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxDesigner.Name = "pictureBoxDesigner";
            this.pictureBoxDesigner.Size = new System.Drawing.Size(135, 28);
            this.pictureBoxDesigner.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxDesigner.TabIndex = 0;
            this.pictureBoxDesigner.TabStop = false;
            this.pictureBoxDesigner.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBoxDesigner_MouseMove);
            this.pictureBoxDesigner.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBoxDesigner_MouseUp);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fILEToolStripMenuItem,
            this.eDITToolStripMenuItem,
            this.swapToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 25);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(407, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fILEToolStripMenuItem
            // 
            this.fILEToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.exportToolStripMenuItem});
            this.fILEToolStripMenuItem.Name = "fILEToolStripMenuItem";
            this.fILEToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.fILEToolStripMenuItem.Text = "&FILE";
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.importToolStripMenuItem.Text = "Import";
            this.importToolStripMenuItem.Click += new System.EventHandler(this.importToolStripMenuItem_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exportToolStripMenuItem.Text = "Save As";
            this.exportToolStripMenuItem.Click += new System.EventHandler(this.exportToolStripMenuItem_Click);
            // 
            // eDITToolStripMenuItem
            // 
            this.eDITToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem});
            this.eDITToolStripMenuItem.Name = "eDITToolStripMenuItem";
            this.eDITToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.eDITToolStripMenuItem.Text = "&EDIT";
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.undoToolStripMenuItem.Text = "Undo";
            this.undoToolStripMenuItem.Click += new System.EventHandler(this.undoToolStripMenuItem_Click);
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.redoToolStripMenuItem.Text = "Redo";
            this.redoToolStripMenuItem.Click += new System.EventHandler(this.redoToolStripMenuItem_Click);
            // 
            // swapToolStripMenuItem
            // 
            this.swapToolStripMenuItem.Name = "swapToolStripMenuItem";
            this.swapToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.swapToolStripMenuItem.Text = "&SWAP";
            this.swapToolStripMenuItem.Click += new System.EventHandler(this.swapToolStripMenuItem_Click);
            // 
            // splitWindow
            // 
            this.splitWindow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitWindow.Location = new System.Drawing.Point(0, 49);
            this.splitWindow.Name = "splitWindow";
            // 
            // splitWindow.Panel1
            // 
            this.splitWindow.Panel1.Controls.Add(this.splitDesigner);
            this.splitWindow.Panel1Collapsed = true;
            // 
            // splitWindow.Panel2
            // 
            this.splitWindow.Panel2.Controls.Add(this.gbPalette);
            this.splitWindow.Panel2.Controls.Add(this.gbObject);
            this.splitWindow.Size = new System.Drawing.Size(407, 431);
            this.splitWindow.SplitterDistance = 135;
            this.splitWindow.TabIndex = 2;
            // 
            // gbPalette
            // 
            this.gbPalette.Controls.Add(this.rbRGBColour);
            this.gbPalette.Controls.Add(this.rbIntegerColour);
            this.gbPalette.Controls.Add(this.textBox1);
            this.gbPalette.ForeColor = System.Drawing.Color.White;
            this.gbPalette.Location = new System.Drawing.Point(22, 221);
            this.gbPalette.Name = "gbPalette";
            this.gbPalette.Size = new System.Drawing.Size(358, 198);
            this.gbPalette.TabIndex = 7;
            this.gbPalette.TabStop = false;
            this.gbPalette.Text = "Palette Data";
            // 
            // rbRGBColour
            // 
            this.rbRGBColour.AutoSize = true;
            this.rbRGBColour.Checked = true;
            this.rbRGBColour.Location = new System.Drawing.Point(14, 22);
            this.rbRGBColour.Name = "rbRGBColour";
            this.rbRGBColour.Size = new System.Drawing.Size(86, 19);
            this.rbRGBColour.TabIndex = 2;
            this.rbRGBColour.TabStop = true;
            this.rbRGBColour.Text = "RGB Colour";
            this.rbRGBColour.UseVisualStyleBackColor = true;
            this.rbRGBColour.CheckedChanged += new System.EventHandler(this.objRadioChanged_CheckedChanged);
            // 
            // rbIntegerColour
            // 
            this.rbIntegerColour.AutoSize = true;
            this.rbIntegerColour.Location = new System.Drawing.Point(106, 22);
            this.rbIntegerColour.Name = "rbIntegerColour";
            this.rbIntegerColour.Size = new System.Drawing.Size(101, 19);
            this.rbIntegerColour.TabIndex = 3;
            this.rbIntegerColour.Text = "Integer Colour";
            this.rbIntegerColour.UseVisualStyleBackColor = true;
            this.rbIntegerColour.CheckedChanged += new System.EventHandler(this.objRadioChanged_CheckedChanged);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(55)))));
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.ForeColor = System.Drawing.Color.White;
            this.textBox1.Location = new System.Drawing.Point(6, 47);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(346, 140);
            this.textBox1.TabIndex = 4;
            // 
            // gbObject
            // 
            this.gbObject.Controls.Add(this.rbHexadecimalObject);
            this.gbObject.Controls.Add(this.textBox2);
            this.gbObject.Controls.Add(this.rbIntegerObject);
            this.gbObject.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gbObject.ForeColor = System.Drawing.Color.White;
            this.gbObject.Location = new System.Drawing.Point(22, 12);
            this.gbObject.Name = "gbObject";
            this.gbObject.Size = new System.Drawing.Size(358, 203);
            this.gbObject.TabIndex = 6;
            this.gbObject.TabStop = false;
            this.gbObject.Text = "Object Data";
            // 
            // rbHexadecimalObject
            // 
            this.rbHexadecimalObject.AutoSize = true;
            this.rbHexadecimalObject.Checked = true;
            this.rbHexadecimalObject.Location = new System.Drawing.Point(14, 22);
            this.rbHexadecimalObject.Name = "rbHexadecimalObject";
            this.rbHexadecimalObject.Size = new System.Drawing.Size(158, 19);
            this.rbHexadecimalObject.TabIndex = 0;
            this.rbHexadecimalObject.TabStop = true;
            this.rbHexadecimalObject.Text = "Hexadecimal Object Data";
            this.rbHexadecimalObject.UseVisualStyleBackColor = true;
            this.rbHexadecimalObject.CheckedChanged += new System.EventHandler(this.objRadioChanged_CheckedChanged);
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(55)))));
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox2.ForeColor = System.Drawing.Color.White;
            this.textBox2.Location = new System.Drawing.Point(6, 47);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(346, 150);
            this.textBox2.TabIndex = 5;
            // 
            // rbIntegerObject
            // 
            this.rbIntegerObject.AutoSize = true;
            this.rbIntegerObject.Location = new System.Drawing.Point(178, 22);
            this.rbIntegerObject.Name = "rbIntegerObject";
            this.rbIntegerObject.Size = new System.Drawing.Size(127, 19);
            this.rbIntegerObject.TabIndex = 1;
            this.rbIntegerObject.Text = "Integer Object Data";
            this.rbIntegerObject.UseVisualStyleBackColor = true;
            this.rbIntegerObject.CheckedChanged += new System.EventHandler(this.objRadioChanged_CheckedChanged);
            // 
            // keyMonitor1
            // 
            this.keyMonitor1.CombinationHandleStyle = GBA_Studio.Controls.KeyMonitor.CombinationHandling.Explicit;
            this.keyMonitor1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.keyMonitor1_KeyDown);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Enabled = false;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // Sprite_Designer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(407, 505);
            this.Controls.Add(this.splitWindow);
            this.Controls.Add(this.menuStrip1);
            this.Location = new System.Drawing.Point(0, 0);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(407, 525);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(407, 505);
            this.Name = "Sprite_Designer";
            this.Resizable = false;
            this.Text = "Sprite Canvas";
            this.TopMost = true;
            this.splitDesigner.Panel1.ResumeLayout(false);
            this.splitDesigner.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitDesigner)).EndInit();
            this.splitDesigner.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPalette)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDesigner)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitWindow.Panel1.ResumeLayout(false);
            this.splitWindow.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitWindow)).EndInit();
            this.splitWindow.ResumeLayout(false);
            this.gbPalette.ResumeLayout(false);
            this.gbPalette.PerformLayout();
            this.gbObject.ResumeLayout(false);
            this.gbObject.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fILEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBoxPalette;
        private System.Windows.Forms.PictureBox pictureBoxDesigner;
        private System.Windows.Forms.Timer canvasTimer;
        private System.Windows.Forms.SplitContainer splitDesigner;
        private Controls.KeyMonitor keyMonitor1;
        private System.Windows.Forms.SplitContainer splitWindow;
        private System.Windows.Forms.ToolStripMenuItem eDITToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem swapToolStripMenuItem;
        private System.Windows.Forms.GroupBox gbPalette;
        private System.Windows.Forms.RadioButton rbRGBColour;
        private System.Windows.Forms.RadioButton rbIntegerColour;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.GroupBox gbObject;
        private System.Windows.Forms.RadioButton rbHexadecimalObject;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.RadioButton rbIntegerObject;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
    }
}