﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using GBA_Studio.Forms;
using GBA_Studio.IO;
using GBA_Studio.Tools;

namespace GBA_Studio.Forms
{
    public partial class BugManagement : VSForm
    {
        private enum Developer
        {
            Zac = 1,
            Zach = 2
        }

        private bool isReporting = false;

        public BugManagement(bool isReporting = false)
        {
            this.isReporting = isReporting;
            InitializeComponent();
        }

        private void BugManagement_Load(object sender, EventArgs e)
        {
            if (Globals.AuthenticationMode == Globals.Authentication.Developer && !isReporting)
            {
                Globals.BugDatabaseConnection.RetrieveBugList();
                LoadBugs();
                this.MaximizeBox = true;
                this.Resizable = true;

                tabControlMain.AttachControl(panelCurrent, tabControlMain.AddTab("Current Bugs"));
                panelCurrent.Visible = true;

                tabControlMain.AttachControl(panelArchive, tabControlMain.AddTab("Bug Archive"));
                panelArchive.Visible = true;
            }
            else
            {
                tabControlMain.AttachControl(panelReport, tabControlMain.AddTab("Report a Bug"));
                panelReport.Visible = true;

                labelDate.Text = "You discovered this bug on " + DateTime.Today.ToShortDateString() + ".";
            }

            if (isReporting)
            {
                tabControlMain.SelectTab(tabControlMain.GetTabByTitle("Report a Bug"));
            }
            else
            {
                tabControlMain.SelectTab(0);
            }

            UpdateStatusBar();
        }

        private void LoadBugs()
        {
            foreach(BugReport bug in Globals.BugReports)
            {
                if(!bug.Archived)
                {
                    sheetKnown.Rows.Add(new Object[] { bug.ID, false, GetDeveloper(bug.Developer), GetSeverity(bug.Classification), bug.DateReported, bug.Description });
                }
                else
                {
                    DateTime dateReport = DateTime.Parse(bug.DateReported);
                    DateTime dateFixed = DateTime.Parse(bug.DateFixed);
                    int duration = (dateFixed - dateReport).Days;
                    sheetArchive.Rows.Add(new Object[] { bug.ID, GetSeverity(bug.Classification), bug.DateReported, bug.DateFixed, duration + " Day" + (duration == 1 ? "" : "s"), bug.Description });
                }
            }
            UpdateStatusBar();
        }

        private string GetSeverity(int severity)
        {
            switch (severity)
            {
                case 1:
                    return "Warning";
                case 2:
                    return "Error";
                case 3:
                    return "Severe";
                case 4:
                    return "Fatal";
                default:
                    return "Unclassified";
            }
        }

        private int GetSeverity(string str)
        {
            switch(str.ToLower())
            {
                case "warning":
                    return 1;
                case "error":
                    return 2;
                case "severe":
                    return 3;
                case "fatal":
                    return 4;
                default:
                    return 0;
            }
        }

        private string GetDeveloper(int developer)
        {
            switch (developer)
            {
                case (int)Developer.Zac:
                    return "Zac";
                case (int)Developer.Zach:
                    return "Zach";
                default:
                    return "Unassigned";
            }
        }

        private int GetDeveloper(string developer)
        {
            switch (developer)
            {
                case "Zac":
                    return (int)Developer.Zac;
                case "Zach":
                    return (int)Developer.Zach;
                default:
                    return 0;
            }
        }

        private void UpdateLists()
        {
            List<BugReport> bugs = new List<BugReport>();
            for (int i = 0; i < sheetKnown.Rows.Count; i++)
            {
                if ((bool)sheetKnown.Rows[i].Cells[1].Value == true)
                {
                    BugReport bug = new BugReport((int)sheetKnown.Rows[i].Cells[0].Value, GetSeverity((string)sheetKnown.Rows[i].Cells[3].Value), (string)sheetKnown.Rows[i].Cells[4].Value, DateTime.Today.Date.ToShortDateString(), (string)sheetKnown.Rows[i].Cells[5].Value, true, GetDeveloper((string)sheetKnown.Rows[i].Cells[2].Value));

                    DateTime dateReport = DateTime.Parse(bug.DateReported);
                    DateTime dateFixed = DateTime.Parse(bug.DateFixed);
                    int duration = (dateFixed - dateReport).Days;
                    string classification = (string)sheetKnown.Rows[i].Cells[3].Value;
                    sheetArchive.Rows.Add(bug.ID, classification, bug.DateReported, bug.DateFixed, duration + " Day" + (duration == 1 ? "" : "s"), bug.Description);
                }
                else
                {
                    BugReport bug = new BugReport((int)sheetKnown.Rows[i].Cells[0].Value, GetSeverity((string)sheetKnown.Rows[i].Cells[3].Value), (string)sheetKnown.Rows[i].Cells[4].Value, (string)sheetKnown.Rows[i].Cells[4].Value, (string)sheetKnown.Rows[i].Cells[5].Value, false, GetDeveloper((string)sheetKnown.Rows[i].Cells[2].Value));
                    bugs.Add(bug);
                }
            }

            foreach (DataGridViewRow row in sheetArchive.Rows)
            {
                BugReport bug = new BugReport((int)row.Cells[0].Value, GetSeverity((string)row.Cells[1].Value), (string)row.Cells[2].Value, (string)row.Cells[3].Value, (string)row.Cells[5].Value, true, 0);
                bugs.Add(bug);
            }

            Globals.BugReports = bugs;
            BugHandling.UpdateBugListExternal();
        }

        private void buttonFix_Click(object sender, EventArgs e)
        {
            UpdateLists();
            UpdateStatusBar();

            sheetKnown.Rows.Clear();
            sheetArchive.Rows.Clear();
            LoadBugs();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tabControlMain_TabChanged(object sender, int index)
        {
            UpdateStatusBar();
        }

        private void UpdateStatusBar()
        {
            if (tabControlMain.Tabs.Count > 0)
            {
                switch (tabControlMain.Tabs[tabControlMain.SelectedIndex].Title)
                {
                    case "Current Bugs":
                        {
                            int bugCount = sheetKnown.Rows.Count;
                            this.StatusText = (bugCount == 0 ? "No" : bugCount.ToString()) + " known bug" + (bugCount != 1 ? "s " : " ") + "currently affecting GBA Studio.";
                            this.AcceptButton = buttonFix;
                            this.CancelButton = buttonCancel;
                            break;
                        }
                    case "Bug Archive":
                        {
                            int bugCount = sheetArchive.Rows.Count;
                            this.StatusText = "There " + (bugCount != 1 ? "are" : "is") + " currently " + (bugCount == 0 ? "no" : bugCount.ToString()) + " archived bug" + (bugCount != 1 ? "s." : ".");
                            this.AcceptButton = null;
                            this.CancelButton = buttonCancel;
                            break;
                        }
                    case "Report a Bug":
                        this.StatusText = "Please complete your bug report here.";
                        this.AcceptButton = buttonSubmitReport;
                        this.CancelButton = buttonCancelReport;
                        break;
                }
            }
        }

        private void buttonSubmitReport_Click(object sender, EventArgs e)
        {
            if(textBoxDescription.Text.Length < 5)
            {
                MessageBox.Show("Please enter a description of the bug.");
            }
            else
            {
                Globals.BugDatabaseConnection.SubmitBugReport(textBoxDescription.Text);
                this.Close();
            }
        }

        private void assignDeveloper(object sender, EventArgs e)
        {
            ToolStripMenuItem t = (ToolStripMenuItem)sender;

            foreach(DataGridViewRow row in sheetKnown.SelectedRows)
            {
                row.Cells[2].Value = t.Text;
            }
            UpdateLists();
        }

        private void changeClassification(object sender, EventArgs e)
        {
            ToolStripMenuItem t = (ToolStripMenuItem)sender;

            foreach(DataGridViewRow row in sheetKnown.SelectedRows)
            {
                row.Cells[3].Value = t.Text;
            }
            UpdateLists();
        }

        private void markAsFixedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem t = (ToolStripMenuItem)sender;

            foreach (DataGridViewRow row in sheetKnown.SelectedRows)
            {
                row.Cells[1].Value = true;
            }
            UpdateLists(); 
            UpdateStatusBar();

            sheetKnown.Rows.Clear();
            sheetArchive.Rows.Clear();
            LoadBugs();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in sheetKnown.SelectedRows)
            {
                Globals.BugDatabaseConnection.RemoveBugReport((int)row.Cells[0].Value);
            }

            foreach (DataGridViewCell cell in sheetKnown.SelectedCells)
            {
                if (cell != null && cell.Selected)
                {
                    sheetKnown.Rows.RemoveAt(cell.RowIndex);
                }
            }

            UpdateLists();
            UpdateStatusBar();

            sheetKnown.Rows.Clear();
            sheetArchive.Rows.Clear();
            LoadBugs();
        }
    }
}
