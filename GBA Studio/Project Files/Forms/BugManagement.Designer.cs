﻿namespace GBA_Studio.Forms
{
    partial class BugManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BugManagement));
            this.panelCurrent = new System.Windows.Forms.SplitContainer();
            this.sheetKnown = new System.Windows.Forms.DataGridView();
            this.knownBugID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isFixed = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.assignedDeveloper = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.classification = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextKnownBugs = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.severityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unclassifiedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.warningToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.errorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.severeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fatalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.markAsFixedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonFix = new GBA_Studio.Controls.ButtonPro();
            this.buttonCancel = new GBA_Studio.Controls.ButtonPro();
            this.tabControlMain = new TabControl.TabControlPro();
            this.sheetArchive = new System.Windows.Forms.DataGridView();
            this.archivedBugID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateReported = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateFixed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fixDuration = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bugDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelArchive = new System.Windows.Forms.Panel();
            this.panelReport = new System.Windows.Forms.Panel();
            this.buttonCancelReport = new GBA_Studio.Controls.ButtonPro();
            this.buttonSubmitReport = new GBA_Studio.Controls.ButtonPro();
            this.labelDate = new System.Windows.Forms.Label();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.textBoxLabel = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.assignToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zacToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zachToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.panelCurrent)).BeginInit();
            this.panelCurrent.Panel1.SuspendLayout();
            this.panelCurrent.Panel2.SuspendLayout();
            this.panelCurrent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sheetKnown)).BeginInit();
            this.contextKnownBugs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sheetArchive)).BeginInit();
            this.panelArchive.SuspendLayout();
            this.panelReport.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelCurrent
            // 
            this.panelCurrent.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.panelCurrent.IsSplitterFixed = true;
            this.panelCurrent.Location = new System.Drawing.Point(-3000, 2);
            this.panelCurrent.Name = "panelCurrent";
            this.panelCurrent.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // panelCurrent.Panel1
            // 
            this.panelCurrent.Panel1.Controls.Add(this.sheetKnown);
            this.panelCurrent.Panel1.ForeColor = System.Drawing.Color.Black;
            // 
            // panelCurrent.Panel2
            // 
            this.panelCurrent.Panel2.Controls.Add(this.buttonFix);
            this.panelCurrent.Panel2.Controls.Add(this.buttonCancel);
            this.panelCurrent.Panel2Collapsed = true;
            this.panelCurrent.Panel2MinSize = 41;
            this.panelCurrent.Size = new System.Drawing.Size(600, 278);
            this.panelCurrent.SplitterDistance = 25;
            this.panelCurrent.SplitterWidth = 1;
            this.panelCurrent.TabIndex = 0;
            this.panelCurrent.Visible = false;
            // 
            // sheetKnown
            // 
            this.sheetKnown.AllowUserToAddRows = false;
            this.sheetKnown.AllowUserToDeleteRows = false;
            this.sheetKnown.AllowUserToResizeRows = false;
            this.sheetKnown.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.sheetKnown.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.sheetKnown.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.sheetKnown.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.sheetKnown.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sheetKnown.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.knownBugID,
            this.isFixed,
            this.assignedDeveloper,
            this.classification,
            this.date,
            this.description});
            this.sheetKnown.ContextMenuStrip = this.contextKnownBugs;
            this.sheetKnown.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sheetKnown.Location = new System.Drawing.Point(0, 0);
            this.sheetKnown.Name = "sheetKnown";
            this.sheetKnown.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.sheetKnown.Size = new System.Drawing.Size(600, 278);
            this.sheetKnown.TabIndex = 0;
            // 
            // knownBugID
            // 
            this.knownBugID.HeaderText = "ID";
            this.knownBugID.Name = "knownBugID";
            this.knownBugID.ReadOnly = true;
            this.knownBugID.Visible = false;
            // 
            // isFixed
            // 
            this.isFixed.FillWeight = 10F;
            this.isFixed.HeaderText = "Fixed";
            this.isFixed.Name = "isFixed";
            this.isFixed.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.isFixed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.isFixed.Visible = false;
            // 
            // assignedDeveloper
            // 
            this.assignedDeveloper.HeaderText = "Developer";
            this.assignedDeveloper.Name = "assignedDeveloper";
            this.assignedDeveloper.ReadOnly = true;
            // 
            // classification
            // 
            this.classification.FillWeight = 17F;
            this.classification.HeaderText = "Classification";
            this.classification.Name = "classification";
            this.classification.ReadOnly = true;
            // 
            // date
            // 
            this.date.FillWeight = 20F;
            this.date.HeaderText = "Date Reported";
            this.date.Name = "date";
            // 
            // description
            // 
            this.description.FillWeight = 70F;
            this.description.HeaderText = "Description";
            this.description.Name = "description";
            this.description.ReadOnly = true;
            // 
            // contextKnownBugs
            // 
            this.contextKnownBugs.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.assignToolStripMenuItem,
            this.severityToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.markAsFixedToolStripMenuItem});
            this.contextKnownBugs.Name = "contextMenuStrip1";
            this.contextKnownBugs.Size = new System.Drawing.Size(153, 114);
            // 
            // severityToolStripMenuItem
            // 
            this.severityToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.unclassifiedToolStripMenuItem,
            this.warningToolStripMenuItem,
            this.errorToolStripMenuItem,
            this.severeToolStripMenuItem,
            this.fatalToolStripMenuItem});
            this.severityToolStripMenuItem.Name = "severityToolStripMenuItem";
            this.severityToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.severityToolStripMenuItem.Text = "Severity";
            // 
            // unclassifiedToolStripMenuItem
            // 
            this.unclassifiedToolStripMenuItem.Name = "unclassifiedToolStripMenuItem";
            this.unclassifiedToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.unclassifiedToolStripMenuItem.Text = "Unclassified";
            this.unclassifiedToolStripMenuItem.Click += new System.EventHandler(this.changeClassification);
            // 
            // warningToolStripMenuItem
            // 
            this.warningToolStripMenuItem.Name = "warningToolStripMenuItem";
            this.warningToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.warningToolStripMenuItem.Text = "Warning";
            this.warningToolStripMenuItem.Click += new System.EventHandler(this.changeClassification);
            // 
            // errorToolStripMenuItem
            // 
            this.errorToolStripMenuItem.Name = "errorToolStripMenuItem";
            this.errorToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.errorToolStripMenuItem.Text = "Error";
            this.errorToolStripMenuItem.Click += new System.EventHandler(this.changeClassification);
            // 
            // severeToolStripMenuItem
            // 
            this.severeToolStripMenuItem.Name = "severeToolStripMenuItem";
            this.severeToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.severeToolStripMenuItem.Text = "Severe";
            this.severeToolStripMenuItem.Click += new System.EventHandler(this.changeClassification);
            // 
            // fatalToolStripMenuItem
            // 
            this.fatalToolStripMenuItem.Name = "fatalToolStripMenuItem";
            this.fatalToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.fatalToolStripMenuItem.Text = "Fatal";
            this.fatalToolStripMenuItem.Click += new System.EventHandler(this.changeClassification);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.deleteToolStripMenuItem.Text = "Remove";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // markAsFixedToolStripMenuItem
            // 
            this.markAsFixedToolStripMenuItem.Name = "markAsFixedToolStripMenuItem";
            this.markAsFixedToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.markAsFixedToolStripMenuItem.Text = "Mark as Fixed";
            this.markAsFixedToolStripMenuItem.Click += new System.EventHandler(this.markAsFixedToolStripMenuItem_Click);
            // 
            // buttonFix
            // 
            this.buttonFix.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.buttonFix.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(84)))), ((int)(((byte)(92)))));
            this.buttonFix.BorderHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(117)))));
            this.buttonFix.BorderPressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(151)))), ((int)(((byte)(234)))));
            this.buttonFix.ButtonHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(84)))), ((int)(((byte)(92)))));
            this.buttonFix.ButtonPressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.buttonFix.ButtonText = "Mark as Fixed";
            this.buttonFix.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonFix.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonFix.FontHoverColor = System.Drawing.Color.White;
            this.buttonFix.FontPressedColor = System.Drawing.Color.White;
            this.buttonFix.Location = new System.Drawing.Point(309, 1);
            this.buttonFix.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonFix.Name = "buttonFix";
            this.buttonFix.Size = new System.Drawing.Size(139, 39);
            this.buttonFix.TabIndex = 1;
            this.buttonFix.Click += new System.EventHandler(this.buttonFix_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.buttonCancel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(84)))), ((int)(((byte)(92)))));
            this.buttonCancel.BorderHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(117)))));
            this.buttonCancel.BorderPressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(151)))), ((int)(((byte)(234)))));
            this.buttonCancel.ButtonHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(84)))), ((int)(((byte)(92)))));
            this.buttonCancel.ButtonPressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.buttonCancel.ButtonText = "Cancel";
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.None;
            this.buttonCancel.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.FontHoverColor = System.Drawing.Color.White;
            this.buttonCancel.FontPressedColor = System.Drawing.Color.White;
            this.buttonCancel.Location = new System.Drawing.Point(456, 1);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(139, 39);
            this.buttonCancel.TabIndex = 0;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // tabControlMain
            // 
            this.tabControlMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.tabControlMain.ContainerMargin = 2;
            this.tabControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlMain.ExitHighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(176)))), ((int)(((byte)(239)))));
            this.tabControlMain.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControlMain.ForeColor = System.Drawing.Color.White;
            this.tabControlMain.HiddenTabWidth = ((uint)(175u));
            this.tabControlMain.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(138)))), ((int)(((byte)(213)))));
            this.tabControlMain.Location = new System.Drawing.Point(0, 25);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.ReadOnly = true;
            this.tabControlMain.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(114)))), ((int)(((byte)(191)))));
            this.tabControlMain.SelectedColorUnfocused = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.SelectedTabWidth = ((uint)(250u));
            this.tabControlMain.Size = new System.Drawing.Size(600, 255);
            this.tabControlMain.SplitterWidth = 2;
            this.tabControlMain.TabBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(44)))), ((int)(((byte)(47)))));
            this.tabControlMain.TabDock = TabControl.TabControlPro.TabDockStyle.Top;
            this.tabControlMain.TabHeight = ((uint)(18u));
            this.tabControlMain.TabIndex = 1;
            this.tabControlMain.TabChanged += new TabControl.TabControlPro.TabEventHandler(this.tabControlMain_TabChanged);
            // 
            // sheetArchive
            // 
            this.sheetArchive.AllowUserToAddRows = false;
            this.sheetArchive.AllowUserToDeleteRows = false;
            this.sheetArchive.AllowUserToResizeColumns = false;
            this.sheetArchive.AllowUserToResizeRows = false;
            this.sheetArchive.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.sheetArchive.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.sheetArchive.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.sheetArchive.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sheetArchive.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.archivedBugID,
            this.dataGridViewTextBoxColumn1,
            this.dateReported,
            this.dateFixed,
            this.fixDuration,
            this.bugDescription});
            this.sheetArchive.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sheetArchive.Location = new System.Drawing.Point(0, 0);
            this.sheetArchive.Name = "sheetArchive";
            this.sheetArchive.ReadOnly = true;
            this.sheetArchive.Size = new System.Drawing.Size(600, 233);
            this.sheetArchive.TabIndex = 2;
            // 
            // archivedBugID
            // 
            this.archivedBugID.HeaderText = "ID";
            this.archivedBugID.Name = "archivedBugID";
            this.archivedBugID.ReadOnly = true;
            this.archivedBugID.Visible = false;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.FillWeight = 27F;
            this.dataGridViewTextBoxColumn1.HeaderText = "Classification";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dateReported
            // 
            this.dateReported.FillWeight = 20F;
            this.dateReported.HeaderText = "Date Reported";
            this.dateReported.Name = "dateReported";
            this.dateReported.ReadOnly = true;
            // 
            // dateFixed
            // 
            this.dateFixed.FillWeight = 20F;
            this.dateFixed.HeaderText = "Date Fixed";
            this.dateFixed.Name = "dateFixed";
            this.dateFixed.ReadOnly = true;
            // 
            // fixDuration
            // 
            this.fixDuration.FillWeight = 20F;
            this.fixDuration.HeaderText = "Fix Duration";
            this.fixDuration.Name = "fixDuration";
            this.fixDuration.ReadOnly = true;
            // 
            // bugDescription
            // 
            this.bugDescription.HeaderText = "Description";
            this.bugDescription.Name = "bugDescription";
            this.bugDescription.ReadOnly = true;
            // 
            // panelArchive
            // 
            this.panelArchive.Controls.Add(this.sheetArchive);
            this.panelArchive.ForeColor = System.Drawing.Color.Black;
            this.panelArchive.Location = new System.Drawing.Point(-3000, 47);
            this.panelArchive.Name = "panelArchive";
            this.panelArchive.Size = new System.Drawing.Size(600, 233);
            this.panelArchive.TabIndex = 3;
            this.panelArchive.Visible = false;
            // 
            // panelReport
            // 
            this.panelReport.Controls.Add(this.buttonCancelReport);
            this.panelReport.Controls.Add(this.buttonSubmitReport);
            this.panelReport.Controls.Add(this.labelDate);
            this.panelReport.Controls.Add(this.textBoxDescription);
            this.panelReport.Controls.Add(this.textBoxLabel);
            this.panelReport.Controls.Add(this.label1);
            this.panelReport.Location = new System.Drawing.Point(-3000, 47);
            this.panelReport.Name = "panelReport";
            this.panelReport.Size = new System.Drawing.Size(600, 230);
            this.panelReport.TabIndex = 4;
            this.panelReport.Visible = false;
            // 
            // buttonCancelReport
            // 
            this.buttonCancelReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.buttonCancelReport.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(84)))), ((int)(((byte)(92)))));
            this.buttonCancelReport.BorderHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(117)))));
            this.buttonCancelReport.BorderPressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(151)))), ((int)(((byte)(234)))));
            this.buttonCancelReport.ButtonHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(84)))), ((int)(((byte)(92)))));
            this.buttonCancelReport.ButtonPressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.buttonCancelReport.ButtonText = "Cancel Report";
            this.buttonCancelReport.DialogResult = System.Windows.Forms.DialogResult.None;
            this.buttonCancelReport.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancelReport.FontHoverColor = System.Drawing.Color.White;
            this.buttonCancelReport.FontPressedColor = System.Drawing.Color.White;
            this.buttonCancelReport.Location = new System.Drawing.Point(448, 180);
            this.buttonCancelReport.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonCancelReport.Name = "buttonCancelReport";
            this.buttonCancelReport.Size = new System.Drawing.Size(139, 39);
            this.buttonCancelReport.TabIndex = 6;
            this.buttonCancelReport.ButtonClicked += new System.EventHandler(this.buttonCancel_Click);
            this.buttonCancelReport.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonSubmitReport
            // 
            this.buttonSubmitReport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.buttonSubmitReport.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(84)))), ((int)(((byte)(92)))));
            this.buttonSubmitReport.BorderHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(117)))));
            this.buttonSubmitReport.BorderPressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(151)))), ((int)(((byte)(234)))));
            this.buttonSubmitReport.ButtonHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(84)))), ((int)(((byte)(92)))));
            this.buttonSubmitReport.ButtonPressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.buttonSubmitReport.ButtonText = "Submit Report";
            this.buttonSubmitReport.DialogResult = System.Windows.Forms.DialogResult.None;
            this.buttonSubmitReport.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSubmitReport.FontHoverColor = System.Drawing.Color.White;
            this.buttonSubmitReport.FontPressedColor = System.Drawing.Color.White;
            this.buttonSubmitReport.Location = new System.Drawing.Point(301, 180);
            this.buttonSubmitReport.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.buttonSubmitReport.Name = "buttonSubmitReport";
            this.buttonSubmitReport.Size = new System.Drawing.Size(139, 39);
            this.buttonSubmitReport.TabIndex = 5;
            this.buttonSubmitReport.Click += new System.EventHandler(this.buttonSubmitReport_Click);
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.Location = new System.Drawing.Point(6, 153);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(154, 15);
            this.labelDate.TabIndex = 4;
            this.labelDate.Text = "You discovered this bug on ";
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(55)))));
            this.textBoxDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxDescription.ForeColor = System.Drawing.Color.White;
            this.textBoxDescription.Location = new System.Drawing.Point(9, 119);
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.Size = new System.Drawing.Size(578, 23);
            this.textBoxDescription.TabIndex = 2;
            // 
            // textBoxLabel
            // 
            this.textBoxLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.textBoxLabel.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxLabel.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.textBoxLabel.Enabled = false;
            this.textBoxLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(153)))), ((int)(((byte)(153)))));
            this.textBoxLabel.Location = new System.Drawing.Point(7, 41);
            this.textBoxLabel.Multiline = true;
            this.textBoxLabel.Name = "textBoxLabel";
            this.textBoxLabel.ReadOnly = true;
            this.textBoxLabel.Size = new System.Drawing.Size(590, 76);
            this.textBoxLabel.TabIndex = 1;
            this.textBoxLabel.Text = resources.GetString("textBoxLabel.Text");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bug Report";
            // 
            // assignToolStripMenuItem
            // 
            this.assignToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zacToolStripMenuItem,
            this.zachToolStripMenuItem});
            this.assignToolStripMenuItem.Name = "assignToolStripMenuItem";
            this.assignToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.assignToolStripMenuItem.Text = "Assign";
            // 
            // zacToolStripMenuItem
            // 
            this.zacToolStripMenuItem.Name = "zacToolStripMenuItem";
            this.zacToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.zacToolStripMenuItem.Text = "Zac";
            this.zacToolStripMenuItem.Click += new System.EventHandler(this.assignDeveloper);
            // 
            // zachToolStripMenuItem
            // 
            this.zachToolStripMenuItem.Name = "zachToolStripMenuItem";
            this.zachToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.zachToolStripMenuItem.Text = "Zach";
            this.zachToolStripMenuItem.Click += new System.EventHandler(this.assignDeveloper);
            // 
            // BugManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonFix;
            this.ClientSize = new System.Drawing.Size(600, 305);
            this.Controls.Add(this.panelReport);
            this.Controls.Add(this.panelArchive);
            this.Controls.Add(this.panelCurrent);
            this.Controls.Add(this.tabControlMain);
            this.Location = new System.Drawing.Point(0, 0);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BugManagement";
            this.Resizable = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusStripVisible = true;
            this.Text = "Bug Management";
            this.WindowState = System.Windows.Forms.FormWindowState.Normal;
            this.Load += new System.EventHandler(this.BugManagement_Load);
            this.panelCurrent.Panel1.ResumeLayout(false);
            this.panelCurrent.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelCurrent)).EndInit();
            this.panelCurrent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sheetKnown)).EndInit();
            this.contextKnownBugs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sheetArchive)).EndInit();
            this.panelArchive.ResumeLayout(false);
            this.panelReport.ResumeLayout(false);
            this.panelReport.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer panelCurrent;
        private System.Windows.Forms.DataGridView sheetKnown;
        private Controls.ButtonPro buttonFix;
        private Controls.ButtonPro buttonCancel;
        private TabControl.TabControlPro tabControlMain;
        private System.Windows.Forms.DataGridView sheetArchive;
        private System.Windows.Forms.Panel panelArchive;
        private System.Windows.Forms.Panel panelReport;
        private Controls.ButtonPro buttonSubmitReport;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.TextBox textBoxLabel;
        private System.Windows.Forms.Label label1;
        private Controls.ButtonPro buttonCancelReport;
        private System.Windows.Forms.DataGridViewTextBoxColumn archivedBugID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateReported;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateFixed;
        private System.Windows.Forms.DataGridViewTextBoxColumn fixDuration;
        private System.Windows.Forms.DataGridViewTextBoxColumn bugDescription;
        private System.Windows.Forms.ContextMenuStrip contextKnownBugs;
        private System.Windows.Forms.ToolStripMenuItem severityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unclassifiedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem warningToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem errorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem severeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fatalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem markAsFixedToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn knownBugID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isFixed;
        private System.Windows.Forms.DataGridViewTextBoxColumn assignedDeveloper;
        private System.Windows.Forms.DataGridViewTextBoxColumn classification;
        private System.Windows.Forms.DataGridViewTextBoxColumn date;
        private System.Windows.Forms.DataGridViewTextBoxColumn description;
        private System.Windows.Forms.ToolStripMenuItem assignToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zacToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zachToolStripMenuItem;
    }
}