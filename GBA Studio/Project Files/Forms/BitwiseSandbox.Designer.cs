﻿namespace GBA_Studio.Forms
{
    partial class BitwiseSandbox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.resultButton = new GBA_Studio.Controls.ButtonPro();
            this.binaryVisualizer3 = new GBA_Studio.Controls.BinaryVisualizer();
            this.binaryVisualizer2 = new GBA_Studio.Controls.BinaryVisualizer();
            this.binaryVisualizer1 = new GBA_Studio.Controls.BinaryVisualizer();
            this.itemList1 = new GBA_Studio.Controls.ItemList();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.resultButton);
            this.panel1.Controls.Add(this.binaryVisualizer3);
            this.panel1.Controls.Add(this.binaryVisualizer2);
            this.panel1.Controls.Add(this.binaryVisualizer1);
            this.panel1.Controls.Add(this.itemList1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(559, 331);
            this.panel1.TabIndex = 3;
            // 
            // resultButton
            // 
            this.resultButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.resultButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(84)))), ((int)(((byte)(92)))));
            this.resultButton.BorderHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(106)))), ((int)(((byte)(106)))), ((int)(((byte)(117)))));
            this.resultButton.BorderPressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(151)))), ((int)(((byte)(234)))));
            this.resultButton.ButtonHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(84)))), ((int)(((byte)(92)))));
            this.resultButton.ButtonPressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.resultButton.ButtonText = "Generate Result";
            this.resultButton.DialogResult = System.Windows.Forms.DialogResult.None;
            this.resultButton.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resultButton.FontHoverColor = System.Drawing.Color.White;
            this.resultButton.FontPressedColor = System.Drawing.Color.White;
            this.resultButton.Location = new System.Drawing.Point(193, 192);
            this.resultButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.resultButton.Name = "resultButton";
            this.resultButton.Size = new System.Drawing.Size(247, 53);
            this.resultButton.TabIndex = 7;
            this.resultButton.ButtonClicked += new System.EventHandler(this.resultButton_ButtonClicked);
            // 
            // binaryVisualizer3
            // 
            this.binaryVisualizer3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.binaryVisualizer3.BinaryName = "";
            this.binaryVisualizer3.ForeColor = System.Drawing.Color.White;
            this.binaryVisualizer3.Location = new System.Drawing.Point(0, 259);
            this.binaryVisualizer3.Name = "binaryVisualizer3";
            this.binaryVisualizer3.Size = new System.Drawing.Size(566, 47);
            this.binaryVisualizer3.TabIndex = 6;
            // 
            // binaryVisualizer2
            // 
            this.binaryVisualizer2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.binaryVisualizer2.BinaryName = "b";
            this.binaryVisualizer2.ForeColor = System.Drawing.Color.White;
            this.binaryVisualizer2.Location = new System.Drawing.Point(0, 137);
            this.binaryVisualizer2.Name = "binaryVisualizer2";
            this.binaryVisualizer2.Size = new System.Drawing.Size(561, 47);
            this.binaryVisualizer2.TabIndex = 5;
            // 
            // binaryVisualizer1
            // 
            this.binaryVisualizer1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.binaryVisualizer1.BinaryName = "a";
            this.binaryVisualizer1.ForeColor = System.Drawing.Color.White;
            this.binaryVisualizer1.Location = new System.Drawing.Point(0, 14);
            this.binaryVisualizer1.Name = "binaryVisualizer1";
            this.binaryVisualizer1.Size = new System.Drawing.Size(561, 41);
            this.binaryVisualizer1.TabIndex = 4;
            // 
            // itemList1
            // 
            this.itemList1.ButtonBorderColorDefault = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.itemList1.ButtonBorderColorHovered = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(64)))));
            this.itemList1.ButtonBorderColorPressed = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.itemList1.ButtonBorderColorSelected = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(160)))));
            this.itemList1.ButtonColorDefault = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.itemList1.ButtonColorHovered = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.itemList1.ButtonColorPressed = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.itemList1.ButtonColorSelected = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.itemList1.ControlItemGap = 1;
            this.itemList1.ControlItemWidth = 50;
            this.itemList1.ControlMarginLeft = 10;
            this.itemList1.ControlMarginTop = 10;
            this.itemList1.ItemBorderWidth = 1;
            this.itemList1.Location = new System.Drawing.Point(84, 61);
            this.itemList1.Name = "itemList1";
            this.itemList1.Size = new System.Drawing.Size(474, 70);
            this.itemList1.TabIndex = 3;
            // 
            // BitwiseSandbox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(559, 381);
            this.Controls.Add(this.panel1);
            this.Location = new System.Drawing.Point(0, 0);
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(559, 381);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(559, 381);
            this.Name = "BitwiseSandbox";
            this.Resizable = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusStripVisible = true;
            this.StatusText = "Bitwise Sandbox";
            this.Text = "Bitwise Sandbox";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private Controls.ItemList itemList1;
        private Controls.ButtonPro resultButton;
        private Controls.BinaryVisualizer binaryVisualizer3;
        private Controls.BinaryVisualizer binaryVisualizer2;
        private Controls.BinaryVisualizer binaryVisualizer1;

    }
}