﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using GBA_Studio.Properties;
using GBA_Studio.Controls;

namespace GBA_Studio.Forms
{
    public partial class BitwiseSandbox : VSForm
    {
        List<BinaryVisualizer> generatorVariables = new List<BinaryVisualizer>();

        public BitwiseSandbox()
        {
            InitializeComponent();

            ItemList.ItemData one = new ItemList.ItemData();
            ItemList.ItemData two = new ItemList.ItemData();
            ItemList.ItemData thr = new ItemList.ItemData();

            one.Image = Resources.AND;
            one.Caption = "AND";

            two.Image = Resources.OR;
            two.Caption = "OR";

            thr.Image = Resources.XOR;
            thr.Caption = "XOR";

            itemList1.ControlItems.Add(one);
            itemList1.ControlItems.Add(two);
            itemList1.ControlItems.Add(thr);
        }

        private void resultButton_ButtonClicked(object sender, EventArgs e)
        {
            BitArray a = binaryVisualizer1.BinaryValue;
            BitArray b = binaryVisualizer2.BinaryValue;

            BitArray result = a;

            switch (itemList1.SelectedIndex)
            {
                case 0:
                    result = a.And(b);
                    break;
                case 1:
                    result = a.Or(b);
                    break;
                case 2:
                    result = a.Xor(b);
                    break;
            }

            binaryVisualizer3.SetValue(result);
        }

        private bool ByteEquals(BitArray a, BitArray b)
        {
            bool equal = true;

            for (int i = 0; i < a.Length; i++ )
            {
                if (i < 8 && i < b.Length)
                {
                    if (a[i] != b[i])
                    {
                        equal = false;
                    }
                }
            }

            return equal;
        }

        private void calculateButton_Click(object sender, EventArgs e)
        {

        }

        private string LetterFromIndex(int i)
        {
            return i == 0 ? "a" : i == 1 ? "b" : i == 2 ? "c" : "";
        }
    }
}
