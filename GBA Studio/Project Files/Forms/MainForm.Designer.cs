﻿using GBA_Studio.Tools;

namespace GBA_Studio.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.updateConsole = new System.Windows.Forms.Timer(this.components);
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.formTitle = new System.Windows.Forms.ToolStripTextBox();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.toolStripFile = new System.Windows.Forms.ToolStrip();
            this.newSolutionButton = new System.Windows.Forms.ToolStripButton();
            this.newProjectButton = new System.Windows.Forms.ToolStripButton();
            this.newFileButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripDefault = new System.Windows.Forms.ToolStrip();
            this.loadProjectButton = new System.Windows.Forms.ToolStripButton();
            this.saveCurrentFileButton = new System.Windows.Forms.ToolStripButton();
            this.saveAllButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.undoButton = new System.Windows.Forms.ToolStripButton();
            this.redoButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.buildButton = new System.Windows.Forms.ToolStripButton();
            this.buildAndRunButton = new System.Windows.Forms.ToolStripButton();
            this.startButton = new System.Windows.Forms.ToolStripButton();
            this.stopButton = new System.Windows.Forms.ToolStripButton();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.tabControlAttributes = new TabControl.TabControlPro();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.splitContainer6 = new System.Windows.Forms.SplitContainer();
            this.errorList = new GBA_Studio.Controls.ListBoxPro();
            this.tabControlMain = new TabControl.TabControlPro();
            this.itemListInterface1 = new GBA_Studio.Controls.ItemListInterface();
            this.startPage = new System.Windows.Forms.WebBrowser();
            this.tabControlSecondary = new TabControl.TabControlPro();
            this.smartSense1 = new SmartSense.SmartSense();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItemFile = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemNewSolution = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.projectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox2 = new System.Windows.Forms.ToolStripTextBox();
            this.elementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemOpenProjectSolution = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemOpenElement = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemClose = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemCloseSolution = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemSave = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemSaveAll = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemRecentFiles = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemExit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemUndo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemRedo = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemCut = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemFindAndReplace = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemView = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemStartPage = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemConsole = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemSolutionExplorer = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemMemberExplorer = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemErrorList = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemToolbars = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemFileToolbar = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemDefaultToolbar = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemProject = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemAddElement = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemProperties = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemDebug = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemCleanSolution = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemStart = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemStartWithoutBuild = new System.Windows.Forms.ToolStripMenuItem();
            this.buildToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemTools = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemEnterEvaluationMode = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemBitwiseSandbox = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemSpriteCanvas = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemDeveloper = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemBugManagement = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.reportABugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.goolStripMenuItemProfile = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer5 = new System.Windows.Forms.SplitContainer();
            this.gbaMonitor = new System.IO.FileSystemWatcher();
            this.menuBtnMin = new GBA_Studio.Controls.ButtonPro();
            this.menuBtnMax = new GBA_Studio.Controls.ButtonPro();
            this.menuBtnClose = new GBA_Studio.Controls.ButtonPro();
            this.keyMonitor1 = new GBA_Studio.Controls.KeyMonitor(this.components);
            this.menuStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.toolStripFile.SuspendLayout();
            this.toolStripDefault.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).BeginInit();
            this.splitContainer6.Panel1.SuspendLayout();
            this.splitContainer6.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).BeginInit();
            this.splitContainer5.Panel1.SuspendLayout();
            this.splitContainer5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbaMonitor)).BeginInit();
            this.SuspendLayout();
            // 
            // updateConsole
            // 
            this.updateConsole.Enabled = true;
            this.updateConsole.Interval = 1;
            this.updateConsole.Tick += new System.EventHandler(this.updateConsole_Tick);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripMenuItem3.Enabled = false;
            this.toolStripMenuItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem3.Image")));
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(28, 21);
            this.toolStripMenuItem3.Text = "toolStripMenuItem3";
            // 
            // formTitle
            // 
            this.formTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.formTitle.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.formTitle.Enabled = false;
            this.formTitle.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.formTitle.Name = "formTitle";
            this.formTitle.ReadOnly = true;
            this.formTitle.Size = new System.Drawing.Size(100, 21);
            this.formTitle.Text = "GBA Studio";
            // 
            // menuStrip2
            // 
            this.menuStrip2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.menuStrip2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3,
            this.formTitle});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(996, 25);
            this.menuStrip2.TabIndex = 0;
            this.menuStrip2.Text = "menuStrip1";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.ForeColor = System.Drawing.Color.White;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 49);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.splitContainer1.Panel1.Controls.Add(this.toolStripContainer1);
            this.splitContainer1.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.splitContainer1.Size = new System.Drawing.Size(1192, 621);
            this.splitContainer1.SplitterDistance = 51;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 3;
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(1192, 26);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(1192, 51);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStripFile);
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStripDefault);
            this.toolStripContainer1.TopToolStripPanel.ClientSizeChanged += new System.EventHandler(this.toolStripContainer1_TopToolStripPanel_ClientSizeChanged);
            // 
            // toolStripFile
            // 
            this.toolStripFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.toolStripFile.CanOverflow = false;
            this.toolStripFile.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripFile.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newSolutionButton,
            this.newProjectButton,
            this.newFileButton});
            this.toolStripFile.Location = new System.Drawing.Point(3, 0);
            this.toolStripFile.Name = "toolStripFile";
            this.toolStripFile.Size = new System.Drawing.Size(81, 25);
            this.toolStripFile.TabIndex = 1;
            // 
            // newSolutionButton
            // 
            this.newSolutionButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newSolutionButton.Image = ((System.Drawing.Image)(resources.GetObject("newSolutionButton.Image")));
            this.newSolutionButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newSolutionButton.Name = "newSolutionButton";
            this.newSolutionButton.Size = new System.Drawing.Size(23, 22);
            this.newSolutionButton.Text = "New Solution";
            // 
            // newProjectButton
            // 
            this.newProjectButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newProjectButton.Image = ((System.Drawing.Image)(resources.GetObject("newProjectButton.Image")));
            this.newProjectButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newProjectButton.Name = "newProjectButton";
            this.newProjectButton.Size = new System.Drawing.Size(23, 22);
            this.newProjectButton.Text = "New Project";
            // 
            // newFileButton
            // 
            this.newFileButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newFileButton.Image = ((System.Drawing.Image)(resources.GetObject("newFileButton.Image")));
            this.newFileButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newFileButton.Name = "newFileButton";
            this.newFileButton.Size = new System.Drawing.Size(23, 22);
            this.newFileButton.Text = "New File";
            // 
            // toolStripDefault
            // 
            this.toolStripDefault.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.toolStripDefault.CanOverflow = false;
            this.toolStripDefault.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStripDefault.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadProjectButton,
            this.saveCurrentFileButton,
            this.saveAllButton,
            this.toolStripSeparator1,
            this.undoButton,
            this.redoButton,
            this.toolStripSeparator2,
            this.buildButton,
            this.buildAndRunButton,
            this.startButton,
            this.stopButton});
            this.toolStripDefault.Location = new System.Drawing.Point(84, 0);
            this.toolStripDefault.Name = "toolStripDefault";
            this.toolStripDefault.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStripDefault.Size = new System.Drawing.Size(392, 25);
            this.toolStripDefault.TabIndex = 0;
            this.toolStripDefault.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStripDebug_ItemClicked);
            // 
            // loadProjectButton
            // 
            this.loadProjectButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.loadProjectButton.Image = ((System.Drawing.Image)(resources.GetObject("loadProjectButton.Image")));
            this.loadProjectButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.loadProjectButton.Name = "loadProjectButton";
            this.loadProjectButton.Size = new System.Drawing.Size(23, 22);
            this.loadProjectButton.Text = "Load Project";
            // 
            // saveCurrentFileButton
            // 
            this.saveCurrentFileButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveCurrentFileButton.Image = ((System.Drawing.Image)(resources.GetObject("saveCurrentFileButton.Image")));
            this.saveCurrentFileButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveCurrentFileButton.Name = "saveCurrentFileButton";
            this.saveCurrentFileButton.Size = new System.Drawing.Size(23, 22);
            this.saveCurrentFileButton.Text = "toolStripButton1";
            this.saveCurrentFileButton.ToolTipText = "Save";
            this.saveCurrentFileButton.Click += new System.EventHandler(this.saveCurrentFileButton_Click);
            // 
            // saveAllButton
            // 
            this.saveAllButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveAllButton.Image = ((System.Drawing.Image)(resources.GetObject("saveAllButton.Image")));
            this.saveAllButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveAllButton.Name = "saveAllButton";
            this.saveAllButton.Size = new System.Drawing.Size(23, 22);
            this.saveAllButton.Text = "toolStripButton2";
            this.saveAllButton.ToolTipText = "Save All";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.toolStripSeparator1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // undoButton
            // 
            this.undoButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.undoButton.Image = ((System.Drawing.Image)(resources.GetObject("undoButton.Image")));
            this.undoButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.undoButton.Name = "undoButton";
            this.undoButton.Size = new System.Drawing.Size(23, 22);
            this.undoButton.Text = "toolStripButton3";
            this.undoButton.ToolTipText = "Undo";
            this.undoButton.Click += new System.EventHandler(this.undoButton_Click);
            // 
            // redoButton
            // 
            this.redoButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.redoButton.Image = ((System.Drawing.Image)(resources.GetObject("redoButton.Image")));
            this.redoButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.redoButton.Name = "redoButton";
            this.redoButton.Size = new System.Drawing.Size(23, 22);
            this.redoButton.Text = "toolStripButton4";
            this.redoButton.ToolTipText = "Redo";
            this.redoButton.Click += new System.EventHandler(this.redoButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // buildButton
            // 
            this.buildButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buildButton.Image = ((System.Drawing.Image)(resources.GetObject("buildButton.Image")));
            this.buildButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buildButton.Name = "buildButton";
            this.buildButton.Size = new System.Drawing.Size(23, 22);
            this.buildButton.Text = "toolStripButton5";
            this.buildButton.ToolTipText = "Build";
            this.buildButton.Click += new System.EventHandler(this.buildButton_Click);
            // 
            // buildAndRunButton
            // 
            this.buildAndRunButton.ForeColor = System.Drawing.Color.White;
            this.buildAndRunButton.Image = ((System.Drawing.Image)(resources.GetObject("buildAndRunButton.Image")));
            this.buildAndRunButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buildAndRunButton.Name = "buildAndRunButton";
            this.buildAndRunButton.Size = new System.Drawing.Size(51, 22);
            this.buildAndRunButton.Text = "Start";
            this.buildAndRunButton.Click += new System.EventHandler(this.buildAndRunButton_Click);
            // 
            // startButton
            // 
            this.startButton.ForeColor = System.Drawing.Color.White;
            this.startButton.Image = ((System.Drawing.Image)(resources.GetObject("startButton.Image")));
            this.startButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(144, 22);
            this.startButton.Text = "Start Without Building";
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // stopButton
            // 
            this.stopButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.stopButton.Image = ((System.Drawing.Image)(resources.GetObject("stopButton.Image")));
            this.stopButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(35, 22);
            this.stopButton.Text = "Stop";
            this.stopButton.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.tabControlAttributes);
            this.splitContainer2.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.splitContainer2.Panel2.Resize += new System.EventHandler(this.splitContainer3_Panel1_Resize);
            this.splitContainer2.Size = new System.Drawing.Size(1192, 569);
            this.splitContainer2.SplitterDistance = 248;
            this.splitContainer2.SplitterWidth = 5;
            this.splitContainer2.TabIndex = 2;
            // 
            // tabControlAttributes
            // 
            this.tabControlAttributes.AllowDrop = true;
            this.tabControlAttributes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tabControlAttributes.ContainerMargin = 2;
            this.tabControlAttributes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlAttributes.ExitHighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(176)))), ((int)(((byte)(239)))));
            this.tabControlAttributes.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControlAttributes.ForeColor = System.Drawing.Color.White;
            this.tabControlAttributes.HiddenTabWidth = ((uint)(140u));
            this.tabControlAttributes.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(138)))), ((int)(((byte)(213)))));
            this.tabControlAttributes.Location = new System.Drawing.Point(0, 0);
            this.tabControlAttributes.Name = "tabControlAttributes";
            this.tabControlAttributes.ReadOnly = true;
            this.tabControlAttributes.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(114)))), ((int)(((byte)(191)))));
            this.tabControlAttributes.SelectedColorUnfocused = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.tabControlAttributes.SelectedIndex = 0;
            this.tabControlAttributes.SelectedTabWidth = ((uint)(160u));
            this.tabControlAttributes.Size = new System.Drawing.Size(248, 569);
            this.tabControlAttributes.SplitterWidth = 2;
            this.tabControlAttributes.TabBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(44)))), ((int)(((byte)(47)))));
            this.tabControlAttributes.TabDock = TabControl.TabControlPro.TabDockStyle.Left;
            this.tabControlAttributes.TabHeight = ((uint)(18u));
            this.tabControlAttributes.TabIndex = 1;
            this.tabControlAttributes.TabChanged += new TabControl.TabControlPro.TabEventHandler(this.tabControlAttributes_TabChanged);
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.splitContainer6);
            this.splitContainer3.Panel1.Controls.Add(this.itemListInterface1);
            this.splitContainer3.Panel1.Controls.Add(this.startPage);
            this.splitContainer3.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.splitContainer3.Panel1.Resize += new System.EventHandler(this.splitContainer3_Panel1_Resize);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.tabControlSecondary);
            this.splitContainer3.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.splitContainer3.Size = new System.Drawing.Size(939, 569);
            this.splitContainer3.SplitterDistance = 374;
            this.splitContainer3.SplitterWidth = 5;
            this.splitContainer3.TabIndex = 0;
            // 
            // splitContainer6
            // 
            this.splitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer6.Location = new System.Drawing.Point(0, 0);
            this.splitContainer6.Name = "splitContainer6";
            // 
            // splitContainer6.Panel1
            // 
            this.splitContainer6.Panel1.Controls.Add(this.errorList);
            this.splitContainer6.Panel1.Controls.Add(this.tabControlMain);
            this.splitContainer6.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // splitContainer6.Panel2
            // 
            this.splitContainer6.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.splitContainer6.Panel2Collapsed = true;
            this.splitContainer6.Size = new System.Drawing.Size(939, 374);
            this.splitContainer6.SplitterDistance = 256;
            this.splitContainer6.SplitterWidth = 5;
            this.splitContainer6.TabIndex = 3;
            // 
            // errorList
            // 
            this.errorList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.errorList.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.errorList.BrushScrollBarClickedColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(239)))));
            this.errorList.BrushScrollBarDefaultColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(104)))), ((int)(((byte)(104)))));
            this.errorList.BrushScrollBarHoveredColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(158)))), ((int)(((byte)(158)))));
            this.errorList.CaseSensitive = true;
            this.errorList.ForeColor = System.Drawing.Color.White;
            this.errorList.Location = new System.Drawing.Point(495, 98);
            this.errorList.Name = "errorList";
            this.errorList.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.errorList.SelectedIndex = 0;
            this.errorList.Size = new System.Drawing.Size(75, 23);
            this.errorList.TabIndex = 2;
            // 
            // tabControlMain
            // 
            this.tabControlMain.AllowDrop = true;
            this.tabControlMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.tabControlMain.ContainerMargin = 2;
            this.tabControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlMain.ExitHighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(176)))), ((int)(((byte)(239)))));
            this.tabControlMain.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControlMain.ForeColor = System.Drawing.Color.White;
            this.tabControlMain.HiddenTabWidth = ((uint)(140u));
            this.tabControlMain.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(138)))), ((int)(((byte)(213)))));
            this.tabControlMain.Location = new System.Drawing.Point(0, 0);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.ReadOnly = false;
            this.tabControlMain.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(114)))), ((int)(((byte)(191)))));
            this.tabControlMain.SelectedColorUnfocused = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.SelectedTabWidth = ((uint)(160u));
            this.tabControlMain.Size = new System.Drawing.Size(939, 374);
            this.tabControlMain.SplitterWidth = 2;
            this.tabControlMain.TabBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(44)))), ((int)(((byte)(47)))));
            this.tabControlMain.TabDock = TabControl.TabControlPro.TabDockStyle.Top;
            this.tabControlMain.TabHeight = ((uint)(18u));
            this.tabControlMain.TabIndex = 0;
            this.tabControlMain.TabStop = false;
            this.tabControlMain.TabChanged += new TabControl.TabControlPro.TabEventHandler(this.tabControlMain_TabChanged);
            this.tabControlMain.TabClosing += new TabControl.TabControlPro.TabEventHandler(this.tabControlMain_TabClosing);
            this.tabControlMain.TabClosed += new TabControl.TabControlPro.TabEventHandler(this.tabControlMain_TabClosed);
            this.tabControlMain.DragDrop += new System.Windows.Forms.DragEventHandler(this.tabControlMain_DragDrop);
            this.tabControlMain.DragEnter += new System.Windows.Forms.DragEventHandler(this.tabControlMain_DragEnter);
            this.tabControlMain.DragOver += new System.Windows.Forms.DragEventHandler(this.tabControlMain_DragOver);
            // 
            // itemListInterface1
            // 
            this.itemListInterface1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.itemListInterface1.ButtonBorderColorDefault = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.itemListInterface1.ButtonBorderColorHovered = System.Drawing.Color.Transparent;
            this.itemListInterface1.ButtonBorderColorPressed = System.Drawing.Color.Transparent;
            this.itemListInterface1.ButtonBorderColorSelected = System.Drawing.Color.Transparent;
            this.itemListInterface1.ButtonColorDefault = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.itemListInterface1.ButtonColorHovered = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(64)))));
            this.itemListInterface1.ButtonColorPressed = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(151)))), ((int)(((byte)(234)))));
            this.itemListInterface1.ButtonColorSelected = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.itemListInterface1.ControlItemGap = 1;
            this.itemListInterface1.ControlItemWidth = 50;
            this.itemListInterface1.ControlMarginLeft = 10;
            this.itemListInterface1.ControlMarginTop = 10;
            this.itemListInterface1.Location = new System.Drawing.Point(86, 32567);
            this.itemListInterface1.Name = "itemListInterface1";
            this.itemListInterface1.Size = new System.Drawing.Size(233, 558);
            this.itemListInterface1.TabIndex = 2;
            this.itemListInterface1.ButtonClicked += new GBA_Studio.Controls.ItemListInterface.ItemListInterfaceHandler(this.itemListInterface1_ButtonClicked);
            // 
            // startPage
            // 
            this.startPage.AllowWebBrowserDrop = false;
            this.startPage.IsWebBrowserContextMenuEnabled = false;
            this.startPage.Location = new System.Drawing.Point(97, 2192);
            this.startPage.MinimumSize = new System.Drawing.Size(23, 23);
            this.startPage.Name = "startPage";
            this.startPage.ScrollBarsEnabled = false;
            this.startPage.Size = new System.Drawing.Size(292, 288);
            this.startPage.TabIndex = 0;
            this.startPage.Visible = false;
            this.startPage.WebBrowserShortcutsEnabled = false;
            this.startPage.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser1_DocumentCompleted);
            this.startPage.VisibleChanged += new System.EventHandler(this.startPage_VisibleChanged);
            // 
            // tabControlSecondary
            // 
            this.tabControlSecondary.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.tabControlSecondary.ContainerMargin = 0;
            this.tabControlSecondary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlSecondary.ExitHighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(176)))), ((int)(((byte)(239)))));
            this.tabControlSecondary.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControlSecondary.ForeColor = System.Drawing.Color.White;
            this.tabControlSecondary.HiddenTabWidth = ((uint)(130u));
            this.tabControlSecondary.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(138)))), ((int)(((byte)(213)))));
            this.tabControlSecondary.Location = new System.Drawing.Point(0, 0);
            this.tabControlSecondary.Name = "tabControlSecondary";
            this.tabControlSecondary.ReadOnly = true;
            this.tabControlSecondary.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(114)))), ((int)(((byte)(191)))));
            this.tabControlSecondary.SelectedColorUnfocused = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.tabControlSecondary.SelectedIndex = 0;
            this.tabControlSecondary.SelectedTabWidth = ((uint)(130u));
            this.tabControlSecondary.Size = new System.Drawing.Size(939, 190);
            this.tabControlSecondary.SplitterWidth = 2;
            this.tabControlSecondary.TabBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(44)))), ((int)(((byte)(47)))));
            this.tabControlSecondary.TabDock = TabControl.TabControlPro.TabDockStyle.Bottom;
            this.tabControlSecondary.TabHeight = ((uint)(18u));
            this.tabControlSecondary.TabIndex = 1;
            // 
            // smartSense1
            // 
            this.smartSense1.BackColor = System.Drawing.Color.Red;
            this.smartSense1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.smartSense1.BrushScrollBarClickedColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(235)))), ((int)(((byte)(239)))));
            this.smartSense1.BrushScrollBarDefaultColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(104)))), ((int)(((byte)(104)))));
            this.smartSense1.BrushScrollBarHoveredColor = System.Drawing.Color.FromArgb(((int)(((byte)(158)))), ((int)(((byte)(158)))), ((int)(((byte)(158)))));
            this.smartSense1.CaseSensitive = false;
            this.smartSense1.Filter = "";
            this.smartSense1.ForeColor = System.Drawing.Color.White;
            this.smartSense1.Location = new System.Drawing.Point(436, 3);
            this.smartSense1.Name = "smartSense1";
            this.smartSense1.SelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.smartSense1.Size = new System.Drawing.Size(125, 97);
            this.smartSense1.TabIndex = 3;
            this.smartSense1.Visible = false;
            this.smartSense1.Click += new System.EventHandler(this.smartSense1_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemFile,
            this.toolStripMenuItemEdit,
            this.toolStripMenuItemView,
            this.toolStripMenuItemProject,
            this.toolStripMenuItemDebug,
            this.toolStripMenuItemTools,
            this.toolStripMenuItemDeveloper,
            this.toolStripMenuItemHelp,
            this.goolStripMenuItemProfile});
            this.menuStrip1.Location = new System.Drawing.Point(0, 25);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStrip1.Size = new System.Drawing.Size(1192, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip2";
            // 
            // toolStripMenuItemFile
            // 
            this.toolStripMenuItemFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemNewSolution,
            this.addToolStripMenuItem,
            this.toolStripMenuItemOpen,
            this.toolStripSeparator6,
            this.toolStripMenuItemClose,
            this.toolStripMenuItemCloseSolution,
            this.toolStripSeparator7,
            this.toolStripMenuItemSave,
            this.toolStripMenuItemSaveAll,
            this.toolStripSeparator8,
            this.toolStripMenuItemRecentFiles,
            this.toolStripSeparator9,
            this.toolStripMenuItemExit});
            this.toolStripMenuItemFile.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemFile.Name = "toolStripMenuItemFile";
            this.toolStripMenuItemFile.Size = new System.Drawing.Size(40, 20);
            this.toolStripMenuItemFile.Text = "&FILE";
            // 
            // toolStripMenuItemNewSolution
            // 
            this.toolStripMenuItemNewSolution.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemNewSolution.Name = "toolStripMenuItemNewSolution";
            this.toolStripMenuItemNewSolution.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.toolStripMenuItemNewSolution.Size = new System.Drawing.Size(214, 22);
            this.toolStripMenuItemNewSolution.Text = "New";
            this.toolStripMenuItemNewSolution.Click += new System.EventHandler(this.toolStripMenuItemNewSolution_Click);
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.projectToolStripMenuItem,
            this.elementToolStripMenuItem});
            this.addToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
            this.addToolStripMenuItem.Text = "Add";
            // 
            // projectToolStripMenuItem
            // 
            this.projectToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox2});
            this.projectToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.projectToolStripMenuItem.Name = "projectToolStripMenuItem";
            this.projectToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.projectToolStripMenuItem.Text = "Project";
            // 
            // toolStripTextBox2
            // 
            this.toolStripTextBox2.Name = "toolStripTextBox2";
            this.toolStripTextBox2.Size = new System.Drawing.Size(100, 23);
            // 
            // elementToolStripMenuItem
            // 
            this.elementToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.elementToolStripMenuItem.Name = "elementToolStripMenuItem";
            this.elementToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.elementToolStripMenuItem.Text = "Element";
            this.elementToolStripMenuItem.Click += new System.EventHandler(this.elementToolStripMenuItem_Click);
            // 
            // toolStripMenuItemOpen
            // 
            this.toolStripMenuItemOpen.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemOpenProjectSolution,
            this.toolStripMenuItemOpenElement});
            this.toolStripMenuItemOpen.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemOpen.Name = "toolStripMenuItemOpen";
            this.toolStripMenuItemOpen.Size = new System.Drawing.Size(214, 22);
            this.toolStripMenuItemOpen.Text = "Open";
            // 
            // toolStripMenuItemOpenProjectSolution
            // 
            this.toolStripMenuItemOpenProjectSolution.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemOpenProjectSolution.Name = "toolStripMenuItemOpenProjectSolution";
            this.toolStripMenuItemOpenProjectSolution.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.toolStripMenuItemOpenProjectSolution.Size = new System.Drawing.Size(203, 22);
            this.toolStripMenuItemOpenProjectSolution.Text = "Project/Solution";
            this.toolStripMenuItemOpenProjectSolution.Click += new System.EventHandler(this.toolStripMenuItemOpenProjectSolution_Click);
            // 
            // toolStripMenuItemOpenElement
            // 
            this.toolStripMenuItemOpenElement.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemOpenElement.Name = "toolStripMenuItemOpenElement";
            this.toolStripMenuItemOpenElement.Size = new System.Drawing.Size(203, 22);
            this.toolStripMenuItemOpenElement.Text = "Element";
            this.toolStripMenuItemOpenElement.Click += new System.EventHandler(this.toolStripMenuItemOpenElement_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(211, 6);
            // 
            // toolStripMenuItemClose
            // 
            this.toolStripMenuItemClose.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemClose.Name = "toolStripMenuItemClose";
            this.toolStripMenuItemClose.Size = new System.Drawing.Size(214, 22);
            this.toolStripMenuItemClose.Text = "Close";
            this.toolStripMenuItemClose.Click += new System.EventHandler(this.toolStripMenuItemClose_Click);
            // 
            // toolStripMenuItemCloseSolution
            // 
            this.toolStripMenuItemCloseSolution.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemCloseSolution.Name = "toolStripMenuItemCloseSolution";
            this.toolStripMenuItemCloseSolution.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.X)));
            this.toolStripMenuItemCloseSolution.Size = new System.Drawing.Size(214, 22);
            this.toolStripMenuItemCloseSolution.Text = "Close Solution";
            this.toolStripMenuItemCloseSolution.Click += new System.EventHandler(this.toolStripMenuItemCloseSolution_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(211, 6);
            // 
            // toolStripMenuItemSave
            // 
            this.toolStripMenuItemSave.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemSave.Name = "toolStripMenuItemSave";
            this.toolStripMenuItemSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.toolStripMenuItemSave.Size = new System.Drawing.Size(214, 22);
            this.toolStripMenuItemSave.Text = "Save";
            this.toolStripMenuItemSave.Click += new System.EventHandler(this.saveCurrentFileButton_Click);
            // 
            // toolStripMenuItemSaveAll
            // 
            this.toolStripMenuItemSaveAll.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemSaveAll.Name = "toolStripMenuItemSaveAll";
            this.toolStripMenuItemSaveAll.Size = new System.Drawing.Size(214, 22);
            this.toolStripMenuItemSaveAll.Text = "Save All";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(211, 6);
            // 
            // toolStripMenuItemRecentFiles
            // 
            this.toolStripMenuItemRecentFiles.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemRecentFiles.Name = "toolStripMenuItemRecentFiles";
            this.toolStripMenuItemRecentFiles.Size = new System.Drawing.Size(214, 22);
            this.toolStripMenuItemRecentFiles.Text = "Recent Files";
            this.toolStripMenuItemRecentFiles.Click += new System.EventHandler(this.toolStripMenuItemRecentFiles_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(211, 6);
            // 
            // toolStripMenuItemExit
            // 
            this.toolStripMenuItemExit.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemExit.Name = "toolStripMenuItemExit";
            this.toolStripMenuItemExit.Size = new System.Drawing.Size(214, 22);
            this.toolStripMenuItemExit.Text = "Exit";
            this.toolStripMenuItemExit.Click += new System.EventHandler(this.toolStripMenuItemExit_Click);
            // 
            // toolStripMenuItemEdit
            // 
            this.toolStripMenuItemEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemUndo,
            this.toolStripMenuItemRedo,
            this.toolStripSeparator3,
            this.toolStripMenuItemCopy,
            this.toolStripMenuItemCut,
            this.toolStripMenuItemPaste,
            this.toolStripSeparator4,
            this.toolStripMenuItemSelectAll,
            this.toolStripSeparator5,
            this.toolStripMenuItemFindAndReplace});
            this.toolStripMenuItemEdit.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemEdit.Name = "toolStripMenuItemEdit";
            this.toolStripMenuItemEdit.Size = new System.Drawing.Size(43, 20);
            this.toolStripMenuItemEdit.Text = "&EDIT";
            // 
            // toolStripMenuItemUndo
            // 
            this.toolStripMenuItemUndo.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemUndo.Name = "toolStripMenuItemUndo";
            this.toolStripMenuItemUndo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.toolStripMenuItemUndo.Size = new System.Drawing.Size(204, 22);
            this.toolStripMenuItemUndo.Text = "Undo";
            this.toolStripMenuItemUndo.Click += new System.EventHandler(this.toolStripMenuItemUndo_Click);
            // 
            // toolStripMenuItemRedo
            // 
            this.toolStripMenuItemRedo.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemRedo.Name = "toolStripMenuItemRedo";
            this.toolStripMenuItemRedo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.toolStripMenuItemRedo.Size = new System.Drawing.Size(204, 22);
            this.toolStripMenuItemRedo.Text = "Redo";
            this.toolStripMenuItemRedo.Click += new System.EventHandler(this.toolStripMenuItemRedo_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(201, 6);
            // 
            // toolStripMenuItemCopy
            // 
            this.toolStripMenuItemCopy.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemCopy.Name = "toolStripMenuItemCopy";
            this.toolStripMenuItemCopy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.toolStripMenuItemCopy.Size = new System.Drawing.Size(204, 22);
            this.toolStripMenuItemCopy.Text = "Copy";
            this.toolStripMenuItemCopy.Click += new System.EventHandler(this.toolStripMenuItemCopy_Click);
            // 
            // toolStripMenuItemCut
            // 
            this.toolStripMenuItemCut.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemCut.Name = "toolStripMenuItemCut";
            this.toolStripMenuItemCut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.toolStripMenuItemCut.Size = new System.Drawing.Size(204, 22);
            this.toolStripMenuItemCut.Text = "Cut";
            this.toolStripMenuItemCut.Click += new System.EventHandler(this.toolStripMenuItemCut_Click);
            // 
            // toolStripMenuItemPaste
            // 
            this.toolStripMenuItemPaste.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemPaste.Name = "toolStripMenuItemPaste";
            this.toolStripMenuItemPaste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.toolStripMenuItemPaste.Size = new System.Drawing.Size(204, 22);
            this.toolStripMenuItemPaste.Text = "Paste";
            this.toolStripMenuItemPaste.Click += new System.EventHandler(this.toolStripMenuItemPaste_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(201, 6);
            // 
            // toolStripMenuItemSelectAll
            // 
            this.toolStripMenuItemSelectAll.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemSelectAll.Name = "toolStripMenuItemSelectAll";
            this.toolStripMenuItemSelectAll.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.toolStripMenuItemSelectAll.Size = new System.Drawing.Size(204, 22);
            this.toolStripMenuItemSelectAll.Text = "Select All";
            this.toolStripMenuItemSelectAll.Click += new System.EventHandler(this.toolStripMenuItemSelectAll_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(201, 6);
            // 
            // toolStripMenuItemFindAndReplace
            // 
            this.toolStripMenuItemFindAndReplace.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemFindAndReplace.Name = "toolStripMenuItemFindAndReplace";
            this.toolStripMenuItemFindAndReplace.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.toolStripMenuItemFindAndReplace.Size = new System.Drawing.Size(204, 22);
            this.toolStripMenuItemFindAndReplace.Text = "Find and Replace";
            this.toolStripMenuItemFindAndReplace.Click += new System.EventHandler(this.toolStripMenuItemFindAndReplace_Click);
            // 
            // toolStripMenuItemView
            // 
            this.toolStripMenuItemView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemStartPage,
            this.toolStripSeparator11,
            this.toolStripMenuItemConsole,
            this.toolStripMenuItemSolutionExplorer,
            this.toolStripMenuItemMemberExplorer,
            this.toolStripMenuItemErrorList,
            this.toolStripMenuItemToolbars});
            this.toolStripMenuItemView.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemView.Name = "toolStripMenuItemView";
            this.toolStripMenuItemView.Size = new System.Drawing.Size(46, 20);
            this.toolStripMenuItemView.Text = "&VIEW";
            // 
            // toolStripMenuItemStartPage
            // 
            this.toolStripMenuItemStartPage.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemStartPage.Name = "toolStripMenuItemStartPage";
            this.toolStripMenuItemStartPage.Size = new System.Drawing.Size(164, 22);
            this.toolStripMenuItemStartPage.Text = "Start Page";
            this.toolStripMenuItemStartPage.Click += new System.EventHandler(this.toolStripMenuItemStartPage_Click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(161, 6);
            // 
            // toolStripMenuItemConsole
            // 
            this.toolStripMenuItemConsole.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemConsole.Name = "toolStripMenuItemConsole";
            this.toolStripMenuItemConsole.Size = new System.Drawing.Size(164, 22);
            this.toolStripMenuItemConsole.Text = "Console";
            this.toolStripMenuItemConsole.Click += new System.EventHandler(this.toolStripMenuItemConsole_Click);
            // 
            // toolStripMenuItemSolutionExplorer
            // 
            this.toolStripMenuItemSolutionExplorer.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemSolutionExplorer.Name = "toolStripMenuItemSolutionExplorer";
            this.toolStripMenuItemSolutionExplorer.Size = new System.Drawing.Size(164, 22);
            this.toolStripMenuItemSolutionExplorer.Text = "Solution Explorer";
            this.toolStripMenuItemSolutionExplorer.Click += new System.EventHandler(this.toolStripMenuItemSolutionExplorer_Click);
            // 
            // toolStripMenuItemMemberExplorer
            // 
            this.toolStripMenuItemMemberExplorer.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemMemberExplorer.Name = "toolStripMenuItemMemberExplorer";
            this.toolStripMenuItemMemberExplorer.Size = new System.Drawing.Size(164, 22);
            this.toolStripMenuItemMemberExplorer.Text = "Member Explorer";
            this.toolStripMenuItemMemberExplorer.Click += new System.EventHandler(this.toolStripMenuItemMemberExplorer_Click);
            // 
            // toolStripMenuItemErrorList
            // 
            this.toolStripMenuItemErrorList.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemErrorList.Name = "toolStripMenuItemErrorList";
            this.toolStripMenuItemErrorList.Size = new System.Drawing.Size(164, 22);
            this.toolStripMenuItemErrorList.Text = "Error List";
            this.toolStripMenuItemErrorList.Click += new System.EventHandler(this.toolStripMenuItemErrorList_Click);
            // 
            // toolStripMenuItemToolbars
            // 
            this.toolStripMenuItemToolbars.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemFileToolbar,
            this.toolStripMenuItemDefaultToolbar});
            this.toolStripMenuItemToolbars.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemToolbars.Name = "toolStripMenuItemToolbars";
            this.toolStripMenuItemToolbars.Size = new System.Drawing.Size(164, 22);
            this.toolStripMenuItemToolbars.Text = "Toolbars";
            // 
            // toolStripMenuItemFileToolbar
            // 
            this.toolStripMenuItemFileToolbar.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemFileToolbar.Name = "toolStripMenuItemFileToolbar";
            this.toolStripMenuItemFileToolbar.Size = new System.Drawing.Size(109, 22);
            this.toolStripMenuItemFileToolbar.Text = "File";
            this.toolStripMenuItemFileToolbar.Click += new System.EventHandler(this.toolStripMenuItemFileToolbar_Click);
            // 
            // toolStripMenuItemDefaultToolbar
            // 
            this.toolStripMenuItemDefaultToolbar.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemDefaultToolbar.Name = "toolStripMenuItemDefaultToolbar";
            this.toolStripMenuItemDefaultToolbar.Size = new System.Drawing.Size(109, 22);
            this.toolStripMenuItemDefaultToolbar.Text = "Debug";
            this.toolStripMenuItemDefaultToolbar.Click += new System.EventHandler(this.toolStripMenuItemDebugToolbar_Click);
            // 
            // toolStripMenuItemProject
            // 
            this.toolStripMenuItemProject.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemAddElement,
            this.toolStripMenuItemProperties});
            this.toolStripMenuItemProject.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemProject.Name = "toolStripMenuItemProject";
            this.toolStripMenuItemProject.Size = new System.Drawing.Size(67, 20);
            this.toolStripMenuItemProject.Text = "&PROJECT";
            // 
            // toolStripMenuItemAddElement
            // 
            this.toolStripMenuItemAddElement.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemAddElement.Name = "toolStripMenuItemAddElement";
            this.toolStripMenuItemAddElement.Size = new System.Drawing.Size(142, 22);
            this.toolStripMenuItemAddElement.Text = "Add Element";
            // 
            // toolStripMenuItemProperties
            // 
            this.toolStripMenuItemProperties.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemProperties.Name = "toolStripMenuItemProperties";
            this.toolStripMenuItemProperties.Size = new System.Drawing.Size(142, 22);
            this.toolStripMenuItemProperties.Text = "Properties";
            // 
            // toolStripMenuItemDebug
            // 
            this.toolStripMenuItemDebug.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemCleanSolution,
            this.toolStripMenuItemStart,
            this.toolStripMenuItemStartWithoutBuild,
            this.buildToolStripMenuItem});
            this.toolStripMenuItemDebug.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemDebug.Name = "toolStripMenuItemDebug";
            this.toolStripMenuItemDebug.Size = new System.Drawing.Size(56, 20);
            this.toolStripMenuItemDebug.Text = "&DEBUG";
            // 
            // toolStripMenuItemCleanSolution
            // 
            this.toolStripMenuItemCleanSolution.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemCleanSolution.Name = "toolStripMenuItemCleanSolution";
            this.toolStripMenuItemCleanSolution.Size = new System.Drawing.Size(237, 22);
            this.toolStripMenuItemCleanSolution.Text = "Clean Solution";
            this.toolStripMenuItemCleanSolution.Click += new System.EventHandler(this.cleanSolution_Click);
            // 
            // toolStripMenuItemStart
            // 
            this.toolStripMenuItemStart.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemStart.Name = "toolStripMenuItemStart";
            this.toolStripMenuItemStart.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.toolStripMenuItemStart.Size = new System.Drawing.Size(237, 22);
            this.toolStripMenuItemStart.Text = "Start";
            this.toolStripMenuItemStart.Click += new System.EventHandler(this.buildAndRunButton_Click);
            // 
            // toolStripMenuItemStartWithoutBuild
            // 
            this.toolStripMenuItemStartWithoutBuild.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemStartWithoutBuild.Name = "toolStripMenuItemStartWithoutBuild";
            this.toolStripMenuItemStartWithoutBuild.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F5)));
            this.toolStripMenuItemStartWithoutBuild.Size = new System.Drawing.Size(237, 22);
            this.toolStripMenuItemStartWithoutBuild.Text = "Start Without Building";
            this.toolStripMenuItemStartWithoutBuild.Click += new System.EventHandler(this.startButton_Click);
            // 
            // buildToolStripMenuItem
            // 
            this.buildToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.buildToolStripMenuItem.Name = "buildToolStripMenuItem";
            this.buildToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F5)));
            this.buildToolStripMenuItem.Size = new System.Drawing.Size(237, 22);
            this.buildToolStripMenuItem.Text = "Build";
            this.buildToolStripMenuItem.Click += new System.EventHandler(this.buildButton_Click);
            // 
            // toolStripMenuItemTools
            // 
            this.toolStripMenuItemTools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemEnterEvaluationMode,
            this.toolStripMenuItemBitwiseSandbox,
            this.toolStripMenuItemSpriteCanvas});
            this.toolStripMenuItemTools.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemTools.Name = "toolStripMenuItemTools";
            this.toolStripMenuItemTools.Size = new System.Drawing.Size(55, 20);
            this.toolStripMenuItemTools.Text = "&TOOLS";
            // 
            // toolStripMenuItemEnterEvaluationMode
            // 
            this.toolStripMenuItemEnterEvaluationMode.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemEnterEvaluationMode.Name = "toolStripMenuItemEnterEvaluationMode";
            this.toolStripMenuItemEnterEvaluationMode.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.E)));
            this.toolStripMenuItemEnterEvaluationMode.Size = new System.Drawing.Size(256, 22);
            this.toolStripMenuItemEnterEvaluationMode.Text = "Enter Evaluation Mode";
            this.toolStripMenuItemEnterEvaluationMode.Click += new System.EventHandler(this.toolStripMenuItemEnterEvaluationMode_Click);
            // 
            // toolStripMenuItemBitwiseSandbox
            // 
            this.toolStripMenuItemBitwiseSandbox.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemBitwiseSandbox.Name = "toolStripMenuItemBitwiseSandbox";
            this.toolStripMenuItemBitwiseSandbox.Size = new System.Drawing.Size(256, 22);
            this.toolStripMenuItemBitwiseSandbox.Text = "Bitwise Sandbox";
            this.toolStripMenuItemBitwiseSandbox.Click += new System.EventHandler(this.toolStripMenuItemBitwiseSandbox_Click);
            // 
            // toolStripMenuItemSpriteCanvas
            // 
            this.toolStripMenuItemSpriteCanvas.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemSpriteCanvas.Name = "toolStripMenuItemSpriteCanvas";
            this.toolStripMenuItemSpriteCanvas.Size = new System.Drawing.Size(256, 22);
            this.toolStripMenuItemSpriteCanvas.Text = "Sprite Canvas";
            this.toolStripMenuItemSpriteCanvas.Click += new System.EventHandler(this.toolStripMenuItemSpriteCanvas_Click);
            // 
            // toolStripMenuItemDeveloper
            // 
            this.toolStripMenuItemDeveloper.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemBugManagement});
            this.toolStripMenuItemDeveloper.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemDeveloper.Name = "toolStripMenuItemDeveloper";
            this.toolStripMenuItemDeveloper.Size = new System.Drawing.Size(81, 20);
            this.toolStripMenuItemDeveloper.Text = "DEVE&LOPER";
            this.toolStripMenuItemDeveloper.Visible = false;
            // 
            // toolStripMenuItemBugManagement
            // 
            this.toolStripMenuItemBugManagement.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemBugManagement.Name = "toolStripMenuItemBugManagement";
            this.toolStripMenuItemBugManagement.Size = new System.Drawing.Size(169, 22);
            this.toolStripMenuItemBugManagement.Text = "Bug Management";
            this.toolStripMenuItemBugManagement.Click += new System.EventHandler(this.toolStripMenuItemBugManagement_Click);
            // 
            // toolStripMenuItemHelp
            // 
            this.toolStripMenuItemHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reportABugToolStripMenuItem,
            this.toolStripSeparator10,
            this.toolStripMenuItemAbout});
            this.toolStripMenuItemHelp.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemHelp.Name = "toolStripMenuItemHelp";
            this.toolStripMenuItemHelp.Size = new System.Drawing.Size(47, 20);
            this.toolStripMenuItemHelp.Text = "&HELP";
            // 
            // reportABugToolStripMenuItem
            // 
            this.reportABugToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.reportABugToolStripMenuItem.Name = "reportABugToolStripMenuItem";
            this.reportABugToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.reportABugToolStripMenuItem.Text = "Report a Bug";
            this.reportABugToolStripMenuItem.Click += new System.EventHandler(this.reportABugToolStripMenuItem_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(139, 6);
            // 
            // toolStripMenuItemAbout
            // 
            this.toolStripMenuItemAbout.ForeColor = System.Drawing.Color.White;
            this.toolStripMenuItemAbout.Name = "toolStripMenuItemAbout";
            this.toolStripMenuItemAbout.Size = new System.Drawing.Size(142, 22);
            this.toolStripMenuItemAbout.Text = "About";
            this.toolStripMenuItemAbout.Click += new System.EventHandler(this.toolStripMenuItemAbout_Click);
            // 
            // goolStripMenuItemProfile
            // 
            this.goolStripMenuItemProfile.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.goolStripMenuItemProfile.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(153)))), ((int)(((byte)(153)))));
            this.goolStripMenuItemProfile.Image = global::GBA_Studio.Properties.Resources.personIcon;
            this.goolStripMenuItemProfile.Name = "goolStripMenuItemProfile";
            this.goolStripMenuItemProfile.Size = new System.Drawing.Size(28, 20);
            this.goolStripMenuItemProfile.Click += new System.EventHandler(this.goolStripMenuItemGuest_Click);
            // 
            // splitContainer5
            // 
            this.splitContainer5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer5.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer5.IsSplitterFixed = true;
            this.splitContainer5.Location = new System.Drawing.Point(0, 0);
            this.splitContainer5.Name = "splitContainer5";
            // 
            // splitContainer5.Panel1
            // 
            this.splitContainer5.Panel1.Controls.Add(this.menuStrip2);
            // 
            // splitContainer5.Panel2
            // 
            this.splitContainer5.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.splitContainer5.Size = new System.Drawing.Size(1022, 25);
            this.splitContainer5.SplitterDistance = 996;
            this.splitContainer5.SplitterWidth = 1;
            this.splitContainer5.TabIndex = 0;
            this.splitContainer5.TabStop = false;
            this.splitContainer5.Resize += new System.EventHandler(this.splitContainer5_Resize);
            // 
            // gbaMonitor
            // 
            this.gbaMonitor.EnableRaisingEvents = true;
            this.gbaMonitor.SynchronizingObject = this;
            this.gbaMonitor.Changed += new System.IO.FileSystemEventHandler(this.gbaMonitor_Changed);
            // 
            // menuBtnMin
            // 
            this.menuBtnMin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.menuBtnMin.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.menuBtnMin.BorderHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.menuBtnMin.BorderPressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.menuBtnMin.ButtonHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.menuBtnMin.ButtonPressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.menuBtnMin.ButtonText = "";
            this.menuBtnMin.DialogResult = System.Windows.Forms.DialogResult.None;
            this.menuBtnMin.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuBtnMin.FontHoverColor = System.Drawing.Color.White;
            this.menuBtnMin.FontPressedColor = System.Drawing.Color.White;
            this.menuBtnMin.Location = new System.Drawing.Point(0, 0);
            this.menuBtnMin.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.menuBtnMin.Name = "menuBtnMin";
            this.menuBtnMin.Size = new System.Drawing.Size(34, 24);
            this.menuBtnMin.TabIndex = 2;
            this.menuBtnMin.ButtonClicked += new System.EventHandler(this.menuBtnMin_Click);
            // 
            // menuBtnMax
            // 
            this.menuBtnMax.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.menuBtnMax.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.menuBtnMax.BorderHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.menuBtnMax.BorderPressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.menuBtnMax.ButtonHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.menuBtnMax.ButtonPressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.menuBtnMax.ButtonText = "";
            this.menuBtnMax.DialogResult = System.Windows.Forms.DialogResult.None;
            this.menuBtnMax.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuBtnMax.FontHoverColor = System.Drawing.Color.White;
            this.menuBtnMax.FontPressedColor = System.Drawing.Color.White;
            this.menuBtnMax.Location = new System.Drawing.Point(34, 0);
            this.menuBtnMax.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.menuBtnMax.Name = "menuBtnMax";
            this.menuBtnMax.Size = new System.Drawing.Size(34, 24);
            this.menuBtnMax.TabIndex = 1;
            this.menuBtnMax.ButtonClicked += new System.EventHandler(this.menuBtnMax_Click);
            // 
            // menuBtnClose
            // 
            this.menuBtnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.menuBtnClose.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.menuBtnClose.BorderHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.menuBtnClose.BorderPressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.menuBtnClose.ButtonHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(65)))));
            this.menuBtnClose.ButtonPressedColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.menuBtnClose.ButtonText = "";
            this.menuBtnClose.DialogResult = System.Windows.Forms.DialogResult.None;
            this.menuBtnClose.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuBtnClose.FontHoverColor = System.Drawing.Color.White;
            this.menuBtnClose.FontPressedColor = System.Drawing.Color.White;
            this.menuBtnClose.Location = new System.Drawing.Point(68, 0);
            this.menuBtnClose.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.menuBtnClose.Name = "menuBtnClose";
            this.menuBtnClose.Size = new System.Drawing.Size(34, 24);
            this.menuBtnClose.TabIndex = 0;
            this.menuBtnClose.ButtonClicked += new System.EventHandler(this.menuBtnClose_Click);
            // 
            // keyMonitor1
            // 
            this.keyMonitor1.CombinationHandleStyle = GBA_Studio.Controls.KeyMonitor.CombinationHandling.Explicit;
            this.keyMonitor1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.keyMonitor1_KeyDown);
            this.keyMonitor1.KeyCombinationDown += new GBA_Studio.Controls.KeyMonitor.KeyCombinationEventHandler(this.keyMonitor1_KeyCombinationDown);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.ClientSize = new System.Drawing.Size(1192, 695);
            this.Controls.Add(this.smartSense1);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Location = new System.Drawing.Point(0, 0);
            this.MainMenuStrip = this.menuStrip2;
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.MinimumSize = new System.Drawing.Size(100, 50);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusRightToLeft = true;
            this.StatusStripVisible = true;
            this.StatusText = "Design Mode";
            this.Text = "GBA Studio 2016";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.toolStripFile.ResumeLayout(false);
            this.toolStripFile.PerformLayout();
            this.toolStripDefault.ResumeLayout(false);
            this.toolStripDefault.PerformLayout();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.splitContainer6.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).EndInit();
            this.splitContainer6.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer5.Panel1.ResumeLayout(false);
            this.splitContainer5.Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).EndInit();
            this.splitContainer5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gbaMonitor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.EditPad consoleBox;
        private System.Windows.Forms.Timer updateConsole;
        private System.Windows.Forms.TreeView solutionExplorer;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripTextBox formTitle;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.SplitContainer splitContainer5;
        private Controls.ButtonPro menuBtnMin;
        private Controls.ButtonPro menuBtnMax;
        private Controls.ButtonPro menuBtnClose;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.ToolStrip toolStripFile;
        private System.Windows.Forms.ToolStripButton newSolutionButton;
        private System.Windows.Forms.ToolStripButton newProjectButton;
        private System.Windows.Forms.ToolStripButton newFileButton;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private Controls.ItemListInterface itemListInterface1;
        private System.Windows.Forms.WebBrowser startPage;
        private TabControl.TabControlPro tabControlMain;
        private TabControl.TabControlPro tabControlSecondary;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemFile;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemNewSolution;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem projectToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox2;
        private System.Windows.Forms.ToolStripMenuItem elementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemOpen;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemOpenProjectSolution;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemOpenElement;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemClose;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemCloseSolution;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSave;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemRecentFiles;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemExit;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemEdit;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemUndo;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemRedo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemCopy;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemCut;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemPaste;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSelectAll;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemView;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemStartPage;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemConsole;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSolutionExplorer;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemMemberExplorer;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemToolbars;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemFileToolbar;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemDefaultToolbar;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemProject;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemAddElement;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemProperties;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemDebug;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemStart;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemStartWithoutBuild;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemTools;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemEnterEvaluationMode;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemDeveloper;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemHelp;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemAbout;
        private System.Windows.Forms.SplitContainer splitContainer6;
        private System.IO.FileSystemWatcher gbaMonitor;
        private System.Windows.Forms.ToolStripMenuItem buildToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSaveAll;
        private System.Windows.Forms.ToolStrip toolStripDefault;
        private System.Windows.Forms.ToolStripButton loadProjectButton;
        private System.Windows.Forms.ToolStripButton saveCurrentFileButton;
        private System.Windows.Forms.ToolStripButton saveAllButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton undoButton;
        private System.Windows.Forms.ToolStripButton redoButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton buildButton;
        private System.Windows.Forms.ToolStripButton buildAndRunButton;
        private System.Windows.Forms.ToolStripButton startButton;
        private System.Windows.Forms.ToolStripButton stopButton;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemCleanSolution;
        public Controls.KeyMonitor keyMonitor1;
        private TabControl.TabControlPro tabControlAttributes;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemBitwiseSandbox;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemErrorList;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemBugManagement;
        private System.Windows.Forms.ToolStripMenuItem reportABugToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSpriteCanvas;
        private System.Windows.Forms.ToolStripMenuItem goolStripMenuItemProfile;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemFindAndReplace;
        private Controls.ListBoxPro errorList;
        private SmartSense.SmartSense smartSense1;
    }
}

