﻿namespace GBA_Studio.Controls
{
    partial class BinaryVisualizer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bit128 = new System.Windows.Forms.TextBox();
            this.bit16 = new System.Windows.Forms.TextBox();
            this.bit32 = new System.Windows.Forms.TextBox();
            this.bit64 = new System.Windows.Forms.TextBox();
            this.bit4 = new System.Windows.Forms.TextBox();
            this.bit2 = new System.Windows.Forms.TextBox();
            this.bit1 = new System.Windows.Forms.TextBox();
            this.bit8 = new System.Windows.Forms.TextBox();
            this.binaryName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bit128
            // 
            this.bit128.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(45)))));
            this.bit128.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bit128.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bit128.ForeColor = System.Drawing.Color.White;
            this.bit128.Location = new System.Drawing.Point(71, 3);
            this.bit128.MaxLength = 1;
            this.bit128.Name = "bit128";
            this.bit128.ReadOnly = true;
            this.bit128.Size = new System.Drawing.Size(45, 35);
            this.bit128.TabIndex = 0;
            this.bit128.Text = "0";
            this.bit128.KeyDown += new System.Windows.Forms.KeyEventHandler(this.bit_KeyDown);
            this.bit128.KeyUp += new System.Windows.Forms.KeyEventHandler(this.bit_KeyUp);
            // 
            // bit16
            // 
            this.bit16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(45)))));
            this.bit16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bit16.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bit16.ForeColor = System.Drawing.Color.White;
            this.bit16.Location = new System.Drawing.Point(224, 3);
            this.bit16.MaxLength = 1;
            this.bit16.Name = "bit16";
            this.bit16.ReadOnly = true;
            this.bit16.Size = new System.Drawing.Size(45, 35);
            this.bit16.TabIndex = 3;
            this.bit16.Text = "0";
            this.bit16.KeyDown += new System.Windows.Forms.KeyEventHandler(this.bit_KeyDown);
            this.bit16.KeyUp += new System.Windows.Forms.KeyEventHandler(this.bit_KeyUp);
            // 
            // bit32
            // 
            this.bit32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(45)))));
            this.bit32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bit32.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bit32.ForeColor = System.Drawing.Color.White;
            this.bit32.Location = new System.Drawing.Point(173, 3);
            this.bit32.MaxLength = 1;
            this.bit32.Name = "bit32";
            this.bit32.ReadOnly = true;
            this.bit32.Size = new System.Drawing.Size(45, 35);
            this.bit32.TabIndex = 2;
            this.bit32.Text = "0";
            this.bit32.KeyDown += new System.Windows.Forms.KeyEventHandler(this.bit_KeyDown);
            this.bit32.KeyUp += new System.Windows.Forms.KeyEventHandler(this.bit_KeyUp);
            // 
            // bit64
            // 
            this.bit64.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(45)))));
            this.bit64.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bit64.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bit64.ForeColor = System.Drawing.Color.White;
            this.bit64.Location = new System.Drawing.Point(122, 3);
            this.bit64.MaxLength = 1;
            this.bit64.Name = "bit64";
            this.bit64.ReadOnly = true;
            this.bit64.Size = new System.Drawing.Size(45, 35);
            this.bit64.TabIndex = 1;
            this.bit64.Text = "0";
            this.bit64.KeyDown += new System.Windows.Forms.KeyEventHandler(this.bit_KeyDown);
            this.bit64.KeyUp += new System.Windows.Forms.KeyEventHandler(this.bit_KeyUp);
            // 
            // bit4
            // 
            this.bit4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(45)))));
            this.bit4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bit4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bit4.ForeColor = System.Drawing.Color.White;
            this.bit4.Location = new System.Drawing.Point(326, 3);
            this.bit4.MaxLength = 1;
            this.bit4.Name = "bit4";
            this.bit4.ReadOnly = true;
            this.bit4.Size = new System.Drawing.Size(45, 35);
            this.bit4.TabIndex = 5;
            this.bit4.Text = "0";
            this.bit4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.bit_KeyDown);
            this.bit4.KeyUp += new System.Windows.Forms.KeyEventHandler(this.bit_KeyUp);
            // 
            // bit2
            // 
            this.bit2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(45)))));
            this.bit2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bit2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bit2.ForeColor = System.Drawing.Color.White;
            this.bit2.Location = new System.Drawing.Point(377, 3);
            this.bit2.MaxLength = 1;
            this.bit2.Name = "bit2";
            this.bit2.ReadOnly = true;
            this.bit2.Size = new System.Drawing.Size(45, 35);
            this.bit2.TabIndex = 6;
            this.bit2.Text = "0";
            this.bit2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.bit_KeyDown);
            this.bit2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.bit_KeyUp);
            // 
            // bit1
            // 
            this.bit1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(45)))));
            this.bit1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bit1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bit1.ForeColor = System.Drawing.Color.White;
            this.bit1.Location = new System.Drawing.Point(428, 3);
            this.bit1.MaxLength = 1;
            this.bit1.Name = "bit1";
            this.bit1.ReadOnly = true;
            this.bit1.Size = new System.Drawing.Size(45, 35);
            this.bit1.TabIndex = 7;
            this.bit1.Text = "0";
            this.bit1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.bit_KeyDown);
            this.bit1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.bit_KeyUp);
            // 
            // bit8
            // 
            this.bit8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(43)))), ((int)(((byte)(43)))), ((int)(((byte)(45)))));
            this.bit8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bit8.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bit8.ForeColor = System.Drawing.Color.White;
            this.bit8.Location = new System.Drawing.Point(275, 3);
            this.bit8.MaxLength = 1;
            this.bit8.Name = "bit8";
            this.bit8.ReadOnly = true;
            this.bit8.Size = new System.Drawing.Size(45, 35);
            this.bit8.TabIndex = 4;
            this.bit8.Text = "0";
            this.bit8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.bit_KeyDown);
            this.bit8.KeyUp += new System.Windows.Forms.KeyEventHandler(this.bit_KeyUp);
            // 
            // binaryName
            // 
            this.binaryName.AutoSize = true;
            this.binaryName.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.binaryName.Location = new System.Drawing.Point(6, 5);
            this.binaryName.Name = "binaryName";
            this.binaryName.Size = new System.Drawing.Size(26, 29);
            this.binaryName.TabIndex = 8;
            this.binaryName.Text = "a";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(38, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 29);
            this.label1.TabIndex = 9;
            this.label1.Text = "=";
            // 
            // BinaryVisualizer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.Controls.Add(this.label1);
            this.Controls.Add(this.binaryName);
            this.Controls.Add(this.bit4);
            this.Controls.Add(this.bit2);
            this.Controls.Add(this.bit1);
            this.Controls.Add(this.bit8);
            this.Controls.Add(this.bit64);
            this.Controls.Add(this.bit32);
            this.Controls.Add(this.bit16);
            this.Controls.Add(this.bit128);
            this.ForeColor = System.Drawing.Color.White;
            this.Name = "BinaryVisualizer";
            this.Size = new System.Drawing.Size(481, 41);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox bit128;
        private System.Windows.Forms.TextBox bit16;
        private System.Windows.Forms.TextBox bit32;
        private System.Windows.Forms.TextBox bit64;
        private System.Windows.Forms.TextBox bit4;
        private System.Windows.Forms.TextBox bit2;
        private System.Windows.Forms.TextBox bit1;
        private System.Windows.Forms.TextBox bit8;
        private System.Windows.Forms.Label binaryName;
        private System.Windows.Forms.Label label1;
    }
}
