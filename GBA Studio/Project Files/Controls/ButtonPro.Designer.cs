﻿namespace GBA_Studio.Controls
{
    partial class ButtonPro
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ButtonPro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "ButtonPro";
            this.Size = new System.Drawing.Size(139, 39);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonPro_MouseDown);
            this.MouseLeave += new System.EventHandler(this.ButtonPro_MouseLeave);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ButtonPro_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ButtonPro_MouseUp);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
