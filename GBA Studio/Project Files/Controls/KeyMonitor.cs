﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Threading;
using System.Collections.Specialized;

namespace GBA_Studio.Controls
{
    public partial class KeyMonitor : Component
    {
        [DllImport("user32.dll")]
        private static extern short GetAsyncKeyState(int vKey);

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        private static extern short GetKeyState(int keyCode);

        [Flags]
        private enum KeyStates
        {
            None = 0,
            Down = 1,
            Toggled = 2
        }

        public enum CombinationHandling
        {
            Explicit,
            Implicit
        }

        private short ctrlState = GetAsyncKeyState(0x11);
        private short cState = GetAsyncKeyState(0x43);
        private short tabState = GetAsyncKeyState(0x09);

        private bool isRunning = false;

        private List<KeyCombination> keyCombinations = new List<KeyCombination>();
        private List<Keys> keysDown = new List<Keys>();
        private List<Keys> keysWatched = new List<Keys>();

        CombinationHandling combinationHandling = CombinationHandling.Explicit;

        private volatile bool canExitSafely = true;
        private Thread scanThread;
        
        #region Constructors
        public KeyMonitor()
        {
            InitializeComponent();
        }
        public KeyMonitor(IContainer container)
        {
            container.Add(this);

            InitializeComponent();

        }
        #endregion
        
        #region Functions
        public void Begin()
        {
            isRunning = true;
            canExitSafely = false;

            scanThread = new Thread(CheckForKeyStates);
            scanThread.Name = "Key Monitor";
            scanThread.Start();
        }

        public void Terminate()
        {
            isRunning = false;
        }

        private void CheckForKeyStates()
        {
            while (isRunning)
            {
                UpdateKeyStatus();
                UpdateKeyCombinationStatus();
            }
            canExitSafely = true;
        }
        private void UpdateKeyStatus()
        {
            foreach (Keys key in keysWatched)
            {
                short keyState = GetAsyncKeyState((int)key);

                KeyStates state = KeyStates.None;

                if ((keyState & 0x8000) == 0x8000)
                {
                    state |= KeyStates.Down;
                }
                else if ((keyState & 1) == 1)
                {
                    state |= KeyStates.Toggled;
                }

                if (state.HasFlag(KeyStates.Down) || state.HasFlag(KeyStates.Toggled))
                {
                    if (!keysDown.Contains(key))
                    {
                        keysDown.Add(key);
                        Raise(key, state);
                    }
                }
                else
                {
                    if (keysDown.Contains(key))
                    {
                        keysDown.Remove(key);
                        Raise(key, state);
                    }
                }
            }
        }
        private void UpdateKeyCombinationStatus()
        {
            if (keysDown.Count >= 2)
            {
                foreach (KeyCombination keyCombination in keyCombinations)
                {
                    Keys combination = keyCombination.CombinationFlags;
                    Keys currentCombo = Keys.None;
                    List<Keys> keysFound = new List<Keys>();
                    foreach (Keys key in keysDown)
                    {
                        if (combination.HasFlag(key))
                        {
                            currentCombo |= key;
                            keysFound.Add(key);
                        }

                    }
                    if (currentCombo == combination &&
                        (combinationHandling == CombinationHandling.Implicit ||
                        (combinationHandling == CombinationHandling.Explicit &&
                        keysDown.Count == keysFound.Count)))
                    {
                        if (KeyCombinationDown != null)
                        {
                            KeyCombinationEventArgs eventArgs = new KeyCombinationEventArgs(keysFound);
                            KeyCombinationDown(this, new KeyCombinationEventArgs(keysFound));
                        }
                    }
                }
            }
        }

        private void Raise(Keys key, KeyStates state)
        {
            KeyEventArgs eventArgs = new KeyEventArgs(key);
            switch (state)
            {
                case KeyStates.Down:
                    if (KeyDown != null)
                    {
                        KeyDown(this, eventArgs);
                    }
                    break;
                case KeyStates.None:
                    if (KeyUp != null)
                    {
                        KeyUp(this, eventArgs);
                    }
                    break;
                case KeyStates.Toggled:
                    if (KeyToggled != null)
                    {
                        KeyToggled(this, eventArgs);
                    }
                    break;
            }
        }
        public void AddWatchedKey(params Keys[] keys)
        {
            for (int i = 0; i < keys.Length; i++)
            {
                if (!keysWatched.Contains(keys[i]))
                {
                    keysWatched.Add(keys[i]);
                }
            }
        }
        public void RemoveWatchedKey(params Keys[] keys)
        {
            for (int i = 0; i < keys.Length; i++)
            {
                if (keysWatched.Contains(keys[i]))
                {
                    keysWatched.Remove(keys[i]);
                }
            }
        }
        public void AddKeyCombination(params Keys[] keys)
        {
            KeyCombination combination = new KeyCombination(keys);
            if (!keyCombinations.Contains(combination))
            {
                keyCombinations.Add(combination);
                for (int i = 0; i < keys.Length; i++)
                {
                    AddWatchedKey(keys[i]);
                }
            }
        }
        public void RemoveKeyCombination(params Keys[] keys)
        {
            KeyCombination combination = new KeyCombination(keys);
            if (keyCombinations.Contains(combination))
            {
                keyCombinations.Remove(combination);
            }
        }
        #endregion

        public bool CanExitSafely
        {
            get
            {
                return canExitSafely;
            }
        }

        public CombinationHandling CombinationHandleStyle
        {
            get
            {
                return combinationHandling;
            }
            set
            {
                combinationHandling = value;
            }
        }

        #region Events
        public delegate void KeyCombinationEventHandler(object sender, KeyCombinationEventArgs e);

        public event KeyEventHandler KeyDown;
        public event KeyEventHandler KeyUp;
        public event KeyEventHandler KeyToggled;
        public event KeyCombinationEventHandler KeyCombinationDown;

        public class KeyCombinationEventArgs
        {
            private List<Keys> combination;

            public KeyCombinationEventArgs(List<Keys> keys)
            {
                combination = keys;
            }
            public List<Keys> Combination
            {
                get
                {
                    return combination;
                }
            }
        }
        public class KeyCombination
        {
            Keys keyCombination = Keys.None;

            public KeyCombination(params Keys[] keys)
            {
                foreach (Keys key in keys)
                {
                    keyCombination |= key;
                }
            }
            public void AddKey(Keys key)
            {
                keyCombination |= key;
            }
            public void RemoveKey(Keys key)
            {
                keyCombination &= ~key;
            }
            public Keys CombinationFlags
            {
                get
                {
                    return keyCombination;
                }
            }

        }
        #endregion
        
    }

}
