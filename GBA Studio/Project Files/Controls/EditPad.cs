﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Document;
using System.Runtime.InteropServices;
using ICSharpCode.AvalonEdit.Indentation.CSharp;
using ICSharpCode.AvalonEdit.Highlighting;
using System.IO;
using System.Xml;

using GBA_Studio.IO;
using GBA_Studio.Management;

namespace GBA_Studio.Controls
{
    public enum PadType
    {
        File,
        NotFile
    }

    public partial class EditPad : UserControl
    {
        public delegate void KeyCharEventHandler(object sender, KeyCharEventArgs e, out bool cancel);
        public new event KeyCharEventHandler KeyCharDown;

        public new event System.Windows.Input.MouseEventHandler Click;

        public class KeyCharEventArgs
        {
            private char keyChar;

            public KeyCharEventArgs(char character)
            {
                keyChar = character;
            }
            public char Character
            {
                get { return keyChar; }
            }
        }

        public new event EventHandler TextChanged;
        public new event System.Windows.Input.KeyEventHandler KeyDown;
        public new event System.Windows.Input.KeyEventHandler KeyUp;

        public new event EventHandler CopiedText;
        public new event EventHandler CtrlTab;
        public new event EventHandler FinishedCopy;

        private bool ctrlPressed = false;
        private bool cPressed = false;
        private bool tabPressed = false;

        private TextDocumentAccessor indentor;

        private ICSharpCode.AvalonEdit.Indentation.IIndentationStrategy indentation;

        private PadType padType;

        private System.Windows.Media.Brush background = (System.Windows.Media.Brush)(new System.Windows.Media.BrushConverter().ConvertFrom("#1e1e1e"));
        private System.Windows.Media.Brush foreground = (System.Windows.Media.Brush)(new System.Windows.Media.BrushConverter().ConvertFrom("#ffffff"));

        private string lastSaveText = "";

        private TextEditor t = new TextEditor();

        private Project parentProject;

        public Project ParentProject
        {
            get
            {
                return parentProject;
            }
        }

        public string SelectedText
        {
            get
            {
                return t.SelectedText;
            }
        }

        private string filePath;

        public string FilePath
        {
            get
            {
                return filePath;
            }
        }
        [DllImport("user32.dll")]
        public static extern int ToUnicode(
            uint wVirtKey,
            uint wScanCode,
            byte[] lpKeyState,
            [Out, MarshalAs(UnmanagedType.LPWStr, SizeParamIndex = 4)]
                    StringBuilder pwszBuff,
            int cchBuff,
            uint wFlags);

        [DllImport("user32.dll")]
        public static extern bool GetKeyboardState(byte[] lpKeyState);

        [DllImport("user32.dll")]
        public static extern uint MapVirtualKey(uint uCode, MapType uMapType);

        public enum MapType : uint
        {
            MAPVK_VK_TO_VSC = 0x0,
            MAPVK_VSC_TO_VK = 0x1,
            MAPVK_VK_TO_CHAR = 0x2,
            MAPVK_VSC_TO_VK_EX = 0x3,
        }

        public static char GetCharFromKey(System.Windows.Input.Key key)
        {
            char ch = ' ';

            int virtualKey = System.Windows.Input.KeyInterop.VirtualKeyFromKey(key);
            byte[] keyboardState = new byte[256];
            GetKeyboardState(keyboardState);

            uint scanCode = MapVirtualKey((uint)virtualKey, MapType.MAPVK_VK_TO_VSC);
            StringBuilder stringBuilder = new StringBuilder(2);

            int result = ToUnicode((uint)virtualKey, scanCode, keyboardState, stringBuilder, stringBuilder.Capacity, 0);
            switch (result)
            {
                case -1:
                    break;
                case 0:
                    break;
                case 1:
                    {
                        ch = stringBuilder[0];
                        break;
                    }
                default:
                    {
                        ch = stringBuilder[0];
                        break;
                    }
            }
            return ch;
        }

        public bool IsInDate()
        {
            return lastSaveText == t.Text;
        }

        public new void Load(string filePath)
        {
            this.filePath = filePath;

            lastSaveText = File.ReadAllText(filePath);
            
            t.Load(InputOutput.GetDrivePath(filePath));
        }


        public void Undo()
        {
            t.Undo();
        }

        public void Redo()
        {
            t.Redo();
        }

        public TextEditor Editor
        {
            get
            {
                return t;
            }
        }

        public override string Text
        {
            get
            {
                return t.Text;
            }
            set
            {
                t.Text = value;
                t.UpdateLayout();

                UpdateBars(t);
            }
        }

        public void Save()
        {
            t.Save(filePath);

            lastSaveText = t.Text;
        }

        public void Paste()
        {
            t.Paste();
        }

        public bool ShowLineNumbers
        {
            get
            {
                return t.ShowLineNumbers;
            }
            set
            {
                t.ShowLineNumbers = value;
            }
        }

        public ICSharpCode.AvalonEdit.Highlighting.IHighlightingDefinition SyntaxHighlighting
        {
            get
            {
                return t.SyntaxHighlighting;
            }
            set
            {
                t.SyntaxHighlighting = value;
            }
        }

        public EditPad()
        {
            InitializeComponent();
            InitializeEditPad();

            padType = PadType.NotFile;
        }

        public EditPad(PadType type = PadType.NotFile)
        {
            InitializeComponent();
            InitializeEditPad();

            padType = type;
        }

        public EditPad(PadType type, Project parent)
        {
            InitializeComponent();
            InitializeEditPad();

            padType = type;

            parentProject = parent;

            //t.KeyDown += t_KeyDown;
        }

        void InitializeEditPad()
        {
            IHighlightingDefinition customHighlighting;
            using (Stream s = typeof(Forms.MainForm).Assembly.GetManifestResourceStream("GBA_Studio.Resources.Syntax_Highlighting.cpp.xshd"))
            {
                if (s == null)
                    throw new InvalidOperationException("Could not find embedded resource");
                using (XmlReader reader = new XmlTextReader(s))
                {
                    customHighlighting = ICSharpCode.AvalonEdit.Highlighting.Xshd.
                        HighlightingLoader.Load(reader, HighlightingManager.Instance);
                }
            }
            HighlightingManager.Instance.RegisterHighlighting("CustomCPP", new string[] { ".gc", ".gs", ".gh" }, customHighlighting);

            t.SyntaxHighlighting = HighlightingManager.Instance.GetDefinition("CustomCPP");

            t.FontFamily = new System.Windows.Media.FontFamily("Lucida Console");
            t.Background = background;
            t.Foreground = foreground;
            t.VerticalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Hidden;
            t.HorizontalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Hidden;
            t.ShowLineNumbers = true;
            t.TextChanged += t_TextChanged;
            t.PreviewKeyDown += t_PreviewKeyDown;
            
            t.Loaded += t_Loaded;
            t.DocumentChanged += t_DocumentChanged;

            elementHost1.Child = t;
        }

        void t_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            UpdateBars((TextEditor)sender);
        }

        void t_DocumentChanged(object sender, EventArgs e)
        {
            UpdateBars((TextEditor)sender);
        }

        void t_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            char key = GetCharFromKey(e.Key);

            UpdateBars((TextEditor)sender);
            if (KeyCharDown != null)
            {
                bool cancel = false;
                KeyCharDown(sender, new KeyCharEventArgs(key), out cancel);

                if (cancel)
                {
                    e.Handled = true;
                }
            }
        }

        void t_TextChanged(object sender, EventArgs e)
        {
            if (TextChanged != null)
            {
                TextChanged(this, e);
            }

            UpdateBars((TextEditor)sender);
        }

        public void GotoLine(int lineNumber)
        {
            t.ScrollToLine(lineNumber);
        }

        public void CaretColumn(int columnNumber)
        {
            t.TextArea.Caret.Column = columnNumber;
        }

        public void CaretLine(int lineNumber)
        {
            t.TextArea.Caret.Line = lineNumber;
        }

        public void UpdateBars(TextEditor sender)
        {
            scrollBarH.Update((float)sender.ActualWidth, (float)sender.ExtentWidth, (float)sender.HorizontalOffset);
            scrollBarV.Update((float)sender.ActualHeight, (float)sender.ExtentHeight, (float)sender.VerticalOffset);
        }

        private void EditPad_Resize(object sender, EventArgs e)
        {
            elementHost1.Width = this.Width - 17;
            elementHost1.Height = this.Height - 17;

            scrollBarH.Width = this.Width - 17;
            scrollBarV.Height = this.Height - 17;

            scrollBarH.Top = this.Height - 17;
            scrollBarV.Left = this.Width - 17;
        }
        float minVal = 1.0f;
        float maxVal = -1.0f;
        private void scrollBarV_Scrolling(object sender, EventArgs e)
        {
            ScrollBar b = (ScrollBar)sender;
            float trackSize = b.Height - b.BarSize - 34.0f;
            float perc = (b.Position - 17.0f) / trackSize;

            t.ScrollToVerticalOffset((t.ExtentHeight * perc));

            OnScroll(new ScrollEventArgs(ScrollEventType.First, ((int)t.ExtentHeight * (int)perc)));
        }

        [DllImport("user32.dll")]
        private static extern short GetAsyncKeyState(int vKey);

        private double prevHeight = 0;
        private double prevWidth = 0;

        private System.Windows.Vector prevScrollOffset = new System.Windows.Vector();

        private void updateTimer_Tick(object sender, EventArgs e)
        {
            if (!scrollBarV.Holding)
            {
                scrollBarV.Position = (float)(t.VerticalOffset / t.ExtentHeight * (scrollBarV.Height - 34.0f)) + 17.0f;

                if (prevScrollOffset != t.TextArea.TextView.ScrollOffset)
                {
                    prevScrollOffset = t.TextArea.TextView.ScrollOffset;

                    OnScroll(new ScrollEventArgs(ScrollEventType.Last, GetCaretPosition().X));
                }

                if (scrollBarV.Position > t.ActualHeight - scrollBarV.BarSize - 17)
                {
                    scrollBarV.Position = (float)t.ActualHeight - scrollBarV.BarSize - 17.0f;
                }

                scrollBarV.Update();
            }

            if (t != null)
            {
                if (t.ExtentWidth != prevWidth || t.ExtentHeight != prevHeight)
                {
                    prevHeight = t.ExtentHeight;
                    prevWidth = t.ExtentWidth;

                    UpdateBars(t);
                }
            }

            short ctrlState = GetAsyncKeyState(0x11);
            short cState = GetAsyncKeyState(0x43);
            short tabState = GetAsyncKeyState(0x09);

            if (ctrlState == 0)
            {
                ctrlPressed = false;
                if (FinishedCopy != null)
                {
                    FinishedCopy(this, EventArgs.Empty);
                }
            }
            else
            {
                ctrlPressed = true;
            }
            if (cState == 0)
            {
                cPressed = false;
                if (FinishedCopy != null)
                {
                    FinishedCopy(this, EventArgs.Empty);
                }
            }
            else
            {
                cPressed = true;
            }
            if (tabState == 0)
            {
                tabPressed = false;
            }
            else
            {
                tabPressed = true;
            }


            if (ctrlPressed && cPressed && padType == PadType.File)
            {
                if (CopiedText != null)
                {
                    CopiedText(this, EventArgs.Empty);
                }
            }
            else if (tabPressed && ctrlPressed)
            {
                if (CtrlTab != null)
                {
                    CtrlTab(this, EventArgs.Empty);
                }
            }
        }

        private void scrollBarH_Scrolling(object sender, EventArgs e)
        {
            ScrollBar b = (ScrollBar)sender;
            float trackSize = b.Width - b.BarSize - 34.0f;
            float perc = (b.Position - 17.0f) / trackSize;

            if (perc < minVal)
            {
                minVal = perc;
            }

            if (perc > maxVal)
            {
                maxVal = perc;
            }

            t.ScrollToHorizontalOffset((t.ExtentWidth * perc));
        }

        public int TextLength
        {
            get
            {
                return t.Text.Length;
            }
        }

        public void ScrollToBottom()
        {
            t.ScrollToEnd();
        }
        public void SelectText(int index, int length)
        {
            t.Select(index, length);
        }
        public int CurrentCaretPosition()
        {
            return t.CaretOffset;
        }
        public int GetLine()
        {
            int caretOffset = t.CaretOffset;
            for(int i = 0; i < t.LineCount; i++)
            {
                
            }
            t.UpdateLayout();
            return 0;
        }
        public void Replace(string replace)
        {
            int index = t.SelectionStart;
            int length = t.SelectionLength;
            t.Text = t.Text.Remove(index, length).Insert(index, replace);
        }
        public void ScrollToSelection()
        {
            int lineNumber = t.TextArea.Caret.Line;
            int lineColumn = t.TextArea.Caret.Column;
            t.ScrollTo(lineNumber, lineColumn);
        }

        public Point GetCaretPosition()
        {
            System.Windows.Point p = t.TextArea.TextView.GetVisualPosition(t.TextArea.Caret.Position, ICSharpCode.AvalonEdit.Rendering.VisualYPosition.TextBottom) - t.TextArea.TextView.ScrollOffset;
            return new Point((int)p.X + 22, (int)p.Y + 22);
        }
    }
}
