﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GBA_Studio.Properties;
using GBA_Studio.IO;

namespace GBA_Studio.Controls
{
    public partial class ScrollBar : UserControl
    {
        private enum Element
        {
            Background,
            ArrowUp,
            ArrowDown,
            ArrowLeft,
            ArrowRight,
            VScroll,
            VScrollHovered,
            HScroll,
            HScrollHovered
        }

        Bitmap[] elementImages;

        private float originalPosition;
        private Point original;
        private bool holding;
        private bool hovering;
        private Color foreColorHover;

        public bool Holding
        {
            get
            {
                return holding;
            }
        }

        public float BarSize
        {
            get
            {
                return barSize;
            }
            set
            {
                barSize = value;
                Draw();
            }
        }
        public float Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;

                Draw();
            }
        }
        private float barSize;
        private float position;
        private Orientation barOrientation = Orientation.Horizontal;

        public ScrollBar()
        {
            barOrientation = Orientation.Vertical;
            InitializeComponent();
            LoadResources();
            Draw();
        }

        private void LoadResources()
        {
            ParseDescription desc;
            desc.direction = ParseDirection.Horizontal;
            desc.sourceImg = Resources.Scrollbar;
            desc.tileStridePixelsH = 17;
            desc.tileStridePixelsV = 17;
            desc.transparentColor = Color.White;

            GraphicParser.Parse(desc, out elementImages);

        }

        private void Draw()
        {
            if (this.Width > 0 && this.Height > 0)
            {
                Bitmap scroll = new Bitmap(this.Width, this.Height);
                Graphics gfx = Graphics.FromImage(scroll);

                switch (barOrientation)
                {
                    case Orientation.Horizontal:
                        gfx.DrawImage(elementImages[(int)Element.ArrowLeft], 0, 0, 17, 17);
                        gfx.DrawImage(elementImages[(int)Element.ArrowRight], this.Width - 17, 0, 17, 17);

                        gfx.FillRectangle(new SolidBrush(hovering || holding ? foreColorHover : this.ForeColor), Position, 4, BarSize, 9);

                        break;
                    case Orientation.Vertical:
                        gfx.DrawImage(elementImages[(int)Element.ArrowUp], 0, 0, 17, 17);
                        gfx.DrawImage(elementImages[(int)Element.ArrowDown], 0, this.Height - 17, 17, 17);

                        gfx.FillRectangle(new SolidBrush(hovering || holding ? foreColorHover : this.ForeColor), 4, Position, 9, BarSize);

                        break;
                }
                pictureBox1.Image = scroll;
            }
        }

        public void Update(float lineCapacity, float lineCount, float lineOn)
        {
            if (lineCount > lineCapacity)
            {
                float barSizePercentage = lineCapacity / lineCount;
                BarSize = ((barOrientation == Orientation.Horizontal ? this.Width : this.Height) - 34.0f) * barSizePercentage;
                Position = (((barOrientation == Orientation.Horizontal ? this.Width : this.Height) - 34.0f) * (lineOn / lineCount)) + 17;

                if (position + BarSize > lineCapacity - 17)
                {
                    position = lineCapacity - 17 - BarSize;
                }
            }
            Draw();
        }


        #region Properties

        public Orientation BarOrientation
        {
            get
            {
                return barOrientation;
            }
            set
            {
                barOrientation = value;
            }
        }

        public Color ForeColorHover
        {
            get
            {
                return foreColorHover;
            }
            set
            {
                foreColorHover = value;
            }
        }
        #endregion

        #region Events

        private void ScrollBar_Resize(object sender, EventArgs e)
        {
            Draw();
        }

        private void ScrollBar_MouseMove(object sender, MouseEventArgs e)
        {
            if (barOrientation == Orientation.Horizontal)
            {
                if (e.X > Position && e.X < Position + BarSize && e.Y > 4 && e.Y < 13)
                {
                    hovering = true;
                }
                else
                {
                    hovering = false;
                }
                if (holding)
                {
                    Scrolling(this, null);
                    int offset = e.X - original.X;
                    Position = originalPosition + offset;
                    Console.WriteLine(offset.ToString());
                    if (Position > this.Width - 17.0f - BarSize)
                    {
                        Position = this.Width - 17.0f - BarSize;
                    }
                    else if (Position < 17)
                    {
                        Position = 17;
                    }
                }
            }
            else
            {
                if (e.Y > Position && e.Y < Position + BarSize && e.X > 4 && e.X < 13)
                {
                    hovering = true;
                }
                else
                {
                    hovering = false;
                }
                if (holding)
                {
                    Scrolling(this, null);
                    int offset = e.Y - original.Y;
                    Position = originalPosition + offset;
                    Console.WriteLine(offset.ToString());
                    if (Position > this.Height - 17.0f - BarSize)
                    {
                        Position = this.Height - 17.0f - BarSize;
                    }
                    else if (Position < 17)
                    {
                        Position = 17;
                    }
                }
            }
            Draw();
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (hovering)
            {
                originalPosition = Position;
                holding = true;
                original = new Point(e.X, e.Y);
            }
            if (barOrientation == Orientation.Horizontal)
            {
                if (e.X < 17)
                {
                    Position -= this.Width * 0.01f;
                    if (Position < 17)
                    {
                        Position = 17;
                    }
                }
                else if (e.X > this.Width - 17)
                {
                    Position += this.Width * 0.01f;
                    if (Position > this.Width - BarSize - 17)
                    {
                        Position = this.Width - BarSize - 17;
                    }
                }
            }
            else
            {
                if (e.Y < 17)
                {
                    Position -= this.Height * 0.01f;
                    if (Position < 17)
                    {
                        Position = 17;
                    }
                }
                else if (e.Y > this.Height - 17)
                {
                    Position += this.Height * 0.01f;
                    if (Position > this.Height - BarSize - 17)
                    {
                        Position = this.Height - BarSize - 17;
                    }
                }
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            holding = false;
            Draw();
        }

        public event EventHandler Scrolling;
        #endregion


    }
}
