﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

using GBA_Studio.Properties;

namespace GBA_Studio.Controls
{
    public partial class ListBoxPro : UserControl
    {
        #region Enumerations
        private enum SCROLL_ELEMENT : int
        {
            ARROW_UP = 0,
            ARROW_DOWN = 1,
            ARROW_HOVER_UP = 2,
            ARROW_HOVER_DOWN = 3,
            ARROW_PRESS_UP = 4,
            ARROW_PRESS_DOWN = 5
        }

        enum REGION
        {
            REGION_ENTRY = 0,
            REGION_SCROLL_BUTTON_TOP = 1,
            REGION_SCROLL_BUTTON_BOTTOM = 2,
            REGION_SCROLL_BAR = 3,
            REGION_SCROLL_BAR_BACKGROUND = 4
        }
        #endregion

        #region Variables
        public bool CaseSensitive { get; set; }

        public List<Tuple<Object, string>> Items
        {
            get
            {
                return entries;
            }
        }

        private List<Tuple<Object, string>> entries { get; set; }
        private List<RectangleF> entryRectangles;
        private int ControlWidth { get; set; }
        private Graphics ControlGraphics { get; set; }  //Used for string measurement.

        //Brushes
        private Brush brushForeColor;
        private Brush brushBackColor;
        private Brush brushSelectedColor;

        private Brush brushScrollBarDefaultColor;
        private Brush brushScrollBarHoveredColor;
        private Brush brushScrollBarClickedColor;

        //Pens
        private Pen penBorderColor;

        //Scrolling Variables
        private Image[] scrollImages = new Image[6];
        private RectangleF scrollBarAreaRect;
        private RectangleF scrollBarRect;
        private bool isScrolling = false;           //Determines whether user is scrolling
        private float initialScrollOffset = 0;        //Initial offset, used for storage
        private int initialX = 0;                   //Y Location the user clicked
        #endregion

        #region Properties
        //Colour Properties
        public Color SelectedColor { get; set; }
        public Color BorderColor { get; set; }
        public Color BrushScrollBarDefaultColor { get; set; }
        public Color BrushScrollBarHoveredColor { get; set; }
        public Color BrushScrollBarClickedColor { get; set; }

        //Scrolling Properties
        private float ScrollableArea { get; set; }
        private float ScrollOffset { get; set; }
        private float ScrollBarWidth { get; set; }
        private float ScrollBarHeight { get; set; }
        private int EntryHeight { get; set; }
        private int EntryDisplayCount { get; set; }
        private bool ScrollBarVisible { get; set; }

        private int ListScrollOffset { get; set; }

        //Border Variables
        private int BorderWidth { get; set; }

        //Selection Variables
        public int SelectedIndex { get; set; }

        #endregion

        #region Events

        public new event MouseEventHandler DoubleClick;

        #endregion

        #region Initialization
        public ListBoxPro()
        {
            InitializeComponent();

            InitializeProperties();
            PopulateScrollBar();
            PopulateEntryRectangles();
            UpdateControl();
            UpdateScrollBar();
        }

        private void InitializeProperties()
        {
            entryRectangles = new List<RectangleF>();
            ControlGraphics = Graphics.FromImage(new Bitmap(10, 10));
            entries = new List<Tuple<object, string>>();

            //Initialize Properties
            BackColor = Color.FromArgb(37, 37, 38);
            ForeColor = Color.White;
            SelectedColor = Color.FromArgb(0, 122, 204);
            BorderColor = Color.FromArgb(45, 45, 48);
            BrushScrollBarDefaultColor = Color.FromArgb(104, 104, 104);
            BrushScrollBarHoveredColor = Color.FromArgb(158, 158, 158);
            BrushScrollBarClickedColor = Color.FromArgb(239, 235, 239);

            FormBrush(BackColor, ref brushBackColor);
            FormBrush(ForeColor, ref brushForeColor);
            FormBrush(SelectedColor, ref brushSelectedColor);
            FormPen(BorderColor, ref penBorderColor);

            FormBrush(BrushScrollBarDefaultColor, ref brushScrollBarDefaultColor);
            FormBrush(BrushScrollBarHoveredColor, ref brushScrollBarHoveredColor);
            FormBrush(BrushScrollBarClickedColor, ref brushScrollBarClickedColor);

            EntryHeight = 19;
            ScrollOffset = 0;
            ScrollBarWidth = 17;
            BorderWidth = 1;
            EntryDisplayCount = 5;
            SelectedIndex = 0;
            CaseSensitive = true;
        }

        private void PopulateScrollBar()
        {
            Bitmap source = Resources.smartSense_Scroll;
            for (int i = 0; i < 6; i++)
            {
                Bitmap b = new Bitmap(17, 17);
                Graphics g = Graphics.FromImage(b);

                Rectangle sourceRect = new Rectangle(i * 17, 0, 17, 17);
                Rectangle destRect = new Rectangle(0, 0, 17, 17);
                g.DrawImage(source, destRect, sourceRect, GraphicsUnit.Pixel);
                scrollImages[i] = b;
            }
        }

        public void Clear()
        {
            entries.Clear();

            PopulateScrollBar();
            PopulateEntryRectangles();
            UpdateControl();
            UpdateScrollBar();
        }

        public void AddItem(Object tag, string item)
        {
            entries.Add(new Tuple<Object, string>(tag, item));

            PopulateScrollBar();
            PopulateEntryRectangles();
            UpdateControl();
            UpdateScrollBar();
        }

        private void PopulateEntryRectangles()
        {
            for (int i = 0; i < entries.Count; i++)
            {
                entryRectangles.Add(GetEntryRect(i));
            }
        }
        #endregion

        public void GetScrollBarRect(ref RectangleF rect)
        {
            rect = new RectangleF(this.Width - ScrollBarWidth - BorderWidth, BorderWidth, ScrollBarWidth, this.Height - (2 * BorderWidth));
        }

        public RectangleF GetEntryRect(int index)
        {
            ListScrollOffset = (int)(GetScrollMultiplier() * EntryHeight * (entries.Count - EntryDisplayCount));

            int offsetY = -ListScrollOffset + (index * EntryHeight);
            RectangleF rect = new RectangleF(BorderWidth, BorderWidth + offsetY, (float)this.Width - (float)(2 * BorderWidth) - (ScrollBarVisible ? ScrollBarWidth : 0), EntryHeight);
            return rect;
        }

        #region Updates
        private void UpdateControl()
        {
            this.Visible = true;
            ListScrollOffset = (int)Clamp(ListScrollOffset, 0, (entries.Count - EntryDisplayCount) * EntryHeight);
            this.Height = ((ScrollBarVisible ? EntryDisplayCount : entries.Count) * EntryHeight) + (BorderWidth * 2);
            this.Width = ControlWidth;
        }

        void UpdateEntryRectangles()
        {
            for (int i = 0; i < entries.Count; i++)
            {
                entryRectangles[i] = GetEntryRect(i);
            }
        }

        void UpdateScrollBar()
        {
            GetScrollBarRect(ref scrollBarAreaRect);

            //17 is the height of the scrollbar button
            ScrollableArea = this.Height - (2 * 17);

            float displayMultiplier = (float)EntryDisplayCount / (float)entries.Count;
            ScrollBarHeight = (int)((float)ScrollableArea * displayMultiplier);

            int barOffsetX = 4;
            float barWidth = ScrollBarWidth - (float)(2 * barOffsetX);
            scrollBarRect = new RectangleF(scrollBarAreaRect.Left + barOffsetX, 17 + ScrollOffset, barWidth, ScrollBarHeight);
        }
        #endregion

        #region Drawing
        protected override void OnPaint(PaintEventArgs e)
        {
            if (this.Visible && entries.Count > 0)
            {
                UpdateControl();
                DrawBackground(e.Graphics);
                DrawEntries(e.Graphics);

                ScrollBarVisible = (this.Height / EntryHeight) < entries.Count;

                if (ScrollBarVisible)
                {
                    DrawScrollBar(e.Graphics);
                }
                DrawOverlay(e.Graphics);
            }
            else
            {
                this.Visible = false;
                //TODO: Find solution for this later. Crashed if you hide it.
            }
        }

        private void DrawBackground(Graphics g)
        {
            g.FillRectangle(brushBackColor, new Rectangle(0, 0, this.Width, this.Height));
        }

        private void DrawScrollBar(Graphics g)
        {
            int imageMultiplerUp = (int)regionStatus[(int)REGION.REGION_SCROLL_BUTTON_TOP];
            int imageMultiplerDown = (int)regionStatus[(int)REGION.REGION_SCROLL_BUTTON_BOTTOM];

            UpdateScrollBar();
            int status = (int)regionStatus[(int)REGION.REGION_SCROLL_BAR];
            Brush drawBrush = (status == 0 ? brushScrollBarDefaultColor : (status == 1 ? brushScrollBarHoveredColor : brushScrollBarClickedColor));

            g.FillRectangle(FormBrush(Color.FromArgb(62, 62, 66)), scrollBarAreaRect);
            g.DrawImage(scrollImages[(int)SCROLL_ELEMENT.ARROW_UP + (2 * imageMultiplerUp)], scrollBarAreaRect.Left, 0, 17, 17);
            g.DrawImage(scrollImages[(int)SCROLL_ELEMENT.ARROW_DOWN + (2 * imageMultiplerDown)], scrollBarAreaRect.Left, scrollBarAreaRect.Bottom - 17, 17, 17);

            g.FillRectangle(drawBrush, scrollBarRect);
        }

        private void DrawEntries(Graphics g)
        {
            int charHeight = (int)Math.Ceiling(Font.SizeInPoints);
            int buffer = 5;
            EntryHeight = charHeight + (2 * buffer);

            bool canScroll = ScrollBarVisible;

            for (int i = 0; i < entries.Count; i++)
            {
                if (canScroll || TestVisible(i))
                {
                    RectangleF entryRect = GetEntryRect(i);
                    if (i == SelectedIndex)
                        g.FillRectangle(brushSelectedColor, entryRect);

                    g.DrawString(entries[i].Item2, this.Font, brushForeColor, new PointF(BorderWidth + 5, entryRect.Top + buffer - 2));
                }
            }
        }

        private void DrawOverlay(Graphics g)
        {
            g.DrawLine(penBorderColor, 0, 0, this.Width - 1, 0);
            g.DrawLine(penBorderColor, this.Width - 1, 0, this.Width - 1, this.Height - 1);
            g.DrawLine(penBorderColor, this.Width - 1, this.Height - 1, 0, this.Height - 1);
            g.DrawLine(penBorderColor, 0, this.Height - 1, 0, 0);
        }

        /// <summary>
        /// Returns whether an entry is currently visible.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private bool TestVisible(int index)
        {
            return (entryRectangles[index].Bottom > 0 && entryRectangles[index].Top < this.Height);
        }

        /// <summary>
        /// Forms a brush from a provided colour.
        /// </summary>
        /// <param name="clr"></param>
        /// <param name="brush"></param>
        public void FormBrush(Color clr, ref Brush brush)
        {
            brush = new SolidBrush(clr);
        }

        /// <summary>
        /// Forms a brush from a provided colour and returns it.
        /// </summary>
        /// <param name="clr"></param>
        /// <returns></returns>
        public Brush FormBrush(Color clr)
        {
            return new SolidBrush(clr);
        }

        /// <summary>
        /// Forms a pen from a provided colour.
        /// </summary>
        /// <param name="clr"></param>
        /// <param name="pen"></param>
        public void FormPen(Color clr, ref Pen pen)
        {
            pen = new Pen(clr);
        }
        #endregion

        #region Control Interaction

        /// <summary>
        /// Returns a percentage representative of how far the scroll bar is scrolled down.
        /// </summary>
        /// <returns></returns>
        private float GetScrollMultiplier()
        {
            if (ScrollBarVisible)
            {
                return ((float)ScrollOffset / (float)(ScrollableArea - ScrollBarHeight));
            }
            return 0;
        }

        private void CodeCompletion_MouseDown(object sender, MouseEventArgs e)
        {
            REGION clickedRegion = GetClickedRegion(e.Location);
            ChangeFocus(clickedRegion, REGION_STATUS.REGION_STATUS_CLICK);

            switch (clickedRegion)
            {
                case REGION.REGION_ENTRY:
                    break;
                case REGION.REGION_SCROLL_BAR:
                    initialScrollOffset = (int)ScrollOffset;
                    initialX = e.Y - 17;
                    isScrolling = true;
                    break;
                case REGION.REGION_SCROLL_BAR_BACKGROUND:
                    break;
                case REGION.REGION_SCROLL_BUTTON_BOTTOM:
                    ScrollDown();
                    break;
                case REGION.REGION_SCROLL_BUTTON_TOP:
                    ScrollUp();
                    break;
            }
            this.Invalidate();
        }
        private void ListBoxPro_DoubleClick(object sender, MouseEventArgs e)
        {
            REGION clickedRegion = GetClickedRegion(e.Location);
            ChangeFocus(clickedRegion, REGION_STATUS.REGION_STATUS_CLICK);

            switch (clickedRegion)
            {
                case REGION.REGION_ENTRY:
                    if (DoubleClick != null)
                    {
                        DoubleClick(sender, e);
                    }
                    break;
                case REGION.REGION_SCROLL_BAR:
                    initialScrollOffset = (int)ScrollOffset;
                    initialX = e.Y - 17;
                    isScrolling = true;
                    break;
                case REGION.REGION_SCROLL_BAR_BACKGROUND:
                    break;
                case REGION.REGION_SCROLL_BUTTON_BOTTOM:
                    ScrollDown();
                    break;
                case REGION.REGION_SCROLL_BUTTON_TOP:
                    ScrollUp();
                    break;
            }
            this.Invalidate();
        }

        private void CodeCompletion_MouseUp(object sender, MouseEventArgs e)
        {
            //Just incase I have any region specific code for later
            REGION clickedRegion = GetClickedRegion(e.Location);
            ChangeFocus(clickedRegion, REGION_STATUS.REGION_STATUS_NONE);

            switch (clickedRegion)
            {
                case REGION.REGION_ENTRY:
                    SelectedIndex = GetClickedIndex(e.Location);
                    break;
                case REGION.REGION_SCROLL_BAR:

                    break;
                case REGION.REGION_SCROLL_BAR_BACKGROUND:
                    break;
                case REGION.REGION_SCROLL_BUTTON_BOTTOM:
                    break;
                case REGION.REGION_SCROLL_BUTTON_TOP:
                    break;
            }

            UpdateEntryRectangles();
            this.Invalidate();
            isScrolling = false;
        }

        REGION_STATUS[] regionStatus = new REGION_STATUS[5];

        enum REGION_STATUS
        {
            REGION_STATUS_NONE = 0,
            REGION_STATUS_HOVER = 1,
            REGION_STATUS_CLICK = 2
        }

        private void ChangeFocus(REGION region, REGION_STATUS status)
        {
            if (regionStatus[(int)region] != status)
            {
                for (int i = 0; i < 5; i++)
                {
                    if (i == (int)region)
                    {
                        regionStatus[(int)region] = status;
                    }
                    else
                    {
                        regionStatus[i] = REGION_STATUS.REGION_STATUS_NONE;
                    }
                }
                this.Invalidate();
            }
        }
        private void CodeCompletion_MouseMove(object sender, MouseEventArgs e)
        {
            REGION hoverRegion = GetClickedRegion(e.Location);
            ChangeFocus(hoverRegion, REGION_STATUS.REGION_STATUS_HOVER);

            if (isScrolling)
            {
                int newOffset = (int)Clamp((initialScrollOffset + (e.Y - 17 - initialX)), 0, ScrollableArea - ScrollBarHeight);
                if (newOffset != ScrollOffset)
                {
                    //It has moved, update the locations
                    UpdateEntryRectangles();
                    UpdateScrollBar();
                    ScrollOffset = newOffset;
                }
                if (scrollBarRect.Top == 58)
                {
                    int c = 12;
                }
                this.Invalidate();
            }
        }

        /// <summary>
        /// Returns an enumerator representing the region that was clicked.
        /// </summary>
        /// <param name="click"></param>
        /// <returns></returns>
        private REGION GetClickedRegion(Point click)
        {
            if (Contains(click, BorderWidth, BorderWidth, this.Width - ScrollBarWidth - (2 * BorderWidth), this.Height - (2 * BorderWidth)))
            {
                //Entry Area
                return REGION.REGION_ENTRY;
            }
            else if (Contains(click, this.Width - BorderWidth - ScrollBarWidth, BorderWidth, ScrollBarWidth, 17))
            {
                //Scroll Bar Up Button
                return REGION.REGION_SCROLL_BUTTON_TOP;
            }
            else if (Contains(click, this.Width - BorderWidth - ScrollBarWidth, this.Height - BorderWidth - 17, ScrollBarWidth, 17))
            {
                //Scroll Bar Down Button
                return REGION.REGION_SCROLL_BUTTON_BOTTOM;
            }
            else if (Contains(click, scrollBarRect.X, scrollBarRect.Y, scrollBarRect.Width, scrollBarRect.Height))
            {
                //Scroll Bar
                return REGION.REGION_SCROLL_BAR;
            }
            else
            {
                //Scroll Bar Background
                return REGION.REGION_SCROLL_BAR_BACKGROUND;
            }
        }

        /// <summary>
        /// Returns the index of the item that was clicked.
        /// </summary>
        /// <param name="click"></param>
        /// <returns></returns>
        private int GetClickedIndex(Point click)
        {
            // Fix bug where index would return 0 for the first few rows
            if (entries.Count * EntryHeight <= this.Height)
            {
                ListScrollOffset = 0;
            }
            
            //Rather than iterating a testing for intersection, direct calculation is faster.
            int clickIndex = (ListScrollOffset + (click.Y - 1)) / EntryHeight;



            return clickIndex;
        }
        #endregion

        #region Miscellaneous
        /// <summary>
        /// Indicates whether a point is contained within a specified region.
        /// </summary>
        /// <param name="click"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        private bool Contains(Point click, float x, float y, float width, float height)
        {
            return (click.X > x && click.X < x + width) && (click.Y > y && click.Y < y + height);
        }

        /// <summary>
        /// Indicates whether a point is contained within a rectangle.
        /// </summary>
        /// <param name="click"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        private bool Contains(Point click, Rectangle rect)
        {
            return (click.X > rect.Left && click.X < rect.Right) && (click.Y > rect.Top && click.Y < rect.Bottom);
        }

        /// <summary>
        /// Clamps a value between a minimum and a maximum.
        /// </summary>
        /// <param name="val"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        private float Clamp(float val, float min, float max)
        {
            return (val < min ? min : (val > max ? max : val));
        }
        #endregion


        private void ScrollUp()
        {
            float currentLocalPosition = scrollBarRect.Top - 17;

            float increment = (float)ScrollableArea / (float)entries.Count;
            ScrollOffset = (int)Clamp(ScrollOffset - increment, 0, ScrollableArea - scrollBarRect.Height);
        }

        private void ScrollDown()
        {
            float currentLocalPosition = scrollBarRect.Top - 17;

            float increment = (float)ScrollableArea / (float)entries.Count;
            ScrollOffset = (int)Clamp(ScrollOffset + increment, 0, ScrollableArea - scrollBarRect.Height);
        }
        
        private void UpdateControlWidth(float potential)
        {
            if (potential > ControlWidth)
                ControlWidth = (int)potential;
        }

        private void ResetScroll()
        {
            ScrollOffset = 0;
            SelectedIndex = -1;
            ListScrollOffset = 0;
        }

        private void CodeCompletion_KeyDown(object sender, KeyEventArgs e)
        {
            ProcessKeyInput(e.KeyCode);
        }

        void ProcessKeyInput(Keys key)
        {
            switch (key)
            {
                case Keys.Down:
                    ScrollDown();
                    break;
                case Keys.Up:
                    ScrollDown();
                    break;
            }
        }
    }
}
