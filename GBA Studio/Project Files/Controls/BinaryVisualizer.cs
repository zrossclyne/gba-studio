﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

namespace GBA_Studio.Controls
{
    public partial class BinaryVisualizer : UserControl
    {
        private List<TextBox> digits;

        public BitArray BinaryValue
        {
            get
            {
                BitArray rtn = new BitArray(8);
                
                int i = 0;
                foreach (TextBox t in digits)
                {
                    int tmpVal = 0;

                    if (!int.TryParse(t.Text, out tmpVal))
                    {
                        tmpVal = 0;
                    }

                    if (tmpVal > 0)
                    {
                        tmpVal = 1;
                    }

                    rtn.Set(i, tmpVal == 0 ? false : true);

                    i++;
                }
                
                return rtn;
            }
        }

        public string BinaryName
        {
            get
            {
                return binaryName.Text;
            }
            set
            {
                binaryName.Text = value;
            }
        }

        public void SetValue(BitArray input)
        {
            for (int i = 0; i < input.Length; i++)
            {
                if (i < 8)
                {
                    digits[i].Text = input[i] ? "1" : "0";
                }
            }
        }

        public BinaryVisualizer()
        {
            InitializeComponent();

            digits = new List<TextBox>();

            digits.Add(bit1);
            digits.Add(bit2);
            digits.Add(bit4);
            digits.Add(bit8);
            digits.Add(bit16);
            digits.Add(bit32);
            digits.Add(bit64);
            digits.Add(bit128);
        }

        private void bit_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void bit_KeyUp(object sender, KeyEventArgs e)
        {
            if((char)e.KeyCode == '0' || (char)e.KeyCode == '1')
            {
                TextBox obj = (TextBox)sender;
                obj.Text = char.ToString((char)e.KeyCode);
                if(obj.TabIndex < 7)
                {
                    digits[6 - obj.TabIndex].Focus();
                }
            }
        }
    }
}
