﻿namespace GBA_Studio.Controls
{
    partial class ClipboardInterface
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.itemList1 = new Controls.ItemList();
            this.clipboardTextDisplay = new Controls.EditPad();
            this.SuspendLayout();
            // 
            // itemList1
            // 
            this.itemList1.BackColor = System.Drawing.Color.Transparent;
            this.itemList1.ButtonBorderColorDefault = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.itemList1.ButtonBorderColorHovered = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(64)))));
            this.itemList1.ButtonBorderColorPressed = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.itemList1.ButtonBorderColorSelected = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(160)))));
            this.itemList1.ButtonColorDefault = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.itemList1.ButtonColorHovered = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.itemList1.ButtonColorPressed = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.itemList1.ButtonColorSelected = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.itemList1.ControlItemGap = 20;
            this.itemList1.ControlItemWidth = 75;
            this.itemList1.ControlMarginLeft = 10;
            this.itemList1.ControlMarginTop = 10;
            this.itemList1.Location = new System.Drawing.Point(0, 0);
            this.itemList1.Name = "itemList1";
            this.itemList1.Size = new System.Drawing.Size(723, 96);
            this.itemList1.TabIndex = 0;
            // 
            // clipboardTextDisplay
            // 
            this.clipboardTextDisplay.Location = new System.Drawing.Point(0, 102);
            this.clipboardTextDisplay.Name = "clipboardTextDisplay";
            this.clipboardTextDisplay.ShowLineNumbers = false;
            this.clipboardTextDisplay.Size = new System.Drawing.Size(723, 306);
            this.clipboardTextDisplay.SyntaxHighlighting = null;
            this.clipboardTextDisplay.TabIndex = 1;
            // 
            // ClipboardInterface
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(42)))), ((int)(((byte)(45)))));
            this.ClientSize = new System.Drawing.Size(723, 408);
            this.Controls.Add(this.clipboardTextDisplay);
            this.Controls.Add(this.itemList1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "ClipboardInterface";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ClipboardInterface_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClipboardInterface_KeyUp);
            this.ResumeLayout(false);

        }

        #endregion

        private ItemList itemList1;
        private EditPad clipboardTextDisplay;

    }
}
