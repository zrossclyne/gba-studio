﻿namespace GBA_Studio.Controls
{
    partial class EditPad
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.updateTimer = new System.Windows.Forms.Timer(this.components);
            this.scrollBarH = new Controls.ScrollBar();
            this.scrollBarV = new Controls.ScrollBar();
            this.SuspendLayout();
            // 
            // elementHost1
            // 
            this.elementHost1.Location = new System.Drawing.Point(0, 0);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(330, 330);
            this.elementHost1.TabIndex = 0;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = null;
            // 
            // updateTimer
            // 
            this.updateTimer.Enabled = true;
            this.updateTimer.Interval = 1;
            this.updateTimer.Tick += new System.EventHandler(this.updateTimer_Tick);
            // 
            // scrollBarH
            // 
            this.scrollBarH.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.scrollBarH.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.scrollBarH.BarOrientation = System.Windows.Forms.Orientation.Horizontal;
            this.scrollBarH.BarSize = 0F;
            this.scrollBarH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(153)))), ((int)(((byte)(153)))));
            this.scrollBarH.ForeColorHover = System.Drawing.Color.White;
            this.scrollBarH.Location = new System.Drawing.Point(0, 330);
            this.scrollBarH.Name = "scrollBarH";
            this.scrollBarH.Position = 17F;
            this.scrollBarH.Size = new System.Drawing.Size(330, 17);
            this.scrollBarH.TabIndex = 2;
            this.scrollBarH.Scrolling += new System.EventHandler(this.scrollBarH_Scrolling);
            // 
            // scrollBarV
            // 
            this.scrollBarV.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(62)))), ((int)(((byte)(66)))));
            this.scrollBarV.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.scrollBarV.BarOrientation = System.Windows.Forms.Orientation.Vertical;
            this.scrollBarV.BarSize = 0F;
            this.scrollBarV.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(153)))), ((int)(((byte)(153)))));
            this.scrollBarV.ForeColorHover = System.Drawing.Color.White;
            this.scrollBarV.Location = new System.Drawing.Point(330, 0);
            this.scrollBarV.Name = "scrollBarV";
            this.scrollBarV.Position = 17F;
            this.scrollBarV.Size = new System.Drawing.Size(17, 330);
            this.scrollBarV.TabIndex = 1;
            this.scrollBarV.Scrolling += new System.EventHandler(this.scrollBarV_Scrolling);
            // 
            // EditPad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.scrollBarH);
            this.Controls.Add(this.scrollBarV);
            this.Controls.Add(this.elementHost1);
            this.Name = "EditPad";
            this.Size = new System.Drawing.Size(347, 347);
            this.Resize += new System.EventHandler(this.EditPad_Resize);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private Controls.ScrollBar scrollBarV;
        private Controls.ScrollBar scrollBarH;
        private System.Windows.Forms.Timer updateTimer;
    }
}
