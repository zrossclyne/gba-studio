﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GBA_Studio.Controls
{
    public partial class ItemListInterface : UserControl
    {
        public ItemListInterface()
        {
            InitializeComponent();
        }

        static string defaultString = "Choose an item to create.";
        string currentString = defaultString;

        private void itemList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            splitContainer2.Panel2Collapsed = false;
            textBox2.Focus();
        }

        private void itemList1_HoveredIndexChanged(object sender, EventArgs e)
        {
            currentString = FormDescription(itemList1.HoverIndex);
            UpdateControl();
        }
        
        private void itemList1_HoveredIndexLost(object sender, EventArgs e)
        {
            currentString = itemList1.SelectedIndex == -1 ? defaultString : FormDescription(itemList1.SelectedIndex);
            UpdateControl();
        }

        int prevHeight = 0;
        private void UpdateControl(bool force = false)
        {
            itemList1.Invalidate();
            int tempHeight = itemList1.GenerateWantHeight() + splitContainer1.Panel1.Height;
            if(tempHeight > 0 && (prevHeight != tempHeight || force))
            {
                prevHeight = tempHeight;
                splitContainer2.SplitterDistance = prevHeight;
            }

            textBox1.TextAlign = HorizontalAlignment.Center;
            textBox1.Text = currentString;
        }

        #region Properties
        [Category("Control"),
        Description("A collection of items that the control uses."),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)] //Ensures the data is used, and not disregarded upon initialization.
        
        public List<ItemList.ItemData> Items
        {
            get
            {
                return itemList1.ControlItems;
            }
        }

        [Category("Control"),
        Description("A collection of items that the control uses.")]
        public int ControlItemGap
        {
            get
            {
                return itemList1.ControlItemGap;
            }
            set
            {
                itemList1.ControlItemGap = value;
            }
        }

        [Category("Control"),
        Description("A collection of items that the control uses.")]
        public int ControlItemWidth
        {
            get
            {
                return itemList1.ControlItemWidth;
            }
            set
            {
                itemList1.ControlItemWidth = value;
            }
        }

        [Category("Control"),
        Description("A collection of items that the control uses.")]
        public int ControlMarginLeft
        {
            get
            {
                return itemList1.ControlMarginLeft;
            }
            set
            {
                itemList1.ControlMarginLeft = value;
            }
        }

        [Category("Control"),
        Description("A collection of items that the control uses.")]
        public int ControlMarginTop
        {
            get
            {
                return itemList1.ControlMarginTop;
            }
            set
            {
                itemList1.ControlMarginTop = value;
            }
        }

        [Category("Control"),
        Description("A collection of items that the control uses.")]
        public Color ButtonColorDefault
        {
            get
            {
                return itemList1.ButtonColorDefault;
            }
            set
            {
                itemList1.ButtonColorDefault = value;
            }
        }

        [Category("Control"),
        Description("A collection of items that the control uses.")]
        public Color ButtonColorHovered
        {
            get
            {
                return itemList1.ButtonColorHovered;
            }
            set
            {
                itemList1.ButtonColorHovered = value;
            }
        }

        [Category("Control"),
        Description("A collection of items that the control uses.")]
        public Color ButtonColorPressed
        {
            get
            {
                return itemList1.ButtonColorPressed;
            }
            set
            {
                itemList1.ButtonColorPressed = value;
            }
        }

        [Category("Control"),
        Description("A collection of items that the control uses.")]
        public Color ButtonColorSelected
        {
            get
            {
                return itemList1.ButtonColorSelected;
            }
            set
            {
                itemList1.ButtonColorSelected = value;
            }
        }

        [Category("Control"),
        Description("A collection of items that the control uses.")]
        public Color ButtonBorderColorDefault
        {
            get
            {
                return itemList1.ButtonBorderColorDefault;
            }
            set
            {
                itemList1.ButtonBorderColorDefault = value;
            }
        }

        [Category("Control"),
        Description("A collection of items that the control uses.")]
        public Color ButtonBorderColorHovered
        {
            get
            {
                return itemList1.ButtonBorderColorHovered;
            }
            set
            {
                itemList1.ButtonBorderColorHovered = value;
            }
        }

        [Category("Control"),
        Description("A collection of items that the control uses.")]
        public Color ButtonBorderColorPressed
        {
            get
            {
                return itemList1.ButtonBorderColorPressed;
            }
            set
            {
                itemList1.ButtonBorderColorPressed = value;
            }
        }

        [Category("Control"),
        Description("A collection of items that the control uses.")]
        public Color ButtonBorderColorSelected
        {
            get
            {
                return itemList1.ButtonBorderColorSelected;
            }
            set
            {
                itemList1.ButtonBorderColorSelected = value;
            }
        }


        public int SelectedIndex
        {
            get
            {
                return itemList1.SelectedIndex;
            }
        }

        #endregion


        private string FormDescription(int index)
        {
            return itemList1.ControlItems[index].Caption;
        }

        public void Reset()
        {
            itemList1.Reset();
            textBox2.Text = "";
            splitContainer2.Panel2Collapsed = true;
        }

        public void SelectItem(int index)
        {
            itemList1.SelectItem(index);
            if(itemList1.SelectedIndex >= 0)
            {
                splitContainer2.Panel2Collapsed = false;
                textBox2.Focus();
            }
        }

        private void ItemListInterface_Load(object sender, EventArgs e)
        {
            UpdateControl(true);
        }

        private void ItemListInterface_SizeChanged(object sender, EventArgs e)
        {
            UpdateControl();
        }

        private void textBox2_Resize(object sender, EventArgs e)
        {

        }

        private void splitContainer2_Panel2_Resize(object sender, EventArgs e)
        {
            int margin = 10;

            textBox2.Top = 0;
            textBox2.Width = splitContainer2.Panel2.Width - margin;
            textBox2.Left = margin / 2;

            buttonCreateObject.Top = textBox2.Bottom + 3;
            buttonCreateObject.Left = margin / 2;
            buttonCreateObject.Width = textBox2.Width;
        }

        private void buttonCreateObject_ButtonClicked(object sender, EventArgs e)
        {
            if(textBox2.Text.Length > 0)
            {
                ThrowClickEvent();
            }
        }

        public event ItemListInterfaceHandler ButtonClicked;

        public delegate void ItemListInterfaceHandler(object sender, ItemListInterfaceEventArgs e);

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                ThrowClickEvent();
            }
        }

        private void ThrowClickEvent()
        {
                // TODO: NAME VALIDATION AGAINST PRE-EXISTING NAMES
            if (ValidateName())
            {
                ItemListInterfaceEventArgs args = new ItemListInterfaceEventArgs(itemList1.SelectedIndex, textBox2.Text);
                ButtonClicked(this, args);
            }
        }
        private bool ValidateName()
        {
            textBox2.Text = textBox2.Text.Replace("\r\n", "");
            return true;
        }
    }
    public class ItemListInterfaceEventArgs : EventArgs
    {
        private int selectedIndex;
        private string selectedText;

        public ItemListInterfaceEventArgs(int index, string text)
        {
            selectedIndex = index;
            selectedText = text;
        }

        public int SelectedIndex
        {
            get
            {
                return selectedIndex;
            }
        }

        public string SelectedText
        {
            get
            {
                return selectedText;
            }
        }
    }
}
