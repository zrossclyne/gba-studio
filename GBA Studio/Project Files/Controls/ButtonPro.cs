﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GBA_Studio.Controls
{
    public partial class ButtonPro : UserControl, IButtonControl
    {
        #region Variables
        bool isHovered = false;
        bool isPressed = false;

        private string text = "New Button";
        private Color colorButtonHover = Color.FromArgb(84, 84, 92);
        private Color colorButtonPressed = Color.FromArgb(0, 122, 204);
        private Color colorBorderDefault = Color.FromArgb(84, 84, 92);
        private Color colorBorderHover = Color.FromArgb(106, 106, 117);
        private Color colorBorderPressed = Color.FromArgb(28, 151, 234);
        private Color colorFontHover = Color.White;
        private Color colorFontPressed = Color.White;

        DialogResult result;
        #endregion

        #region Constructors
        public ButtonPro()
        {
            InitializeComponent();
        }
        #endregion

        #region Functions
        protected override void OnPaint(PaintEventArgs e)
        {
            Draw(e.Graphics);
        }

        private void Draw(Graphics g)
        {
            Brush brushBack = new SolidBrush(isPressed ? colorButtonPressed : (isHovered ? colorButtonHover : BackColor));
            Pen brushBorder = new Pen(isPressed ? colorBorderPressed : (isHovered ? colorBorderHover : colorBorderDefault));
            Brush brushFont = new SolidBrush(isPressed ? colorFontPressed : (isHovered ? colorFontHover : ForeColor));

            g.FillRectangle(brushBack, 0, 0, this.Width - 1, this.Height - 1);
            g.DrawRectangle(brushBorder, 0, 0, this.Width - 1, this.Height - 1);

            if (BackgroundImage != null)
            {
                int imgOffsetX = (this.Width - BackgroundImage.Width) / 2;
                int imgOffsetY = (this.Height - BackgroundImage.Height) / 2;

                g.DrawImage(BackgroundImage, new Rectangle(imgOffsetX < 0 ? 0 : imgOffsetX, imgOffsetY < 0 ? 0 : imgOffsetY, imgOffsetX < 0 ? this.Width : BackgroundImage.Width, imgOffsetY < 0 ? this.Height : BackgroundImage.Height));
            }
            SizeF stringLength = g.MeasureString(text, this.Font);

            float offsetX = (float)(this.Width - stringLength.Width) / 2.0f;
            float offsetY = (float)(this.Height - stringLength.Height) / 2.0f;
            g.DrawString(text, this.Font, brushFont, new PointF(offsetX, offsetY));
        }
        #endregion

        public void NotifyDefault(bool value)
        {

        }

        public void PerformClick()
        {

        }

        #region Properties
        public Color ButtonHoverColor
        {
            get
            {
                return colorButtonHover;
            }
            set
            {
                colorButtonHover = value;
                this.Invalidate();
            }
        }
        public Color ButtonPressedColor
        {
            get
            {
                return colorButtonPressed;
            }
            set
            {
                colorButtonPressed = value;
                this.Invalidate();
            }
        }
        public Color BorderColor
        {
            get
            {
                return colorBorderDefault;
            }
            set
            {
                colorBorderDefault = value;
                this.Invalidate();
            }
        }
        public Color BorderHoverColor
        {
            get
            {
                return colorBorderHover;
            }
            set
            {
                colorBorderHover = value;
                this.Invalidate();
            }
        }
        public Color BorderPressedColor
        {
            get
            {
                return colorBorderPressed;
            }
            set
            {
                colorBorderPressed = value;
                this.Invalidate();
            }
        }
        public Color FontHoverColor
        {
            get
            {
                return colorFontHover;
            }
            set
            {
                colorFontHover = value;
                this.Invalidate();
            }
        }
        public Color FontPressedColor
        {
            get
            {
                return colorFontPressed;
            }
            set
            {
                colorFontPressed = value;
                this.Invalidate();
            }
        }
        public String ButtonText
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
                this.Invalidate();
            }
        }

        public DialogResult DialogResult
        {
            get
            {
                return result;
            }
            set
            {
                if (Enum.IsDefined(typeof(DialogResult), value))
                    result = value;
                else
                    throw new InvalidEnumArgumentException("value", (int)value, typeof(DialogResult));
            }
        }
        #endregion

        #region Events
        public event EventHandler ButtonClicked;
        #endregion

        private void ButtonPro_MouseDown(object sender, MouseEventArgs e)
        {
            isPressed = true;
            this.Invalidate();
        }

        private void ButtonPro_MouseUp(object sender, MouseEventArgs e)
        {
            if(e.X >= 0 & e.Y <= this.Width && e.Y >= 0 && e.Y <= this.Height)
            {
                if (ButtonClicked != null)
                {
                    result = DialogResult.OK;
                    ButtonClicked(this, EventArgs.Empty);
                }
            }
            isPressed = false;
            this.Invalidate();
        }

        private void ButtonPro_MouseMove(object sender, MouseEventArgs e)
        {
            isHovered = true;
            this.Invalidate();
        }

        private void ButtonPro_MouseLeave(object sender, EventArgs e)
        {
            isHovered = false;
            this.Invalidate();
        }

    }
}
