﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GBA_Studio.Tools;

namespace GBA_Studio.Controls
{
    public partial class ClipboardInterface : Form
    {
        public class ClipboardEventArgs : EventArgs
        {
            private string clipboard;

            public string Clipboard
            {
                get
                {
                    return clipboard;
                }
            }

            public ClipboardEventArgs(string clipboard)
            {
                this.clipboard = clipboard;
            }
        }
        public delegate void ClipboardEventHandler(object sender, ClipboardEventArgs e);

        public event ClipboardEventHandler ClipboardItemSelected;

        private ClipboardManager clipboardManager;
        private int selectedIndex = 0;

        private bool sessionPasted = false;
        private bool nextRequested = false;

        public int SelectedIndex
        {
            get
            {
                return selectedIndex;
            }
        }

        private Button acceptButton = new Button();
        private Button cancelButton = new Button();

        public ClipboardManager Clipboard
        {
            get
            {
                return clipboardManager;
            }
        }

        public ClipboardInterface()
        {
            InitializeComponent();

            clipboardManager = new ClipboardManager();

            this.CancelButton = cancelButton;
            this.AcceptButton = acceptButton;

            clipboardTextDisplay.KeyDown += ClipboardInterface_KeyDownWPF;
            clipboardTextDisplay.KeyUp += ClipboardInterface_KeyUpWPF;

            itemList1.KeyDown += ClipboardInterface_KeyDown;
            itemList1.KeyUp += ClipboardInterface_KeyUp;

            this.Shown += ClipboardInterface_Shown;
        }

        void ClipboardInterface_Shown(object sender, EventArgs e)
        {
            sessionPasted = false;

            itemList1.SelectItem(selectedIndex);
            clipboardTextDisplay.Text = itemList1.ControlItems[selectedIndex].Description;
            itemList1.Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            //base.OnPaint(e);
        }

        public void AddClipboardItem(string clipboard)
        {
            clipboardManager.AddToClipboard(clipboard);

            ItemList.ItemData tmpItem = new ItemList.ItemData("", clipboard, null, true);
            
            itemList1.ControlItems.Insert(0, tmpItem);

            if (itemList1.ControlItems.Count > 10)
            {
                itemList1.ControlItems.RemoveRange(10, itemList1.ControlItems.Count - 10);
            }
        }
        
        #region Property Hiding
        [Obsolete("Not applicable in this class.")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        public override Color BackColor
        {
            get
            {
                return base.BackColor;
            }
            set
            {
                base.BackColor = value;
            }
        }
        #endregion

        private void ClipboardInterface_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Tab && e.Control && !nextRequested)
            {
                nextRequested = true;
                if (selectedIndex + 1 >= clipboardManager.ClipboardCount)
                {
                    selectedIndex = -1;
                }

                itemList1.SelectItem(++selectedIndex);
                clipboardTextDisplay.Text = itemList1.ControlItems[selectedIndex].Description;
                itemList1.Invalidate();
            }
            else if (e.KeyCode == Keys.Delete && e.Control)
            {
                this.Cancel();
            }
        }

        private void ClipboardInterface_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Cancel();
            }
            else if (!e.Control)
            {
                this.Accept();
            }
            else if (e.KeyCode == Keys.Tab && e.Control)
            {
                nextRequested = false;
            }
        }

        private void ClipboardInterface_KeyDownWPF(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (((e.Key & System.Windows.Input.Key.LeftCtrl) == System.Windows.Input.Key.LeftCtrl) || (e.Key & System.Windows.Input.Key.RightCtrl) == System.Windows.Input.Key.RightCtrl)
            {
                if (e.Key == System.Windows.Input.Key.Tab)
                {
                    if (selectedIndex + 1 >= clipboardManager.ClipboardCount)
                    {
                        selectedIndex = -1;
                    }

                    itemList1.SelectItem(++selectedIndex);
                    clipboardTextDisplay.Text = itemList1.ControlItems[selectedIndex].Description;
                    itemList1.Invalidate();
                }
            }
        }

        private void ClipboardInterface_KeyUpWPF(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Escape)
            {
                this.Cancel();
            }
            else if (((e.Key & System.Windows.Input.Key.LeftCtrl) == System.Windows.Input.Key.LeftCtrl) || (e.Key & System.Windows.Input.Key.RightCtrl) == System.Windows.Input.Key.RightCtrl)
            {
                this.Accept();
            }
        }

        private void Cancel()
        {
            //cancelButton.PerformClick();
            this.Close();
        }

        private void Accept()
        {
            if (!sessionPasted)
            {
                if (ClipboardItemSelected != null)
                {
                    ClipboardItemSelected(this, new ClipboardEventArgs(Clipboard.GetFromClipboard(selectedIndex)));
                }
                sessionPasted = true;
                //acceptButton.PerformClick();
                this.Close();
            }
        }
    }
}
