﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GBA_Studio.Controls
{
    public partial class ItemList : UserControl
    {
        public class ItemData
        {
            string caption;
            string description;
            Image image;
            Rectangle prevDrawRect;
            bool spacer = false;
            
            public ItemData()
            {

            }
            public ItemData(string caption, string description, Image image, bool enabled)
            {
                this.caption = caption;
                this.description = description;
                this.image = image;
            }

            public string Caption
            {
                get
                {
                    return this.caption;
                }
                set
                {
                    this.caption = value;
                }
            }
            public string Description
            {
                get
                {
                    return this.description;
                }
                set
                {
                    this.description = value;
                }
            }
            public Image Image
            {
                get
                {
                    return this.image;
                }
                set
                {
                    this.image = value;
                }
            }
            public Rectangle DrawRect
            {
                get
                {
                    return prevDrawRect;
                }
                set
                {
                    prevDrawRect = value;
                }
            }

            public bool Spacer
            {
                get
                {
                    return spacer;
                }
                set
                {
                    spacer = value;
                }
            }
        }
        #region Variables
        int indexSelected = -1;
        int indexHover = -1;
        bool indexIsClicked = false;

        List<ItemData> itemData;
        int itemGap = 20;
        int itemWidth = 50;
        int marginLeft = 10;
        int marginTop = 10;

        private Color buttonColorDefault = Color.FromArgb(45, 45, 48);
        private Color buttonColorHovered = Color.FromArgb(128, 128, 128);
        private Color buttonColorPressed = Color.FromArgb(128, 128, 128);
        private Color buttonColorSelected = Color.FromArgb(128, 128, 128);
        private Color buttonBorderColorDefault = Color.FromArgb(45, 45, 48);
        private Color buttonBorderColorHovered = Color.FromArgb(62, 62, 64);
        private Color buttonBorderColorPressed = Color.FromArgb(0, 122, 204);
        private Color buttonBorderColorSelected = Color.FromArgb(0, 80, 160);
        private int borderWidth = 1;

        public int wantHeight = 0;
        #endregion

        public ItemList()
        {
            InitializeComponent();
            itemData = new List<ItemData>();
        }

        public int DetermineColumnCount()
        {
            int count = -1;

            int w = this.Width;

            int cur = 1;
            while (count == -1)
            {
                int rowWidth = (cur * itemWidth) + ((cur - 1) * itemGap) + (2 * marginLeft);
                if (rowWidth > this.Width)
                {
                    count = (cur - 1);
                }
                cur++;
            }
            return count;
        }

        public int GenerateWantHeight()
        {
            int columnCount = DetermineColumnCount();
            if (columnCount > 0)
            {
                int modulo = itemData.Count % columnCount;
                int rowCount = (itemData.Count / columnCount) + (modulo == 0 ? 0 : 1);
                wantHeight = (rowCount * itemWidth) + ((rowCount - 1) * itemGap) + (2 * marginTop);
            }
            return wantHeight;
        }

        public void OrganiseButtons(Graphics g)
        {
            int listSize = itemData.Count;
            if (listSize > 0)
            {
                int columnCount = DetermineColumnCount();
                columnCount = columnCount > itemData.Count ? itemData.Count : columnCount;
                int modulo = itemData.Count % columnCount;

                int numIcons = columnCount > ControlItems.Count ? ControlItems.Count : columnCount;
                int widthTotalIcons = ((columnCount * itemWidth) + ((columnCount - 1) * itemGap));
                int centerOffset = (this.Width - widthTotalIcons) / 2;

                int currentRow = 0;
                int currentColumn = 0;
                for (int i = 0; i < itemData.Count; i++)
                {

                    if (!itemData[i].Spacer)
                    {
                        int rowOffset = (int)Math.Floor((float)i / (float)columnCount);
                        rowOffset = currentRow;


                        int itemOffsetX = centerOffset + (currentColumn * (itemWidth + itemGap));
                        int itemOffsetY = marginTop + (rowOffset * (itemWidth + itemGap));

                        Color colorBorder = indexSelected == i ? buttonBorderColorSelected : (indexHover == i ? (indexIsClicked ? buttonBorderColorPressed : buttonBorderColorHovered) : buttonBorderColorDefault);
                        Color colorBack = indexSelected == i ? buttonColorSelected : (indexHover == i ? (indexIsClicked ? buttonColorPressed : buttonColorHovered) : buttonColorDefault);
                        
                        Brush penBorder = new SolidBrush(colorBorder);
                        Brush brushBack = new SolidBrush(colorBack);

                        itemData[i].DrawRect = new Rectangle(itemOffsetX, itemOffsetY, itemWidth, itemWidth);

                        g.FillRectangle(penBorder, itemData[i].DrawRect);
                        g.FillRectangle(brushBack, itemData[i].DrawRect.X + borderWidth, itemData[i].DrawRect.Y + borderWidth, itemData[i].DrawRect.Width - (2 * borderWidth), itemData[i].DrawRect.Height - (2 * borderWidth));
                        
                        if (itemData[i].Image != null)
                        {
                            int imageOffsetX = (itemWidth - itemData[i].Image.Width) / 2;
                            int imageOffsetY = (itemWidth - itemData[i].Image.Height) / 2;
                            imageOffsetX = imageOffsetX < 0 ? 0 : imageOffsetX;
                            imageOffsetY = imageOffsetY < 0 ? 0 : imageOffsetY;

                            int imgWidth = (itemData[i].Image.Width > itemWidth ? itemWidth : itemData[i].Image.Width);
                            int imgHeight = (itemData[i].Image.Height > itemWidth ? itemWidth : itemData[i].Image.Height);
                            Bitmap b = new Bitmap(itemData[i].Image);
                            //b.MakeTransparent(b.GetPixel(0, 0));
                            g.DrawImage(b, new Rectangle(itemOffsetX + imageOffsetX + borderWidth, itemOffsetY + imageOffsetY + borderWidth, imgWidth - (2 * borderWidth), imgHeight - (2 * borderWidth)));
                        }

                        currentColumn++;
                        if((currentColumn) % columnCount == 0)
                        {
                            currentRow++;
                            currentColumn = 0;
                        }
                    }
                    else
                    { 
                        currentRow++;
                        currentColumn = 0;
                    }
                }
            }
        }

        public void Reset()
        {
            indexSelected = -1;
        }

        public void SelectItem(int index)
        {
            if(index >= 0 && index < itemData.Count)
            {
                indexSelected = index;
            }
        }

        #region Properties
        [Category("Control"),
        Description("A collection of items that the control uses."),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)] //Ensures the data is used, and not disregarded upon initialization.
        public List<ItemData> ControlItems
        {
            get
            {
                return itemData;
            }
        }
        
        [Category("Control"),
        Description("A collection of items that the control uses.")]
        public int ControlItemGap
        {
            get
            {
                return itemGap;
            }
            set
            {
                itemGap = value;
            }
        }
        
        [Category("Control"),
        Description("A collection of items that the control uses.")]
        public int ControlItemWidth
        {
            get
            {
                return itemWidth;
            }
            set
            {
                itemWidth = value;
            }
        }

        [Category("Control"),
        Description("A collection of items that the control uses.")]
        public int ControlMarginLeft
        {
            get
            {
                return marginLeft;
            }
            set
            {
                marginLeft = value;
            }
        }

        [Category("Control"),
        Description("A collection of items that the control uses.")]
        public int ControlMarginTop
        {
            get
            {
                return marginTop;
            }
            set
            {
                marginTop = value;
            }
        }
        
        [Category("Control"),
        Description("A collection of items that the control uses.")]
        public Color ButtonColorDefault
        {
            get
            {
                return buttonColorDefault;
            }
            set
            {
                buttonColorDefault = value;
            }
        }

        [Category("Control"),
        Description("A collection of items that the control uses.")]
        public Color ButtonColorHovered
        {
            get
            {
                return buttonColorHovered;
            }
            set
            {
                buttonColorHovered = value;
            }
        }

        [Category("Control"),
        Description("A collection of items that the control uses.")]
        public Color ButtonColorPressed
        {
            get
            {
                return buttonColorPressed;
            }
            set
            {
                buttonColorPressed = value;
            }
        }

        [Category("Control"),
        Description("A collection of items that the control uses.")]
        public Color ButtonColorSelected
        {
            get
            {
                return buttonColorSelected;
            }
            set
            {
                buttonColorSelected = value;
            }
        }

        [Category("Control"),
        Description("A collection of items that the control uses.")]
        public Color ButtonBorderColorDefault
        {
            get
            {
                return buttonBorderColorDefault;
            }
            set
            {
                buttonBorderColorDefault = value;
            }
        }

        [Category("Control"),
        Description("A collection of items that the control uses.")]
        public Color ButtonBorderColorHovered
        {
            get
            {
                return buttonBorderColorHovered;
            }
            set
            {
                buttonBorderColorHovered = value;
            }
        }

        [Category("Control"),
        Description("A collection of items that the control uses.")]
        public Color ButtonBorderColorPressed
        {
            get
            {
                return buttonBorderColorPressed;
            }
            set
            {
                buttonBorderColorPressed = value;
            }
        }

        [Category("Control"),
        Description("A collection of items that the control uses.")]
        public Color ButtonBorderColorSelected
        {
            get
            {
                return buttonBorderColorSelected;
            }
            set
            {
                buttonBorderColorSelected = value;
            }
        }

        [Category("Control"),
        Description("Gets or sets the border width for the item")]
        public int ItemBorderWidth
        {
            get
            {
                return borderWidth;
            }
            set
            {
                borderWidth = value;
            }
        }

        public int HoverIndex
        {
            get
            {
                return indexHover;
            }
        }

        public int SelectedIndex
        {
            get
            {
                return indexSelected;
            }
        }

        #endregion

        int previousHoverIndex = -2;
        int previousSelectedIndex = -1;
        bool previousIsClicked = false;

        protected override void OnPaint(PaintEventArgs e)
        {
            bool requiresUpdate = false;

            requiresUpdate |= previousHoverIndex != indexHover;
            requiresUpdate |= previousIsClicked != indexIsClicked;

            //if (requiresUpdate)
            //{
                previousHoverIndex = indexHover;
                previousIsClicked = indexIsClicked;
                OrganiseButtons(e.Graphics);
                base.OnPaint(e);
            //}

        }

        private void InterfaceNewItem_MouseMove(object sender, MouseEventArgs e)
        {
            indexHover = GetHoverIndex(e.Location);
            if(indexHover != previousHoverIndex)
            {
                if (indexHover == -1 && HoveredIndexLost != null)
                {
                    HoveredIndexLost(this, EventArgs.Empty);
                }
                else if (HoveredIndexChanged != null)
                {
                    HoveredIndexChanged(this, EventArgs.Empty);
                }
            }
            if(indexHover == -1)
            {
                indexIsClicked = false;
            }
            this.Invalidate();
        }

        private void InterfaceNewItem_MouseDown(object sender, MouseEventArgs e)
        {
            if (indexHover >= 0)
            {
                indexIsClicked = true;
            }
            else
            {
                indexIsClicked = false;
            }
            this.Invalidate();
        }

        private void InterfaceNewItem_MouseUp(object sender, MouseEventArgs e)
        {
            indexIsClicked = false;
            if (indexHover >= 0)
            {
                SelectIndex(indexHover);
            }
            this.Invalidate();
        }

        private int GetHoverIndex(Point location)
        {
            for(int i = 0; i < itemData.Count; i++)
            {
                if(CheckIsWithin(location, itemData[i].DrawRect))
                {
                    return i;
                }
            }
            return -1;
        }

        private bool CheckIsWithin(Point location, Rectangle rect)
        {
            if (location.X >= rect.Left && location.X <= rect.Right)
            {
                if (location.Y >= rect.Top && location.Y <= rect.Bottom)
                {
                    return true;
                }
            }
            return false;
        }

        private void SelectIndex(int index)
        {
            if (index >= 0 && index < itemData.Count)
            {
                indexSelected = index;
                if (indexSelected != previousSelectedIndex)
                {
                    if (SelectedIndexChanged != null)
                    {
                        SelectedIndexChanged(this, EventArgs.Empty);
                    }
                }
            }
        }
        public event EventHandler SelectedIndexChanged;
        public event EventHandler HoveredIndexChanged;
        public event EventHandler HoveredIndexLost;

    }
}
